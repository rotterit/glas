import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by thijs on 8-5-2015.
 */
public class ITT_Search {

    @Test
    public void test_getSearch(){
        String address = "Http://localhost:8082/login";
        HttpPost request = new HttpPost(address);
        ArrayList<NameValuePair> postParameters = new ArrayList<>();
        postParameters.add(new BasicNameValuePair("username", "test"));
        postParameters.add(new BasicNameValuePair("password", "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"));
        try {
            request.setEntity(new UrlEncodedFormEntity(postParameters));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HttpResponse response = null;
        try {
            response = new DefaultHttpClient().execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_OK));
        String Token = response.getHeaders("token")[0].getValue();
        address = "Http://localhost:8082/secure/raw/filtered?userid=1&Authorization="+Token;
        HttpPost request2 = new HttpPost(address);
        request2.addHeader("Content-Type", "application/json");
        request2.setEntity(new StringEntity("{\n" +
                "  \"sizeids\" : [ 1 ],\n" +
                "  \"supplierids\" : [ 1 ],\n" +
                "  \"productTypeids\" : [ 1 ],\n" +
                "  \"seriesids\" : [ 1 ],\n" +
                "  \"colorids\" : [ 1 ],\n" +
                "  \"qualityids\" : [ 1 ]\n" +
                "}\n","UTF8"));
        HttpResponse response2 = null;
        try {
            response2 = new DefaultHttpClient().execute(request2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = response2.getEntity();
        if(entity.getContentLength()!=0) {
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(entity.getContent()), 65728);
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        assertThat(response2.getStatusLine().getStatusCode(), is(HttpStatus.SC_OK));
    }

    @Test
    public void test_searchrightdescription(){
        String address = "Http://localhost:8082/login";
        HttpPost request = new HttpPost(address);
        ArrayList<NameValuePair> postParameters = new ArrayList<>();
        postParameters.add(new BasicNameValuePair("username", "test"));
        postParameters.add(new BasicNameValuePair("password", "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"));
        try {
            request.setEntity(new UrlEncodedFormEntity(postParameters));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HttpResponse response = null;
        try {
            response = new DefaultHttpClient().execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_OK));
        String Token = response.getHeaders("token")[0].getValue();
        address = "Http://localhost:8082/secure/raw/bydescription?userid=1&searchstring=test&Authorization="+Token;
        HttpGet request2 = new HttpGet(address);
        HttpResponse response2 = null;
        try {
            response2 = new DefaultHttpClient().execute(request2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = response2.getEntity();
        if(entity.getContentLength()!=0) {
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(entity.getContent()), 65728);
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        assertThat(response2.getStatusLine().getStatusCode(), is(HttpStatus.SC_OK));
    }

    @Test
    public void test_searchwrongdescription(){
        String address = "Http://localhost:8082/login";
        HttpPost request = new HttpPost(address);
        ArrayList<NameValuePair> postParameters = new ArrayList<>();
        postParameters.add(new BasicNameValuePair("username", "test"));
        postParameters.add(new BasicNameValuePair("password", "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"));
        try {
            request.setEntity(new UrlEncodedFormEntity(postParameters));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HttpResponse response = null;
        try {
            response = new DefaultHttpClient().execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_OK));
        String Token = response.getHeaders("token")[0].getValue();
        address = "Http://localhost:8082/secure/raw/bydescription?userid=1&searchstring=ezel&Authorization="+Token;
        HttpGet request2 = new HttpGet(address);
        HttpResponse response2 = null;
        try {
            response2 = new DefaultHttpClient().execute(request2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = response2.getEntity();
        if(entity.getContentLength()!=0) {
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(entity.getContent()), 65728);
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        assertThat(response2.getStatusLine().getStatusCode(), is(HttpStatus.SC_OK));
    }

    @Test
    public void test_getStock(){
        String address = "Http://localhost:8082/login";
        HttpPost request = new HttpPost(address);
        ArrayList<NameValuePair> postParameters = new ArrayList<>();
        postParameters.add(new BasicNameValuePair("username", "test"));
        postParameters.add(new BasicNameValuePair("password", "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"));
        try {
            request.setEntity(new UrlEncodedFormEntity(postParameters));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HttpResponse response = null;
        try {
            response = new DefaultHttpClient().execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_OK));
        String Token = response.getHeaders("token")[0].getValue();
        address = "Http://localhost:8082/secure/stocks/raw/all?userid=1&Authorization="+Token;
        HttpGet request2 = new HttpGet(address);
        HttpResponse response2 = null;
        try {
            response2 = new DefaultHttpClient().execute(request2);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = response2.getEntity();
        if(entity.getContentLength()!=0) {
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(entity.getContent()), 65728);
                String line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        assertThat(response2.getStatusLine().getStatusCode(), is(HttpStatus.SC_OK));
    }
}
