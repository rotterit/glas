import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Test;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by thijs on 5-5-2015.
 */
public class ITT_login {

    @Test
    public void test_login() {
        String address = "Http://localhost:8082/login";
        HttpPost request = new HttpPost(address);
        ArrayList<NameValuePair> postParameters = new ArrayList<>();
        postParameters.add(new BasicNameValuePair("username", "test"));
        postParameters.add(new BasicNameValuePair("password", "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"));

        try {
            request.setEntity(new UrlEncodedFormEntity(postParameters));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        HttpResponse response = null;
        try {
            response = new DefaultHttpClient().execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(response.getStatusLine().getStatusCode(), is(HttpStatus.SC_OK));

        HttpPost request2 = new HttpPost(address);
        ArrayList<NameValuePair> postParameters2 = new ArrayList<>();
        postParameters2.add(new BasicNameValuePair("username", "test"));
        postParameters2.add(new BasicNameValuePair("password", "test2"));

        try {
            request2.setEntity(new UrlEncodedFormEntity(postParameters2));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        HttpResponse response2 = null;
        try {
            response2 = new DefaultHttpClient().execute(request2);
        } catch (IOException e) {
            e.printStackTrace();
        }

        assertThat(response2.getStatusLine().getStatusCode(), is(HttpStatus.SC_UNAUTHORIZED));

    }


}
