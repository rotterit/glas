/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var qualityController = angular.module('clarity.crud');

qualityController.controller('qualityCrudController', ['$scope', '$log', 'quality','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, quality, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingQuality = null;
    var oldEditingQuality = null;
    $scope.ValueMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;

        if(($scope.editingQuality.data == "" && $scope.crudType == "create") || ($scope.editingQuality.name == "" && $scope.crudType == "edit")){
            $scope.ValueMissingError = true;
            return;
        }
        if(crudOption == 'create'){
            $log.log("Create quality " + $scope.editingQuality);
            httpServiceFactory.createPostPromise('/qualities/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingQuality, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Quality Saved.");
                    $location.path('/qualityOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('Edit quality ' + $scope.editingQuality);
            httpServiceFactory.createPostPromise('/qualities/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingQuality, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Quality Saved.");
                    $location.path('/qualityOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function init(){
        $log.log(quality);
        $scope.editingQuality = quality;
        oldEditingQuality = angular.copy(quality);
        if(crudOption == "create"){
            $scope.editingQuality = {"data": ""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);