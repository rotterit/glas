/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var manufacturingTypeController = angular.module('clarity.crud');

manufacturingTypeController.controller('manufacturingTypeCrudController', ['$scope', '$log', 'manufacturingType','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, manufacturingType, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingManufacturingType = null;
    var oldEditingManufacturingType = null;
    $scope.ValueMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;

        $log.log(oldEditingManufacturingType);
        $log.log($scope.editingManufacturingType);
        if($scope.editingManufacturingType.code == "" || $scope.editingManufacturingType.name == ""){
            $scope.ValueMissingError = true;
            return;
        }

        if(crudOption == 'create'){
            $log.log('edit productType');
            $log.log($scope.editingManufacturingType);
            httpServiceFactory.createPostPromise('/manufacturingtypes/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingManufacturingType, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("ProductType Saved.");
                    $location.path('/manufacturingTypeOverview');
                })
                .error(function (error, status) {
                    $log.log("Saving new manufacturing type Error: " + error + " status: " + status);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('edit productType');
            $log.log($scope.editingManufacturingType);
            httpServiceFactory.createPostPromise('/manufacturingtypes/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingManufacturingType, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Manufacturing type saved.");
                    $location.path('/manufacturingTypeOverview');
                })
                .error(function (error, status) {
                    $log.log("Saving edited manufacturing type Error: " + error + " status: " + status);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function init(){
        $log.log(manufacturingType);
        $scope.editingManufacturingType = manufacturingType;
        oldEditingManufacturingType = angular.copy(manufacturingType);
        if(crudOption == "create"){
            $scope.editingManufacturingType = {"code":"", "name":"", "description":""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);