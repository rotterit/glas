/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var manufacturingTypeController = angular.module('clarity.overview');

manufacturingTypeController.controller('manufacturingTypeOverviewController', ['$scope', '$log', 'manufacturingTypes', 'crudService', '$location', function($scope, $log, manufacturingTypes, crudService, $location){
    $scope.manufacturingTypeGrid = {
        data: 'manufacturingTypeGridDate',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'code', displayName: 'code', minWidth: 120, width: 120, maxWidth: 200},
            {field: 'name', displayName: 'name', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'description', displayName: 'description', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}  {{row.entity.lastModifiedDate.hour}}:{{row.entity.lastModifiedDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingManufacturingType(rowItem.entity);
            $location.path('/manufacturingTypeEdit');
        }
    };

    $scope.manufacturingTypeGridDate={};

    $scope.refresh = function(){
        crudService.getManufacturingTypes().then(function(data){$scope.manufacturingTypeGridDate = data.data;});
    };

    $scope.createManufacturingType = function(){
        $location.path('/manufacturingTypeCreate');
    };

    function init() {
        $scope.manufacturingTypeGridDate = manufacturingTypes.data;
    }
    init();
}]);