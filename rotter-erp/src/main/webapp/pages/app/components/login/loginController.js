/**
 * Created by It director Trudy @ Rotter-Glas
 */
'use strict';
var controller = angular.module('clarity');

controller.controller('LoginCtrl', ['$scope', '$location', '$log', 'notifications', 'loginService', function($scope, $location, $log, notifications, loginService){
    $scope.username = "";
    $scope.password = "";
    $scope.logginIn = false;

    $scope.login = function(){
        $log.log("start login");
        $scope.logginIn = true;
        var promise = loginService.login($scope.username, $scope.password);
        promise.then(function(data){
            $scope.logginIn = false;
            $location.path("/landingPage");
        }, function(error){
            $scope.logginIn = false;
        });
    };
}]);