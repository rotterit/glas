/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var categoryController = angular.module('clarity.overview');

categoryController.controller('categoryOverviewController', ['$scope', '$log', 'categories', 'crudService', '$location', function($scope, $log, categories, crudService, $location){
    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'name', displayName: 'name', width: 120},
            {field: 'baseTime', displayName: 'base time', width: 80},
            {field: 'cslist', displayName: 'sizes', width: 120, cellTemplate:"<div class='ngCellText' ng-repeat='size in row.entity.cslist'>{{size.sizeCode}}, </div>"},
            {field: 'note.content', displayName: 'note', width: 200},
            {field: 'creationDate', displayName: 'creation date', width: 140, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"},
            {field: 'createdBy', displayName: 'created by', width: 120},
            {field: 'lastModifiedDate', displayName: 'last modified date', width: 140, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', width: 120}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingCategory(rowItem.entity);
            $location.path('/categoryEdit');
        }
    };

    $scope.data={};

    $scope.refresh = function(){
        crudService.getCategories().then(function(data){$scope.data = data.data;});
    };

    $scope.createCategory = function(){
        $log.log("test");
        $location.path('/categoryCreate');
    };

    function init() {
        $scope.data = categories.data;
        $scope.gridOptions.data = 'data';
    }
    init();
}]);