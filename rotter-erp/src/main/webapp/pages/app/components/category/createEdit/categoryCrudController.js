/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var categoryController = angular.module('clarity.crud');

categoryController.controller('categoryCrudController', ['$scope', '$log', 'category','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, category, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingCategory = null;
    var oldEditingCategory = null;
    $scope.baseHour = "";
    $scope.baseMinute = "";
    $scope.ValueMissingError = false;
    $scope.MissingSizeError = false;
    $scope.SizeFormatError = false;
    $scope.NaNError = false;
    $scope.InvalideBaseTimeError = false;
    $scope.crudType = "create";
    $scope.sizes = {};
    $scope.selectedSize = {};

    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "",
            width: 50,
            cellTemplate: '<div class="ngCellText" ng-click="removeSize(row)">X</div>',
            sortable: false
        },
            {field: 'code', displayName: 'code', minWidth: 120, width: 120, maxWidth: 350, sortable: false},
            {field: 'name', displayName: 'name', minWidth: 120, width: 120, maxWidth: 350, sortable: false},
            {field: 'maxTime', displayName: 'max time', minWidth: 120, width: 120, maxWidth: 350, sortable: false, enableCellEdit: true},
            {field: 'minTime', displayName: 'min time', minWidth: 120, width: 120, maxWidth: 350, sortable: false, enableCellEdit: true},
            {field: 'basePrice', displayName: 'base price', minWidth: 120, width: 120, maxWidth: 350, sortable: false, enableCellEdit: true}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            //$location.path('/categoryEdit');
        }
    };

    $scope.data=[];

    $scope.changeSizeSelection = function(size){
        for(var i = 0; i < $scope.data.length; i++){
            if($scope.data[i].sizeId == size.id){
                return;
            }
        }
        size.maxTime = "";
        size.minTime = "";
        size.basePrice = "";
        size.sizeId = size.id;
        size.id = 0;
        $scope.data.push(size);
    };

    $scope.removeSize = function(size){
        for(var i = 0; i < $scope.data.length; i++){
            if($scope.data[i].id == size.entity.id){
                $scope.data.splice(i,1);
                return;
            }
        }
    };

    $scope.save = function() {
        $scope.ValueMissingError = false;
        $scope.MissingSizeError = false;
        $scope.SizeFormatError = false;
        $scope.NaNError = false;
        $scope.InvalideBaseTimeError = false;

        if ($scope.editingCategory.name == "" || $scope.baseMinute.toString() == "") {
            $scope.ValueMissingError = true;
            return;
        }

        if (isNaN($scope.baseMinute) || ($scope.baseHour.toString() == "" && isNaN($scope.baseHour))) {
            $scope.NaNError = true;
            return;
        }

        if (Number($scope.baseMinute) <= 0) {
            $scope.InvalideBaseTimeError = true;
            return;
        }

        if ($scope.baseHour.toString() != "" && !isNaN($scope.baseHour) && Number($scope.baseHour) > 0) {
            $scope.editingCategory.baseTime = (Number($scope.baseHour) * 60) + Number($scope.baseMinute);
        } else {
            $scope.editingCategory.baseTime = Number($scope.baseMinute);
        }

        if (crudOption == "create") {
            $scope.editingCategory.categorySizes = [];
            for (var i = 0; i < $scope.data.length; i++) {
                if ($scope.data[i].maxTime.toString() == "" || $scope.data[i].minTime.toString() == "" || $scope.data[i].basePrice.toString() == "") {
                    //$scope.editingCategory.categorySizes.push({"id": $scope.data[i].id, "minTime": $scope.data[i].minTime, "maxTime": $scope.data[i].maxTime, "basePrice": $scope.data[i].basePrice});
                    $scope.MissingSizeError = true;
                    return;
                } else if (isNaN($scope.data[i].maxTime) || isNaN($scope.data[i].minTime) || isNaN($scope.data[i].basePrice)) {
                    $scope.NaNError = true;
                    return;
                } else {
                    $scope.editingCategory.categorySizes.push({
                        "sizeId": Number($scope.data[i].sizeId),
                        "minTime": Number($scope.data[i].minTime),
                        "maxTime": Number($scope.data[i].maxTime),
                        "basePrice": Number($scope.data[i].basePrice).toFixed(2)
                    });
                }
            }
        }else if(crudOption == "edit"){
            $log.log("ready for editOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
            $log.log($scope.editingCategory);
            var oldList = $scope.editingCategory.cslist;
            $scope.editingCategory.cslist = [];
            for(var i = 0; i < $scope.data.length; i++){
                $scope.editingCategory.cslist.push({"sizeId": Number($scope.data[i].sizeId),
                    "sizeCode": $scope.data[i].code,
                    "sizeName": $scope.data[i].name,
                    "maxTime": Number($scope.data[i].maxTime),
                    "minTime": Number($scope.data[i].minTime),
                    "basePrice": Number($scope.data[i].basePrice),
                    "id": Number($scope.data[i].id)});
            }
            $log.log($scope.data);
            $log.log($scope.editingCategory);
        }

        $log.log("saving and all that shit.");
        $log.log($scope.editingCategory);
        if ($scope.crudType == "create") {
            for (var i = 0; i < $scope.editingCategory.categorySizes.length; i++) {
                $scope.editingCategory.categorySizes[i].maxTime = Number($scope.editingCategory.categorySizes[i].maxTime);
                $scope.editingCategory.categorySizes[i].minTime = Number($scope.editingCategory.categorySizes[i].minTime);
                $scope.editingCategory.categorySizes[i].sizeId = Number($scope.editingCategory.categorySizes[i].sizeId);
                $scope.editingCategory.categorySizes[i].basePrice = Number($scope.editingCategory.categorySizes[i].basePrice).toFixed(2);
            }
        }else if($scope.crudType == "edit"){
        }
        $log.log($scope.editingCategory);

        if(crudOption == 'create'){
            httpServiceFactory.createPostPromise('/categories/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingCategory, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Category Saved.");
                    $location.path('/categoryOverview');
                })
                .error(function (error, status) {
                    $log.log("Saving edited category Error: " + error + " Status: " + status);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){httpServiceFactory.createPostPromise('/categories/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingCategory, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Category Saved.");
                    $location.path('/categoryOverview');
                })
                .error(function (error, status) {
                    $log.log("Saving edited category Error: " + error + " Status: " + status);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function loadSizes(){
        httpServiceFactory.createGetPromise('/sizes/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.sizes = data;
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get roles from server.");
            });
    }

    $scope.cancel = function(){
        $location.path("/categoryOverview");
    };

    function init(){
        loadSizes();
        $scope.editingCategory = category;
        oldEditingCategory = angular.copy(category);
        if(crudOption == "create"){
            $scope.editingCategory = {"name":"", "baseTime":"", "note":"", "categorySizes":[]};
        }else if(crudOption == "edit"){
            $log.log("what i get in+++++++++++++++++++++++++++++++");
            $log.log(category);
            $scope.baseMinute = $scope.editingCategory.baseTime % 60;
            $scope.baseHour = ($scope.editingCategory.baseTime - $scope.baseMinute)/60;
            var gridData = [];
            for(var i = 0; i < $scope.editingCategory.cslist.length; i++){
                gridData.push({"sizeId": $scope.editingCategory.cslist[i].sizeid, "code": $scope.editingCategory.cslist[i].sizeCode,
                "name": $scope.editingCategory.cslist[i].sizeName, "maxTime": $scope.editingCategory.cslist[i].maxTime,
                "minTime": $scope.editingCategory.cslist[i].minTime, "id": $scope.editingCategory.cslist[i].id,
                "basePrice": $scope.editingCategory.cslist[i].basePrice});
            }
            $scope.data = gridData;
        }
        $scope.crudType = crudOption;
    }
    init();
}]);