/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var roleController = angular.module('clarity.crud');

roleController.controller('roleCrudController', ['$scope', '$log', 'role','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, role, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingRole = null;
    var oldEditingRole = null;
    $scope.ValueMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;

        if(($scope.editingRole.data == ""  && $scope.crudType == "create") || ($scope.editingRole.name == "" && $scope.crudType == "edit")){
            $scope.ValueMissingError = true;
            return;
        }
        if(crudOption == 'create'){
            $log.log("create role " + $scope.editingRole);
            httpServiceFactory.createPostPromise('/roles/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingRole, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Role Saved.");
                    $location.path('/roleOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('edit role ' + $scope.editingRole);
            httpServiceFactory.createPostPromise('/roles/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingRole, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Role Saved.");
                    $location.path('/roleOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function init(){
        $log.log(role);
        $scope.editingRole = role;
        oldEditingRole = angular.copy(role);
        if(crudOption == "create"){
            $scope.editingRole = {"data":""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);