/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var roleController = angular.module('clarity.overview');

roleController.controller('roleOverviewController', ['$scope', '$log', 'roles', 'crudService', '$location', function($scope, $log, roles, crudService, $location){
    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'name', displayName: 'role', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingRole(rowItem.entity);
            $location.path('/roleEdit');
        }
    };

    $scope.data={};

    $scope.refresh = function(){
        crudService.getRoles().then(function(data){$scope.data = data.data;});
        $scope.gridOptions.data = 'data';
    };

    $scope.createRole = function(){
        $location.path('/roleCreate');
    };

    function init() {
        $scope.data = roles.data;
        $scope.gridOptions.data = 'data';
    }
    init();
}]);