/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var designController = angular.module('clarity.crud');

designController.controller('designCrudController', ['$scope', '$log', 'design','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, design, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingDesign = null;
    var oldEditingDesign = null;
    $scope.ValueMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;

        if($scope.editingDesign.designCode == "" || $scope.editingDesign.name == ""){
            $scope.ValueMissingError = true;
            return;
        }
        if(crudOption == 'create'){
            $log.log("create design " + $scope.editingDesign);
            httpServiceFactory.createPostPromise('/designs/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingDesign, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Design Saved.");
                    $location.path('/designOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('edit design ' + $scope.editingDesign);
            httpServiceFactory.createPostPromise('/designs/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingDesign, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Design Saved.");
                    $location.path('/designOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function init(){
        $log.log(design);
        $scope.editingDesign = design;
        oldEditingDesign = angular.copy(design);
        if(crudOption == "create"){
            $scope.editingDesign = {"designCode":"", "name":"", "description":""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);