/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var designController = angular.module('clarity.overview');

designController.controller('designOverviewController', ['$scope', '$log', 'designs', 'crudService', '$location', function($scope, $log, designs, crudService, $location){
    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'designCode', displayName: 'number', minWidth: 100, width: 100, maxWidth: 350, pinned: true},
            {field: 'name', displayName: 'name', minWidth: 100, width: 100, maxWidth: 350},
            {field: 'description', displayName: 'description', minWidth: 120, width: 150},
            {field: 'creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingDesign(rowItem.entity);
            $location.path('/designEdit');
        }
    };

    $scope.data={};

    $scope.refresh = function(){
        crudService.getDesigns().then(function(data){$scope.data = data.data;});
        $scope.gridOptions.data = 'data';
    };

    $scope.createDesign = function(){
        $location.path('/designCreate');
    };

    function init() {
        $scope.data = designs.data;
        $scope.gridOptions.data = 'data';
    }
    init();
}]);