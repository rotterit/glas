/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var customerController = angular.module('clarity.overview');

customerController.controller('customerOverviewController', ['$scope', '$log', 'customers', 'crudService', '$location', function($scope, $log, customers, crudService, $location){
    $scope.customerGrid = {
        data: 'customerGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'name', displayName: 'name', width: 120, pinned: true},
            {field: 'baseDiscountPercentage', displayName: 'Discount %', width: 120},
            {field: 'baseLineDiscountPercentage', displayName: 'Line Discount %', width: 120},
            {field: 'basePackingCostPercentage', displayName: 'Packing Cost %', width: 120},
            {field: 'baseVAT', displayName: 'VAT %', width: 60},
            {field: 'companyName', displayName: 'company', width: 120},
            {field: 'email', displayName: 'email', width: 200},
            {field: 'tel', displayName: 'phone number', width: 120},
            {field: 'vatnumber', displayName: 'vat number', width: 120}],
        afterSelectionChange: function(rowItem){
            crudService.setEditingCustomer(rowItem.entity);
            $location.path('/customerEdit');
        }
    };

    $scope.customerGridData={};

    $scope.refresh = function(){
        crudService.getCustomers().then(function(data){$scope.customerGridData = data.data;});
    };

    $scope.createCustomer = function(){
        $location.path('/customerCreate');
    };

    function init() {
        $scope.customerGridData = customers.data;
    }
    init();
}]);