/**
 * Created by Suited Coder Gunther Claessens.
 */
/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var customerController = angular.module('clarity.crud');

customerController.controller('customerCrudController', ['$scope', '$log', 'customer','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, customer, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingCustomer = null;
    $scope.ValueMissingError = false;
    $scope.NaNError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;
        $scope.NaNError = false;

        $log.log("+++++++++++++++++++++++");
        $log.log($scope.editingCustomer);
        if($scope.editingCustomer.name == "" || $scope.editingCustomer.baseVAT.toString() == "" || $scope.editingCustomer.basePackingCostPercentage.toString() == "" || $scope.editingCustomer.baseDiscountPercentage.toString() == "" || $scope.editingCustomer.baseLineDiscountPercentage.toString() == ""){
            $scope.ValueMissingError = true;
            return;
        }
        if(isNaN($scope.editingCustomer.baseVAT) || isNaN($scope.editingCustomer.basePackingCostPercentage) || isNaN($scope.editingCustomer.baseDiscountPercentage) || isNaN($scope.editingCustomer.baseLineDiscountPercentage)){
            $scope.NaNError = true;
            return;
        }

        $log.log("ben tot aan saven geraakt.");
        if(crudOption == 'create'){
            httpServiceFactory.createPostPromise('/customers/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingCustomer, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Customer Saved.");
                    $location.path('/customerOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            httpServiceFactory.createPostPromise('/customers/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingCustomer, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Customer Saved.");
                    $location.path('/customerOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function init(){
        $scope.editingCustomer = customer;
        if(crudOption == "create"){
            $scope.editingCustomer = {"name":"",
                "description":"",
                "baseVAT":"",
                "baseDiscountPercentage":"",
                "baseLineDiscountPercentage":"",
                "basePackingCostPercentage":"",
                "companyName": "",
                "adress": "",
                "ZIP": "",
                "city": "",
                "country": "",
                "TEL": "",
                "email": "",
                "VATnumber": ""
            };
        }
        $scope.crudType = crudOption;
    }
    init();
}]);