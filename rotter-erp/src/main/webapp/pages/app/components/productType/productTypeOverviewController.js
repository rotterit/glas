/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var productTypeController = angular.module('clarity.overview');

productTypeController.controller('productTypeOverviewController', ['$scope', '$log', 'productTypes', 'crudService', '$location', function($scope, $log, productTypes, crudService, $location){
    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'code', displayName: 'code', minWidth: 100, width: 100, maxWidth: 350},
            {field: 'name', displayName: 'name', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'description', displayName: 'description', minWidth: 200, width: 200},
            {field: 'creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'modifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.modifiedDate.dayOfMonth}} {{row.entity.modifiedDate.month}} {{row.entity.modifiedDate.year}}  {{row.entity.modifiedDate.hour}}:{{row.entity.modifiedDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingProductType(rowItem.entity);
            $location.path('/productTypeEdit');
        }
    };

    $scope.data={};

    $scope.refresh = function(){
        crudService.getProductTypes().then(function(data){$scope.data = data.data;});
    };

    $scope.createProductType = function(){
        $location.path('/productTypeCreate');
    };

    function init() {
        $scope.data = productTypes.data;
    }
    init();
}]);