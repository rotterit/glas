/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var productTypeController = angular.module('clarity.crud');

productTypeController.controller('productTypeCrudController', ['$scope', '$log', 'productType','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, productType, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingProductType = null;
    var oldEditingProductType = null;
    $scope.ValueMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;

        $log.log(oldEditingProductType);
        $log.log($scope.editingProductType);
        if($scope.editingProductType.code == null || $scope.editingProductType.name == ""){
            $scope.ValueMissingError = true;
            return;
        }
        if(crudOption == 'create'){
            $log.log("create productType " + $scope.editingProductType);
            httpServiceFactory.createPostPromise('/producttypes/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingProductType, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("ProductType Saved.");
                    $location.path('/productTypeOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('edit productType ' + $scope.editingProductType);
            httpServiceFactory.createPostPromise('/producttypes/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingProductType, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("ProductType Saved.");
                    $location.path('/productTypeOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function init(){
        $log.log(productType);
        $scope.editingProductType = productType;
        oldEditingProductType = angular.copy(productType);
        if(crudOption == "create"){
            $scope.editingProductType = {"code":"", "name":"", "description":""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);