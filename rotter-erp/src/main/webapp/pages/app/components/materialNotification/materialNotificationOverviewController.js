/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var materialNotificationController = angular.module('clarity.overview');

materialNotificationController.controller('materialNotificationOverviewController', ['$scope', '$log', 'materialNotifications', 'crudService', '$location', 'notificationService', 'httpServiceFactory', 'applicationInformation', function($scope, $log, materialNotifications, crudService, $location, notificationService, httpServiceFactory, applicationInformation){
    var isDeleting = false;


    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 40,
            cellTemplate: '<div class="ngCellText" ng-click="removeMaterialNotification(row.entity)">X</div>',
            sortable: false
        },
            {field: 'rawMaterialCode', displayName: 'code', width: 120},
            {field: 'amount', displayName: 'amount', width: 120},
            {field: 'predate', displayName: 'pre date', width: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.predate.dayOfMonth}} {{row.entity.predate.month}} {{row.entity.predate.year}}</div>"},
            {field: 'startdate', displayName: 'start date', width: 120, cellTemplate:"<div class='ngCellText'>{{row.entity.startdate.dayOfMonth}} {{row.entity.startdate.month}} {{row.entity.startdate.year}}</div>"},
            {field: 'enddate', displayName: 'end date', width: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.enddate.dayOfMonth}} {{row.entity.enddate.month}} {{row.entity.enddate.year}}</div>"}],
        selectedItems: [],
        afterSelectionChange: function(rowItem) {
            /*if (!isDeleting) {
                crudService.setEditingMaterialNotification(rowItem.entity);
                $location.path('/materialNotificationEdit');
            }*/
        }
    };

    $scope.data = [];

    $scope.refresh = function(){
        crudService.getMaterialNotifications().then(function(data){$scope.data = data.data;});
    };

    $scope.createMaterialNotification = function(){
        $location.path('/materialNotificationCreate');
    };

    $scope.removeMaterialNotification = function(materialNotificationToDelete) {
        var isDeleting = true;
        httpServiceFactory.createPostPromise('/materialnotifications/delete', 'userid=' + applicationInformation.getUserinformation().userId + "&id=" + materialNotificationToDelete.id, '', true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                notificationService.showMessageNotification("Material notification deleted.");
                $scope.refresh();
                var isDeleting = false;
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showWarningNotification("You can not delete your material notification.");
                var isDeleting = false;
            });
    };

    function init() {
        $scope.data = materialNotifications.data;
    }
    init();
}]);