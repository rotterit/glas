/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var materialNotificationController = angular.module('clarity.crud');

materialNotificationController.controller('materialNotificationCrudController', ['$scope', '$log', '$document', 'materialNotification','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', 'materialNotifications', function($scope, $log, $document, materialNotification, httpServiceFactory, applicationInformation, crudOption, notificationService, $location, materialNotifications){
    $scope.editingMaterialNotification = null;
    var existingMaterialNotifications = materialNotifications.data;
    $scope.ValueMissingError = false;
    $scope.DateWrongFormatError = false;
    $scope.MissingDateError = false;
    $scope.WrongAmountFormatError = false;
    $scope.MissingRawMaterialError = false;
    $scope.DateMismatchError = false;
    var periode = {"preDate": "", "startDate":"", "endDate":""};
    $scope.selectedRawMaterial = [];
    $scope.crudType = "create";

    $scope.rawMaterialsGrid = {
        data: 'rawMaterialsGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'productCode', displayName: 'code', minWidth: 120, width: 120, maxWidth: 200},
            {field: 'productType', displayName: 'type', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'color', displayName: 'color', minWidth: 180, width: 180, maxWidth: 180},
            {field: 'minimumAmountInStock', displayName: 'minimum amount in stock', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'minimumorderQuantity', displayName: 'minimum order quantity', minWidth: 180, width: 180, maxWidth: 180},
            {field: 'supplier', displayName: 'supplier', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: $scope.selectedRawMaterial,
        multiSelect: false,
        afterSelectionChange: function(rowItem){
            $scope.materialNotificationsGridData = [];
            for(var i = 0; i < existingMaterialNotifications.length; i++){
                if(existingMaterialNotifications[i].rawMaterialId == rowItem.entity.id){
                    $scope.materialNotificationsGridData.push(existingMaterialNotifications[i]);
                }
            }
        }
    };

    $scope.rawMaterialsGridData = [];

    $scope.materialNotificationGrid = {
        data: 'materialNotificationsGridData',
        enablePinning: false,
        enableColumnResize: true,
        enableRowSelect: false,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'rawMaterialCode', displayName: 'material code', minWidth: 120, width: 120, maxWidth: 200},
            {field: 'amount', displayName: 'amount', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'predate', displayName: 'pre date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.predate.dayOfMonth}} {{row.entity.predate.month}} {{row.entity.predate.year}}</div>"},
            {field: 'startdate', displayName: 'start date', minWidth: 120, width: 120, maxWidth: 350, cellTemplate:"<div class='ngCellText'>{{row.entity.startdate.dayOfMonth}} {{row.entity.startdate.month}} {{row.entity.startdate.year}}</div>"},
            {field: 'enddate', displayName: 'end date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.enddate.dayOfMonth}} {{row.entity.enddate.month}} {{row.entity.enddate.year}}</div>"}]
    };

    $scope.materialNotificationsGridData = [];

    $scope.materialNotificationEditGrid = {
        data: 'materialNotificationsEditGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 50,
            cellTemplate: '<div class="ngCellText" ng-click="removeNotification(row)">X</div>',
            sortable: false
        },
            {field: 'rawMaterialCode', displayName: 'material code', minWidth: 120, width: 120, maxWidth: 200},
            {field: 'amount', displayName: 'amount', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'predate', displayName: 'pre date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.predate.dayOfMonth}} {{row.entity.predate.month}} {{row.entity.predate.year}}</div>"},
            {field: 'startdate', displayName: 'start date', minWidth: 120, width: 120, maxWidth: 350, cellTemplate:"<div class='ngCellText'>{{row.entity.startdate.dayOfMonth}} {{row.entity.startdate.month}} {{row.entity.startdate.year}}</div>"},
            {field: 'enddate', displayName: 'end date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.enddate.dayOfMonth}} {{row.entity.enddate.month}} {{row.entity.enddate.year}}</div>"}],
        multiSelect: false,
        afterSelectionChange: function(rowItem){

        }
    };

    $scope.materialNotificationsEditGridData = [];

    function stringToDate(dateString){
        var re_date = /^(\d+)\-(\d+)\-(\d+)\s+(\d+)\:(\d+)\:(\d+)$/;
        if (!re_date.exec(dateString)){
            $scope.DateWrongFormatError = true;
            return null;
        }
        return new Date (RegExp.$3, RegExp.$2-1, RegExp.$1, 12, 0, 0);
    }

    $scope.save = function() {
        $scope.ValueMissingError = false;
        $scope.DateWrongFormatError = false;
        $scope.MissingDateError = false;
        $scope.WrongAmountFormatError = false;
        $scope.MissingRawMaterialError = false;
        $scope.DateMismatchError = false;

        $log.log($scope.selectedRawMaterial);

        periode.preDate = stringToDate($document.find('#preDateField')[0].value);
        periode.startDate = stringToDate($document.find('#startDateField')[0].value);
        periode.endDate = stringToDate($document.find('#endDateField')[0].value);

        if (periode.preDate == null || periode.startDate == null || periode.endDate == null) {
            $scope.MissingDateError = true;
            return;
        }

        if ($scope.amount == "" || $scope.amount == null || $scope.amount == undefined || isNaN($scope.amount)) {
            $scope.WrongAmountFormatError = true;
            return;
        }

        if ($scope.selectedRawMaterial[0].id == "" || $scope.selectedRawMaterial[0].id == null || $scope.selectedRawMaterial[0].id == undefined || isNaN($scope.selectedRawMaterial[0].id)) {
            $scope.MissingRawMaterialError = true;
            return;
        }

        if (periode.preDate > periode.startDate || periode.preDate > periode.endDate || periode.startDate >= periode.endDate) {
            $scope.DateMismatchError = true;
            return;
        }

        var notification = {
            "predate": periode.preDate.getTime(),
            "startdate": periode.startDate.getTime(),
            "enddate": periode.endDate.getTime(),
            "amount": Number($scope.amount),
            "rawMaterialId": Number($scope.selectedRawMaterial[0].id)
        };

        httpServiceFactory.createPostPromise('/materialnotifications/create', 'userid=' + applicationInformation.getUserinformation().userId, notification, true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                notificationService.showMessageNotification("Material notification saved.");
                $location.path('/materialNotificationOverview');
            })
            .error(function (error, status) {
                if (status == 409) {
                    notificationService.showWarningNotification("You can not enter a notification for overlapping dates.");
                    $document.find('#preDateField')[0].value = "";
                    $document.find('#startDateField')[0].value = "";
                    $document.find('#endDateField')[0].value = "";
                    return;
                }
                $log.log(error);
                notificationService.showErrorNotification(status + error);
            });
    };

    $scope.removeNotification = function(rowItem){
        //$log.log(rowItem.entity);
        //TODO: implement the delete call;
        if (confirm("Are you sure you want to delete the material notification?") == true) {
            /*httpServiceFactory.createPostPromise('/materialnotifications/remove', 'userid=' + applicationInformation.getUserinformation().userId + "&orderid=" + row.entity.id, '', true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Your order is completed.");
                    $location.path("/materialNotificationOverview");
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get your order completed.");
                });*/
        }
    };
    $scope.cancel = function(){
        $location.path('/materialNotificationOverview');
    };

    function loadRawMaterials(){
        httpServiceFactory.createPostPromise('/raw/filtered', 'userid=' + applicationInformation.getUserinformation().userId, {"productTypeids":[], "sizeids":[], "seriesids":[], "supplierids":[], "qualityids":[1], "colorids":[]}, true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                $scope.rawMaterialsGridData = data;
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get raw materials from the trudy.");
            });
    }

    function init(){
        $scope.crudType =  crudOption;
        if($scope.crudType == "create"){
            //$scope.editingSize = {"predate":"", "startdate":"", "enddate":"", "amount":"", "rawMaterialId":""};
        }else if($scope.crudType == "edit"){
            $scope.materialNotificationsEditGridData = [];
            for(var i = 0; i < existingMaterialNotifications.length; i++){
                if(existingMaterialNotifications[i].rawMaterialId == materialNotification.rawMaterialId){
                    $scope.materialNotificationsEditGridData.push(existingMaterialNotifications[i]);
                }
            }
        }else{
            $location.path("/materialNotificationOverview");
        }
        loadRawMaterials();
    }
    init();
}]);