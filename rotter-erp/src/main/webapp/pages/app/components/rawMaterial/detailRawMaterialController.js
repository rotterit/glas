/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var detailRawMaterialController = angular.module('clarity.crud');

detailRawMaterialController.controller('detailRawMaterialController', ['$scope', '$log', '$document','httpServiceFactory', 'applicationInformation', 'notificationService', '$location', 'rawMaterial', 'crudService', function($scope, $log, $document, httpServiceFactory, applicationInformation, notificationService, $location, rawMaterial, crudService){
    $scope.rawMateriel = {};
    $scope.editingRawMaterial = {};

    $scope.ValueMissingError = false;
    $scope.isNaNError = false;
    $scope.crudType = "detail";

    $scope.stocks = [];
    $scope.selectedStock = {};

    $scope.edit = function(){
        if(applicationInformation.getRole() == 7) {
            return
        }else {
            $scope.crudType = "edit";
        }
    };

    $scope.cancelEdit = function(){
        $scope.editingRawMaterial = {};
        $scope.crudType = "detail";
    };

    $scope.save = function() {
        $scope.ValueMissingError = false;
        $scope.isNaNError = false;

        if ($scope.editingRawMaterial.description == "" || $scope.editingRawMaterial.price == "" || $scope.editingRawMaterial.minimumorderQuantity == "" || $scope.editingRawMaterial.minimumAmountInStock == "") {
            $scope.ValueMissingError = true;
            return;
        }

        if(isNaN($scope.editingRawMaterial.price) || isNaN($scope.editingRawMaterial.minimumorderQuantity) || isNaN($scope.editingRawMaterial.minimumAmountInStock)){
            $scope.isNaNError = true;
            return;
        }

        var newRawMaterial = {};
        newRawMaterial.id = $scope.editingRawMaterial.id;
        newRawMaterial.description = $scope.editingRawMaterial.description;
        newRawMaterial.noteId = $scope.editingRawMaterial.noteId;
        newRawMaterial.note = $scope.editingRawMaterial.note;
        newRawMaterial.price = $scope.editingRawMaterial.price;
        newRawMaterial.MOQ  = $scope.editingRawMaterial.minimumorderQuantity;
        newRawMaterial.baseAmount = $scope.editingRawMaterial.minimumAmountInStock;

        httpServiceFactory.createPostPromise('/raw/edit', 'userid=' + applicationInformation.getUserinformation().userId, newRawMaterial, true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                $log.log(status);
                notificationService.showMessageNotification("Raw material Saved.");
                $location.path('/rawMaterialOverview');
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + error);
            });
    };

    function loadAvailableStocks(){
        httpServiceFactory.createGetPromise('/raw/freestocks', 'userid=' + applicationInformation.getUserinformation().userId + "&materialid=" + $scope.editingRawMaterial.id, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.stocks = data;
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get stocks from server.");
            });
    }

    $scope.addStockLocation = function(){
        if(applicationInformation.getRole() == 7) {
            return
        }else {
            $scope.crudType = "addStock";
            loadAvailableStocks();
        }
    };

    $scope.saveNewStockLocation = function(){
        $scope.StockMissingError = false;

        if(Object.keys($scope.selectedStock).length === 0){
            $scope.StockMissingError = true;
        }else {
            httpServiceFactory.createPostPromise('/raw/addtostock', 'userid=' + applicationInformation.getUserinformation().userId + "&stockid=" + $scope.selectedStock.id + "&rawmaterialid=" + $scope.editingRawMaterial.id, createEditedFinishedProductObject(), true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("A new stock location is added to the raw material.");
                    $scope.cancel();
                })
                .error(function (error, status) {
                    notificationService.showErrorNotification("Your new stock location could not be saved.");
                    $scope.cancel();
                });
        }
    };

    $scope.cancel = function(){
        $scope.crudType = "detail";
        $scope.stocks = [];
        $scope.selectedStock = {};
    };

    $scope.makeStockCorrection = function(){
        crudService.setCorrectingRawMaterial($scope.editingRawMaterial);
        $location.path("/rawMaterialStockCorrection");
    };

    function init(){
        $scope.editingRawMaterial = rawMaterial.data;
    }
    init();
}]);