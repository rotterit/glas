/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var rawMaterialController = angular.module('clarity.crud');

rawMaterialController.controller('rawCrudController', ['$scope', '$log', '$document','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', 'productTypes', 'sizes', 'series', 'suppliers', 'colors', 'stocks', function($scope, $log, $document, httpServiceFactory, applicationInformation, crudOption, notificationService, $location, productTypes, sizes, series, suppliers, colors, stocks){
    $scope.editingRawMaterial = null;
    $scope.ValueMissingError = false;
    $scope.ColorValueMissingError = false;

    $scope.productTypes = [];
    $scope.selectedProductType = {};
    $scope.sizes = [];
    $scope.selectedSize = {};
    $scope.series = [];
    $scope.selectedSeries = {};
    $scope.suppliers = [];
    $scope.selectedSupplier = {};
    $scope.colors = [];
    $scope.stocks = [];
    $scope.selectedStock = {};

    $scope.crudType = "create";

    $scope.colorGrid = {
        data: 'colorGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [
            {field: 'name', displayName: 'color', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'supplierCode', displayName: 'supplier code', minWidth: 120, width: 200, maxWidth: 350, enableCellEdit: true},
            {field: 'price', displayName: 'price', minWidth: 120, width: 200, maxWidth: 350, enableCellEdit: true},
            {field: 'MOQ', displayName: 'MOQ', minWidth: 120, width: 200, maxWidth: 200, enableCellEdit: true},
            {field: 'baseAmount', displayName: 'base stock amount', minWidth: 120, width: 200, maxWidth: 200, enableCellEdit: true},
            {field: 'note', displayName: 'note', minWidth: 120, width: 200, enableCellEdit: true}]
    };

    $scope.colorGridData = [];

    $scope.updateColor = function($event, color){
        if($event.target.checked){
            color.supplierCode = "";
            color.price = "";
            color.MOQ = "";
            color.baseAmount = "";
            color.note = "";
            $scope.colorGridData.push(color);
        }else{
            for(var i = 0; i < $scope.colorGridData.length; i++){
                if($scope.colorGridData[i].id == color.id){
                    $scope.colorGridData.splice(i, 1);
                }
            }
        }
    };

    $scope.selectAllColors = function(){
        $scope.colorGridData = [];
        for(var i = 0; i < $scope.colors.length; i++){
            var colorCheckbox = $document.find('#cl' + $scope.colors[i].id);
            colorCheckbox[0].checked = true;
            var color = angular.copy($scope.colors[i]);
            color.supplierCode = "";
            color.price = "";
            color.MOQ = "";
            color.baseAmount = "";
            color.note = "";
            $scope.colorGridData.push(color);
        }
    };

    $scope.deselectAllColors = function(){
        for(var i = 0; i < $scope.colors.length; i++){
            var colorCheckbox = $document.find('#cl' + $scope.colors[i].id);
            colorCheckbox[0].checked = false;
        }
        $scope.colorGridData = [];
    };

    $scope.stockGrid = {
        data: 'stockGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "",
            width: 50,
            cellTemplate: '<div class="ngCellText" ng-click="removeStock(row)">X</div>',
            sortable: false
        },
            {field: 'name', displayName: 'name', width: 200},
            {field: 'location', displayName: 'location', width: 200},
            {field: 'creationDate', displayName: 'creation date', width: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"},
            {field: 'createdBy', displayName: 'created by', width: 120},
            {field: 'lastModifiedDate', displayName: 'last modified date', width: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', width: 120}]
    };

    $scope.stockGridData=[];

    $scope.changeStockSelection = function(stock){
        for(var i = 0; i < $scope.stockGridData.length; i++){
            if($scope.stockGridData[i].id == stock.id){
                return;
            }
        }
        $scope.stockGridData.push(stock);
    };

    $scope.removeStock = function(stock){
        for(var i = 0; i < $scope.stockGridData.length; i++){
            if($scope.stockGridData[i].id == stock.entity.id){
                $scope.stockGridData.splice(i,1);
                return;
            }
        }
    };

    $scope.save = function(){
        $scope.ValueMissingError = false;
        $scope.ColorValueMissingError = false;

        if($scope.editingRawMaterial.description == "" || $scope.selectedProductType == "" || $scope.selectedSize == "" || $scope.series =="" || $scope.suppliers == "" || $scope.stockGridData.length <= 0 || $scope.colorGridData.length <= 0){
            $scope.ValueMissingError = true;
            return;
        }

        for(var i = 0; i < $scope.colorGridData.length; i++){
            if($scope.colorGridData[i].supplierCode == "" || $scope.colorGridData[i].price == "" || $scope.colorGridData[i].MOQ == "" || $scope.colorGridData[i].baseAmount == ""){
                $scope.ColorValueMissingError = true;
                return;
            }
        }


        $scope.editingRawMaterial.productTypeId = $scope.selectedProductType.id;
        $scope.editingRawMaterial.sizeId = $scope.selectedSize.id;
        $scope.editingRawMaterial.supplierId = $scope.selectedSupplier.id;
        $scope.editingRawMaterial.seriesId = $scope.selectedSeries.id;
        $scope.editingRawMaterial.stockIds = [];
        for(var i = 0; i < $scope.stockGridData.length; i++){
            $scope.editingRawMaterial.stockIds.push($scope.stockGridData[i].id);
        }

        if(crudOption == 'create'){
            saveMaterials($scope.colorGridData, 0, 0);
        }
    };

    function saveMaterials(colorList, index, successfulSaves){
        if(index < colorList.length){
            httpServiceFactory.createPostPromise('/raw/create', 'userid=' + applicationInformation.getUserinformation().userId, createRawMaterialObject(colorList[index]), true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    saveMaterials(colorList, index + 1, successfulSaves + 1);
                })
                .error(function (error, status) {
                    saveMaterials(colorList, index + 1, successfulSaves);
                });
        } else {
            notificationService.showMessageNotification(successfulSaves + " out of " + colorList.length + " new materials have been saved successfully.");
            $location.path('/rawMaterialOverview');
        }
    }

    function createRawMaterialObject(colorGridItem){
        var rawMaterial = {};
        rawMaterial.colorId = colorGridItem.id;
        rawMaterial.supplierCode = colorGridItem.supplierCode;
        rawMaterial.price = colorGridItem.price.toFixed(2);
        rawMaterial.MOQ = colorGridItem.MOQ;
        rawMaterial.baseAmount = colorGridItem.baseAmount;
        rawMaterial.note = colorGridItem.note;
        rawMaterial.description = $scope.editingRawMaterial.description;
        rawMaterial.productTypeId = $scope.selectedProductType.id;
        rawMaterial.seriesId = $scope.selectedSeries.id;
        rawMaterial.sizeId = $scope.selectedSize.id;
        rawMaterial.stockIds = $scope.editingRawMaterial.stockIds;
        rawMaterial.supplierId = $scope.editingRawMaterial.supplierId;
        return rawMaterial;
    }

    function init(){
        $scope.productTypes = productTypes.data;
        $scope.sizes = sizes.data;
        $scope.series = series.data;
        $scope.suppliers = suppliers.data;
        $scope.colors = colors.data;
        $scope.stocks = stocks.data;
        $scope.editingRawMaterial = {};
        if(crudOption == "create"){
            $scope.editingRawMaterial = {"productTypeId":"", "sizeId":"", "supplierId":"", "seriesId":"", "colorId":"", "description":"", "stockIds":[], "supplierCode":"", "price":"", "MOQ":"", "baseAmount":"", "note":""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);