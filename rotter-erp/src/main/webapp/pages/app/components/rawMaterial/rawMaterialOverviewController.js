/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: class responsible for the raw material overview page.This has to handle all the logic that is not in a directive like the search directive.
 */
'use strict';
var controller = angular.module('clarity.material.raw');

controller.controller('rawMaterialOverviewCtrl', ['$scope', '$log', '$location', 'copypasterinoService', 'httpServiceFactory', 'applicationInformation', 'searchPopulationService', 'crudService', 'shoppingCart', 'overviewType', function($scope, $log, $location, copypasterinoService, httpServiceFactory, applicationInformation, searchPopulationService, crudService, shoppingCart, overviewType){
    $scope.showNavBar = true;
    $scope.searchPopulationData = undefined;
    $scope.orderAmount = {};
    $scope.overviewType = overviewType;
    $scope.orderDetailInfo = "";

    //if overview type is 'overview'.
    $scope.gridOptions = {
        data: 'data',
        enablePinning: true,
        enableColumnResize: true,
        columnDefs: [{
            field: 'linecolor',
            displayName: "pos",
            width: 50,
            cellTemplate: '<div class="ngCellText" style="background-color: {{row.getProperty(col.field)}}">{{row.rowIndex + 1}}</div>',
            pinned: true,
            pinnable: false,
            sortable: false
        },
            {field: 'productCode', displayName: 'id', width: 100, pinned: true},
            {field: 'productType', displayName: 'pro. type', width: 120},
            {field: 'size', displayName: 'size', width: 110},
            {field: 'color', displayName: 'color', width: 100},
            {field: 'quality', displayName: 'quality', width: 120},
            {field: 'description', displayName: 'description', width: 250},
            {field: 'amountInStock', displayName: 'amount in stock', width: 132},
            {field: 'minimumAmountInStock', displayName: 'base stock amount', width: 157},
            {field: 'price', displayName: 'price', width: 80, cellTemplate: '<div class="ngCellText">{{row.entity.price.toFixed(2)}}</div>'},
            {field: 'minimumorderQuantity', displayName: 'minimum order quantity', width: 120},
            {field: 'lastModifiedDate', displayName: 'last modified date', width: 145, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"},
            {field: 'lastModifiedBy', displayName: 'last modified by', width: 140}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingRawMaterial(rowItem.entity);
            $location.path('/detailMaterialOverview');
        }
    };

    $scope.csvData = "";
    $scope.getCsvData = function(){
        $log.log($scope.data);
        $scope.csvData = "productCode,productType,size,color,quality,description,amountInStock,minimumAmountInStock,price,MOQ,lastModifiedDate,lastModifiedBy\n";
        for(var i = 0; i < $scope.data.length; i++){
            $scope.csvData = $scope.csvData.concat($scope.data[i].productCode).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].productType).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].size).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].color).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].quality).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].description).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].amountInStock).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].minimumAmountInStock).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].price).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].minimumorderQuantity).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].lastmodified.dayOfMonth + " " + $scope.data[i].lastmodified.month + " " + $scope.data[i].lastmodified.year + "-" + $scope.data[i].lastmodified.hour + ":" + $scope.data[i].lastmodified.minute).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.data[i].lastModifiedBy).concat("\n");
        }
        copypasterinoService.showText($scope.csvData);
        $scope.csvData = "";
    };

    $scope.updateGrid=function(gridData){
        $scope.data = gridData;
        $scope.gridOptions.data = 'data';
    };

    $scope.data={};

    //if overview type is 'order'.
    $scope.gridOrderOptions = {
        data: 'data',
        enablePinning: true,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            width: 50,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            pinned: true,
            pinnable: false,
            sortable: false
        },
            {field: '', displayName: '', width: 94, pinned: true, pinnable: false, cellTemplate: '<input class="ngCellText" type="button" value="add to order" ng-click="addToOrder(row)">'},
            {field: 'productCode', displayName: 'id', width: 100, pinned: true},
            {field: 'productType', displayName: 'pro. type', width: 120},
            {field: 'size', displayName: 'size', width: 110},
            {field: 'color', displayName: 'color', width: 100},
            {field: 'supplier', displayName: 'supplier', width: 130},
            {field: 'description', displayName: 'description', width: 250},
            {field: 'amountInStock', displayName: 'amount in stock', width: 132},
            {field: 'minimumAmountInStock', displayName: 'base stock amount', width: 157},
            {field: 'price', displayName: 'price', width: 80, cellTemplate: '<div class="ngCellText">{{row.entity.price.toFixed(2)}}</div>'},
            {field: 'minimumorderQuantity', displayName: 'minimum order quantity', width: 120},
            {field: 'lastModifiedDate', displayName: 'last modified date', width: 145, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"},
            {field: 'lastModifiedBy', displayName: 'last modified by', width: 140}]
    };

    $scope.addToOrder = function(row) {
        $log.log("---------------------");
        $log.log(row.entity);
            shoppingCart.addToRawOrderCart(row.entity, $scope.orderAmount.rawMaterialAmount);
            $scope.orderAmount.rawMaterialAmount = "";
        $scope.orderDetailInfo = shoppingCart.getRawOrderDetailLine();
    };

    $scope.openOrderCart = function(){
        $location.path("/createOrderRaw");
    };

    function init(){
        $log.log('init raw search params');
        if(searchPopulationService.getRawMaterialSearchParameters() == undefined){
            httpServiceFactory.createGetPromise('/raw/searchterms', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function(data, status, headers, config){
                    $log.log(data);
                    searchPopulationService.setRawMaterialSearchParameters(data);
                    $scope.searchPopulationData = searchPopulationService.getRawMaterialSearchParameters();
                })
                .error(function(error, status){
                    $log.log(error);
                })
        }else
        {
            $scope.searchPopulationData = searchPopulationService.getRawMaterialSearchParameters();
        }
        $scope.orderDetailInfo = shoppingCart.getRawOrderDetailLine();
    }
    init();
}]);