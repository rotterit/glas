/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var rawStockCorrectionController = angular.module('clarity.crud');

rawStockCorrectionController.controller('rawStockCorrectionController', ['$scope', '$log','httpServiceFactory', 'applicationInformation', 'crudService', 'notificationService', '$location', function($scope, $log, httpServiceFactory, applicationInformation, crudService, notificationService, $location){
    var rawMaterial = {};
    $scope.amount = "";
    $scope.stocks = [];
    $scope.selectedStock = {};

    $scope.ValueFormatError = false;
    $scope.StockMissingError = false;

    $scope.save = function(){
        $scope.ValueFormatError = false;
        $scope.StockMissingError = false;

        if(Object.keys($scope.selectedStock).length === 0){
            $scope.StockMissingError = true;
        } else if(isNaN($scope.amount)){
            $scope.ValueFormatError = true;
        } else if(Number($scope.amount) == 0){
            $scope.ValueFormatError = true;
        } else{
            httpServiceFactory.createPostPromise('/stockcorrections/raw/create', 'userid=' + applicationInformation.getUserinformation().userId + "&materialid=" + rawMaterial.id + "&amount=" + Number($scope.amount) + "&stockid=" + $scope.selectedStock.id, '', true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Stock updated.");
                    $location.path('/rawMaterialOverview');
                })
                .error(function (error, status) {
                    notificationService.showErrorNotification("Unable to correct the stock");
                });
        }
    };

    $scope.cancel = function(){
        $location.path("/rawMaterialOverview");
    };

    function loadAvailibleStocks(){
        httpServiceFactory.createGetPromise('/stockcorrections/getstockspermaterial', 'userid=' + applicationInformation.getUserinformation().userId + "&materialid=" + rawMaterial.id, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.stocks = data;
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get stocks from server.");
            });
    }

    function init(){
        rawMaterial = crudService.getCorrectingRawMaterial();
        loadAvailibleStocks();
    }
    init();
}]);