/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: This is the controller that handles the creation of a new order and the edit of an existing order.
 */
'use strict';
var createInvoiceController = angular.module('clarity.crud');

createInvoiceController.controller('createInvoiceController', ['$scope', '$log', 'httpServiceFactory', 'applicationInformation', 'notificationService', '$location', 'invoiceShoppingCart', 'crudService', '$document', function($scope, $log, httpServiceFactory, applicationInformation, notificationService, $location, invoiceShoppingCart, crudService, $document){
    $scope.crudType = "create";
    var isDone = false;

    $scope.creationDate = new Date().toLocaleDateString();
    $scope.creatingUser = applicationInformation.getUsername();
    $scope.customers = [];
    $scope.selectedContactPerson = {};
    $scope.customerSearch = "";

    var invoiceLines = null;
    $scope.invoiceInformation = null;
    $scope.extraInvoiceInfo = {"paymentDueDate": "", "inlineDiscount": 0, "useInlineDiscount": false, "dueDate": "", "total": 0, "totalAmount": 0, "netto1": 0, "netto2": 0, "warrenty": 0};

    //errors
    $scope.NoDueDateSelectedError = false;
    $scope.NoPaymentDueDateSelectedError = false;
    $scope.NoCustomerSelectedError = false;
    $scope.WrongFormatError = false;



    $scope.invoiceProductsGrid = {
        data: 'invoiceProductsGridData',
        enablePinning: true,
        enableColumnResize: true,
        columnDefs: [{
            field: 'linecolor',
            displayName: "pos",
            width: 50,
            cellTemplate: '<div class="ngCellText" style="background-color: {{row.getProperty(col.field)}}">{{row.rowIndex + 1}}</div>',
            pinned: true,
            pinnable: false,
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 50,
            cellTemplate: '<div class="ngCellText" ng-click="removeFromInvoice(row.entity)">X</div>',
            sortable: false, pinned: true, pinnable: false
        },
            {field: 'quantity', displayName: 'amount', width: 92, pinned: true, enableCellEdit: true, cellTemplate: '<input class="ngCellText" type="text" ng-change="invoiceVariablesChanged()" ng-model="row.entity.quantity">'},
            {field: 'inlineDiscount', displayName: 'inline disc.[%]', width: 120, pinned: true, enableCellEdit: true, cellTemplate: '<input class="ngCellText" type="text" ng-change="invoiceVariablesChanged()" ng-model="row.entity.inlineDiscount">'},
            {field: 'finishedProduct.productCode', displayName: 'id', width: 120, pinned: true},
            {field: 'finishedProduct.price', displayName: 'whole sale price', width: 120},
            {field: '', displayName: 'total', width: 120, cellTemplate: '<div class="ngCellText">{{row.entity.quantity * row.entity.finishedProduct.price}}</div>'},
            {field: 'finishedProduct.stock.name', displayName: 'stock', width: 120},
            {field: 'finishedProduct.name', displayName: 'name', width: 120},
            {field: 'finishedProduct.designName', displayName: 'design', width: 120},
            {field: 'finishedProduct.stockAmount', displayName: 'amount in stock', width: 120},
            {field: 'finishedProduct.reservedStock', displayName: 'amount reserved', width: 120},
            {field: 'finishedProduct.onComission', displayName: 'on commission', width: 120},
            {field: 'finishedProduct.categoryName', displayName: 'category', width: 120},
            {field: 'finishedProduct.productTypeName', displayName: 'product type', width: 120},
            {field: 'finishedProduct.sizeCode', displayName: 'size', width: 120},
            {field: 'finishedProduct.color', displayName: 'color', width: 120},
            {field: 'finishedProduct.manufacturingTypeName', displayName: 'manufacturing type', minWidth: 180, width: 180, maxWidth: 180}]
    };

    $scope.invoiceProductsGridData = [];

    $scope.removeFromInvoice = function(invoiceLineItem) {
        invoiceLines = invoiceShoppingCart.removeProductFromInvoice(invoiceLineItem.finishedProduct.id);
        $scope.invoiceProductsGridData = invoiceLines;
    };

    $scope.save = function(){
        $log.log($scope.selectedContactPerson);

        var invoice =  create();
        $log.log("the created invoice");
        $log.log(invoice);
        if(invoice == null){return;}
        httpServiceFactory.createPostPromise('/invoices/create', 'userid=' + applicationInformation.getUserinformation().userId, invoice, true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                $log.log(status);
                notificationService.showMessageNotification("Invoice saved.");
                isDone = true;
                $location.path('/offerInvoices');
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + error);
            });
    };

    function create(){
        $scope.NoDueDateSelectedError = false;
        $scope.NoPaymentDueDateSelectedError = false;
        $scope.NoCustomerSelectedError = false;
        $scope.WrongFormatError = false;


        if(stringToDate($document.find('#deliverDateField')[0].value) == null){
            $scope.NoDueDateSelectedError = true;
        } else if(stringToDate($document.find('#paymentDueDate')[0].value) == null){
            $scope.NoPaymentDueDateSelectedError = true;
        } else if(Object.keys($scope.selectedContactPerson).length == 0){
            $scope.NoCustomerSelectedError = true;
        }else if (isNaN($scope.extraInvoiceInfo.netto2) || isNaN($scope.invoiceInformation.shippingCost) || isNaN($scope.invoiceInformation.upfrontpercentage) || isNaN($scope.invoiceInformation.packingCost) || isNaN($scope.invoiceInformation.packingCostPercentage) || isNaN($scope.invoiceInformation.discount) || isNaN($scope.invoiceInformation.discountpercentage) || isNaN($scope.invoiceInformation.VAT) || isNaN($scope.invoiceInformation.VATpercentage) || isNaN($scope.invoiceInformation.price) || isNaN($scope.invoiceInformation.ValutaFactor) || isNaN($scope.invoiceInformation.localPrice)) {
            $scope.WrongFormatError = true;
        } else {
            var dueDate = stringToDate($document.find('#deliverDateField')[0].value).getTime();
            var paymentDueDate = stringToDate($document.find('#paymentDueDate')[0].value).getTime();
            var newInvoice = {
                "customerId": $scope.selectedContactPerson.id,
                "dueDate": dueDate,
                "paymentDueDate": paymentDueDate,
                "priceWithoutVAT": Number($scope.extraInvoiceInfo.netto2).toFixed(2),
                "localPrice": Number($scope.invoiceInformation.localPrice).toFixed(2),
                "ValutaFactor": Number($scope.invoiceInformation.ValutaFactor),
                "price": Number($scope.invoiceInformation.price).toFixed(2),
                "VATpercentage": Number($scope.invoiceInformation.VATpercentage),
                "VAT": Number($scope.invoiceInformation.VAT).toFixed(2),
                "discountpercentage": Number($scope.invoiceInformation.discountpercentage),
                "discount": Number($scope.invoiceInformation.discount).toFixed(2),
                "packingCostPercentage": Number($scope.invoiceInformation.packingCostPercentage),
                "packingCost": Number($scope.invoiceInformation.packingCost).toFixed(2),
                "upfrontpercentage": Number($scope.invoiceInformation.upfrontpercentage),
                "valuta": $scope.invoiceInformation.valuta,
                "note": $scope.invoiceInformation.note,
                "invoiceCreateLines": [],
                "shippingCost": Number($scope.invoiceInformation.shippingCost).toFixed(2),
                "shippingMethod": $scope.invoiceInformation.shippingMethod
            };
            for(var i = 0; i < invoiceLines.length; i++){
                $log.log("creating invloce  lines");
                $log.log(invoiceLines[i]);
                newInvoice.invoiceCreateLines.push({"productId": invoiceLines[i].finishedProduct.id,
                    "amount":Number(invoiceLines[i].quantity),
                    "inlineDiscount": ((Number(invoiceLines[i].quantity) * Number(invoiceLines[i].finishedProduct.price)) * (Number(invoiceLines[i].inlineDiscount)/100)),
                    "inlineDiscountPercentage": Number(invoiceLines[i].inlineDiscount),
                    "inlinePrice": ((Number(invoiceLines[i].quantity) * Number(invoiceLines[i].finishedProduct.price)) - ((Number(invoiceLines[i].quantity) * Number(invoiceLines[i].finishedProduct.price)) * (Number(invoiceLines[i].inlineDiscount)/100))),
                    "stockId": Number(invoiceLines[i].finishedProduct.stock.id)})
            }

            return newInvoice;
        }
        return null;
    }

    $scope.show = function(){
        $log.log($scope.invoiceInformation);
        $log.log($scope.extraInvoiceInfo);
    };

    //change methodes
    $scope.invoiceVariablesChanged = function(){
        if(isNaN($scope.invoiceInformation.discountpercentage)){
            $scope.invoiceInformation.discountpercentage = 0;
        }else if(isNaN($scope.invoiceInformation.packingCostPercentage)){
            $scope.invoiceInformation.packingCostPercentage = 0;
        }else if(isNaN($scope.invoiceInformation.shippingCost)){
            $scope.invoiceInformation.shippingCost = 0;
        }else if(isNaN($scope.invoiceInformation.VATpercentage)){
            $scope.invoiceInformation.VATpercentage = 0;
        }else if(isNaN($scope.invoiceInformation.ValutaFactor)){
            $scope.invoiceInformation.ValutaFactor = 1;
        }
        calculate();
    };

    $scope.updatePaymentDueDate = function(){
        $scope.extraInvoiceInfo.paymentDueDate = stringToDate($document.find('#deliverDateField')[0].value);
        $document.find('#paymentDueDate')[0].value = dateToString($scope.extraInvoiceInfo.paymentDueDate + new Date(14 ,0,0,0,0,0));
        //periode.endDate = stringToDate($document.find('#endDateField')[0].value);
    };

    function stringToDate(dateString){
        var re_date = /^(\d+)\-(\d+)\-(\d+)\s+(\d+)\:(\d+)\:(\d+)$/;
        if (!re_date.exec(dateString)){
            $scope.DateWrongFormatError = true;
            return null;
        }
        return new Date (RegExp.$3, RegExp.$2-1, RegExp.$1, 12, 0, 0);
    }

    function dateToString(date){
        return (new String(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()));
    }

    //calculate all the info
    function calculate(){
        var totalInvoiceLinePrice = 0;
        for(var i = 0; i < invoiceLines.length; i++){
            $log.log(invoiceLines[i]);
            totalInvoiceLinePrice = totalInvoiceLinePrice + ((Number(invoiceLines[i].finishedProduct.price) * Number(invoiceLines[i].quantity)) - ((Number(invoiceLines[i].finishedProduct.price) * Number(invoiceLines[i].quantity)) * (Number(invoiceLines[i].inlineDiscount)/100)));
        }

        $scope.extraInvoiceInfo.total = totalInvoiceLinePrice;
        $scope.invoiceInformation.discount = $scope.extraInvoiceInfo.total * (Number($scope.invoiceInformation.discountpercentage)/100);
        $scope.extraInvoiceInfo.netto1 = $scope.extraInvoiceInfo.total - $scope.invoiceInformation.discount;
        $scope.invoiceInformation.packingCost = $scope.extraInvoiceInfo.netto1 * (Number($scope.invoiceInformation.packingCostPercentage/100));
        $scope.extraInvoiceInfo.netto2 = $scope.extraInvoiceInfo.netto1 + $scope.invoiceInformation.packingCost + Number($scope.invoiceInformation.shippingCost);
        $scope.invoiceInformation.VAT = $scope.extraInvoiceInfo.netto2 * (Number($scope.invoiceInformation.VATpercentage)/100);
        $scope.invoiceInformation.localPrice = $scope.extraInvoiceInfo.netto2 + $scope.invoiceInformation.VAT;
        $scope.invoiceInformation.price = $scope.invoiceInformation.localPrice * Number($scope.invoiceInformation.ValutaFactor);
    }

    $scope.$on('ngGridEventEndCellEdit', function(event) {
        calculate();
    });

    //if you go away from this page you save the note in the shopping cart.
    $scope.$on('$locationChangeStart', function(event) {
        if(isDone){
            invoiceShoppingCart.closeInvoice();
        } else {
            if ($location.path() == "/invoiceShoppingCart" || $location.path() == "/createInvoice") {
                invoiceShoppingCart.saveInvoice(invoiceInformation, invoiceLines);
            } else {
                if (confirm("You are leaving the invoice creation. Trudy is not happy about that. Are you sure you want to leave. !All invoice information will be lost!") == true) {
                    invoiceShoppingCart.cancelInvoice();
                } else {
                    event.preventDefault();
                }
            }
        }
    });

    $scope.addMoreProducts = function(){
        $location.path("/invoiceShoppingCart");
    };

    $scope.searchContacts = function(){
        $scope.selectedContactPerson = {};
        httpServiceFactory.createGetPromise('/customers/byname', 'userid=' + applicationInformation.getUserinformation().userId + "&searchterm=" + $scope.customerSearch, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log(data);
                $scope.customers = data;
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get customers from server.");
                $location.path("/notsendInvoices");
            });
    };

    $scope.selectACustomer = function(){
        $scope.invoiceInformation.discountpercentage = $scope.selectedContactPerson.baseDiscountPercentage;
        $scope.invoiceInformation.packingCostPercentage = $scope.selectedContactPerson.basePackingCostPercentage;
        $scope.invoiceInformation.VAT = $scope.selectedContactPerson.baseVAT;
        for(var i = 0; i < $scope.invoiceProductsGridData.length; i++){
            $scope.invoiceProductsGridData[i].inlineDiscount = $scope.selectedContactPerson.baseLineDiscountPercentage;
        }
    };

    function loadCustomers(){
        httpServiceFactory.createGetPromise('/customers/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.customers = data;
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get customers from server.");
                $location.path("/notsendInvoices");
            });
    }

    function init(){
        loadCustomers();
        invoiceLines = invoiceShoppingCart.getInvoiceShoppingCart();
        $scope.invoiceProductsGridData = invoiceLines;
        $log.log("shopping cart");
        $log.log(invoiceLines);
        $scope.invoiceInformation = invoiceShoppingCart.getInvoiceInformation();
        calculate();
    }
    init();
}]);