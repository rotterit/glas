/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var invoiceSummaryController = angular.module('clarity.overview');

invoiceSummaryController.controller('invoiceSummaryController', ['$scope', '$log', 'copypasterinoService', '$location', 'httpServiceFactory', 'applicationInformation', 'notificationService', '$routeParams', 'invoiceDetail', 'crudService', function($scope, $log, copypasterinoService, $location, httpServiceFactory, applicationInformation, notificationService, $routeParams, invoiceDetail, crudService){
    $scope.invoice = {};
    $scope.customer = {};

    $scope.invoiceLineGrid = {
        data: 'invoiceLineGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'productCode', displayName: 'Unsere Artikel-Nr', width: 150},
            {field: 'description', displayName: 'Bescheibung', width: 150},
            {field: 'amount', displayName: 'Menge', width: 150},
            {field: 'pricePerPiece', displayName: 'Stückpreis', width: 150},
            {field: 'totalLinePrice', displayName: 'Gesamtpreis', width: 150}]
    };

    $scope.invoiceLineGridData = [];

    $scope.back = function() {
        $location.path("/offerInvoices");
    };

    function loadCustomer(){
        $scope.selectedContactPerson = {};
        httpServiceFactory.createGetPromise('/customers/byid', 'userid=' + applicationInformation.getUserinformation().userId + "&customerid=" + $scope.invoice.customerId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log(data);
                $scope.customer = data;
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get customers from server.");
                $location.path("/offerInvoices");
            });
    }

    function loadInvoiceLineGrid() {
        for (var i = 0; i < $scope.invoice.order.length; i++) {
            var invoiceLine = {
                "productCode": $scope.invoice.order[i].product.productCode,
                "description": "",
                "amount": $scope.invoice.order[i].amount,
                "pricePerPiece": (Number($scope.invoice.order[i].inlinePrice) / Number($scope.invoice.order[i].amount)).toFixed(2),
                "totalLinePrice": $scope.invoice.order[i].inlinePrice.toFixed(2)
            };
            for(var j = 0; j < $scope.invoice.order[i].product.descriptions.length; j++){
                if($scope.invoice.order[i].product.descriptions[j].languageCode == "de"){
                    invoiceLine.description = $scope.invoice.order[i].product.descriptions[j].description;
                }
            }
            $scope.invoiceLineGridData.push(invoiceLine);
        }
    }

    $scope.createCustomerCsv = function() {
        $log.log("customer");
        $log.log($scope.customer);
        $scope.csvData = "";
        $scope.csvData = $scope.csvData.concat("Company name:,").concat($scope.customer.companyName).concat("\n");
        $scope.csvData = $scope.csvData.concat("Contact person:,").concat($scope.customer.name).concat("\n");
        $scope.csvData = $scope.csvData.concat("Adress:,").concat($scope.customer.adress).concat("\n");
        $scope.csvData = $scope.csvData.concat(",").concat($scope.customer.zip).concat(" ").concat($scope.customer.city).concat(" ").concat($scope.customer.country).concat("\n");
        $scope.csvData = $scope.csvData.concat("Telephone:,").concat($scope.customer.tel).concat("\n");
        $scope.csvData = $scope.csvData.concat("email:,").concat($scope.customer.email).concat("\n");
        $scope.csvData = $scope.csvData.concat("VAT nr.:,").concat($scope.customer.vatnumber);
        copypasterinoService.showText($scope.csvData);
        $scope.csvData = "";
    };

    $scope.createManufacturerCsv = function(){
        $log.log("manufacturer");
        $log.log($scope.invoice);
        $scope.csvData = "";
        $scope.csvData = $scope.csvData.concat("Invoice Nr.:,").concat($scope.invoice.invoicenr).concat("\n");
        $scope.csvData = $scope.csvData.concat("Datum:,").concat("\n");
        $scope.csvData = $scope.csvData.concat("Ansprechpartner:,").concat($scope.invoice.createdBy).concat("\n");
        $scope.csvData = $scope.csvData.concat("Delivery Date:,").concat($scope.invoice.sendDate.dayOfMonth).concat(" ").concat($scope.invoice.sendDate.month).concat(" ").concat($scope.invoice.sendDate.year).concat("\n");
        $scope.csvData = $scope.csvData.concat("Payment Due:,").concat($scope.invoice.paymentDueDate.dayOfMonth).concat(" ").concat($scope.invoice.paymentDueDate.month).concat(" ").concat($scope.invoice.paymentDueDate.year).concat("\n");
        $scope.csvData = $scope.csvData.concat("Shippingmethode:,").concat($scope.invoice.shippingMethod).concat("\n");
        copypasterinoService.showText($scope.csvData);
        $scope.csvData = "";
    };

    $scope.createProductLinesCsv = function(){
        $log.log("invoice lines");
        $log.log($scope.invoiceLineGridData);
        $scope.csvData = "pos.,Unsere Artikel-Nr.,Beschreibung,Menge,Stuckpreis,Gesamtpreis\n";
        for(var i = 0; i < $scope.invoiceLineGridData.length; i++){
            $scope.csvData = $scope.csvData.concat(i + 1).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.invoiceLineGridData[i].productCode).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.invoiceLineGridData[i].description).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.invoiceLineGridData[i].amount).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.invoiceLineGridData[i].pricePerPiece).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.invoiceLineGridData[i].totalLinePrice).concat(",\n");
        }
        copypasterinoService.showText($scope.csvData);
        $scope.csvData = "";
    };

    function init() {
        $scope.invoice = crudService.getDetailInvoice();
        loadCustomer();
        loadInvoiceLineGrid();
        $log.log("my invoice");
        $log.log($scope.invoice);
    }
    init();
}]);