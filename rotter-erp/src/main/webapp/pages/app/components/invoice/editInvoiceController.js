/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: This is the controller that handles the creation of a new order and the edit of an existing order.
 */
'use strict';
var editInvoiceController = angular.module('clarity.crud');

editInvoiceController.controller('editInvoiceController', ['$scope', '$log', 'httpServiceFactory', 'applicationInformation', 'notificationService', '$location', 'invoiceShoppingCart', 'crudService', '$document', function($scope, $log, httpServiceFactory, applicationInformation, notificationService, $location, invoiceShoppingCart, crudService, $document){
    $scope.crudType = "create";

    $scope.creationDate = new Date().toLocaleDateString();
    $scope.creatingUser = applicationInformation.getUsername();
    $scope.customer = {};

    $scope.fullEditingInvoice = {};
    $scope.invoiceId = null;
    var invoiceLines = null;
    $scope.invoiceInformation = null;
    $scope.extraInvoiceInfo = {"paymentDueDate": "", "inlineDiscount": 0, "useInlineDiscount": false, "dueDate": "", "total": 0, "totalAmount": 0, "netto1": 0, "netto2": 0, "warrenty": 0};

    //errors
    $scope.NoDueDateSelectedError = false;
    $scope.NoPaymentDueDateSelectedError = false;
    $scope.NoCustomerSelectedError = false;
    $scope.WrongFormatError = false;



    $scope.invoiceProductsGrid = {
        data: 'invoiceProductsGridData',
        enablePinning: true,
        enableColumnResize: true,
        columnDefs: [{
            field: 'linecolor',
            displayName: "pos",
            width: 50,
            cellTemplate: '<div class="ngCellText" style="background-color: {{row.getProperty(col.field)}}">{{row.rowIndex + 1}}</div>',
            pinned: true,
            pinnable: false,
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 50,
            cellTemplate: '<div class="ngCellText" ng-click="removeFromInvoice(row.entity)">X</div>',
            sortable: false, pinned: true, pinnable: false
        },
            {field: 'quantity', displayName: 'amount', width: 92, pinned: true, enableCellEdit: true, cellTemplate: '<input class="ngCellText" type="text" ng-change="invoiceVariablesChanged()" ng-model="row.entity.quantity">'},
            {field: 'inlineDiscount', displayName: 'inline disc.[%]', width: 120, pinned: true, enableCellEdit: true, cellTemplate: '<input class="ngCellText" type="text" ng-change="invoiceVariablesChanged()" ng-model="row.entity.inlineDiscount">'},
            {field: 'finishedProduct.productCode', displayName: 'id', width: 120, pinned: true},
            {field: 'finishedProduct.price', displayName: 'whole sale price', width: 120},
            {field: '', displayName: 'total', width: 120, cellTemplate: '<div class="ngCellText">{{row.entity.quantity * row.entity.finishedProduct.price}}</div>'},
            {field: 'finishedProduct.stock.name', displayName: 'stock', width: 120},
            {field: 'finishedProduct.name', displayName: 'name', width: 120},
            {field: 'finishedProduct.designName', displayName: 'design', width: 120},
            {field: 'finishedProduct.stockAmount', displayName: 'amount in stock', width: 120},
            {field: 'finishedProduct.reservedStock', displayName: 'amount reserved', width: 120},
            {field: 'finishedProduct.onComission', displayName: 'on commission', width: 120},
            {field: 'finishedProduct.categoryName', displayName: 'category', width: 120},
            {field: 'finishedProduct.productTypeName', displayName: 'product type', width: 120},
            {field: 'finishedProduct.sizeCode', displayName: 'size', width: 120},
            {field: 'finishedProduct.color', displayName: 'color', width: 120},
            {field: 'finishedProduct.manufacturingTypeName', displayName: 'manufacturing type', minWidth: 180, width: 180, maxWidth: 180}]
    };

    $scope.invoiceProductsGridData = [];

    $scope.removeFromInvoice = function(invoiceLineItem) {
        invoiceLines = invoiceShoppingCart.removeProductFromInvoice(invoiceLineItem.finishedProduct.id);
        $scope.invoiceProductsGridData = invoiceLines;
    };

    $scope.save = function(){
        $log.log($scope.selectedContactPerson);

        var invoice =  create();
        if(invoice == null){return;}
        httpServiceFactory.createPostPromise('/invoices/edit', 'userid=' + applicationInformation.getUserinformation().userId, invoice, true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                notificationService.showMessageNotification("Invoice saved.");
                $location.path('/offerInvoices');
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Could not save the edited invoice.");
            });
    };

    function create(){
        $scope.NoDueDateSelectedError = false;
        $scope.NoPaymentDueDateSelectedError = false;
        $scope.NoCustomerSelectedError = false;
        $scope.WrongFormatError = false;


        if(stringToDate($document.find('#deliverDateField')[0].value) == null){
            $scope.NoDueDateSelectedError = true;
        } else if(stringToDate($document.find('#paymentDueDate')[0].value) == null){
            $scope.NoPaymentDueDateSelectedError = true;
        } else if (isNaN($scope.extraInvoiceInfo.netto2) || isNaN($scope.invoiceInformation.shippingCost) || isNaN($scope.invoiceInformation.upfrontpercentage) || isNaN($scope.invoiceInformation.packingCost) || isNaN($scope.invoiceInformation.packingCostPercentage) || isNaN($scope.invoiceInformation.discount) || isNaN($scope.invoiceInformation.discountpercentage) || isNaN($scope.invoiceInformation.VAT) || isNaN($scope.invoiceInformation.VATpercentage) || isNaN($scope.invoiceInformation.price) || isNaN($scope.invoiceInformation.ValutaFactor) || isNaN($scope.invoiceInformation.localPrice)) {
            $scope.WrongFormatError = true;
        } else {
            var dueDate = stringToDate($document.find('#deliverDateField')[0].value).getTime();
            var paymentDueDate = stringToDate($document.find('#paymentDueDate')[0].value).getTime();
            var newInvoice = {
                "customerId": $scope.invoiceInformation.customerId,
                "dueDate": dueDate,
                "paymentDueDate": paymentDueDate,
                "priceWithoutVAT": Number($scope.extraInvoiceInfo.netto2),
                "localPrice": Number($scope.invoiceInformation.localPrice),
                "ValutaFactor": Number($scope.invoiceInformation.ValutaFactor),
                "price": Number($scope.invoiceInformation.price),
                "VATpercentage": Number($scope.invoiceInformation.VATpercentage),
                "VAT": Number($scope.invoiceInformation.VAT),
                "discountpercentage": Number($scope.invoiceInformation.discountpercentage),
                "discount": Number($scope.invoiceInformation.discount),
                "packingCostPercentage": Number($scope.invoiceInformation.packingCostPercentage),
                "packingCost": Number($scope.invoiceInformation.packingCost),
                "upfrontpercentage": Number($scope.invoiceInformation.upfrontpercentage),
                "valuta": $scope.invoiceInformation.valuta,
                "note": $scope.invoiceInformation.note,
                "invoiceCreateLines": [],
                "shippingCost": Number($scope.invoiceInformation.shippingCost),
                "shippingMethod": $scope.invoiceInformation.shippingMethod
            };
            for(var i = 0; i < invoiceLines.length; i++){
                newInvoice.invoiceCreateLines.push({"productId": invoiceLines[i].finishedProduct.id,
                    "amount":Number(invoiceLines[i].quantity),
                    "inlineDiscount": ((Number(invoiceLines[i].quantity) * Number(invoiceLines[i].finishedProduct.price)) * (Number(invoiceLines[i].inlineDiscount)/100)),
                    "inlineDiscountPercentage": Number(invoiceLines[i].inlineDiscount),
                    "inlinePrice": ((Number(invoiceLines[i].quantity) * Number(invoiceLines[i].finishedProduct.price)) - ((Number(invoiceLines[i].quantity) * Number(invoiceLines[i].finishedProduct.price)) * (Number(invoiceLines[i].inlineDiscount)/100))),
                    "stockId": Number(invoiceLines[i].finishedProduct.stock.id)})
            }

            return {"invoiceId": $scope.fullEditingInvoice.id, "invoice": newInvoice};
        }
        return null;
    }

    /*$scope.show = function(){
        $log.log($scope.invoiceInformation);
        $log.log($scope.extraInvoiceInfo);
    };*/

    //change methodes
    $scope.invoiceVariablesChanged = function(){
        if(isNaN($scope.invoiceInformation.discountpercentage)){
            $scope.invoiceInformation.discountpercentage = 0;
        }else if(isNaN($scope.invoiceInformation.packingCostPercentage)){
            $scope.invoiceInformation.packingCostPercentage = 0;
        }else if(isNaN($scope.invoiceInformation.shippingCost)){
            $scope.invoiceInformation.shippingCost = 0;
        }else if(isNaN($scope.invoiceInformation.VATpercentage)){
            $scope.invoiceInformation.VATpercentage = 0;
        }else if(isNaN($scope.invoiceInformation.ValutaFactor)){
            $scope.invoiceInformation.ValutaFactor = 1;
        }
        calculate();
    };

    $scope.updatePaymentDueDate = function(){
        $scope.extraInvoiceInfo.paymentDueDate = stringToDate($document.find('#deliverDateField')[0].value);
        $document.find('#paymentDueDate')[0].value = dateToString($scope.extraInvoiceInfo.paymentDueDate + new Date(14 ,0,0,0,0,0));
        //periode.endDate = stringToDate($document.find('#endDateField')[0].value);
    };

    function stringToDate(dateString){
        var re_date = /^(\d+)\-(\d+)\-(\d+)\s+(\d+)\:(\d+)\:(\d+)$/;
        if (!re_date.exec(dateString)){
            $scope.DateWrongFormatError = true;
            return null;
        }
        return new Date (RegExp.$3, RegExp.$2-1, RegExp.$1, 12, 0, 0);
    }

    function dateToString(date){
        return (new String(date.getDate()+"-"+(date.getMonth()+1)+"-"+date.getFullYear()+" "+date.getHours()+":"+date.getMinutes()+":"+date.getSeconds()));
    }

    //calculate all the info
    function calculate(){
        var totalInvoiceLinePrice = 0;
        for(var i = 0; i < invoiceLines.length; i++){
            totalInvoiceLinePrice = totalInvoiceLinePrice + ((Number(invoiceLines[i].finishedProduct.price) * Number(invoiceLines[i].quantity)) - ((Number(invoiceLines[i].finishedProduct.price) * Number(invoiceLines[i].quantity)) * (Number(invoiceLines[i].inlineDiscount)/100)));
        }

        $scope.extraInvoiceInfo.total = totalInvoiceLinePrice;
        $scope.invoiceInformation.discount = $scope.extraInvoiceInfo.total * (Number($scope.invoiceInformation.discountpercentage)/100);
        $scope.extraInvoiceInfo.netto1 = $scope.extraInvoiceInfo.total - $scope.invoiceInformation.discount;
        $scope.invoiceInformation.packingCost = $scope.extraInvoiceInfo.netto1 * (Number($scope.invoiceInformation.packingCostPercentage/100));
        $scope.invoiceInformation.shippingCost = Number($scope.invoiceInformation.shippingCost);
        $scope.extraInvoiceInfo.netto2 = $scope.extraInvoiceInfo.netto1 + $scope.invoiceInformation.packingCost + $scope.invoiceInformation.shippingCost;
        $scope.invoiceInformation.VAT = $scope.extraInvoiceInfo.netto2 * (Number($scope.invoiceInformation.VATpercentage)/100);
        $scope.invoiceInformation.localPrice = $scope.extraInvoiceInfo.netto2 + $scope.invoiceInformation.VAT;
        $scope.invoiceInformation.price = $scope.invoiceInformation.localPrice * Number($scope.invoiceInformation.ValutaFactor);
    }

    $scope.$on('ngGridEventEndCellEdit', function(event) {
        calculate();
    });

    //if you go away from this page you save the note in the shopping cart.
    $scope.$on('$locationChangeStart', function(event) {
        invoiceShoppingCart.closeInvoice();
    });

    function loadCustomer(){
        httpServiceFactory.createGetPromise('/customers/byid', 'userid=' + applicationInformation.getUserinformation().userId + "&customerid=" + $scope.invoiceInformation.customerId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.customer = data;
            })
            .error(function (error, status) {
                notificationService.showErrorNotification(status + ": oops... Could not get customers from server.");
                $location.path("/offerInvoices");
            });
    }

    $scope.cancelEdit = function(){
        invoiceShoppingCart.closeInvoice();
        $location.path("/offerInvoices");
    };

    function init(){
        invoiceLines = invoiceShoppingCart.getInvoiceShoppingCart();
        $scope.fullEditingInvoice = invoiceShoppingCart.getEditingInvoice();
        $scope.invoiceProductsGridData = invoiceLines;
        $scope.invoiceInformation = invoiceShoppingCart.getInvoiceInformation();
        loadCustomer();
        $scope.invoiceId = Number(invoiceShoppingCart.getInvoiceId());
        $scope.invoiceNr = invoiceShoppingCart.getInvoiceNr();
        $scope.extraInvoiceInfo.dueDate = dateToString(new Date($scope.fullEditingInvoice.sendDate.year, $scope.fullEditingInvoice.sendDate.monthValue, $scope.fullEditingInvoice.sendDate.dayOfMonth, 12,0,0)).toString();
        $scope.extraInvoiceInfo.paymentDueDate = dateToString(new Date($scope.fullEditingInvoice.paymentDueDate.year, $scope.fullEditingInvoice.paymentDueDate.monthValue, $scope.fullEditingInvoice.paymentDueDate.dayOfMonth, 12,0,0)).toString();
        calculate();
    }
    init();
}]);