/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var invoiceDetailController = angular.module('clarity.overview');

invoiceDetailController.controller('invoiceDetailController', ['$scope', '$log', 'notificationService', 'invoiceDetail', 'crudService', '$location', 'httpServiceFactory', 'applicationInformation', function($scope, $log, notificationService, invoiceDetail, crudService, $location, httpServiceFactory, applicationInformation){
    $scope.invoice = invoiceDetail;
    $scope.invoiceInformation = {"invoiceNr": 123213, "totalPayed": 0, "paymentLeft":0, "totalUpfront": 0, "itemsLeftToPack": 0};

    $scope.isShowingInproductionItems = false;

    $scope.isAddingPayment = false;
    $scope.addingPayment = {"amount": 0};
    $scope.isEditingPayment = false;
    $scope.editingPayment = {"amount": 0};

    //packing creation
    $scope.isCreatingNewPackage = false;
    $scope.newPackage = [];
    $scope.selectedProduct = {};
    $scope.newPackageLine = {"selectedProduct": [], "amount": ""};
    $scope.packages = [];
    $scope.selectedPackage = {};

    $scope.NoProductSelectedError = false;
    $scope.AmountFormatError = false;
    $scope.NotThisManyAmountLeftError = false;
    //end packing creation


    $scope.invoiceProductsGrid = {
        data: 'invoiceProductsGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'amount', displayName: 'amount', minWidth: 100, width: 100, maxWidth: 350},
            {field: 'product.productCode', displayName: 'product code', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'product.productTypeName', displayName: 'product type', minWidth: 140, width: 140},
            {field: 'product.designName', displayName: 'design', minWidth: 170, width: 170, maxWidth: 170},
            {field: 'product.sizeCode', displayName: 'total amount', minWidth: 120, width: 120, maxWidth: 120},
            {field: 'product.color', displayName: 'color', minWidth: 180, width: 180, maxWidth: 180},
            {field: 'product.categoryName', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            $log.log("##########editing order going in#############");
            $log.log(rowItem.entity);
            /*if( $scope.confirmingOrder == rowItem.entity){

             }else {
             crudService.setEditingOrder(rowItem.entity);
             $location.path('/editOrder');
             }*/
        }
    };

    $scope.invoiceProductsGridData = [];

    $scope.inProductionGrid = {
        data: 'inProductionGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'amount', displayName: 'amount', minWidth: 100, width: 100, maxWidth: 350},
            {field: 'product', displayName: 'product', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            $log.log("##########editing order going in#############");
            $log.log(rowItem.entity);
            /*if( $scope.confirmingOrder == rowItem.entity){

             }else {
             crudService.setEditingOrder(rowItem.entity);
             $location.path('/editOrder');
             }*/
        }
    };

    $scope.inProductionGridData = [];

    $scope.paymentsGrid = {
        data: 'paymentsGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 105,
            cellTemplate: '<input type="button" value="delete payment" class="ngCellText" ng-click="deletePayment(row.entity)">',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },
            {field: 'amount', displayName: 'amount', minWidth: 100, width: 100, maxWidth: 350},
            {field: 'createdBy', displayName: 'booked by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'creationDate', displayName: 'booked on', minWidth: 140, width: 140, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'lastModifiedDate', displayName: 'modified on', minWidth: 170, width: 170, maxWidth: 170, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}  {{row.entity.lastModifiedDate.hour}}:{{row.entity.lastModifiedDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'modified by', minWidth: 120, width: 120, maxWidth: 120}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            $log.log("##########editing order going in#############");
            $log.log(rowItem.entity);
            /*if( $scope.confirmingOrder == rowItem.entity){
             deletePayment()
             }else {
             crudService.setEditingOrder(rowItem.entity);
             $location.path('/editOrder');
             }*/
        }
    };

    $scope.paymentsGridData = [];

    $scope.packagesGrid = {
        data: 'packagesGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 113,
            cellTemplate: '<input type="button" value="delete package" class="ngCellText" ng-click="deletePackingSlip(row.entity)">',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },{
            field: '',
            displayName: "",
            width: 120,
            cellTemplate: '<div ng-show="!row.entity.packingSlip.ready" style="background-color: #ff0000"><img style="height:28px;" src="assets/img/package.png" ng-click="packagedPacket(row.entity)"></div><div ng-show="row.entity.packingSlip.ready" style="background-color: #00ff00"><img style="height:28px;" src="assets/img/package.png">{{row.entity.packingSlip.packedDate.dayOfMonth}} {{row.entity.packingSlip.packedDate.month}} {{row.entity.packingSlip.packedDate.year}}</div>',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },{
            field: '',
            displayName: "",
            width: 120,
            cellTemplate: '<div ng-show="row.entity.packingSlip.shippedDate == null" style="background-color: #ff0000"><img style="height:28px;" src="assets/img/ship.png" ng-click="shippedPacket(row.entity)"></div><div ng-show="row.entity.packingSlip.shippedDate != null" style="background-color: #00ff00"><img style="height:28px;" src="assets/img/ship.png">{{row.entity.packingSlip.shippedDate.dayOfMonth}} {{row.entity.packingSlip.shippedDate.month}} {{row.entity.packingSlip.shippedDate.year}}</div>',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },
            {field: 'packingSlip.creationDate', displayName: 'created', minWidth: 100, width: 100, maxWidth: 350, cellTemplate:"<div class='ngCellText'>{{row.entity.packingSlip.creationDate.dayOfMonth}} {{row.entity.packingSlip.creationDate.month}} {{row.entity.packingSlip.creationDate.year}}</div>"},
            {field: 'packingSlip.createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'productsOnSlip', displayName: 'total glasses', minWidth: 140, width: 140},
            {field: 'packingSlip.packedBy', displayName: 'packed by', minWidth: 120, width: 120, maxWidth: 350, cellTemplate: '<div class="ngCellText" ng-show="row.entity.packingSlip.shippedDate != null">{{row.entity.packingSlip.packedBy}}</div>'},
            {field: 'packingSlip.shippedBy', displayName: 'shipped by', minWidth: 120, width: 120, maxWidth: 350, cellTemplate: '<div class="ngCellText" ng-show="row.entity.packingSlip.shippedDate != null">{{row.entity.packingSlip.shippedBy}}</div>'},
            {field: 'products', displayName: 'products', minWidth: 180, width: 180, maxWidth: 180}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
        }
    };

    $scope.packagesGridData = [];

    //package creation
    $scope.leftForPackingGrid = {
        data: 'leftForPackingGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'product.productCode', displayName: 'product', minWidth: 100, width: 100, maxWidth: 350},
            {field: 'amount', displayName: 'amount', minWidth: 100, width: 100, maxWidth: 350}],
        selectedItems: $scope.newPackageLine.selectedProduct,
        multiSelect: false,
        afterSelectionChange: function(rowItem){
            $log.log("##########selected item to package#############");
            $log.log(rowItem.entity);
        }
    };

    $scope.leftForPackingGridData = [];

    $scope.newPackageGrid = {
        data: 'newPackageGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'productCode', displayName: 'product', minWidth: 100, width: 100, maxWidth: 350},
            {field: 'amount', displayName: 'amount', minWidth: 100, width: 100, maxWidth: 350}],
        multiSelect: false
    };

    $scope.newPackageGridData = [];

    $scope.addPackage = function(){
        $log.log("create new package");
        $log.log($scope.invoice.order);
        $scope.isCreatingNewPackage = true;
        var productsLeftToPack = [];
        $log.log($scope.leftForPackingGridData);
        $log.log(productsLeftToPack);
        for(var i = 0; i < $scope.invoice.order.length; i++){
            if(isAlreadyInProductsLeftToPackageList(productsLeftToPack, $scope.invoice.order[i])){
                for(var j = 0; j < productsLeftToPack.length; j++){
                    if(productsLeftToPack[j].product.id == $scope.invoice.order[i].product.id){
                        productsLeftToPack[j].amount = productsLeftToPack[j].amount + $scope.invoice.order[i].amount;
                    }
                }
            }else{
                productsLeftToPack.push(angular.copy($scope.invoice.order[i]));
            }
        }

        $scope.leftForPackingGridData = productsLeftToPack;

        for(var i = 0; i < $scope.leftForPackingGridData.length; i++){
            $scope.leftForPackingGridData[i].amount = Number($scope.leftForPackingGridData[i].amount) - Number(amountAlreadyPackaged($scope.leftForPackingGridData[i].product.id));
        }
    };

    function isAlreadyInProductsLeftToPackageList(productsLeftToPackList, product){
        for(var i = 0; i < productsLeftToPackList.length; i++){
            if(productsLeftToPackList[i].product.id == product.product.id){
                return true;
            }
        }
        return false;
    }

    function amountAlreadyPackaged(productId){
        var amountInUse = 0;
        for(var i = 0; i < $scope.invoice.packingSlip.length; i++){
            for(var j = 0; j < $scope.invoice.packingSlip[i].stockmove.stockmoveItems.length; j++){
                if($scope.invoice.packingSlip[i].stockmove.stockmoveItems[j].productId == productId){
                    amountInUse = Number(amountInUse) + Number($scope.invoice.packingSlip[i].stockmove.stockmoveItems[j].amount);
                }
            }
         }
        return amountInUse;
    }

    $scope.addToPackage = function(){
        $scope.NoProductSelectedError = false;
        $scope.AmountFormatError = false;
        $scope.NotThisManyAmountLeftError = false;
        $log.log("adding to package");
        $log.log($scope.newPackageLine);
        if($scope.newPackageLine.selectedProduct[0] == null){
            $scope.NoProductSelectedError = true;
            return;
        } else if(Object.keys($scope.newPackageLine.selectedProduct[0]).length === 0){
            $scope.NoProductSelectedError = true;
            return;
        } else if(isNaN($scope.newPackageLine.amount) || Number($scope.newPackageLine.amount) <= 0){
            $scope.AmountFormatError = true;
            return;
        } else if(Number($scope.newPackageLine.amount) > Number($scope.newPackageLine.selectedProduct[0].amount)){
            $scope.NotThisManyAmountLeftError = true;
            return;
        }else {

            for(var i = 0; i < $scope.leftForPackingGridData.length; i++){
                if($scope.leftForPackingGridData[i].product.id == $scope.newPackageLine.selectedProduct[0].product.id){
                    $scope.leftForPackingGridData[i].amount = Number($scope.leftForPackingGridData[i].amount) - Number($scope.newPackageLine.amount);
                }
            }
            if(isProductAlreadyInPackage()){
                for(var i = 0; i < $scope.newPackageGridData.length; i++){
                    if($scope.newPackageGridData[i].productid == $scope.newPackageLine.selectedProduct[0].product.id){
                        $scope.newPackageGridData[i].amount = Number($scope.newPackageGridData[i].amount) + Number($scope.newPackageLine.amount);
                    }
                }
            }else {
                $scope.newPackageGridData.push({"productid": $scope.newPackageLine.selectedProduct[0].product.id, "productCode": $scope.newPackageLine.selectedProduct[0].product.productCode,"amount": $scope.newPackageLine.amount});
            }
        }
    };

    function isProductAlreadyInPackage(){
        for(var i = 0; i < $scope.newPackageGridData.length; i++){
            if($scope.newPackageGridData[i].productid == $scope.newPackageLine.selectedProduct[0].product.id){
                return true;
            }
        }
        return false;
    }

    $scope.savePackage = function(){
        httpServiceFactory.createPostPromise('/invoices/packingslips/add', 'userid=' + applicationInformation.getUserinformation().userId + "&inoviceid=" + $scope.invoice.id, createNewPackage(), true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                $log.log(status);
                notificationService.showMessageNotification("package saved.");
                //$location.path('/offerInvoices');
                $scope.cancelPackage();
                refresh($scope.invoice.id);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + error);
                $scope.cancelPackage();
            });
    };

    $scope.cancelPackage = function(){
        $scope.isCreatingNewPackage = false;
        $scope.selectedProduct = [];
        $scope.newPackageLine.amount = "";
        $scope.leftForPackingGrid.selectedItems = [];
        $scope.leftForPackingGridData = [];
        $scope.newPackageGridData = [];
    };

    function createNewPackage(){
        var packageSlip = {"packingSlipLineList": []};
        for(var i = 0; i < $scope.newPackageGridData.length; i++){
            packageSlip.packingSlipLineList.push({"productid": $scope.newPackageGridData[i].productid, "amount": $scope.newPackageGridData[i].amount});
        }
        return packageSlip;
    }

    $scope.packagedPacket = function(packageItem){
        if(confirm("Are you sure you have packed the package?")){
            httpServiceFactory.createPostPromise('/invoices/packingslips/ready', 'userid=' + applicationInformation.getUserinformation().userId + "&packingslipid=" + packageItem.packingSlip.id, '', true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Package packed.");
                    refresh($scope.invoice.id);
                })
                .error(function (error, status) {
                    $log.log("invoice detail(package packed): error:" + error + " status:" + status);
                    notificationService.showErrorNotification("Something went wrong when packing your packet.");
                });
        }
    };

    $scope.shippedPacket =function(packageItem){
        if($scope.invoiceInformation.totalPayed < $scope.invoiceInformation.totalUpfront){
            notificationService.showWarningNotification("The upfront payment has not been fully paid. You can not send the packages yet..");
        }else if(confirm("Are you sure you have shipped the package?")){
            httpServiceFactory.createPostPromise('/invoices/packingslips/ship', 'userid=' + applicationInformation.getUserinformation().userId + "&packingslipid=" + packageItem.packingSlip.id, '', true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Package shipped.");
                    refresh($scope.invoice.id);
                })
                .error(function (error, status) {
                    $log.log("invoice detail(shipping packet): error:" + error + " status:" + status);
                    notificationService.showErrorNotification("Something went wrong when shipping your packet.");
                });
        }
    };

    $scope.deletePackingSlip = function(packageSlip){
        if(packageSlip.packingSlip.shippedDate == null){
            httpServiceFactory.createPostPromise('/invoices/packingslips/delete', 'userid=' + applicationInformation.getUserinformation().userId + "&invoceid=" + $scope.invoice.id + "&packingslipid=" + packageSlip.packingSlip.id, createNewPackage(), true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Package deleted.");
                    $scope.cancelPackage();
                    refresh($scope.invoice.id);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + error);
                    $scope.cancelPackage();
                });
        }else{
            notificationService.showWarningNotification("You can not delete a package that has been shipped.");
        }
    };
    //end package creation

    function refresh(invoiceId){
        httpServiceFactory.createGetPromise('/invoices/byid', 'userid=' + applicationInformation.getUserinformation().userId + "&invoiceid=" + invoiceId, '', true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.invoice = data;
                init();
            })
            .error(function (error, status) {
                $log.log("invoice detail(refreshing invoice): error:" + error + " status:" + status);
                notificationService.showErrorNotification("Something went wrong when refreshing your invoice.");
                $location.path('/offerInvoices');
            });
    }

    $scope.save = function(){
        if($scope.editingPayment){
            savePayment();
        } else if($scope.isAddingPayment){
            savePayment()
        }else{
            return;
        }
    };

    function savePayment(){
        if($scope.isAddingPayment){
            if($scope.invoiceInformation.paymentLeft < Number($scope.addingPayment.amount)){
                notificationService.showWarningNotification("The unpaid amount is smaller then the amount you have put in.");
                $scope.addingPayment.amount = $scope.invoiceInformation.paymentLeft;
            }else {
                httpServiceFactory.createPostPromise('/invoices/payments/add', 'userid=' + applicationInformation.getUserinformation().userId + "&inoviceid=" + $scope.invoice.id + "&amount=" + Number($scope.addingPayment.amount), '', true, applicationInformation.getToken(), true)
                    .success(function (data, status, headers, config) {
                        notificationService.showMessageNotification("Your payment is saved.");
                        $scope.isAddingPayment = false;
                        refresh($scope.invoice.id);
                    })
                    .error(function (error, status) {
                        $log.log("invoice detail(add payment): error:" + error + " status:" + status);
                        notificationService.showErrorNotification("Something went wrong when adding your payment.");
                        $scope.isAddingPayment = false;
                        $location.path('/offerInvoices');
                    });
            }
        }else if($scope.isEditingPayment){
            httpServiceFactory.createPostPromise('/invoices/payments/edit', 'userid=' + applicationInformation.getUserinformation().userId + "&paymentid=" + $scope.invoice.id + "&amount=" + Number($scope.editingPayment.amount), '', true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Your payment is saved.");
                    $scope.isEditingPayment = false;
                    refresh($scope.invoice.id);
                })
                .error(function (error, status) {
                    $log.log("invoice detail(edit payment): error:" + error + " status:" + status);
                    notificationService.showErrorNotification("Something went wrong when editing your payment.");
                    $scope.isEditingPayment = false;
                    $location.path('/offerInvoices');
                });
        }else{}
    }

    $scope.deletePayment = function(payment){
        if(confirm("Are you sure you want to delete this payment?")){
            httpServiceFactory.createPostPromise('/invoices/payments/delete', 'userid=' + applicationInformation.getUserinformation().userId + "&inoviceid=" + $scope.invoice.id + "&paymentid=" + payment.id, '', true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Payment deleted.");
                    refresh($scope.invoice.id);
                })
                .error(function (error, status) {
                    $log.log("invoice detail(delete payment): error:" + error + " status:" + status);
                    notificationService.showErrorNotification("Something went wrong when deleting your payment.");
                });
        }
    };

    $scope.cancel = function(){
        if($scope.isEditingPayment){
            $scope.isEditingPayment = false;
            $scope.editingPayment = {"amount": 0};
        } else if($scope.isAddingPayment){
            $scope.isAddingPayment = false;
            $scope.addingPayment = {"amount": 0};
        }else {
            $location.path("/offerInvoices");
        }
    };

    $scope.addPayment = function(){
        if($scope.invoiceInformation.paymentLeft <= 0){
            notificationService.showWarningNotification("The invoice has been fully paid.");
        }else{
            $scope.isAddingPayment = true;
            $scope.addingPayment = {"amount": $scope.invoiceInformation.paymentLeft};
        }
    };

    $scope.editPayment = function(){
        $scope.isEditingPayment = true;
        $scope.editingPayment = {"amount": 0};
    };

    $scope.showItemsInProduction = function(){
        $scope.isShowingInproductionItems = true;
    };
    $scope.showInvoiceOverview = function(){
        $scope.isShowingInproductionItems = false;
    };

    function init() {
        $log.log("the invoice");
        $log.log($scope.invoice);
        $scope.invoiceProductsGridData = $scope.invoice.order;
        if($scope.invoice.inProduction) {
            $scope.inProductionGridData = $scope.invoice.needed.stockmoveItems;
        }
        $scope.paymentsGridData = $scope.invoice.payment;
        $scope.packagesGridData = [];
        for(var i = 0; i < $scope.invoice.packingSlip.length; i++){
            var productsInPackingSlip = "";
            var productOnPackingSlip = 0;
            for(var j = 0; j < $scope.invoice.packingSlip[i].stockmove.stockmoveItems.length; j++){
                $log.log($scope.invoice.packingSlip[i].stockmove.stockmoveItems[j].product);
                productsInPackingSlip = productsInPackingSlip.concat(angular.copy($scope.invoice.packingSlip[i].stockmove.stockmoveItems[j].product)).concat(",");
                productOnPackingSlip = Number(productOnPackingSlip) + Number($scope.invoice.packingSlip[i].stockmove.stockmoveItems[j].amount);
            }
            $scope.packagesGridData.push({"packingSlip": $scope.invoice.packingSlip[i], "products": productsInPackingSlip, "productsOnSlip": productOnPackingSlip});
        }
        $scope.invoiceInformation = {"totalPayed": 0, "paymentLeft":0, "totalUpfront": 0, "itemsLeftToPack": 0};
        for(var i = 0; i < $scope.invoice.payment.length; i++) {
            $scope.invoiceInformation.totalPayed = $scope.invoiceInformation.totalPayed + Number($scope.invoice.payment[i].amount);
        }//itemsLeftToPack
        $scope.invoiceInformation.paymentLeft = $scope.invoice.localPrice - $scope.invoiceInformation.totalPayed;
        $scope.invoiceInformation.totalUpfront = ($scope.invoice.localPrice * ($scope.invoice.upfrontpercentage/100));
        var itemsPacked = 0;
        for(var i = 0; i < $scope.invoice.packingSlip.length; i++){
            for(var j = 0; j < $scope.invoice.packingSlip[i].stockmove.stockmoveItems.length; j++){
                itemsPacked = itemsPacked + Number($scope.invoice.packingSlip[i].stockmove.stockmoveItems[j].amount);
            }
        }
        var totalItems = 0;
        for(var i = 0; i < $scope.invoice.order.length; i++){
            totalItems = totalItems + $scope.invoice.order[i].amount;
        }
        $scope.invoiceInformation.itemsLeftToPack = totalItems - itemsPacked;
    }
    init();
}]);