/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var invoiceOverviewController = angular.module('clarity.overview');

invoiceOverviewController.controller('invoiceOverviewController', ['$scope', '$log', 'notificationService', 'overviewType', 'crudService', '$location', 'httpServiceFactory', 'applicationInformation', 'invoiceShoppingCart', function($scope, $log, notificationService, overviewType, crudService, $location, httpServiceFactory, applicationInformation, invoiceShoppingCart){
    $scope.overviewType = overviewType;

    //detail
    $scope.showingSummary = true;
    $scope.invoiceDetail = {};


    $scope.InvalidDateFormatError = false;
    $scope.WrongDateError = false;
    $scope.NoStockSelectedError = false;
    $scope.NoProcessingItemSelectedError = false;
    $scope.IncorrectProcessingItemAmountError = false;
    $scope.NoQualitySelectedError = false;


    $scope.offerGrid = {
        data: 'offerGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false, enableColumnResize: false
        },{
            field: '',
            displayName: "",
            minWidth: 30,
            width: 30,
            maxWidth: 30,
            cellTemplate: '<img style="height:28px;" src="assets/img/workingIcon.png" ng-click="startEditInvoice(row.entity)">',
            sortable: false,
            enableColumnResize: false
        },{
            field: '',
            displayName: "",
            minWidth: 30,
            width: 30,
            maxWidth: 30,
            cellTemplate: '<img style="height:28px;" src="assets/img/editIcon.png" ng-click="loadInvoice(row.entity)">',
            sortable: false,
            enableColumnResize: false
        },{
            field: '',
            displayName: "",
            minWidth: 94,
            width: 94,
            maxWidth: 94,
            cellTemplate: '<input type="button" value="confirm offer" class="ngCellText" ng-click="startConfirmingOffer(row.entity)">',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false, enableColumnResize: false
        },{
            field: '',
            displayName: "",
            minWidth: 89,
            width: 89,
            maxWidth: 89,
            cellTemplate: '<input type="button" value="cancel offer" class="ngCellText" ng-click="startCancelInvoice(row.entity)">',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false, enableColumnResize: false
        },
            {field: 'customer', displayName: 'customer', minWidth: 100, width: 100, maxWidth: 350},
            {field: 'localPrice', displayName: 'price', minWidth: 120, width: 120, maxWidth: 350, cellTemplate: '<div class="ngCellText">{{row.entity.localPrice.toFixed(2)}}</div>'},
            {field: 'discountpercentage', displayName: 'discount', minWidth: 90, width: 90},
            {field: 'sendDate', displayName: 'due date', width: 115, cellTemplate:"<div class='ngCellText'>{{row.entity.sendDate.dayOfMonth}} {{row.entity.sendDate.month}} {{row.entity.sendDate.year}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 180, width: 180, maxWidth: 180},
            {field: 'creationDate', displayName: 'created on', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            if($scope.showingSummary){
                openInvoiceSummary(rowItem.entity);
            }
        }
    };

    $scope.offerGridData = [];

    $scope.confirmedGrid = {
        data: 'confirmedGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false, enableColumnResize: false
        },{
            field: '',
            displayName: "",
            minWidth: 30,
            width: 30,
            maxWidth: 30,
            cellTemplate: '<img style="height:28px;" src="assets/img/workingIcon.png" ng-click="startEditInvoice(row.entity)">',
            sortable: false,
            enableColumnResize: false
        },{
            field: '',
            displayName: "",
            minWidth: 30,
            width: 30,
            maxWidth: 30,
            cellTemplate: '<img style="height:28px;" src="assets/img/editIcon.png" ng-click="loadInvoice(row.entity)">',
            sortable: false,
            enableColumnResize: false
        },{
            field: '',
            displayName: "",
            minWidth: 120,
            width: 120,
            maxWidth: 120,
            cellTemplate: '<input type="button" value="put in production" class="ngCellText" ng-click="startProductionOnConfirmation(row.entity)">',
            sortable: false, pinned: true, pinnable: false, enableColumnResize: false
        },{
            field: '',
            displayName: "",
            minWidth: 138,
            width: 138,
            maxWidth: 138,
            cellTemplate: '<input type="button" value="cancel confirmation" class="ngCellText" ng-click="startCancelInvoice(row.entity)">',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false, enableColumnResize: false
        },
            {field: 'customer', displayName: 'customer', minWidth: 100, width: 100, maxWidth: 350},
            {field: 'localPrice', displayName: 'price', minWidth: 120, width: 120, maxWidth: 350, cellTemplate: '<div class="ngCellText">{{row.entity.localPrice.toFixed(2)}}</div>'},
            {field: 'discountpercentage', displayName: 'discount', minWidth: 90, width: 90},
            {field: 'sendDate', displayName: 'due date', width: 115, cellTemplate:"<div class='ngCellText'>{{row.entity.sendDate.dayOfMonth}} {{row.entity.sendDate.month}} {{row.entity.sendDate.year}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 180, width: 180, maxWidth: 180},
            {field: 'creationDate', displayName: 'created on', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            if($scope.showingSummary){
                openInvoiceSummary(rowItem.entity);
            }
        }
    };

    $scope.confirmedGridData = [];

    $scope.inProductionGrid = {
        data: 'inProductionGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div ng-show="row.entity.ready" style="background-color: #00ff00" class="ngCellText">{{row.rowIndex + 1}}</div><div ng-hide="ready" class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            minWidth: 30,
            width: 30,
            maxWidth: 30,
            cellTemplate: '<img style="height:28px;" src="assets/img/editIcon.png" ng-click="loadInvoice(row.entity)">',
            sortable: false,
            enableColumnResize: false
        },{
            field: '',
            displayName: "",
            minWidth: 93,
            width: 93,
            maxWidth: 93,
            cellTemplate: '<input type="button" value="send invoice" class="ngCellText" ng-click="startSending(row.entity)">',
            sortable: false
        },{
            field: '',
            displayName: "",
            minWidth: 142,
            width: 142,
            maxWidth: 142,
            cellTemplate: '<input type="button" style="background-color: #ff9900" value="cancel in production" class="ngCellText" ng-click="startCancelInvoice(row.entity)">',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },
            {field: 'customer', displayName: 'customer', width: 100},
            {field: 'localPrice', displayName: 'price', width: 120, cellTemplate: '<div class="ngCellText">{{row.entity.localPrice.toFixed(2)}}</div>'},
            {field: 'note.content', displayName: 'note', width: 140},
            {field: 'sendDate', displayName: 'due date', width: 170, cellTemplate:"<div class='ngCellText'>{{row.entity.sendDate.dayOfMonth}} {{row.entity.sendDate.month}} {{row.entity.sendDate.year}}</div>"},
            {field: 'paymentDueDate', displayName: 'payment due date', width: 120, cellTemplate:"<div class='ngCellText'>{{row.entity.paymentDueDate.dayOfMonth}} {{row.entity.paymentDueDate.month}} {{row.entity.paymentDueDate.year}}</div>"},
            {field: 'shippingMethod', displayName: 'shipping methode', width: 180},
            {field: 'creationDate', displayName: 'creation date', width: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"}],
        selectedItems: $scope.selectedProcessingDeliveryItem,
        multiSelect: false,
        afterSelectionChange: function(rowItem){
            if($scope.showingSummary){
                openInvoiceSummary(rowItem.entity);
            } else {
                $scope.showingSummary = true;
            }
        }
    };

    $scope.inProductionGridData = [];

    $scope.sendGrid = {
        data: 'sendGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'invoicenr', displayName: 'invoice Nr.', minWidth: 120, width: 120, maxWidth: 350},
            {
            field: '',
            displayName: "",
            minWidth: 76,
            width: 76,
            maxWidth: 76,
            cellTemplate: '<input type="button" value="Complete" class="ngCellText" ng-click="startInvoiceCompleting(row.entity)">',
            sortable: false,
        },
            {field: 'sentDate', displayName: 'sent date', width: 100, cellTemplate:"<div class='ngCellText'>{{row.entity.sentDate.dayOfMonth}} {{row.entity.sentDate.month}} {{row.entity.sentDate.year}}</div>"},
            {field: 'sentBy', displayName: 'sent by',  width: 120},
            {field: '', displayName: 'payment status', width: 140, cellTemplate: '<div ng-show="row.entity.ready" style="background-color: #00ff00" class="ngCellText">COMPLETE</div><div ng-show="row.entity.overDue" style="background-color: #ff9900" class="ngCellText">OVERDUE</div><div ng-show="row.entity.pastTwoWeeks" style="background-color: #ff0000" class="ngCellText">TO LATE</div>'},
            {field: 'customer', displayName: 'customer', width: 170, maxWidth: 170},
            {field: 'paymentDueDate', displayName: 'payment due date', width: 150, cellTemplate:"<div class='ngCellText'>{{row.entity.paymentDueDate.dayOfMonth}} {{row.entity.paymentDueDate.month}} {{row.entity.paymentDueDate.year}}</div>"}],
        selectedItems: $scope.selectedProcessingDeliveryItem,
        multiSelect: false,
        afterSelectionChange: function(rowItem){
            if($scope.showingSummary){
                openInvoiceSummary(rowItem.entity);
            } else {
                $scope.showingSummary = true;
            }
        }
    };

    $scope.sendGridData = [];

    $scope.completedGrid = {
        data: 'completedGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'customer', displayName: 'customer', width: 120},
            {field: 'creationDate', displayName: 'created', width: 120, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"},
            {field: 'localPrice', displayName: 'price', width: 120},
            {field: 'discountpercentage', displayName: 'discount %', width: 120},
            {field: 'upfrontpercentage', displayName: 'upfront %', width: 120},
            {field: 'totalCost', displayName: 'total amount', width: 120},
            {field: 'completionDate', displayName: 'completed', width: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.completionDate.dayOfMonth}} {{row.entity.completionDate.month}} {{row.entity.completionDate.year}}</div>"},
            {field: 'sendDate', displayName: 'due date', width: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.sendDate.dayOfMonth}} {{row.entity.sendDate.month}} {{row.entity.sendDate.year}}</div>"}],
        selectedItems: $scope.selectedProcessingDeliveryItem,
        multiSelect: false,
        afterSelectionChange: function(rowItem){
            openInvoiceSummary(rowItem.entity);
        }
    };

    $scope.completedGridData = [];


    //offers
    $scope.startConfirmingOffer = function(offer){
        $scope.showingSummary = false;
        httpServiceFactory.createPostPromise('/invoices/confirm', 'userid=' + applicationInformation.getUserinformation().userId + "&invoiceid=" + offer.id, '', true, applicationInformation.getToken(), false)
            .success(function (data, status, headers, config) {
                notificationService.showMessageNotification("Your order is confirmed.");
                $location.path("/confirmedInvoices");
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get your offer confirmed.");
            });
    };
    //end offers

    //confirmations
    $scope.startProductionOnConfirmation = function(confirmation){
        if (confirm("Are you sure you want to put this invoice in production? When you want to cancel an invoice after this. You will mess with the work queue and worksheets.")) {
            $scope.showingSummary = false;
            httpServiceFactory.createPostPromise('/invoices/produce', 'userid=' + applicationInformation.getUserinformation().userId + "&invoiceid=" + confirmation.id, '', true, applicationInformation.getToken(), false)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Your confirmation is in production.");
                    $location.path("/inProductionInvoices");
                })
                .error(function (error, status) {
                    notificationService.showErrorNotification("Oops... Could not get your confirmation into production.");
                });
        }
    };
    //end confirmations

    //in production
    $scope.startSending = function(inProductionInvoice){
        $scope.showingSummary = false;
        if(inProductionInvoice.ready) {
            if (confirm("Are you sure you want to send the invoice? After this you can not cancel an invoice anymore.")) {
                httpServiceFactory.createPostPromise('/invoices/send', 'userid=' + applicationInformation.getUserinformation().userId + "&invoiceid=" + inProductionInvoice.id, '', true, applicationInformation.getToken(), false)
                    .success(function (data, status, headers, config) {
                        notificationService.showMessageNotification("Your in production invoice is sent.");
                        $location.path("/sendInvoices");
                    })
                    .error(function (error, status) {
                        notificationService.showErrorNotification("Oops... Could not get your invoice sent.");
                    });
            }
        } else {
            notificationService.showWarningNotification("Not everything is payed, produced or shipped.");
        }
    };
    //end in production

    //send
    $scope.startInvoiceCompleting = function(sendInvoice){
        $scope.showingSummary = false;
        httpServiceFactory.createPostPromise('/invoices/complete', 'userid=' + applicationInformation.getUserinformation().userId + "&inoviceid=" + sendInvoice.id, '', true, applicationInformation.getToken(), false)
            .success(function (data, status, headers, config) {
                notificationService.showMessageNotification("Your sent invoice is completed.");
                $location.path("/completedInvoices");
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get your invoice completed.");
            });
    };
    //end send



    $scope.startEditInvoice = function(editingInvoice){
        $scope.showingSummary = false;
        $log.log("editingin invoice");
        $log.log(editingInvoice);
        invoiceShoppingCart.setEditingInvoice(editingInvoice);
        var dueDate = editingInvoice.sendDate.monthValue + "-" + editingInvoice.sendDate.dayOfMonth + "-" + editingInvoice.sendDate.year + " " + editingInvoice.sendDate.hour + ":" + editingInvoice.sendDate.minute + ":" + editingInvoice.sendDate.second;
        var paymentDate = editingInvoice.paymentDueDate.monthValue + "-" + editingInvoice.paymentDueDate.dayOfMonth + "-" + editingInvoice.paymentDueDate.year + " " + editingInvoice.paymentDueDate.hour + ":" + editingInvoice.paymentDueDate.minute + ":" + editingInvoice.paymentDueDate.second;
        var invoiceInformation = {"customerId": editingInvoice.customerId, "dueDate": dueDate, "paymentDueDate": paymentDate, "priceWithoutVAT": Number(editingInvoice.priceWithoutVAT), "localPrice": Number(editingInvoice.localPrice), "ValutaFactor": Number(editingInvoice.valutaFactor), "price": Number(editingInvoice.price),
            "VATpercentage": Number(editingInvoice.vatpercentage), "VAT": Number(editingInvoice.vat), "discountpercentage": Number(editingInvoice.discountpercentage), "discount": Number(editingInvoice.discount), "packingCostPercentage": Number(editingInvoice.packingCostPercentage), "packingCost": Number(editingInvoice.packingCost),
            "upfrontpercentage": Number(editingInvoice.upfrontpercentage), "valuta": editingInvoice.valuta, "shippingCost": Number(editingInvoice.shippingCost), "shippingMethod": editingInvoice.shippingMethod, "note": editingInvoice.note.content, "invoiceCreateLines": []};
        invoiceShoppingCart.saveInvoiceInformation(invoiceInformation);
        $log.log(invoiceInformation);
        var invoiceLines = [];
        for(var i = 0; i < editingInvoice.order.length; i++){
            invoiceLines.push({"finishedProduct": editingInvoice.order[i].product, "quantity": Number(editingInvoice.order[i].amount), "inlineDiscount": Number(editingInvoice.order[i].inlineDiscountPercentage)});
        }
        invoiceShoppingCart.saveInvoiceLines(invoiceLines);
        invoiceShoppingCart.setInvoiceId(editingInvoice.id);
        invoiceShoppingCart.setInvoiceNr(editingInvoice.invoicenumber);
        $location.path("/editInvoice");
        //$scope.showingSummary = true;
    };

    $scope.startSendInvoice = function(sendingInvoice){
        $scope.showingSummary = false;
        if (confirm("Are you sure you want to send the invoice?")) {
            httpServiceFactory.createPostPromise('/invoices/send', 'userid=' + applicationInformation.getUserinformation().userId + "&invoiceid=" + sendingInvoice.id, '', true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Your invoice is send.");
                    $scope.showingSummary = true;
                    $scope.openSendInvoice();
                })
                .error(function (error, status) {
                    $log.log("error:" + error + " status: " +status);
                    notificationService.showErrorNotification(status + ": oops... Could not send your invoice.");
                    $scope.showingSummary = true;
                });
        } else {
            return;
        }
    };

    $scope.startCancelInvoice = function(cancelingInvoice){
        $scope.showingSummary = false;
        if($scope.overviewType == "offers" || $scope.overviewType == "confirmed") {
            if (confirm("Are you sure you want to cancel the invoice?")) {
                httpServiceFactory.createPostPromise('/invoices/cancel', 'userid=' + applicationInformation.getUserinformation().userId + "&invoiceid=" + cancelingInvoice.id, '', true, applicationInformation.getToken(), true)
                    .success(function (data, status, headers, config) {
                        notificationService.showMessageNotification("Your invoice is canceled.");
                        $scope.showingSummary = true;
                        init();
                    })
                    .error(function (error, status) {
                        notificationService.showErrorNotification(status + ": oops... Could not cancel your invoice.");
                        $scope.showingSummary = true;
                        init();
                    });
            } else {
                return;
            }
        } else if($scope.overviewType == "inProduction"){
            if (confirm("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Are you sure you want to cancel the invoice?!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Deleting an invoice when it is in the process state will create problems in the production queue and on the worksheets. Please refresh and recheck your worksheets.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")) {
                httpServiceFactory.createPostPromise('/invoices/cancel', 'userid=' + applicationInformation.getUserinformation().userId + "&invoiceid=" + cancelingInvoice.id, '', true, applicationInformation.getToken(), true)
                    .success(function (data, status, headers, config) {
                        notificationService.showMessageNotification("Your invoice is canceled.");
                        $scope.showingSummary = true;
                        init();
                    })
                    .error(function (error, status) {
                        notificationService.showErrorNotification("Oops... Could not cancel your invoice.");
                        $scope.showingSummary = true;
                        init();
                    });
            } else {
                return;
            }
        }
    };

    function loadOffersInvoices(){
        httpServiceFactory.createGetPromise('/invoices/offers', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("---------offer invoices------------");
                $scope.offerGridData = data;
                $log.log($scope.offerGridData);
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get not send invoices from server.");
            });
    }

    function loadConfirmedInvoices(){
        httpServiceFactory.createGetPromise('/invoices/confirmed', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("---------confirmed invoices------------");
                $scope.confirmedGridData = data;
                $log.log($scope.confirmedGridData);
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get not send invoices from server.");
            });
    }

    function loadInProductionInvoices(){
        httpServiceFactory.createGetPromise('/invoices/inprodcution', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("---------in production invoices------------");
                $scope.inProductionGridData = data;
                $log.log($scope.inProductionGridData);
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get not send invoices from server.");
            });
    }

    function loadSendInvoices(){
        httpServiceFactory.createGetPromise('/invoices/sent', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("---------send invoices------------");
                $scope.sendGridData = data;
                $log.log($scope.sendGridData);
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get suppliers from server.");
            });
    }

    function loadCompletedInvoices(){
        httpServiceFactory.createGetPromise('/invoices/completed', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("---------completed invoice------------");
                $scope.completedGridData = data;
                $log.log($scope.completedGridData);
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get the processed deliveries from the server.");
            });
    }

    $scope.loadInvoice = function(detailingInvoice){
        $scope.showingSummary = false;
        crudService.setDetailInvoice(detailingInvoice);
        $location.path("/invoiceDetail");
    };

    $scope.openOfferInvoice = function(){
        if($scope.overviewType != "offers"){
            $scope.overviewType = "offers";
            init();
        }
    };
    $scope.openConfirmedInvoice = function(){
        if($scope.overviewType != "confirmed") {
            $scope.overviewType = "confirmed";
            init();
        }
    };
    $scope.openInProductionInvoice = function(){
        if($scope.overviewType != "inProduction"){
            $scope.overviewType = "inProduction";
            init();
        }
    };
    $scope.openSendInvoice = function(){
        if($scope.overviewType != "sent"){
            $scope.overviewType = "sent";
            init();
        }
    };
    $scope.openCompletedInvoice = function(){
        if($scope.overviewType != "completed"){
            $scope.overviewType = "completed";
            init();
        }
    };
    function openInvoiceSummary(detailingInvoice){
        crudService.setDetailInvoice(detailingInvoice);
        $location.path("/invoiceSummary");
    }

    $scope.cancel = function(){
        /*if(overviewType == "new") {
            $scope.confirmationDate = {"year": new Date().getFullYear(), "month": new Date().getMonth() + 1, "day": 1};
            $scope.isInConfirmationProcess = false;
            $scope.confirmingOrder = {};
            $scope.WrongDateError = false;
        }else if(overviewType == "confirmed"){
            $scope.amount = "";
            $scope.isInDeliveryProcess = false;
            $scope.deliveryReadyToCommit = false;
            $scope.orderInDeliveryProcess = {};
            $scope.deliveryProcessInformation = {};
            $scope.processedDeliveredItemsGridData = [];
            $scope.unProcessedDeliveredItemsGridData = [];
            $scope.selectedQuality = null;
            $scope.selectedStock = null;
            $scope.remainingDeliveredMaterialsToQualityCheck = [];
            $scope.selectedProcessingDeliveryItem = [];
            $scope.selectedProcessingDeliveryItemAvailibleStocks = [];
        }*/
    };

    function init() {
        if($scope.overviewType == "offers"){
            loadOffersInvoices();
        }else if($scope.overviewType == "confirmed"){
            loadConfirmedInvoices();
        }else if($scope.overviewType == "inProduction"){
            loadInProductionInvoices();
        }else if($scope.overviewType == "sent"){
            loadSendInvoices();
        }else if($scope.overviewType == "completed"){
            loadCompletedInvoices();
        }else if($scope.overviewType == "detail"){
            loadInvoice();
        }
    }
    init();
}]);