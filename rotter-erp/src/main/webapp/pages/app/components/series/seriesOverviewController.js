/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var seriesController = angular.module('clarity.overview');

seriesController.controller('seriesOverviewController', ['$scope', '$log', 'series', 'crudService', '$location', function($scope, $log, series, crudService, $location){
    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'name', displayName: 'name', minWidth: 200, width: 200, maxWidth: 350},
            {field: 'description', displayName: 'description', minWidth: 300, width: 300},
            {field: 'suppliername', displayName: 'supplier', minWidth: 200, width: 200, maxWidth: 350},
            {field: 'creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingSeries(rowItem.entity);
            $location.path('/seriesEdit');
        }
    };

    $scope.data={};

    $scope.refresh = function(){
        crudService.getSeries().then(function(data){$scope.data = data.data;});
        $scope.gridOptions.data = 'data';
    };

    $scope.createSeries = function(){
        $location.path('/seriesCreate');
    };

    function init() {
        $scope.data = series.data;
        $scope.gridOptions.data = 'data';
    }
    init();
}]);