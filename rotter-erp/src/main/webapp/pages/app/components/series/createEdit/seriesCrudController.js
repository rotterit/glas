/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var seriesController = angular.module('clarity.crud');

seriesController.controller('seriesCrudController', ['$scope', '$log', 'series','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, series, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingSeries = null;
    var oldEditingSeries = null;
    $scope.suppliers = null;
    $scope.selectedSupplier = {};
    $scope.ValueMissingError = false;
    $scope.SupplierMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;
        $scope.SupplierMissingError = false;
        if($scope.editingSeries.name == "" || $scope.editingSeries.description == ""){
            $scope.ValueMissingError = true;
            $log.log("missingvalue");
            return;
        }

        $scope.editingSeries.supplierid = $scope.selectedSupplier.id;
        $scope.editingSeries.suppliername =$scope.selectedSupplier.name;
        $log.log($scope.editingSeries.supplierid);
        $log.log($scope.editingSeries.suppliername);
        if($scope.selectedSupplier.id == "" || $scope.selectedSupplier.name == ""){
            $scope.SupplierMissingError = true;
            return;
        }


        if(crudOption == 'create'){
            $log.log("create series " + $scope.editingSeries);
            httpServiceFactory.createPostPromise('/series/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingSeries, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Series Saved.");
                    $location.path('/seriesOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('edit series ' + $scope.editingSeries);
            httpServiceFactory.createPostPromise('/series/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingSeries, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Series Saved.");
                    $location.path('/seriesOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function loadSuppliers(){
        httpServiceFactory.createGetPromise('/suppliers/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log(data);
                $scope.suppliers = data;
                if(crudOption == "edit"){
                    for(var i = 0; i < $scope.suppliers.length; i++){
                        if($scope.editingSeries.supplierid == $scope.suppliers[i].id){
                            $scope.selectedSupplier = $scope.suppliers[i];
                            $log.log($scope.editingSeries);
                            $log.log($scope.selectedSupplier);
                            return;
                        }
                    }
                }
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get suppliers from server.");
            });
    }

    function init(){
        loadSuppliers();
        $log.log("incoming" + series);
        $scope.editingSeries = series;
        oldEditingSeries = angular.copy(series);
        if(crudOption == "create"){
            $scope.editingSeries = {"name":"", "description":"", "suppliername":"", "supplierid":""};
            $scope.selectedSupplier = {"id":"", "name":""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);