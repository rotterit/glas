/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var userController = angular.module('clarity.crud');

userController.controller('userCrudController', ['$scope', '$log', 'user','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, user, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingUser = null;
    var oldEditingUser = null;
    $scope.ValueMissingError = false;
    $scope.RoleMissingError = false;
    $scope.WrongPasswordError = false;
    $scope.UsernameInUserError = false;
    $scope.roles = {};
    $scope.selectedRole = {};
    $scope.crudType = "create";
    $scope.reenterPassword = "";

    $scope.save = function(){
        $scope.ValueMissingError = false;
        $scope.RoleMissingError = false;
        $scope.WrongPasswordError = false;
        $scope.UsernameInUserError = false;

        if($scope.editingUser.name == "" || $scope.editingUser.username == "" || ($scope.editingUser.password == "" && crudOption == "create") || ($scope.reenterPassword == "" && crudOption == "create")){
            $scope.ValueMissingError = true;
            return;
        }

        if($scope.editingUser.password != $scope.reenterPassword && crudOption == "create"){
            $scope.WrongPasswordError = true;
            return;
        }

        $log.log("++++++++++++++++++++");
        $log.log($scope.editingUser);

        $scope.editingUser.roleid = $scope.selectedRole.id;
        $scope.editingUser.rolename =$scope.selectedRole.name;
        if($scope.editingUser.roleid == "" || $scope.editingUser.rolename == ""){
            $scope.RoleMissingError = true;
            return;
        }

        $log.log("++++++++++++++++++++");
        $log.log($scope.editingUser);

        if(crudOption == 'create'){
            $scope.editingUser.password = CryptoJS.SHA256($scope.editingUser.password).toString();
            httpServiceFactory.createPostPromise('/users/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingUser, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("User Saved.");
                    $location.path('/userOverview');
                })
                .error(function (error, status) {
                    if(status == 406){
                        notificationService.showErrorNotification(status +  ": username already in use.");
                        $scope.UsernameInUserError = true;
                        $scope.editingUser.username = "";
                    }else {
                        notificationService.showErrorNotification(status + error);
                    }
                });
        }
        else if(crudOption == 'edit'){
            $log.log("save edit");
            httpServiceFactory.createPostPromise('/users/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingUser, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("User Saved.");
                    $location.path('/userOverview');
                })
                .error(function (error, status) {
                    if(status == 406){
                        //TODO: log to server. this should never happen.
                        notificationService.showErrorNotification(status +  ": username already in use.");
                        $scope.UsernameInUserError = true;
                        $scope.editingUser.username = oldEditingUser.username;
                    }else {
                        notificationService.showErrorNotification(status + error);
                    }
                });
        }
    };

    function loadRoles(){
        httpServiceFactory.createGetPromise('/roles/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log(data);
                $scope.roles = data;
                if(crudOption == "edit"){
                    for(var i = 0; i < $scope.roles.length; i++){
                        if($scope.editingUser.roleid == $scope.roles[i].id){
                            $scope.selectedRole = $scope.roles[i];
                            $log.log($scope.editingUser);
                            $log.log($scope.selectedLocation);
                            return;
                        }
                    }
                }
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get roles from server.");
            });
    }

    function init(){
        loadRoles();
        $log.log(user);
        $scope.editingUser = user;
        oldEditingUser = angular.copy(user);
        if(crudOption == "create"){
            $scope.editingUser = {"name":"", "username":"", "password":"", "roleid":"", "rolename":""};
            $scope.selectedRole = {"id":"", "name":""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);