/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var userController = angular.module('clarity.overview');

userController.controller('userOverviewController', ['$scope', '$log', 'users', 'crudService', '$location', function($scope, $log, users, crudService, $location){
    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'name', displayName: 'name', minWidth: 250, width: 250},
            {field: 'username', displayName: 'username', minWidth: 250, width: 250},
            {field: 'rolename', displayName: 'role', minWidth: 100, width: 100}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingUser(rowItem.entity);
            $location.path('/userEdit');
        }
    };

    $scope.data={};

    $scope.refresh = function(){
        crudService.getUsers().then(function(data){$scope.data = data.data;});
        $scope.gridOptions.data = 'data';
    };

    $scope.createUser = function(){
        $location.path('/userCreate');
    };

    function init() {
        $scope.data = users.data;
        $scope.gridOptions.data = 'data';
    }
    init();
}]);