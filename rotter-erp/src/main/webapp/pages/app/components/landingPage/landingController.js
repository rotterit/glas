/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var landingController = angular.module('clarity.overview');

landingController.controller('landingController', ['$scope', '$log', 'crudService', 'applicationInformation', function($scope, $log, crudService, applicationInformation){
    $scope.landingPageInfo = {};

    $scope.invoiceDueGrid = {
        data: 'invoiceDueGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'invoicenumber', displayName: 'invoice', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'customer', displayName: 'customer', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'localPrice', displayName: 'price', width: 80, cellTemplate: '<div class="ngCellText">{{row.entity.localPrice.toFixed(2)}}</div>'},
            {field: 'upfrontpercentage', displayName: 'upfront %', width: 80, cellTemplate: '<div class="ngCellText">{{row.entity.upfrontpercentage.toFixed(2)}}</div>'},
            {field: 'discountpercentage', displayName: 'discount %', width: 100, cellTemplate: '<div class="ngCellText">{{row.entity.discountpercentage.toFixed(2)}}</div>'},
            {field: 'sendDate', displayName: 'due date', width: 140, cellTemplate:"<div class='ngCellText'>{{row.entity.sendDate.dayOfMonth}} {{row.entity.sendDate.month}} {{row.entity.sendDate.year}}</div>"},
            {field: 'vatpercentage', displayName: 'vat %', width: 70, cellTemplate: '<div class="ngCellText">{{row.entity.vatpercentage.toFixed(2)}}</div>'},
            {field: 'priceWithoutVAT', displayName: 'price without VAT', width: 140, cellTemplate: '<div class="ngCellText">{{row.entity.priceWithoutVAT.toFixed(2)}}</div>'},
            {field: 'shippingCost', displayName: 'shipping', width: 120, cellTemplate: '<div class="ngCellText">{{row.entity.shippingCost.toFixed(2)}}</div>'},
            {field: 'createdBy', displayName: 'created by', width: 120},
            {field: 'lastModifiedDate', displayName: 'last modified date', width: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}</div>"}]
    };

    $scope.invoiceDueGridData = {};

    $scope.paymentDueGrid = {
        data: 'paymentDueGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'invoicenr', displayName: 'invoice Nr.', width: 120},
            {field: 'customer', displayName: 'customer', width: 120},
            {field: '', displayName: 'payment status', width: 180, cellTemplate: '<div ng-show="row.entity.payed" style="background-color: #00ff00" class="ngCellText">COMPLETE</div><div ng-show="row.entity.overDue" style="background-color: #ff9900" class="ngCellText">OVERDUE</div><div ng-show="row.entity.pastTwoWeeks" style="background-color: #ff0000" class="ngCellText">TO LATE</div>'},
            {field: 'paymentDueDate', displayName: 'payment due date', width: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.paymentDueDate.dayOfMonth}} {{row.entity.paymentDueDate.month}} {{row.entity.paymentDueDate.year}}</div>"}]
    };

    $scope.paymentDueGridData = {};

    $scope.refresh = function() {
        if(applicationInformation.getRole() == 2 || applicationInformation.getRole() == 3 || applicationInformation.getRole() == 5) {
            crudService.getLandingpageInfo().then(function (data) {
                if (data == null) {
                    return;
                } else {
                    $scope.landingPageInfo = data.data;
                    $scope.invoiceDueGridData = data.data.dueInvoiceList;
                    $scope.paymentDueGridData = data.data.paymentDueList;
                }
            });
        }
    };

    function init() {
        $scope.refresh();
    }
    init();
}]);