/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var stockController = angular.module('clarity.crud');

stockController.controller('stockCrudController', ['$scope', '$log', 'stock','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, stock, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingStock = null;
    var oldEditingStock = null;
    $scope.locations = null;
    $scope.selectedLocation = {};
    $scope.ValueMissingError = false;
    $scope.SupplierMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;

        $log.log("rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
        $log.log(oldEditingStock);
        $log.log($scope.editingStock);
        if($scope.editingStock.name == ""){
            $scope.ValueMissingError = true;
            $log.log("missingvalue");
            return;
        }

        $scope.editingStock.location = $scope.selectedLocation.description;
        $scope.editingStock.locationid =$scope.selectedLocation.id;
        $log.log($scope.editingStock.location);
        $log.log($scope.editingStock.locationid);
        if($scope.editingStock.location == "" || $scope.editingStock.locationid == ""){
            $scope.SupplierMissingError = true;
            return;
        }
        $log.log($scope.editingStock);

        if(crudOption == 'create'){
            $log.log("create stock " + $scope.editingStock);
            httpServiceFactory.createPostPromise('/stocks/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingStock, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Stock Saved.");
                    $location.path('/stockOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('edit stock ');
            $log.log($scope.editingStock);
            //if($scope.editingStock.raw==""){$scope.editingStock.raw=false;}else{$scope.editingStock.raw=true;}
            httpServiceFactory.createPostPromise('/stocks/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingStock, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Stock Saved.");
                    $location.path('/stockOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function loadLocations(){
        httpServiceFactory.createGetPromise('/locations/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log(data);
                $scope.locations = data;
                if(crudOption == "edit"){
                    for(var i = 0; i < $scope.locations.length; i++){
                        if($scope.editingStock.locationid == $scope.locations[i].id){
                            $scope.selectedLocation = $scope.locations[i];
                            $log.log($scope.editingStock);
                            $log.log($scope.selectedLocation);
                            return;
                        }
                    }
                }
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get locations from server.");
            });
    }

    function init(){
        loadLocations();
        $log.log(stock);
        $scope.editingStock = stock;
        oldEditingStock = angular.copy(stock);
        if(crudOption == "create"){
            $scope.editingStock = {"name":"", "raw":"false", "locationid":"", "note":""};
        }else if(crudOption == "edit"){
            //$scope.editingStock = {"id":"", "name":"", "location":"", "noteid":"", "note":"", "raw":""};
            $scope.editingStock = {"id":"", "name":"", "location":"", "locationid":"", "note":"", "raw":""};
            $scope.editingStock.id = stock.id;
            $scope.editingStock.name = stock.name;
            $scope.editingStock.location = stock.location;
            $scope.editingStock.locationid = stock.locationid;
            //$scope.editingStock.noteid = stock.noteid;
            $scope.editingStock.note = stock.note;
            //$scope.raw = stock.raw;

            /*if(stock.raw == ""){
                $scope.raw = false;
            }else{$scope.raw = true;}*/
        }
        $log.log("---------------------");
        $log.log($scope.editingStock);
        $scope.crudType = crudOption;
    }
    init();
}]);