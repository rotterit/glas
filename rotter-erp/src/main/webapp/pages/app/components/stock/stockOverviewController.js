/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var stockController = angular.module('clarity.overview');

stockController.controller('stockOverviewController', ['$scope', '$log', 'rawStocks', 'finishedStocks', 'crudService', '$location', 'httpServiceFactory', 'applicationInformation', 'notificationService', function($scope, $log, rawStocks, finishedStocks, crudService, $location, httpServiceFactory, applicationInformation, notificationService){
    var wantToEdit = false;
    $scope.overviewType = "overview";
    $scope.rawStockItemDetail = {};
    $scope.finishedStockItemDetail = {};

    $scope.rawStockGridOptions = {
        data: 'rawStockData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 30,
            cellTemplate: '<img style="height:28px;" src="assets/img/editIcon.png" ng-click="editStock(row)">',
            sortable: false,
            enableColumnResize: false
        },
            {field: 'name', displayName: 'name', width: 250},
            {field: 'location', displayName: 'location', width: 120},
            {field: 'creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}],
        afterSelectionChange: function(rowItem){
            if(!wantToEdit) {
                loadRawStockDetail(rowItem.entity.id);
                $scope.overviewType = "detailRaw";
            }
        }
    };

    $scope.rawStockData={};

    $scope.finishedStockGridOptions = {
        data: 'finishedStockData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 30,
            cellTemplate: '<img style="height:28px;" src="assets/img/editIcon.png" ng-click="editStock(row)">',
            sortable: false,
            enableColumnResize: false
        },
            {field: 'name', displayName: 'name', width: 250},
            {field: 'location', displayName: 'location', width: 120},
            {field: 'creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}],
        afterSelectionChange: function(rowItem){
            if(!wantToEdit) {
                loadFinishedStockDetail(rowItem.entity.id);
                $scope.overviewType = "detailFinished";
            }
        }
    };

    $scope.finishedStockData={};

    $scope.stockRawDetailGrid = {
        data: 'stockRawDetailGridData',
        enablePinning: false,
        enableColumnResize: true,
        enableRowSelection: false,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'rawMaterial', displayName: 'material id', width: 120},
            {field: 'amount', displayName: 'amount', width: 120},
            {field: 'productType', displayName: 'product type', width: 120},
            {field: 'quality', displayName: 'quality', width: 120},
            {field: 'size', displayName: 'size', width: 120},
            {field: 'color', displayName: 'color', width: 120},
            {field: 'supplier', displayName: 'supplier', width: 120}]
    };

    $scope.stockRawDetailGridData = [];

    $scope.stockFinishedDetailGrid = {
        data: 'stockFinishedDetailGridData',
        enablePinning: false,
        enableColumnResize: true,
        enableRowSelection: false,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'rawMaterial', displayName: 'material id', width: 120},
            {field: 'amount', displayName: 'amount', width: 120},
            {field: 'productType', displayName: 'product type', width: 120},
            {field: 'quality', displayName: 'quality', width: 120},
            {field: 'size', displayName: 'size', width: 120},
            {field: 'color', displayName: 'color', width: 120},
            {field: 'supplier', displayName: 'supplier', width: 120}]
    };

    $scope.stockFinishedDetailGridData = [];

    function loadRawStockDetail(stockId){
        httpServiceFactory.createGetPromise('/stocks/raw/byid', 'userid=' + applicationInformation.getUserinformation().userId + "&stockid=" + stockId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("eeeeeeeeeeeeeeeeeeeeeeeeeeeeee");//TODO get rid of this
                $log.log(data);
                $scope.rawStockItemDetail = data;
                $scope.stockRawDetailGridData = data.stockLines;
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get stock detail from server.");
            });
    }

    function loadFinishedStockDetail(stockId){
        httpServiceFactory.createGetPromise('/stocks/finished/byid', 'userid=' + applicationInformation.getUserinformation().userId + "&stockid=" + stockId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("rrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");//TODO get rid of this
                $log.log(data);
                $scope.finishedStockItemDetail = data;
                $scope.stockFinishedDetailGridData = data.stockLines;
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get stock detail from server.");
            });
    }

    $scope.refresh = function(){
        crudService.getRawStocks().then(function(data){$scope.rawStockData = data.data;});
        $scope.rawStockGridOptions.data = 'rawStockData';
        crudService.getFinishedStocks().then(function(data){$scope.rawStofinishedStockDatackData = data.data;});
        $scope.finishedStockGridOptions.data = 'finishedStockData';
    };

    $scope.createStock = function(){
        $location.path('/stockCreate');
    };

    $scope.editStock = function(rowItem){
        wantToEdit = true;
        crudService.setEditingStock(rowItem.entity);
        $location.path('/stockEdit');
    };

    $scope.backToOverview = function(){
        $scope.stockRawDetailGridData = [];
        $scope.stockFinishedDetailGridData = [];
        $scope.overviewType = "overview";
    };

    function init() {
        $scope.rawStockData = rawStocks.data;
        $scope.finishedStockData = finishedStocks.data;
        $scope.rawStockGridOptions.data = 'rawStockData';
        $scope.finishedStockGridOptions.data = 'finishedStockData';
    }
    init();
}]);