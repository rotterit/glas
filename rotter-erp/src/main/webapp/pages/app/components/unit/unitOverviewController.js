/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var unitController = angular.module('clarity.overview');

unitController.controller('unitOverviewController', ['$scope', '$log', 'units', 'crudService', '$location', 'httpServiceFactory', 'applicationInformation', 'notificationService', function($scope, $log, units, crudService, $location, httpServiceFactory, applicationInformation, notificationService){
    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 150,
            cellTemplate: '<div>{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'name', displayName: 'key', width: 300},
            {field: 'textValue', displayName: 'value', width: 300, enableCellEdit: true},
            {field: 'note.content', displayName: 'note', width: 300}],
        selectedItems: []
    };

    $scope.data={};
    $scope.ValueMissingError = false;

    $scope.$on('ngGridEventEndCellEdit', function(event) {
        $scope.ValueMissingError = false;
        if (event.targetScope.row.entity.textValue == "") {
            $scope.ValueMissingError = true;
            return;
        }

        $log.log('edit unit ' + $scope.editingUnit);
        httpServiceFactory.createPostPromise('/units/edit', 'userid=' + applicationInformation.getUserinformation().userId, event.targetScope.row.entity, true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                $log.log(status);
                notificationService.showMessageNotification("Unit Saved.");
                $location.path('/unitOverview');
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + error);
            });
    });

    function init() {
        $scope.data = units.data;
        $scope.gridOptions.data = 'data';
    }
    init();
}]);