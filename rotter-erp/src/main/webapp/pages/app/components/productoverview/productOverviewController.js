/**
 * Created by Suited Coder Gunther Claessens
 *
 * @Description: Controller for the product overview page. This handles all high level product actions.
 */
'use strict';
var controller = angular.module('clarity.product');
controller.controller('productOverviewCtrl', ['$scope', function($scope){

}]);
