/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var orderController = angular.module('clarity.overview');

orderController.controller('orderOverviewController', ['$scope', '$log', 'copypasterinoService', 'notificationService', 'overviewType', 'crudService', '$location', 'httpServiceFactory', 'applicationInformation', function($scope, $log, copypasterinoService, notificationService, overviewType, crudService, $location, httpServiceFactory, applicationInformation){
    //print stuff
    //$scope.printGridData = [];
    //end print stuff

    $scope.overviewType = overviewType;

    //new
    $scope.isInConfirmationProcess = false;
    $scope.confirmationDate = {"year": new Date().getFullYear(), "month": new Date().getMonth() + 1, "day": 1};
    $scope.confirmingOrder = {};

    //confirmed
    $scope.amount = "";
    $scope.orderInDeliveryProcess = {};
    $scope.isInDeliveryProcess = false;
    $scope.deliveryReadyToCommit = false;
    $scope.deliveryProcessInformation = {};
    $scope.selectedQuality = null;
    $scope.selectedStock = null;
    $scope.remainingDeliveredMaterialsToQualityCheck = [];
    $scope.selectedProcessingDeliveryItem = [];
    $scope.selectedProcessingDeliveryItemAvailibleStocks = [];

    //detail
    $scope.oderDetailInfo = {};

    $scope.InvalidDateFormatError = false;
    $scope.WrongDateError = false;
    $scope.NoStockSelectedError = false;
    $scope.NoProcessingItemSelectedError = false;
    $scope.IncorrectProcessingItemAmountError = false;
    $scope.NoQualitySelectedError = false;


    $scope.newOrdersGrid = {
        data: 'newOrders',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 103,
            cellTemplate: '<input type="button" value="Confirm order" class="ngCellText" ng-click="startConfirmation(row)">',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },
            {field: 'creationDate', displayName: 'created', width: 117, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"},
            {field: 'supplierName', displayName: 'supplier', width: 120},
            {field: 'totalItems', displayName: 'number of items', width: 122},
            {field: 'totalAmount', displayName: 'total amount of items', width: 159},
            {field: 'totalCost', displayName: 'total amount', width: 103, cellTemplate: '<div class="ngCellText">{{row.entity.totalCost.toFixed(2)}}</div>'},
            {field: 'createdBy', displayName: 'created by', width: 150},
            {field: 'lastModifiedDate', displayName: 'last modified date', width: 140, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', width: 180}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            $log.log("##########editing order going in#############");
            $log.log(rowItem.entity);
            if( $scope.confirmingOrder == rowItem.entity){

            }else {
                crudService.setEditingOrder(rowItem.entity);
                $location.path('/editOrder');
            }
        }
    };

    $scope.newOrders={};

    $scope.confirmationOrdersGrid = {
        data: 'confirmationOrders',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 120,
            cellTemplate: '<input type="button" value="Process delivery" class="ngCellText" ng-click="startDeliveryProcessing(row)">',
            sortable: false, pinned: true, pinnable: false
        },
            {field: 'creationDate', displayName: 'created', width: 117, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"},
            {field: 'supplierName', displayName: 'supplier', width: 120},
            {field: 'totalItems', displayName: 'number of items', width: 140},
            {field: 'totalAmount', displayName: 'total amount of items', width: 170},
            {field: 'totalCost', displayName: 'total amount', width: 103, cellTemplate: '<div class="ngCellText">{{row.entity.totalCost.toFixed(2)}}</div>'},
            {field: 'createdBy', displayName: 'created by', width: 150},
            {field: 'lastModifiedDate', displayName: 'last modified date', width: 140, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', width: 180}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            if($scope.isInDeliveryProcess){
                //Do not change to detail if you have pressed the button in de row.
            }else {
                loadOrderDetail(rowItem);
            }
        }
    };

    $scope.confirmationOrders={};

    $scope.deliveryOrdersGrid = {
        data: 'deliveries',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 77,
            cellTemplate: '<input type="button" value="Complete" class="ngCellText" ng-click="startOrderCompletion(row)">',
            sortable: false, pinned: true, pinnable: false
        },
            {field: 'creationDate', displayName: 'created', width: 117, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"},
            {field: 'supplierName', displayName: 'supplier', width: 120},
            {field: 'totalItems', displayName: 'number of items', width: 121},
            {field: 'totalAmount', displayName: 'total amount of items', width: 156},
            {field: 'totalCost', displayName: 'total amount', width: 103, cellTemplate: '<div class="ngCellText">{{row.entity.totalCost.toFixed(2)}}</div>'},
            {field: 'createdBy', displayName: 'created by', width: 180},
            {field: 'lastModifiedDate', displayName: 'last modified date', width: 140, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', width: 180}],
        selectedItems: $scope.selectedProcessingDeliveryItem,
        multiSelect: false,
        afterSelectionChange: function(rowItem){
            loadOrderDetail(rowItem);
        }
    };

    $scope.deliveries={};

    $scope.completedOrdersGrid = {
        data: 'completedOrders',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'creationDate', displayName: 'created', width: 117, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"},
            {field: 'supplierName', displayName: 'supplier', width: 120},
            {field: 'totalItems', displayName: 'number of items', width: 121},
            {field: 'totalAmount', displayName: 'total amount of items', width: 156},
            {field: 'totalCost', displayName: 'total amount', width: 103, cellTemplate: '<div class="ngCellText">{{row.entity.totalCost.toFixed(2)}}</div>'},
            {field: 'createdBy', displayName: 'created by', width: 180},
            {field: 'lastModifiedDate', displayName: 'last modified date', width: 140, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', width: 180}],
        selectedItems: $scope.selectedProcessingDeliveryItem,
        multiSelect: false,
        afterSelectionChange: function(rowItem){
            loadOrderDetail(rowItem);
        }
    };

    $scope.completedOrders={};

    $scope.orderDetailGrid = {
        data: 'orderDetail',
        enablePinning: false,
        enableColumnResize: true,
        enableRowSelection: false,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'amount', displayName: 'amount', width: 92, sortable: false},
            {field: 'rawMaterial.price', displayName: 'unit price', width: 120, sortable: false, cellTemplate: '<div class="ngCellText">{{row.entity.rawMaterial.price.toFixed(2)}}</div>'},
            {field: 'price', displayName: 'total price', width: 120, sortable: false, cellTemplate: '<div class="ngCellText">{{row.entity.price.toFixed(2)}}</div>'},
            {field: 'rawMaterial.productCode', displayName: 'product id', width: 120, sortable: false},
            {field: 'rawMaterial.rawmaterialcode', displayName: 'supplier code', width: 120, sortable: false},
            {field: 'rawMaterial.minimumorderQuantity', displayName: 'minimum order quantity', width: 179, sortable: false},
            {field: 'rawMaterial.minimumAmountInStock', displayName: 'base stock amount', width: 150, sortable: false},
            {field: 'rawMaterial.amountInStock', displayName: 'amount in stock', width: 124, sortable: false},
            {field: 'rawMaterial.description', displayName: 'description', width: 200, sortable: false}]
    };

    $scope.orderDetail={};

    $scope.orderDetailWithQualitiesGrid = {
        data: 'orderDetailWithQualities',
        enablePinning: false,
        enableColumnResize: true,
        enableRowSelection: false,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'amount', displayName: 'amount', width: 92, sortable: false},
            {field: 'rawMaterial.quality', displayName: 'quality', width: 92, sortable: false},
            {field: 'rawMaterial.price', displayName: 'unit price', width: 120, sortable: false, cellTemplate: '<div class="ngCellText">{{row.entity.rawMaterial.price.toFixed(2)}}</div>'},
            {field: 'price', displayName: 'total price', width: 120, sortable: false, cellTemplate: '<div class="ngCellText">{{row.entity.price.toFixed(2)}}</div>'},
            {field: 'rawMaterial.productCode', displayName: 'product id', width: 120, sortable: false},
            {field: 'rawMaterial.rawmaterialcode', displayName: 'supplier code', width: 120, sortable: false},
            {field: 'rawMaterial.minimumorderQuantity', displayName: 'minimum order quantity', width: 181, sortable: false},
            {field: 'rawMaterial.minimumAmountInStock', displayName: 'base stock amount', width: 144, sortable: false},
            {field: 'rawMaterial.amountInStock', displayName: 'amount in stock', width: 125, sortable: false},
            {field: 'rawMaterial.description', displayName: 'description', width: 350, sortable: false}]
    };

    $scope.orderDetailWithQualities={};

    $scope.remainingDeliveredItems = {
        data: 'unProcessedDeliveredItemsGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'amount', displayName: 'amount', width: 92, sortable: false},
            {field: 'rawMaterial.productCode', displayName: 'product id', width: 120, sortable: false},
            {field: 'rawMaterial.productType', displayName: 'product type', width: 120, sortable: false},
            {field: 'rawMaterial.color', displayName: 'color', width: 120, sortable: false},
            {field: 'rawMaterial.size', displayName: 'size', width: 120, sortable: false},
            {field: 'rawMaterial.series', displayName: 'series', width: 120, sortable: false},
            {field: 'rawMaterial.note', displayName: 'note', width: 120, sortable: false},
            {field: 'rawMaterial.rawmaterialcode', displayName: 'material code', width: 120, sortable: false}],
        selectedItems: $scope.selectedProcessingDeliveryItem,
        multiSelect: false,
        afterSelectionChange: function(rowItem){
            for(var i = 0; i < $scope.deliveryProcessInformation.possibilities.stockPossibilities.length; i++){
                if($scope.deliveryProcessInformation.possibilities.stockPossibilities[i].rawMaterialId == rowItem.entity.rawMaterial.id){
                    $scope.selectedProcessingDeliveryItemAvailibleStocks = $scope.deliveryProcessInformation.possibilities.stockPossibilities[i].stocks;
                    return;
                }
            }
        }
    };

    $scope.unProcessedDeliveredItemsGridData = [];

    $scope.processedDeliveredItems = {
        data: 'processedDeliveredItemsGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 50,
            cellTemplate: '<div class="ngCellText" ng-click="removeProcessedItem(row)">X</div>',
            sortable: false
        },
            {field: 'rawMateralCode', displayName: 'material code', width: 120, sortable: false},
            {field: 'amount', displayName: 'amount', width: 120, sortable: false},
            {field: 'qualityName', displayName: 'quality', width: 180, sortable: false},
            {field: 'stockName', displayName: 'stock', width: 300, sortable: false}],
        multiSelect: false
    };

    $scope.processedDeliveredItemsGridData = [];

    function loadNewOrders(){
        httpServiceFactory.createGetPromise('/orders/new', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("---------new orders------------");
                $log.log(data);
                var orders = data;
                for(var i = 0; i < data.length; i++){
                    var totalItems = 0;
                    var totalAmount = 0;
                    var totalCost = 0;
                    for(var j = 0; j < orders[i].materialmoveItems.length; j++){
                        totalItems++;
                        totalAmount = totalAmount + Number(orders[i].materialmoveItems[j].amount);
                        totalCost = totalCost + Number(orders[i].materialmoveItems[j].price);
                    }
                    orders[i].totalItems = totalItems;
                    orders[i].totalAmount = totalAmount;
                    orders[i].totalCost = totalCost;
                }
                $scope.newOrders = orders;
                $log.log(orders);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get suppliers from server.");
            });
    }

    function loadConfirmedOrders(){
        httpServiceFactory.createGetPromise('/orders/confirmed', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("---------confirmed orders------------");
                var orders = data;
                for(var i = 0; i < data.length; i++){
                    var totalItems = 0;
                    var totalAmount = 0;
                    var totalCost = 0;
                    for(var j = 0; j < orders[i].materialmoveItems.length; j++){
                        totalItems++;
                        totalAmount = totalAmount + Number(orders[i].materialmoveItems[j].amount);
                        totalCost = totalCost + Number(orders[i].materialmoveItems[j].price);
                    }
                    orders[i].totalItems = totalItems;
                    orders[i].totalAmount = totalAmount;
                    orders[i].totalCost = totalCost;
                }
                $scope.confirmationOrders = orders;
                $log.log(orders);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get suppliers from server.");
            });
    }

    function loadDeliveries(){
        httpServiceFactory.createGetPromise('/orders/delivered', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("---------deliveries------------");
                var orders = data;
                for(var i = 0; i < data.length; i++){
                    var totalItems = 0;
                    var totalAmount = 0;
                    var totalCost = 0;
                    for(var j = 0; j < orders[i].materialmoveItems.length; j++){
                        totalItems++;
                        totalAmount = totalAmount + Number(orders[i].materialmoveItems[j].amount);
                        totalCost = totalCost + Number(orders[i].materialmoveItems[j].price);
                    }
                    orders[i].totalItems = totalItems;
                    orders[i].totalAmount = totalAmount;
                    orders[i].totalCost = totalCost;
                }
                $scope.deliveries = orders;
                $log.log(orders);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get the processed deliveries from the server.");
            });
    }

    function loadCompletedOrders(){
        httpServiceFactory.createGetPromise('/orders/completed', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $log.log("---------completed orders------------");
                var orders = data;
                for(var i = 0; i < data.length; i++){
                    var totalItems = 0;
                    var totalAmount = 0;
                    var totalCost = 0;
                    for(var j = 0; j < orders[i].materialmoveItems.length; j++){
                        totalItems++;
                        totalAmount = totalAmount + Number(orders[i].materialmoveItems[j].amount);
                        totalCost = totalCost + Number(orders[i].materialmoveItems[j].price);
                    }
                    orders[i].totalItems = totalItems;
                    orders[i].totalAmount = totalAmount;
                    orders[i].totalCost = totalCost;
                }
                $scope.completedOrders = orders;
                $log.log(orders);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get completed orders from the server.");
            });
    }

    function loadOrderDetail(row){
        $log.log(row.entity);
        if($scope.overviewType == "new" || $scope.overviewType == "confirmed") {
            $scope.orderDetail = row.entity.materialmoveItems;
            $scope.overviewType = "detail";
        }else if($scope.overviewType == "deliver" || $scope.overviewType == "complete") {
            $scope.orderDetailWithQualities = row.entity.materialmoveItems;
            $scope.overviewType = "detailWithQuality";
        }
        $scope.oderDetailInfo = row.entity;
    }

    $scope.openNewOrders = function(){
        if($scope.overviewType != "new"){$location.path("/newOrdersOverview");}
    };
    $scope.openConfirmedOrders = function(){
        if($scope.overviewType != "confirmed"){$location.path("/confirmedOrdersOverview");}
    };
    $scope.openDeliveredOrders = function(){
        if($scope.overviewType != "deliver"){$location.path("/deliveredOrdersOverview");}
    };
    $scope.openCompletedOrders = function(){
        if($scope.overviewType != "complete"){$location.path("/completeOrdersOverview");}
    };

    //confirming a new order
    $scope.startConfirmation = function(row){
        $scope.isInConfirmationProcess = true;
        $scope.confirmingOrder = row.entity;
    };

    $scope.createConfirmation = function(){
        $scope.InvalidDateFormatError = false;
        $scope.WrongDateError = false;

        if(isNaN($scope.confirmationDate.year) || isNaN($scope.confirmationDate.month) || isNaN($scope.confirmationDate.day)){
            $scope.InvalidDateFormatError = true;
            return;
        }

        var today = new Date();
        today.setHours(12);
        today.setMinutes(0);
        today.setSeconds(0);
        today.setMilliseconds(0);
        var confirmationDate = new Date($scope.confirmationDate.year, $scope.confirmationDate.month - 1, $scope.confirmationDate.day, 12, 0, 0, 0);
        if(today < confirmationDate){
            httpServiceFactory.createPostPromise('/orders/confirm', 'userid=' + applicationInformation.getUserinformation().userId + "&orderid=" + $scope.confirmingOrder.id + "&deliverymillis=" + confirmationDate.getTime(), '', true, applicationInformation.getToken(), false)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Your order is confirmed.");
                    $location.path("/confirmedOrdersOverview");
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get your order confirmed.");
                });
        }else{
            $scope.WrongDateError = true;
        }
    };

    //processing a delivery
    $scope.startDeliveryProcessing = function(row){
        $scope.orderInDeliveryProcess = row.entity;
        $scope.isInDeliveryProcess = true;
        httpServiceFactory.createGetPromise('/orders/getqualitychecksetup', 'userid=' + applicationInformation.getUserinformation().userId + "&orderid=" + $scope.orderInDeliveryProcess.id, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.deliveryProcessInformation = data;
                $scope.remainingDeliveredMaterialsToQualityCheck = $scope.deliveryProcessInformation.materialmoveItems;
                $scope.unProcessedDeliveredItemsGridData = $scope.remainingDeliveredMaterialsToQualityCheck;
                $scope.isInDeliveryProcess = true;
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get your order confirmed.");
                $location.path("/orderRawOverview");
            });
    };

    $scope.processSelectedMaterial = function(){
        $scope.NoProcessingItemSelectedError = false;
        $scope.NoStockSelectedError = false;
        $scope.IncorrectProcessingItemAmountError = false;
        $scope.NoQualitySelectedError = false;

        if($scope.selectedProcessingDeliveryItem == [] || $scope.selectedProcessingDeliveryItem == null || $scope.selectedProcessingDeliveryItem == "" || $scope.selectedProcessingDeliveryItem == undefined || $scope.selectedProcessingDeliveryItem == {}){
            $scope.NoProcessingItemSelectedError = true;
            return;
        }

        if($scope.selectedStock == null || $scope.selectedStock == "" || $scope.selectedStock == undefined || $scope.selectedStock == {}){
            $scope.NoStockSelectedError = true;
            return;
        }

        if($scope.amount == "" || $scope.amount == null || $scope.amount == undefined || isNaN($scope.amount)){
            $scope.IncorrectProcessingItemAmountError = true;
            return;
        }else if(Number($scope.amount) > Number($scope.selectedProcessingDeliveryItem[0].amount)){
            $scope.IncorrectProcessingItemAmountError = true;
            return;
        }

        $log.log($scope.selectedQuality);
        if($scope.selectedQuality == null || $scope.selectedQuality == "" || $scope.selectedQuality == undefined || $scope.selectedQuality == {}){
            $scope.NoQualitySelectedError = true;
            return;
        }else{
            if($scope.selectedQuality.id == null || $scope.selectedQuality.id == "" || $scope.selectedQuality.id == undefined){
                $scope.NoQualitySelectedError = true;
                return;
            }
        }

        var processedItem = {"rawMaterialId": $scope.selectedProcessingDeliveryItem[0].rawMaterial.id,
        "rawMateralCode": $scope.selectedProcessingDeliveryItem[0].rawMaterial.productCode,
        "quality": $scope.selectedQuality.id,
        "qualityName": $scope.selectedQuality.name,
        "amount": $scope.amount,
        "stockId": $scope.selectedStock.stockid,
        "stockName": $scope.selectedStock.stockname};

        for(var i = 0; i < $scope.unProcessedDeliveredItemsGridData.length; i++){
            if($scope.unProcessedDeliveredItemsGridData[i].rawMaterial.id == processedItem.rawMaterialId){
                $scope.unProcessedDeliveredItemsGridData[i].amount = Number($scope.unProcessedDeliveredItemsGridData[i].amount) - Number(processedItem.amount);
            }
        }
        for(var i = 0; i < $scope.processedDeliveredItemsGridData.length; i++){
            if($scope.processedDeliveredItemsGridData[i].rawMaterialId == processedItem.rawMaterialId && $scope.processedDeliveredItemsGridData[i].quality == processedItem.quality){
                $scope.processedDeliveredItemsGridData[i].amount = Number($scope.processedDeliveredItemsGridData[i].amount) + Number(processedItem.amount);
                isDeliveryReadyToCommit();
                $scope.amount = "";
                return;
            }
        }

        $scope.processedDeliveredItemsGridData.push(processedItem);
        isDeliveryReadyToCommit();
        $scope.amount = "";
    };

    function isDeliveryReadyToCommit(){
        var totalAmountLeftToProcess = 0;
        for(var i = 0; i < $scope.unProcessedDeliveredItemsGridData.length; i++){
            totalAmountLeftToProcess += Number($scope.unProcessedDeliveredItemsGridData[i].amount);
        }
        if(totalAmountLeftToProcess == 0){
            $scope.deliveryReadyToCommit = true;
        }else{
            $scope.deliveryReadyToCommit = false;
        }
    }

    $scope.removeProcessedItem = function(rowItem){
        var toRemoveItem = rowItem.entity;
        for(var i = 0; i < $scope.unProcessedDeliveredItemsGridData.length; i++){
            if($scope.unProcessedDeliveredItemsGridData[i].rawMaterial.id == toRemoveItem.rawMaterialId){
                $scope.unProcessedDeliveredItemsGridData[i].amount = Number($scope.unProcessedDeliveredItemsGridData[i].amount) + Number(toRemoveItem.amount);
            }
        }
        for(var i = 0; i < $scope.processedDeliveredItemsGridData.length; i++){
            if($scope.processedDeliveredItemsGridData[i].rawMaterialId == toRemoveItem.rawMaterialId && $scope.processedDeliveredItemsGridData[i].quality == toRemoveItem.quality){
                $scope.processedDeliveredItemsGridData.splice(i, 1);
            }
        }
        isDeliveryReadyToCommit();
    };

    $scope.commitDelivery = function(){
        var delivery = createDeliveryObject();
        httpServiceFactory.createPostPromise('/orders/setquality', 'userid=' + applicationInformation.getUserinformation().userId, delivery, true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                notificationService.showMessageNotification("Your delivery is processed.");
                $location.path("/deliveredOrdersOverview");
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get your delivery processed.");
            });
    };

    function createDeliveryObject(){
        var delivery = {"orderId": $scope.orderInDeliveryProcess.id, "qualitySetLines": []};
        for(var i = 0; i < $scope.processedDeliveredItemsGridData.length; i++){
            delivery.qualitySetLines.push({"rawMaterialId": Number($scope.processedDeliveredItemsGridData[i].rawMaterialId),
            "qualityId": Number($scope.processedDeliveredItemsGridData[i].quality),
            "amount": Number($scope.processedDeliveredItemsGridData[i].amount),
            "stockId": Number($scope.processedDeliveredItemsGridData[i].stockId)})
        }
        return delivery;
    }
    //end processing delivery

    //completing an order
    $scope.startOrderCompletion = function(row){
        $log.log("start porder completion");
        if (confirm("Are you sure you want to complete the order?") == true) {
            $log.log("oke");
            $log.log(row.entity.id);
            httpServiceFactory.createPostPromise('/orders/complete', 'userid=' + applicationInformation.getUserinformation().userId + "&orderid=" + row.entity.id, '', true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Your order is completed.");
                    $location.path("/completeOrdersOverview");
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get your order completed.");
                });
        } else {
            $log.log("not oke");
        }
    };
    //end completing an order

    $scope.closeOrderDetail = function(){
        $scope.oderDetailInfo = {};
        switch ($location.path()){
            case '/newOrdersOverview': $scope.overviewType = "new";
                break;
            case '/confirmedOrdersOverview': $scope.overviewType = "confirmed";
                break;
            case '/deliveredOrdersOverview': $scope.overviewType = "deliver";
                break;
            case '/completeOrdersOverview': $scope.overviewType = "complete";
                break;
        }
    };

    $scope.cancel = function(){
        if(overviewType == "new") {
            $scope.confirmationDate = {"year": new Date().getFullYear(), "month": new Date().getMonth() + 1, "day": 1};
            $scope.isInConfirmationProcess = false;
            $scope.confirmingOrder = {};
            $scope.WrongDateError = false;
        }else if(overviewType == "confirmed"){
            $scope.amount = "";
            $scope.isInDeliveryProcess = false;
            $scope.deliveryReadyToCommit = false;
            $scope.orderInDeliveryProcess = {};
            $scope.deliveryProcessInformation = {};
            $scope.processedDeliveredItemsGridData = [];
            $scope.unProcessedDeliveredItemsGridData = [];
            $scope.selectedQuality = null;
            $scope.selectedStock = null;
            $scope.remainingDeliveredMaterialsToQualityCheck = [];
            $scope.selectedProcessingDeliveryItem = [];
            $scope.selectedProcessingDeliveryItemAvailibleStocks = [];
        }
    };


    $scope.orderCsvWithoutQuality = function(){
        $log.log("order lines");
        $log.log($scope.orderDetail);
        $scope.csvData = "pos.,Unsere Artikel-Nr.,Ihre Artikel-Nr.,Beschreibung,Menge,Stuckpreis,Gesamtpreis\n";
        for(var i = 0; i < $scope.orderDetail.length; i++){
            $scope.csvData = $scope.csvData.concat(i + 1).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].rawMaterial.productCode).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].rawMaterial.rawmaterialcode).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].description).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].amount).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].unitPrice).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].price).concat(",\n");
        }
        copypasterinoService.showText($scope.csvData);
        $scope.csvData = "";
    };

    //not in use. needs the info of the original order.
    /*$scope.orderCsvWithQuality = function(){
        $log.log("order lines");
        $log.log($scope.orderDetailWithQualities);
        $scope.csvData = "pos.,Unsere Artikel-Nr.,Ihre Artikel-Nr.,Beschreibung,Menge,Stuckpreis,Gesamtpreis\n";
        for(var i = 0; i < $scope.orderDetail.length; i++){
            $scope.csvData = $scope.csvData.concat(i + 1).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].rawMaterial.productCode).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].rawMaterial.rawmaterialcode).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].description).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].amount).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].unitPrice).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetail[i].price).concat(",\n");
        }
        copypasterinoService.showText($scope.csvData);
        $scope.csvData = "";
    };*/

    $scope.orderDetailCsvWithQuality = function(){
        $log.log("processed order lines");
        $log.log($scope.orderDetailWithQualities);
        $scope.csvData = "pos.,Amount.,Quality.,unit price,total price,product id,supplier\n";
        for(var i = 0; i < $scope.orderDetailWithQualities.length; i++){
            $scope.csvData = $scope.csvData.concat(i + 1).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetailWithQualities[i].amount).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetailWithQualities[i].rawMaterial.quality).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetailWithQualities[i].rawMaterial.price).concat(",");
            $scope.csvData = $scope.csvData.concat((Number($scope.orderDetailWithQualities[i].amount) * Number($scope.orderDetailWithQualities[i].rawMaterial.price)).toFixed(2)).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetailWithQualities[i].rawMaterial.productCode).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.orderDetailWithQualities[i].rawMaterial.supplier).concat(",\n");
        }
        copypasterinoService.showText($scope.csvData);
        $scope.csvData = "";
    };

    $scope.refresh = function(){
        if(overviewType == "new"){
            loadNewOrders();
        }else if(overviewType == "confirmed"){
            loadConfirmedOrders();
        }else if(overviewType == "deliver"){
            loadDeliveries();
        }else if(overviewType == "complete"){
            loadCompletedOrders();
        }
    };

    function init() {
        if(overviewType == "confirmed" && applicationInformation.getRole() == 6) {
            $scope.openConfirmedOrders();
        } else if(overviewType == "deliver" && applicationInformation.getRole() == 6){
            $scope.openDeliveredOrders();
        } else {
            //fillPrintGrid();
            if (overviewType == "new") {
                loadNewOrders();
            } else if (overviewType == "confirmed") {
                loadConfirmedOrders();
            } else if (overviewType == "deliver") {
                loadDeliveries();
            } else if (overviewType == "complete") {
                loadCompletedOrders();
            } else if (overviewType == "detail") {
                loadCompletedOrders();
            }
        }

    }
    init();



    /*Just for printing. This is crap. I hate this!*/

    /*function fillPrintGrid(){
        $scope.printGridData.push({"pos":1, "artNr": "TB-S-Re", "refnr":"4858/60", "descr": "the tumbler in red from the paris series.", "stuck":120, "price":14.50, "total":1740.00});
        $scope.printGridData.push({"pos":2, "artNr": "TB-S-Re", "refnr":"4858/60", "descr": "the tumbler in red from the paris series.", "stuck":120, "price":14.50, "total":1740.00});
        $scope.printGridData.push({"pos":3, "artNr": "TB-S-Re", "refnr":"4858/60", "descr": "the tumbler in red from the paris series.", "stuck":120, "price":14.50, "total":1740.00});
        $scope.printGridData.push({"pos":4, "artNr": "TB-S-Re", "refnr":"4858/60", "descr": "the tumbler in red from the paris series.", "stuck":120, "price":14.50, "total":1740.00});
        $scope.printGridData.push({"pos":5, "artNr": "TB-S-Re", "refnr":"4858/60", "descr": "the tumbler in red from the paris series.", "stuck":120, "price":14.50, "total":1740.00});
        $scope.printGridData.push({"pos":6, "artNr": "TB-S-Re", "refnr":"4858/60", "descr": "the tumbler in red from the paris series.", "stuck":120, "price":14.50, "total":1740.00});
        $scope.printGridData.push({"pos":7, "artNr": "TB-S-Re", "refnr":"4858/60", "descr": "the tumbler in red from the paris series.", "stuck":120, "price":14.50, "total":1740.00});
        $scope.printGridData.push({"pos":8, "artNr": "TB-S-Re", "refnr":"4858/60", "descr": "the tumbler in red from the paris series.", "stuck":120, "price":14.50, "total":1740.00});
        $scope.printGridData.push({"pos":9, "artNr": "TB-S-Re", "refnr":"4858/60", "descr": "the tumbler in red from the paris series.", "stuck":120, "price":14.50, "total":1740.00});
        $scope.printGridData.push({"pos":10, "artNr": "TB-S-Re", "refnr":"4858/60", "descr": "the tumbler in red from the paris series.", "stuck":120, "price":14.50, "total":1740.00});
        $log.log($scope.printGridData);
    }*/
}]);