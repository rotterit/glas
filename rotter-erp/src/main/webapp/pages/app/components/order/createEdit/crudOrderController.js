/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: This is the controller that handles the creation of a new order and the edit of an existing order.
 */
'use strict';
var orderRawController = angular.module('clarity.crud');

orderRawController.controller('rawMaterialOrderCrudController', ['$scope', '$log', 'order', 'httpServiceFactory', 'applicationInformation', 'notificationService', '$location', 'shoppingCart', 'crudService', function($scope, $log, order, httpServiceFactory, applicationInformation, notificationService, $location, shoppingCart, crudService){
    $scope.crudType = "";
    $scope.note = "";
    $scope.supplier = {};

    $scope.NoOrderItemsError = false;
    $scope.SupplierMissingError = false;


    $scope.gridOptions = {
        data: 'data',
        enablePinning: true,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            width: 50,
            cellTemplate: '<div class="ngCellText" style="background-color: {{row.entity.rawMaterial.linecolor}}">{{row.rowIndex + 1}}</div>',
            pinned: true,
            pinnable: false,
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 50,
            cellTemplate: '<div class="ngCellText" ng-click="removeFromOrder(row)">X</div>',
            sortable: false, pinned: true, pinnable: false
        },
            {field: 'quantity', displayName: 'amount', width: 92, pinned: true, pinnable: false, enableCellEdit: true},
            {field: 'rawMaterial.productCode', displayName: 'id', width: 120, pinnable: false, pinned: true},
            {field: 'rawMaterial.price', displayName: 'price', width: 120, cellTemplate: '<div class="ngCellText">{{row.entity.rawMaterial.price.toFixed(2)}}</div>'},
            {field: 'rawMaterial.minimumorderQuantity', displayName: 'minimum order quantity', width: 188},
            {field: 'rawMaterial.minimumAmountInStock', displayName: 'base stock amount', width: 159},
            {field: 'rawMaterial.amountInStock', displayName: 'amount in stock', width: 137},
            {field: 'rawMaterial.productType', displayName: 'pro. type', width: 120},
            {field: 'rawMaterial.size', displayName: 'size', width: 120},
            {field: 'rawMaterial.color', displayName: 'color', width: 85},
            {field: 'rawMaterial.description', displayName: 'description', width: 250}]
    };

    $scope.data={};

    $scope.removeFromOrder = function(orderItem) {
        $scope.shoppingCart = shoppingCart.removeFromRawOrderCart(orderItem.entity.rawMaterial.id);
        $scope.data = $scope.shoppingCart;
    };

    $scope.order = function(){
        $scope.NoOrderItemsError = false;
        $scope.SupplierMissingError = false;
        var yourOrder = createOrderObject();

        if($scope.shoppingCart.length <= 0){
            $log.log("Saving an order without items is not allowed. Please ad items to your shopping cart first.");
            $scope.NoOrderItemsError = true;
            return;
        }

        if(yourOrder.supplierId == null || yourOrder.supplierId == undefined || yourOrder.supplierId == "" || isNaN(yourOrder.supplierId)){
            $log.log("Saving an order with no supplier di is not allowed. Please check if you have items in the shopping cart.")
            $scope.SupplierMissingError = true;
            return;
        }

        if($scope.crudType == 'create'){
            $log.log("Saving new order.");
            $log.log(yourOrder);
            httpServiceFactory.createPostPromise('/orders/create', 'userid=' + applicationInformation.getUserinformation().userId, yourOrder, true, applicationInformation.getToken(),true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Order created.");
                    $location.path('/newOrdersOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                    $location.path("/newOrdersOverview");
                });
        }
        else if($scope.crudType == 'edit'){
            $log.log("Saving order with id " + yourOrder.id);
            httpServiceFactory.createPostPromise('/orders/edit', 'userid=' + applicationInformation.getUserinformation().userId, yourOrder, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Order Saved.");
                    $location.path('/newOrdersOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                    $location.path("/newOrdersOverview");
                });
        }

        shoppingCart.closeOrder();
    };

    $scope.keepShopping = function(){
        $location.path("/orderRawOverview");
    };

    $scope.cancelOrder = function(){
        if($scope.crudType == "create"){
            shoppingCart.cancelRawMaterialOrder();
            $location.path("/orderRawOverview");
        }else if($scope.crudType == "edit"){
            shoppingCart.closeOrder();
            $location.path("/newOrdersOverview");
        }
    };

    //create an order so you can send it to the server. new orders and existing orders are different in json structure.
    function createOrderObject(){
        if($scope.crudType == "create"){
            $log.log("Creating order object for new order.");
            var order = {"supplierId":$scope.supplier.id, "note":shoppingCart.getShoppingCartNote().content, "materialmoveItems":[]};
            for(var i = 0; i < $scope.shoppingCart.length; i++){
                order.materialmoveItems.push({"rawMaterialId": $scope.shoppingCart[i].rawMaterial.id ,"amount":$scope.shoppingCart[i].quantity});
            }
            return order;
        }else if($scope.crudType == "edit"){
            var editingOrder = crudService.getEditingOrder();
            $log.log("Creating order object for editing order with id " + editingOrder.id);
            var order = {"id":editingOrder.id, "supplierId":editingOrder.supplierId, "note":shoppingCart.getShoppingCartNote(), "moveItems":[]};
            for(var i = 0; i < $scope.shoppingCart.length; i++){
                order.moveItems.push({"rawMaterialId": $scope.shoppingCart[i].rawMaterial.id ,"amount":$scope.shoppingCart[i].quantity});
            }
            return order;
        }
    }

    function loadSuppliers(){
        if($scope.shoppingCart.length > 0) {
            httpServiceFactory.createGetPromise('/suppliers/byid', 'userid=' + applicationInformation.getUserinformation().userId + "&supplierid=" + $scope.shoppingCart[0].rawMaterial.supplierid, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $scope.supplier = data;
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get suppliers from server.");
                });
        }
    }

    //if you go away from this page you save the note in the shopping cart.
    $scope.$on('$routeChangeStart', function(next, current) {
        shoppingCart.setShoppingCartNote($scope.note);
    });

    function init(){
        //when you get to this page and the shopping cart has no state this means you have not added an item.
        //So you can assume that you are going to edit and load an existing order back into the shopping cart.
        if(shoppingCart.getShoppingcartState() == ""){
            $scope.crudType = "edit";
            var myOrder = order;
            var newShoppingCart = [];
            if(myOrder == null){

            } else if(Object.keys(myOrder).length === 0){

            }else {
                for (var i = 0; i < myOrder.materialmoveItems.length; i++) {
                    newShoppingCart.push({
                        "quantity": myOrder.materialmoveItems[i].amount,
                        "rawMaterial": {
                            "moveId": myOrder.materialmoveItems[i].id,
                            "id": myOrder.materialmoveItems[i].rawMaterial.id,
                            "supplierid": myOrder.materialmoveItems[i].rawMaterial.supplierid,
                            "productCode": myOrder.materialmoveItems[i].rawMaterial.productCode,
                            "price": myOrder.materialmoveItems[i].rawMaterial.price,
                            "minimumorderQuantity": myOrder.materialmoveItems[i].rawMaterial.minimumorderQuantity,
                            "minimumAmountInStock": myOrder.materialmoveItems[i].rawMaterial.minimumAmountInStock,
                            "amountInStock": myOrder.materialmoveItems[i].rawMaterial.amountInStock,
                            "productType": myOrder.materialmoveItems[i].rawMaterial.productType,
                            "size": myOrder.materialmoveItems[i].rawMaterial.size,
                            "color": myOrder.materialmoveItems[i].rawMaterial.color,
                            "description": myOrder.materialmoveItems[i].rawMaterial.description,
                            "lastmodified": myOrder.materialmoveItems[i].rawMaterial.lastmodified,
                            "lastModifiedBy": myOrder.materialmoveItems[i].rawMaterial.lastModifiedBy,
                            "linecolor": myOrder.materialmoveItems[i].rawMaterial.linecolor
                        }
                    });
                }
                shoppingCart.setShoppingCart(newShoppingCart);
                shoppingCart.setShoppingCartNote({"id": myOrder.note.id, "content": myOrder.note.content});
            }
        }else{
            $scope.crudType = shoppingCart.getShoppingcartState();
        }

        $scope.shoppingCart = shoppingCart.getRawMaterialOrderShoppingCart();
        loadSuppliers();
        $scope.data = $scope.shoppingCart;
        $scope.note = shoppingCart.getShoppingCartNote();
    }
    init();
}]);