/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var sizeController = angular.module('clarity.crud');

sizeController.controller('sizeCrudController', ['$scope', '$log', 'size','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, size, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingSize = null;
    var oldEditingSize = null;
    $scope.ValueMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;

        $log.log(oldEditingSize);
        $log.log($scope.editingSize);
        if($scope.editingSize.code == "" || $scope.editingSize.name == ""){
            $scope.ValueMissingError = true;
            $log.log("missingvalue");
            return;
        }
        if(crudOption == 'create'){
            $log.log("create size " + $scope.editingSize);
            httpServiceFactory.createPostPromise('/sizes/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingSize, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Size Saved.");
                    $location.path('/sizeOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('edit size ' + $scope.editingSize);
            httpServiceFactory.createPostPromise('/sizes/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingSize, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Size Saved.");
                    $location.path('/sizeOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function init(){
        $log.log(size);
        $scope.editingSize = size;
        oldEditingSize = angular.copy(size);
        if(crudOption == "create"){
            $scope.editingSize = {"name":"", "code":""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);