/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var sizeController = angular.module('clarity.overview');

sizeController.controller('sizeOverviewController', ['$scope', '$log', 'sizes', 'crudService', '$location', function($scope, $log, sizes, crudService, $location){
    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'code', displayName: 'code', minWidth: 120, width: 120, maxWidth: 200},
            {field: 'name', displayName: 'name', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingSize(rowItem.entity);
            $location.path('/sizeEdit');
        }
    };

    $scope.data={};

    $scope.refresh = function(){
        crudService.getSizes().then(function(data){$scope.data = data.data;});
        $scope.gridOptions.data = 'data';
    };

    $scope.createSize = function(){
        $location.path('/sizeCreate');
    };

    function init() {
        $scope.data = sizes.data;
        $scope.gridOptions.data = 'data';
    }
    init();
}]);