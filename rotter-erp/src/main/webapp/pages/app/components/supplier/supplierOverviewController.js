/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var supplierController = angular.module('clarity.overview');

supplierController.controller('supplierOverviewController', ['$scope', '$log', 'suppliers', 'crudService', '$location', function($scope, $log, suppliers, crudService, $location){
    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'supplierCode', displayName: 'code', minWidth: 100, width: 100, maxWidth: 350},
            {field: 'name', displayName: 'name', minWidth: 200, width: 200, maxWidth: 350},
            {field: 'creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingSupplier(rowItem.entity);
            $location.path('/supplierEdit');
        }
    };

    $scope.data={};

    $scope.refresh = function(){
        crudService.getSuppliers().then(function(data){$scope.data = data.data;});
        $scope.gridOptions.data = 'data';
    };

    $scope.createSupplier = function(){
        $location.path('/supplierCreate');
    };

    function init() {
        $scope.data = suppliers.data;
        $scope.gridOptions.data = 'data';
    }
    init();
}]);