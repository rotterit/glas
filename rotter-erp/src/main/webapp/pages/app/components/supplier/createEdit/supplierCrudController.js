/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var supplierController = angular.module('clarity.crud');

supplierController.controller('supplierCrudController', ['$scope', '$log', 'supplier','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, supplier, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingSupplier = null;
    var oldEditingSupplier = null;
    $scope.ValueMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;

        $log.log(oldEditingSupplier);
        $log.log($scope.editingSupplier);
        if($scope.editingSupplier.supplierCode == "" || $scope.editingSupplier.name == ""){
            $scope.ValueMissingError = true;
            $log.log("missingvalue");
            return;
        }
        if(crudOption == 'create'){
            $log.log("create supplier " + $scope.editingSupplier);
            httpServiceFactory.createPostPromise('/suppliers/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingSupplier, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Supplier Saved.");
                    $location.path('/supplierOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('edit supplier ' + $scope.editingSupplier);
            httpServiceFactory.createPostPromise('/suppliers/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingSupplier, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Supplier Saved.");
                    $location.path('/supplierOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function init(){
        $log.log(supplier);
        $scope.editingSupplier = supplier;
        oldEditingSupplier = angular.copy(supplier);
        if(crudOption == "create"){
            $scope.editingSupplier = {"supplierCode":"", "name":"", "contactnote":"", "suppliernote":"", "generalnote":""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);