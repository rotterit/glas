/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var productionQueueController = angular.module('clarity.overview');

productionQueueController.controller('productionQueueOverviewController', ['$scope', '$log', 'productionQueueItems', 'crudService', '$location', 'httpServiceFactory', 'applicationInformation', 'notificationService', function($scope, $log, productionQueueItems, crudService, $location, httpServiceFactory, applicationInformation, notificationService){
    $scope.productionQueueItemsGrid = {
        data: 'productionQueueItemsGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'amount', displayName: 'amount', width: 120},
            {field: 'productionTime', displayName: 'production time', width: 120},
            {field: '', displayName: '', minWidth:94, width: 94, maxWidth:94, cellTemplate: '<input ng-show="row.entity.stockCorrectionId != null" type="button" class="ngCellText" value="cancel order" ng-click="cancelStockOrder(row.entity)">'},
            {field: 'productCode', displayName: 'product code', width: 150},
            {field: 'productName', displayName: 'product name', width: 150},
            {field: 'customer', displayName: 'customer', width: 150},
            {field: 'percentagePayed', displayName: 'percentage payed', width: 150},
            {field: 'creationDate', displayName: 'creation date', width: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"}],
        selectedItems: []
    };

    $scope.productionQueueItemsGridData = {};


    $scope.refresh = function(){
        crudService.getProductionQueueItems().then(function(data){
            $scope.productionQueueItemsGridData = data.data;
        });
    };

    $scope.createWorksheet = function(){
        $log.log("all the productionQueueItems");
        $log.log($scope.productionQueueItemsGrid.selectedItems);
        crudService.setCreatingWorksheet($scope.productionQueueItemsGrid.selectedItems);
        $location.path("/createWorksheet");
    };

    /*$scope.refresh = function(){
        $scope.productionQueueItemsGridData = [];
        $scope.productionQueueItemsGridData = crudService.getProductionQueueItems().data;
    };*/

    $scope.cancelStockOrder = function(productionItem){
        httpServiceFactory.createPostPromise('/stockcorrections/finished/cancelstockorder', 'userid=' + applicationInformation.getUserinformation().userId + "&stockorderid=" + productionItem.stockCorrectionId, '', true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                notificationService.showMessageNotification("Production queue item deleted.");
                $scope.refresh();
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Could not delete the production queue item.");
                $scope.refresh();
            });
    };

    function init() {
        $scope.productionQueueItemsGridData = productionQueueItems.data;
    }
    init();
}]);