/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var worksheetDetailController = angular.module('clarity.overview');

worksheetDetailController.controller('worksheetDetailController', ['$scope', '$log', 'crudService', '$location', 'httpServiceFactory', 'applicationInformation', 'notificationService', '$routeParams', function($scope, $log, crudService, $location, httpServiceFactory, applicationInformation, notificationService, $routeParams){
    var overviewType = "overview";
    var redirect = "overview";
    $scope.worksheet = {};

    $scope.productsToMakeGrid = {
        data: 'productsToMakeGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div ng-show="row.entity.compromised" style="background-color: #ff9900" class="ngCellText">{{row.rowIndex + 1}}</div><div ng-hide="row.entity.compromised" class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'amount', displayName: 'amount', width: 150},
            {field: 'product', displayName: 'product', width: 150}]
    };

    $scope.productsToMakeGridData = [];

    $scope.rawMaterialsToUseGrid = {
        data: 'rawMaterialsToUseGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {field: 'amount', displayName: 'amount', width: 150},
            {field: 'product', displayName: 'product', width: 150}]
    };

    $scope.rawMaterialsToUseGridData = [];

    $scope.refresh = function(){
        init();
    };

    $scope.back = function(){
        if(redirect == "myworksheets") {
            $location.path("/cutterWorksheetQueueOverview");
        } else if (redirect == "overview"){
            $location.path("/worksheetQueueOverview");
        }
    };

    function init() {
        redirect = $routeParams.redirect;
        $scope.worksheet = crudService.getDetailWorksheet();
        $log.log($scope.worksheet);
        $scope.productsToMakeGridData = $scope.worksheet.stockmove.stockmoveItems;
        $scope.rawMaterialsToUseGridData = $scope.worksheet.materialmove.materialmoveItems;
    }
    init();
}]);