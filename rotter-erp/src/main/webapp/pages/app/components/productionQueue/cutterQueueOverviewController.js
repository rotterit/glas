/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var cutterWorksheetQueueController = angular.module('clarity.overview');

cutterWorksheetQueueController.controller('cutterWorksheetQueueOverviewController', ['$scope', '$log', 'crudService', '$location', 'httpServiceFactory', 'applicationInformation', 'notificationService', function($scope, $log, crudService, $location, httpServiceFactory, applicationInformation, notificationService){
    var overviewType = "overview";

    $scope.worksheetGrid = {
        data: 'worksheetGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div ng-show="row.entity.compromised" style="background-color: #ff9900" class="ngCellText">{{row.rowIndex + 1}}</div><div ng-hide="row.entity.compromised" class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "started",
            width: 120,
            cellTemplate: '<div ng-show="row.entity.startDate == null" style="background-color: #ff0000"><img style="height:28px;" src="assets/img/workingIcon.png" ng-click="startedWorksheet(row.entity)"></div><div ng-show="row.entity.startDate != null" style="background-color: #00ff00"><img style="height:28px;" src="assets/img/workingIcon.png">{{row.entity.startDate.dayOfMonth}} {{row.entity.startDate.month}} {{row.entity.startDate.year}}</div>',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },{
            field: '',
            displayName: "completed",
            width: 120,
            cellTemplate: '<div ng-show="row.entity.finishedDate == null" style="background-color: #ff0000"><img style="height:28px;" src="assets/img/completedIcon.png" ng-click="completedWorksheet(row.entity)"></div><div ng-show="row.entity.finishedDate != null" style="background-color: #00ff00"><img style="height:28px;" src="assets/img/completedIcon.png">{{row.entity.finishedDate.dayOfMonth}} {{row.entity.finishedDate.month}} {{row.entity.finishedDate.year}}</div>',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },
            {field: 'note', displayName: 'note', width: 300},
            {field: 'createDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.createDate.dayOfMonth}} {{row.entity.createDate.month}} {{row.entity.createDate.year}}  {{row.entity.createDate.hour}}:{{row.entity.createDate.minute}}</div>"},
            {field: 'creator', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}  {{row.entity.lastModifiedDate.hour}}:{{row.entity.lastModifiedDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'modified by', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            if(overviewType == "overview") {
                crudService.setDetailWorksheet(rowItem.entity);
                $location.path("/detailWorksheet/myworksheets");
            }
        }
    };

    $scope.worksheetGridData = [];

    $scope.refresh = function(){
        init();
    };

    $scope.startedWorksheet = function(startingWorksheet){
        overviewType = "startingWorksheet";
        httpServiceFactory.createPostPromise('/worksheets/start', 'userid=' + applicationInformation.getUserinformation().userId + "&worksheetid=" + startingWorksheet.id, '', true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                notificationService.showMessageNotification("Worksheet started.");
                $scope.refresh();
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Could not start the worksheet.");
                overviewType = "overview";
            });
    };

    $scope.completedWorksheet = function(completingWorksheet){
        overviewType = "completingWorksheet";
        httpServiceFactory.createPostPromise('/worksheets/complete', 'userid=' + applicationInformation.getUserinformation().userId + "&worksheetid=" + completingWorksheet.id, '', true, applicationInformation.getToken(), true)
            .success(function (data, status, headers, config) {
                notificationService.showMessageNotification("Worksheet completed.");
                $scope.refresh();
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Could not complete the worksheet.");
                overviewType = "overview";
            });
    };

    function loadMyWorksheets(){
        httpServiceFactory.createGetPromise('/worksheets/mine', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.worksheetGridData = data;
                $log.log("my worksheets");
                $log.log($scope.worksheetGridData);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get worksheets from server.");
                $location.path("/landingPage");
            });
    }

    function init() {
        overviewType = "overview";
        $log.log(applicationInformation.getRole());
        if(applicationInformation.getRole() == 4){
            loadMyWorksheets();
        } else {
            notificationService.showErrorNotification("You are not a cutter.");
            $location.path("/landingPage");
        }
    }
    init();
}]);