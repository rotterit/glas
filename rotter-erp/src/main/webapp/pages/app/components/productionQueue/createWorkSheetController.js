/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var createWorksheetController = angular.module('clarity.crud');

createWorksheetController.controller('createWorksheetController', ['$scope', '$log', 'worksheetItems','httpServiceFactory', 'applicationInformation', 'notificationService', '$location', 'crudService', function($scope, $log, worksheetItems, httpServiceFactory, applicationInformation, notificationService, $location, crudService){
    $scope.cutters = [];
    $scope.selectedCutter = {};
    $scope.note=  "";
    $scope.qualities = [];
    $scope.selectedQuality = {};
    $scope.stocks = [];
    $scope.selectedStock = {};
    $scope.amount = "";
    $scope.totalProductionTime = 0;

    $scope.NoCutterSelected = false;
    $scope.NoItemsOnWorksheet = false;
    $scope.NotEnoughProductsLeftError = false;
    $scope.NoQualitySelectedError =  false;
    $scope.NoStockSelectedError = false;

    $scope.worksheetItemsGrid = {
        data: 'worksheetItemsGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 40,
            cellTemplate: '<div class="ngCellText" ng-click="removeWorksheetItem(row.entity)">X</div>',
            sortable: false
        },
            {field: 'amount', displayName: 'amount', width: 120},
            {field: 'productionTime', displayName: 'production time', width: 120},
            {field: 'productCode', displayName: 'product code', width: 150},
            {field: 'productName', displayName: 'product name', width: 150},
            {field: 'customer', displayName: 'customer', width: 150},
            {field: 'percentagePayed', displayName: 'percentage payed', width: 150},
            {field: 'creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        multiSelect: false,
        afterSelectionChange: function(rowItem){
            $scope.updateAvailableStocks();
        }
    };

    $scope.worksheetItemsGridData = [];

    $scope.worksheetProcessedItemsGrid = {
        data: 'worksheetProcessedItemsGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "",
            width: 40,
            cellTemplate: '<div class="ngCellText" ng-click="removeWorksheetProcessedItem(row.entity)">X</div>',
            sortable: false
        },
            {field: 'amount', displayName: 'amount', width: 120},
            {field: 'quality.name', displayName: 'quality', width: 120},
            {field: 'item.productCode', displayName: 'product code', width: 150},
            {field: 'item.productName', displayName: 'product name', width: 150},
            {field: 'item.customer', displayName: 'customer', width: 150},
            {field: 'item.percentagePayed', displayName: 'percentage payed', width: 150},
            {field: 'item.creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'item.createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            /*if(!wantToEdit) {
             loadRawStockDetail(rowItem.entity.id);
             $scope.overviewType = "detailRaw";
             }*/
        }
    };

    $scope.worksheetProcessedItemsGridData = [];

    $scope.saveWorksheet = function(){
        $scope.NoCutterSelected = false;
        $scope.NoItemsOnWorksheet = false;

        if(Object.keys($scope.selectedCutter).length === 0){
            $scope.NoCutterSelected = true;
        } else if($scope.worksheetItemsGridData.length <= 0){
            $scope.NoItemsOnWorksheet = true;
        } else {
            httpServiceFactory.createPostPromise('/worksheets/create', 'userid=' + applicationInformation.getUserinformation().userId, createWorksheet(), true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Worksheet saved.");
                    $location.path('/productionQueueOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification("Could not create your worksheet.");
                });
        }
    };

    function createWorksheet(){
        var worksheet = {"executorid": $scope.selectedCutter.id, "note": $scope.note, "worksheetLineList": []};
        for(var i = 0; i < $scope.worksheetProcessedItemsGridData.length; i++){
            var quality = {"qualityId": $scope.worksheetProcessedItemsGridData[i].quality.id, "amount": $scope.worksheetProcessedItemsGridData[i].amount, "stockId": $scope.worksheetProcessedItemsGridData[i].stock.id};
            if(isProductAlreadyInWorksheetForDb(worksheet.worksheetLineList, $scope.worksheetProcessedItemsGridData[i].item.stockmoveItemId)){
                for(var j = 0; j < worksheet.worksheetLineList.length; j++){
                    if($scope.worksheetProcessedItemsGridData[i].item.stockmoveItemId == worksheet.worksheetLineList[j].stockmoveitemid){
                        worksheet.worksheetLineList[j].qualityLines.push(quality);
                    }
                }
            } else {
                worksheet.worksheetLineList.push({"stockmoveitemid": $scope.worksheetProcessedItemsGridData[i].item.stockmoveItemId, "qualityLines": [quality]});
            }
        }
        return worksheet;
    }

    function isProductAlreadyInWorksheetForDb(worksheetList, stockmoveItemId){
        for(var i = 0; i < worksheetList.length; i++){
            if(worksheetList[i].stockmoveitemid == stockmoveItemId){
                return true;
            }
        }
        return false;
    }

    $scope.addToWorksheetList = function(){
        $scope.NotEnoughProductsLeftError = false;
        $scope.NoQualitySelectedError =  false;
        $scope.NoStockSelectedError = false;

        if(Number($scope.worksheetItemsGrid.selectedItems[0].amount) < $scope.amount){
            $scope.NotEnoughProductsLeftError = true;
        } else if(Object.keys($scope.selectedQuality).length === 0){
            $scope.NoQualitySelectedError =  true;
        } else if(Object.keys($scope.selectedStock).length === 0){
            $scope.NoStockSelectedError = true;
        } else {
            for (var i = 0; i < $scope.worksheetProcessedItemsGridData.length; i++) {
                if ($scope.worksheetProcessedItemsGridData[i].stock.id == $scope.selectedStock.id && $scope.worksheetProcessedItemsGridData[i].quality.id == $scope.selectedQuality.id) {
                    $scope.worksheetProcessedItemsGridData[i].amount = Number($scope.worksheetProcessedItemsGridData[i].amount) + Number($scope.amount);
                    $scope.worksheetItemsGrid.selectedItems[0].amount = Number($scope.worksheetItemsGrid.selectedItems[0].amount) - Number($scope.amount);
                    return;
                }
            }
            $scope.worksheetProcessedItemsGridData.push({
                "item": $scope.worksheetItemsGrid.selectedItems[0],
                "amount": $scope.amount,
                "quality": $scope.selectedQuality,
                "stock": $scope.selectedStock
            });
            $scope.worksheetItemsGrid.selectedItems[0].amount = Number($scope.worksheetItemsGrid.selectedItems[0].amount) - Number($scope.amount);
        }
    };

    $scope.removeWorksheetItem = function(worksheetItem){
        for(var i = 0; i < $scope.worksheetItemsGridData.length; i++){
            if($scope.worksheetItemsGridData[i].stockmoveItemId == worksheetItem.stockmoveItemId){
                $scope.worksheetItemsGridData.splice(i, 1);
            }
        }
        for(var i = 0; i < $scope.worksheetProcessedItemsGridData.length; i++){
            if($scope.worksheetProcessedItemsGridData[i].item.stockmoveItemId == worksheetItem.stockmoveItemId){
                $scope.worksheetProcessedItemsGridData.splice(i, 1);
            }
        }
    };

    $scope.removeWorksheetProcessedItem = function(processedWorksheetItem){
        for(var i = 0; i < $scope.worksheetProcessedItemsGridData.length; i++){
            if($scope.worksheetProcessedItemsGridData[i].item.stockmoveItemId == processedWorksheetItem.item.stockmoveItemId && $scope.worksheetProcessedItemsGridData[i].quality.id == processedWorksheetItem.quality.id){
                for(var j = 0; j < $scope.worksheetItemsGridData.length; j++){
                    if($scope.worksheetItemsGridData[j].stockmoveItemId == processedWorksheetItem.item.stockmoveItemId){
                        $scope.worksheetItemsGridData[j].amount = Number($scope.worksheetItemsGridData[j].amount) + Number(processedWorksheetItem.amount);
                    }
                }
                $scope.worksheetProcessedItemsGridData.splice(i, 1);
            }
        }
    };

    $scope.cancelWorksheet = function(){
        crudService.setCreatingWorksheet(null);
        $location.path("/productionQueueOverview");
    };

    $scope.updateAvailableStocks = function(){
        $log.log("update the stocks");
        $log.log(Object.keys($scope.worksheetItemsGrid.selectedItems[0]).length);
        if(Object.keys($scope.worksheetItemsGrid.selectedItems[0]).length === 0){
            return;
        } else {
            loadAvailibleStocks();
        }
    };

    function loadCutters(){
        httpServiceFactory.createGetPromise('/worksheets/getcutters', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.cutters = data;
                $log.log("all availible cutters.");
                $log.log(data);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get cutters from server.");
            });
    }

    function loadQualities(){
        httpServiceFactory.createGetPromise('/qualities/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.qualities = data;
                $log.log("all availible qualities.");
                $log.log(data);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get qualities from server.");
            });
    }

    function loadAvailibleStocks(){
        httpServiceFactory.createGetPromise('/worksheets/rawstockbyproduct', 'userid=' + applicationInformation.getUserinformation().userId + "&productid=" + $scope.worksheetItemsGrid.selectedItems[0].productid, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.stocks = data;
                $log.log("all availible qualities.");
                $log.log(data);
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get available stocks from server.");
            });
    }

    function init(){
        loadCutters();
        loadQualities();
        $log.log(worksheetItems);
        $scope.worksheetItemsGridData =  worksheetItems;
        for(var i = 0; i < $scope.worksheetItemsGridData.length; i++){
            $scope.totalProductionTime = $scope.totalProductionTime + Number($scope.worksheetItemsGridData[i].productionTime);
        }
    }
    init();
}]);