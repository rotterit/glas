/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var worksheetQueueController = angular.module('clarity.overview');

worksheetQueueController.controller('worksheetQueueOverviewController', ['$scope', '$log', 'crudService', '$location', 'httpServiceFactory', 'applicationInformation', 'notificationService', function($scope, $log, crudService, $location, httpServiceFactory, applicationInformation, notificationService){
    $scope.overviewType = "new";

    $scope.newWorksheetGrid = {
        data: 'newWorksheetGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div ng-show="row.entity.compromised" style="background-color: #ff9900" class="ngCellText">{{row.rowIndex + 1}}</div><div ng-hide="row.entity.compromised" class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "started",
            width: 120,
            cellTemplate: '<div ng-show="row.entity.startDate == null" style="background-color: #ff0000"><img style="height:28px;" src="assets/img/workingIcon.png"></div><div ng-show="row.entity.startDate != null" style="background-color: #00ff00"><img style="height:28px;" src="assets/img/workingIcon.png">{{row.entity.startDate.dayOfMonth}} {{row.entity.startDate.month}} {{row.entity.startDate.year}}</div>',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },{
            field: '',
            displayName: "completed",
            width: 120,
            cellTemplate: '<div ng-show="row.entity.finishedDate == null" style="background-color: #ff0000"><img style="height:28px;" src="assets/img/completedIcon.png"></div><div ng-show="row.entity.finishedDate != null" style="background-color: #00ff00"><img style="height:28px;" src="assets/img/completedIcon.png">{{row.entity.finishedDate.dayOfMonth}} {{row.entity.finishedDate.month}} {{row.entity.finishedDate.year}}</div>',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },
            {field: 'executor', displayName: 'cutter', width: 120},
            {field: 'createDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.createDate.dayOfMonth}} {{row.entity.createDate.month}} {{row.entity.createDate.year}}  {{row.entity.createDate.hour}}:{{row.entity.createDate.minute}}</div>"},
            {field: 'creator', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}  {{row.entity.lastModifiedDate.hour}}:{{row.entity.lastModifiedDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'modified by', minWidth: 120, width: 120, maxWidth: 350}],
        afterSelectionChange: function(rowItem) {
            crudService.setDetailWorksheet(rowItem.entity);
            $location.path("/detailWorksheet/overview");
        }
    };

    $scope.newWorksheetGridData = [];

    $scope.startedWorksheetGrid = {
        data: 'startedWorksheetGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div ng-show="row.entity.compromised" style="background-color: #ff9900" class="ngCellText">{{row.rowIndex + 1}}</div><div ng-hide="row.entity.compromised" class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "started",
            width: 120,
            cellTemplate: '<div ng-show="row.entity.startDate == null" style="background-color: #ff0000"><img style="height:28px;" src="assets/img/workingIcon.png"></div><div ng-show="row.entity.startDate != null" style="background-color: #00ff00"><img style="height:28px;" src="assets/img/workingIcon.png">{{row.entity.startDate.dayOfMonth}} {{row.entity.startDate.month}} {{row.entity.startDate.year}}</div>',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },{
            field: '',
            displayName: "completed",
            width: 120,
            cellTemplate: '<div ng-show="row.entity.finishedDate == null" style="background-color: #ff0000"><img style="height:28px;" src="assets/img/completedIcon.png"></div><div ng-show="row.entity.finishedDate != null" style="background-color: #00ff00"><img style="height:28px;" src="assets/img/completedIcon.png">{{row.entity.finishedDate.dayOfMonth}} {{row.entity.finishedDate.month}} {{row.entity.finishedDate.year}}</div>',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },
            {field: 'executor', displayName: 'cutter', width: 120},
            {field: 'createDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.createDate.dayOfMonth}} {{row.entity.createDate.month}} {{row.entity.createDate.year}}  {{row.entity.createDate.hour}}:{{row.entity.createDate.minute}}</div>"},
            {field: 'creator', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}  {{row.entity.lastModifiedDate.hour}}:{{row.entity.lastModifiedDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'modified by', minWidth: 120, width: 120, maxWidth: 350}],
        afterSelectionChange: function(rowItem) {
            crudService.setDetailWorksheet(rowItem.entity);
            $location.path("/detailWorksheet/overview");
        }
    };

    $scope.startedWorksheetGridData = [];

    $scope.finishedWorksheetGrid = {
        data: 'finishedWorksheetGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div ng-show="row.entity.compromised" style="background-color: #ff9900" class="ngCellText">{{row.rowIndex + 1}}</div><div ng-hide="row.entity.compromised" class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },{
            field: '',
            displayName: "started",
            width: 120,
            cellTemplate: '<div ng-show="row.entity.startDate == null" style="background-color: #ff0000"><img style="height:28px;" src="assets/img/workingIcon.png"></div><div ng-show="row.entity.startDate != null" style="background-color: #00ff00"><img style="height:28px;" src="assets/img/workingIcon.png">{{row.entity.startDate.dayOfMonth}} {{row.entity.startDate.month}} {{row.entity.startDate.year}}</div>',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },{
            field: '',
            displayName: "completed",
            width: 120,
            cellTemplate: '<div ng-show="row.entity.finishedDate == null" style="background-color: #ff0000"><img style="height:28px;" src="assets/img/completedIcon.png"></div><div ng-show="row.entity.finishedDate != null" style="background-color: #00ff00"><img style="height:28px;" src="assets/img/completedIcon.png">{{row.entity.finishedDate.dayOfMonth}} {{row.entity.finishedDate.month}} {{row.entity.finishedDate.year}}</div>',
            sortable: false, pinned: true, pinnable: false, enableRowSelection: false
        },
            {field: 'executor', displayName: 'cutter', width: 120},
            {field: 'createDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.createDate.dayOfMonth}} {{row.entity.createDate.month}} {{row.entity.createDate.year}}  {{row.entity.createDate.hour}}:{{row.entity.createDate.minute}}</div>"},
            {field: 'creator', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}  {{row.entity.lastModifiedDate.hour}}:{{row.entity.lastModifiedDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'modified by', minWidth: 120, width: 120, maxWidth: 350}],
        afterSelectionChange: function(rowItem) {
            crudService.setDetailWorksheet(rowItem.entity);
            $location.path("/detailWorksheet/overview");
        }
    };

    $scope.finishedWorksheetGridData = [];


    $scope.refresh = function(){
        init();
    };

    function loadNewWorksheets(){
        httpServiceFactory.createGetPromise('/worksheets/new', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.newWorksheetGridData = data;
                $log.log("all available new worksheets.");
                $log.log(data);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get new worksheets from server.");
            });
    }

    function loadStartedWorksheets(){
        httpServiceFactory.createGetPromise('/worksheets/started', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.startedWorksheetGridData = data;
                $log.log("all available started worksheets.");
                $log.log(data);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get started worksheets from server.");
            });
    }

    function loadCompletedWorksheets(){
        httpServiceFactory.createGetPromise('/worksheets/completed', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.finishedWorksheetGridData = data;
                $log.log("all available completed worksheets.");
                $log.log(data);
            })
            .error(function (error, status) {
                $log.log(error);
                notificationService.showErrorNotification(status + ": oops... Could not get completed worksheets from server.");
            });
    }

    $scope.showNewWorksheets = function(){
        $scope.overviewType = "new";
        loadNewWorksheets();
    };
    $scope.showStartedWorksheets = function(){
        $scope.overviewType = "started";
        loadStartedWorksheets();
    };
    $scope.showFinishedWorksheets = function(){
        $scope.overviewType = "finished";
        loadCompletedWorksheets();
    };

    function init() {
        $scope.overviewType = "new";
        loadNewWorksheets();
    }
    init();
}]);