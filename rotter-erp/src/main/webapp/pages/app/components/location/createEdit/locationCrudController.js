/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var locationController = angular.module('clarity.crud');

locationController.controller('locationCrudController', ['$scope', '$log', 'location','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, location, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingLocation = null;
    var oldEditingLocation = null;
    $scope.ValueMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;

        if(($scope.editingLocation.data == "" && $scope.crudType == "create") || ($scope.editingLocation.description == "" && $scope.crudType == "edit")){
            $scope.ValueMissingError = true;
            $log.log("missingvalue");
            return;
        }
        if(crudOption == 'create'){
            $log.log("create location " + $scope.editingLocation);
            httpServiceFactory.createPostPromise('/locations/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingLocation, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Location Saved.");
                    $location.path('/locationOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('edit location ' + $scope.editingLocation);
            httpServiceFactory.createPostPromise('/locations/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingLocation, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Location Saved.");
                    $location.path('/locationOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function init(){
        $log.log(location);
        $scope.editingLocation = location;
        oldEditingLocation = angular.copy(location);
        if(crudOption == "create"){
            $scope.editingLocation = {"data":""};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);