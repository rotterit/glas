/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var colorController = angular.module('clarity.overview');

colorController.controller('colorOverviewController', ['$scope', '$log', 'colors', 'crudService', '$location', function($scope, $log, colors, crudService, $location){
    $scope.gridOptions = {
        data: 'data',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "pos",
            minWidth: 50,
            width: 50,
            maxWidth: 100,
            cellTemplate: '<div class="ngCellText">{{row.rowIndex + 1}}</div>',
            sortable: false
        },
            {
                field: 'value',
                displayName: "color",
                minWidth: 70,
                width: 70,
                maxWidth: 70,
                enableColumnResize: false,
                cellTemplate: '<div class="ngCellText" style="background-color: {{row.getProperty(col.field)}}">{{row.getProperty(col.field)}}</div>',
                sortable: false
            },
            {field: 'code', displayName: 'code', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'name', displayName: 'name', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'creationDate', displayName: 'creation date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'lastModifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            crudService.setEditingColor(rowItem.entity);
            $location.path('/colorEdit');
        }
    };

    $scope.data={};

    $scope.refresh = function(){
        crudService.getColors().then(function(data){$scope.data = data.data;});
        //$scope.gridOptions.data = 'data';
    };

    $scope.createColor = function(){
        $location.path('/colorCreate');
    };

    function init() {
        $scope.data = colors.data;
        //$scope.gridOptions.data = 'data';
    }
    init();
}]);