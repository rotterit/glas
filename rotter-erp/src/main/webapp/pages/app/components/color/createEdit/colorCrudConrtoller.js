/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var colorController = angular.module('clarity.crud');

colorController.controller('colorCrudController', ['$scope', '$log', 'color','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', function($scope, $log, color, httpServiceFactory, applicationInformation, crudOption, notificationService, $location){
    $scope.editingColor = null;
    var oldEditingColor = null;
    $scope.colorValueFormatError = false;
    $scope.ValueMissingError = false;
    $scope.crudType = "create";

    $scope.save = function(){
        $scope.ValueMissingError = false;
        $scope.colorValueFormatError = false;

        $log.log(oldEditingColor);
        $log.log($scope.editingColor);
        if($scope.editingColor.code == "" || $scope.editingColor.name == ""){
            $scope.ValueMissingError = true;
            $log.log("missingvalue");
            return;
        }
        if(checkColorValue()){
            $scope.colorValueFormatError = true;
            $log.log("wrong format");
            return;
        }
        if(crudOption == 'create'){
            $log.log("create color " + $scope.editingColor);
            httpServiceFactory.createPostPromise('/colors/create', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingColor, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Color Saved.");
                    $location.path('/colorOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
        else if(crudOption == 'edit'){
            $log.log('edit color ' + $scope.editingColor);
            httpServiceFactory.createPostPromise('/colors/edit', 'userid=' + applicationInformation.getUserinformation().userId, $scope.editingColor, true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    $log.log(status);
                    notificationService.showMessageNotification("Color Saved.");
                    $location.path('/colorOverview');
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status +  error);
                });
        }
    };

    function checkColorValue(){
        var regWith = /^#[0-9a-fA-F]{6}$/;
        var regWithout = /^[0-9a-fA-F]{6}$/;

        if($scope.editingColor.value.indexOf("#") == 0 && regWith.test($scope.editingColor.value)){
            return false;
        }else if($scope.editingColor.value.length == 6 && regWithout.test($scope.editingColor.value)){
            $scope.editingColor.value = '#'.concat($scope.editingColor.value);
            return false;
        }else{
            return true;
        }
    }

    function init(){
        $log.log(color);
        $scope.editingColor = color;
        oldEditingColor = angular.copy(color);
        if(crudOption == "create"){
            $scope.editingColor = {"name":"", "code":"", "value":"#"};
        }
        $scope.crudType = crudOption;
    }
    init();
}]);