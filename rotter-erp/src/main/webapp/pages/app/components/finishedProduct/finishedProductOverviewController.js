/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: class responsible for the raw material overview page.This has to handle all the logic that is not in a directive like the search directive.
 */
'use strict';
var finishedProductOverviewController = angular.module('clarity.material.finished');

finishedProductOverviewController.controller('finishedProductOverviewCtrl', ['$scope', '$log', '$location', 'copypasterinoService', 'httpServiceFactory', 'applicationInformation', 'searchPopulationService', 'crudService', 'state', 'invoiceShoppingCart', function($scope, $log, $location, copypasterinoService, httpServiceFactory, applicationInformation, searchPopulationService, crudService, state, invoiceShoppingCart){
    $scope.overviewType = state;
    $scope.invoiceAmount = {"amount": ""};
    $scope.invoiceDetailInfo = invoiceShoppingCart.getInvoiceDetailLineInformation();
    $scope.searchPopulationData = {};

    //if overview type is 'overview'.
    $scope.finishedProductGrid = {
        data: 'finishedProductsSearchResultsGridData',
        enablePinning: true,
        enableColumnResize: true,
        columnDefs: [{
            field: 'linecolor',
            displayName: "pos",
            width: 50,
            cellTemplate: '<div class="ngCellText" style="background-color: {{row.getProperty(col.field)}}">{{row.rowIndex + 1}}</div>',
            pinned: true,
            pinnable: false,
            sortable: false
        },
            {field: 'productCode', displayName: 'code', width: 130, pinned: true},
            {field: 'productTypeName', displayName: 'pro. type', width: 120},
            {field: 'designName', displayName: 'design', width: 120},
            {field: 'sizeCode', displayName: 'size', width: 120},
            {field: 'color', displayName: 'color', width: 95},
            {field: 'stockAmount', displayName: 'amount in stock', width: 137},
            {field: 'reservedStock', displayName: 'reserved amount', width: 145},
            {field: 'price', displayName: 'price', width: 95, cellTemplate: '<div class="ngCellText">{{row.entity.price.toFixed(2)}}</div>'},
            {field: 'baseTime', displayName: 'base time', width: 95},
            {field: 'lastModifiedDate', displayName: 'last modified date', width: 150, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"},
            {field: 'modifiedBy', displayName: 'last modified by', width: 120}],
        selectedItems: [],
        afterSelectionChange: function(rowItem){
            if($scope.overviewType == "overview"){
                crudService.setFinishedProductDetail(rowItem.entity);
                $location.path('/finishedProductDetail');
            }
        }
    };

    $scope.invoiceGrid = {
        data: 'finishedProductsSearchResultsGridData',
        enablePinning: true,
        enableColumnResize: true,
        columnDefs: [{
            field: 'linecolor',
            displayName: "pos",
            width: 50,
            cellTemplate: '<div class="ngCellText" style="background-color: {{row.getProperty(col.field)}}">{{row.rowIndex + 1}}</div>',
            pinned: true,
            pinnable: false,
            sortable: false
        },
            {field: '', displayName: '', width: 100, pinned: true, pinnable: false, cellTemplate: '<input class="ngCellText" type="button" value="add to invoice" ng-click="addToInvoice(row.entity)">'},
            {field: 'productCode', displayName: 'code', width: 120, pinnable: false, pinned: true},
            {field: 'productType', displayName: 'pro. type', width: 120},
            {field: 'size', displayName: 'size', width: 120},
            {field: 'color', displayName: 'color', width: 120},
            {field: 'description', displayName: 'description', width: 120},
            {field: 'amountInStock', displayName: 'amount in stock', width: 120},
            {field: 'minimumAmountInStock', displayName: 'base stock amount', width: 120},
            {field: 'price', displayName: 'price', width: 120},
            {field: 'minimumorderQuantity', displayName: 'minimum order quantity', width: 120},
            {field: 'lastModifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'lastModifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}]
    };

    $scope.finishedProductsSearchResultsGridData = [];

    $scope.csvData = "";
    $scope.getCsvData = function(){
        $log.log("finsihed products list.");
        $log.log($scope.finishedProductsSearchResultsGridData);
        $scope.csvData = "pos.,productCode,productType,design,size,color,amount in stock,amount reserved,price\n";
        for(var i = 0; i < $scope.finishedProductsSearchResultsGridData.length; i++){
            $scope.csvData = $scope.csvData.concat(i + 1).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.finishedProductsSearchResultsGridData[i].productCode).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.finishedProductsSearchResultsGridData[i].productTypeName).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.finishedProductsSearchResultsGridData[i].designName).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.finishedProductsSearchResultsGridData[i].sizeCode).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.finishedProductsSearchResultsGridData[i].color).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.finishedProductsSearchResultsGridData[i].stockAmount).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.finishedProductsSearchResultsGridData[i].reservedStock).concat(",");
            $scope.csvData = $scope.csvData.concat($scope.finishedProductsSearchResultsGridData[i].price).concat("\n");
        }
        copypasterinoService.showText($scope.csvData);
        $scope.csvData = "";
    };

    $scope.editFinishedProduct = function(){
        $scope.overviewType = "edit";
    };


    /*$scope.updateGrid=function(gridData){
        $scope.data = gridData;
        $scope.gridOptions.data = 'data';
    };*/

    //if overview type is 'order'.
    /*$scope.gridOrderOptions = {
        data: 'data',
        enablePinning: true,
        enableColumnResize: true,
        columnDefs: [{
            field: 'linecolor',
            displayName: "pos",
            width: 50,
            cellTemplate: '<div class="ngCellText" style="background-color: {{row.getProperty(col.field)}}">{{row.rowIndex + 1}}</div>',
            pinned: true,
            pinnable: false,
            sortable: false
        },
            {field: '', displayName: '', width: 92, pinned: true, pinnable: false, cellTemplate: '<input class="ngCellText" type="button" value="add to order" ng-click="addToOrder(row)">'},
            {field: 'productCode', displayName: 'id', width: 120, pinnable: false, pinned: true},
            {field: 'productType', displayName: 'pro. type', width: 120},
            {field: 'size', displayName: 'size', width: 120},
            {field: 'color', displayName: 'color', width: 120},
            {field: 'description', displayName: 'description', width: 120},
            {field: 'amountInStock', displayName: 'amount in stock', width: 120},
            {field: 'minimumAmountInStock', displayName: 'base stock amount', width: 120},
            {field: 'price', displayName: 'price', width: 120},
            {field: 'minimumorderQuantity', displayName: 'minimum order quantity', width: 120},
            {field: 'lastModifiedDate', displayName: 'last modified date', minWidth: 180, width: 180, maxWidth: 180, cellTemplate:"<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}  {{row.entity.creationDate.hour}}:{{row.entity.creationDate.minute}}</div>"},
            {field: 'lastModifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}]
    };*/

    /*$scope.addToOrder = function(row) {
        $log.log("---------------------");
        $log.log(row.entity);
        shoppingCart.addToRawOrderCart(row.entity, $scope.orderAmount.rawMaterialAmount);
        $scope.orderAmount.rawMaterialAmount = "";
        $scope.orderDetailInfo = shoppingCart.getRawOrderDetailLine();
    };*/

    /*$scope.openOrderCart = function(){
        $location.path("/createOrderRaw");
    };*/

    $scope.addToInvoice = function(row){
        $log.log("--------------adding to invoice");
        $log.log(row);
        invoiceShoppingCart.addItemToInvoice(row, $scope.invoiceAmount.amount);
        $scope.invoiceAmount.amount = "";
        $scope.invoiceDetailInfo = invoiceShoppingCart.getInvoiceDetailLineInformation();
    };

    $scope.goToInvoice = function(){
        $location.path("/createInvoice");

    };

    //if you go away from this page you save the note in the shopping cart.
    $scope.$on('$locationChangeStart', function(event) {
        $log.log($location.path());
        if($scope.overviewType == "invoice") {
            if ($location.path() == "/invoiceShoppingCart" || $location.path() == "/createInvoice") {
                //go to the page.
            } else {
                if (confirm("You are leaving the invoice creation. Trudy is not happy about that. Are you sure you want to leave. !All invoice information will be lost!") == true) {
                    invoiceShoppingCart.cancelInvoice();
                } else {
                    event.preventDefault();
                }
            }
        }
    });

    function loadSearchPopulation(){
        httpServiceFactory.createGetPromise('/products/searchterms', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
            .success(function(data, status, headers, config){
                $log.log("search population data for finished products");
                $log.log(data);
                $scope.searchPopulationData = data;
            })
            .error(function(error, status){
                $log.log(error);
            })
    }

    function init(){
        $log.log('init finished products search params');
        loadSearchPopulation();
    }
    init();
}]);