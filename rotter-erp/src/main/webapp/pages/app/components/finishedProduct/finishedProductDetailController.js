/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var finishedProductDetailController = angular.module('clarity.crud');

finishedProductDetailController.controller('finishedProductDetailController', ['$scope', '$log', '$document','httpServiceFactory', 'applicationInformation', 'notificationService', '$location', 'finishedProduct', 'crudService', function($scope, $log, $document, httpServiceFactory, applicationInformation, notificationService, $location, finishedProduct, crudService){
    $scope.finishedProduct = {};
    $scope.editingFinishedProduct = {};
    $scope.ValueMissingError = false;
    $scope.isNaNError = false;
    $scope.StockMissingError = false;
    $scope.crudType = "detail";

    $scope.stocks = [];
    $scope.selectedStock = {};

    //edit
    $scope.startEdit = function(){
        $scope.crudType = "edit";
        $scope.editingFinishedProduct = angular.copy($scope.finishedProduct);
    };

    $scope.cancelEdit = function(){
        $scope.editingFinishedProduct = {};
        $scope.crudType = "detail";
    };

    $scope.save = function() {
        $scope.ValueMissingError = false;
        $scope.isNaNError = false;

        if ($scope.editingFinishedProduct.name == "" || $scope.editingFinishedProduct.minTime == "" || $scope.editingFinishedProduct.baseTime == "" || $scope.editingFinishedProduct.maxTime == "" || $scope.editingFinishedProduct.price == "") {
            $scope.ValueMissingError = true;
        } else if(isNaN($scope.editingFinishedProduct.price) || isNaN($scope.editingFinishedProduct.minTime) || isNaN($scope.editingFinishedProduct.baseTime) || isNaN($scope.editingFinishedProduct.maxTime)){
            $scope.isNaNError = true;
        } else {
            $log.log(createEditedFinishedProductObject());
            httpServiceFactory.createPostPromise('/products/edit', 'userid=' + applicationInformation.getUserinformation().userId, createEditedFinishedProductObject(), true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("Edited finished product saved.");
                    $scope.cancelEdit();
                    $location.path('/finishedProductOverview');
                })
                .error(function (error, status) {
                    notificationService.showErrorNotification("Your product could not be saved.");
                });
        }
    };

    function createEditedFinishedProductObject(){
        var newEditedFinishedProduct = {};
        newEditedFinishedProduct.id = $scope.editingFinishedProduct.id;
        newEditedFinishedProduct.name = $scope.editingFinishedProduct.name;
        newEditedFinishedProduct.baseTime = Number($scope.editingFinishedProduct.baseTime);
        newEditedFinishedProduct.minTime = Number($scope.editingFinishedProduct.minTime);
        newEditedFinishedProduct.maxTime = Number($scope.editingFinishedProduct.maxTime);
        newEditedFinishedProduct.price = Number($scope.editingFinishedProduct.price);
        return newEditedFinishedProduct;
    }

    //adding stocks to store shit.
    function loadAvailableStocks(){
        httpServiceFactory.createGetPromise('/products/freestocks', 'userid=' + applicationInformation.getUserinformation().userId + "&productid=" + $scope.finishedProduct.id, undefined, true, applicationInformation.getToken())
            .success(function (data, status, headers, config) {
                $scope.stocks = data;
            })
            .error(function (error, status) {
                notificationService.showErrorNotification("Oops... Could not get stocks from server.");
                $scope.cancelAddingStockLocation();
            });
    }

    $scope.addStockLocation = function(){
        $scope.crudType = "addStock";
        loadAvailableStocks();
    };

    $scope.addStockOrder = function(){
        crudService.setStockCreationFinishedProduct($scope.finishedProduct);
        $location.path("/finishedProductStockOrder");
    };

    $scope.saveNewStockLocation = function(){
        $scope.StockMissingError = false;

        if(Object.keys($scope.selectedStock).length === 0){
            $scope.StockMissingError = true;
        }else {
            httpServiceFactory.createPostPromise('/products/addstock', 'userid=' + applicationInformation.getUserinformation().userId + "&stockid=" + $scope.selectedStock.id + "&productid=" + $scope.finishedProduct.id, createEditedFinishedProductObject(), true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    notificationService.showMessageNotification("A new stock location is added to the finished product.");
                    $scope.cancelAddingStockLocation();
                })
                .error(function (error, status) {
                    notificationService.showErrorNotification("Your new stock location could not be saved.");
                    $scope.cancelAddingStockLocation();
                });
        }
    };

    $scope.cancelAddingStockLocation = function(){
        $scope.crudType = "detail";
        $scope.stocks = [];
        $scope.selectedStock = {};
    };

    //making stock correction after you fucked up.
    $scope.makeStockCorrection = function(){
        crudService.setCorrectingFinishedProduct($scope.finishedProduct);
        $location.path("/finishedProductStockCorrection");
    };

    function init(){
        $scope.finishedProduct = finishedProduct;
    }
    init();
}]);