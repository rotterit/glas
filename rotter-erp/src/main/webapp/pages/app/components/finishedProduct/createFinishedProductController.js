/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';

var createFinishedProductController = angular.module('clarity.crud');

createFinishedProductController.controller('createFinishedProductController', ['$scope', '$log', '$document','httpServiceFactory', 'applicationInformation', 'crudOption', 'notificationService', '$location', 'productTypes', 'sizes', 'suppliers', 'colors', 'stocks', 'categories', 'manufacturingTypes', 'designs', function($scope, $log, $document, httpServiceFactory, applicationInformation, crudOption, notificationService, $location, productTypes, sizes, suppliers, colors, stocks, categories, manufacturingTypes, designs) {
    $scope.availibleProductTypes = productTypes.data;
    $scope.selectedProductType = {};
    $scope.availibleSizes = sizes.data;
    $scope.selectedSize = {};
    $scope.availibleSuppliers = suppliers.data;
    $scope.selectedSupplier = {};
    $scope.availibleColors = colors.data;
    $scope.availibleStocks = stocks.data;
    $scope.selectedStock = {};
    $scope.availibleCategories = categories.data;
    $scope.selectedCategory = {};
    $scope.advicedCategory = {};
    $scope.haveAdviceedCategory = false;
    $scope.availibleManufacturingTypes = manufacturingTypes.data;
    $scope.selectedManufacturingType = {};
    $scope.availibleDesigns = designs.data;
    $scope.selectedDesign = {};
    $scope.description = {"languageCode": "", "languageName": "", "description": ""};
    $scope.descriptions = {"colors": []};
    $scope.productInfo = {"name": "", "minTime": "", "baseTime": "", "maxTime": ""};
    $scope.useSuggestedCategory = false;

    $scope.selectedColor = [];
    $scope.showDescriptionInput = true;

    $scope.NoColorSelectedError = false;
    $scope.LanguageCodeFormatError = false;
    $scope.MissingDescriptionValuesError = false;
    $scope.DescriptionAlreadyExistsError = false;

    $scope.NoProductTypeSelectedError = false;
    $scope.NoSizeSelectedError = false;
    $scope.NoSupplierSelectedError = false;
    $scope.NoDesignSelectedError = false;
    $scope.NoCategorySelectedError = false;
    $scope.NoManufacturingTypeSelectedError = false;
    $scope.NoStockAddedError = false;
    $scope.AmountOfColorsAndDescriptionsMismatchError = false;
    $scope.NoColorsSelectedError = false;
    $scope.MissingDescriptionError = false;
    $scope.MissingProductInforError = false;
    $scope.ProductInfoFormatError = false;
    $scope.NotAllColorsHaveAPriceError = false;

    var numberOfSavedProducts = 0;
    var numberOfFailedSaves = 0;

    $scope.selectedColorGrid = {
        data: 'selectedColorGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [
            {field: 'name', displayName: 'color', minWidth: 120, width: 120, maxWidth: 350},
            {field: 'price', displayName: 'price', minWidth: 120, width: 200, maxWidth: 350, enableCellEdit: true},
            {field: 'languages', displayName: 'languages', minWidth: 120, width: 200}],
        selectedItems: $scope.selectedColor,
        multiSelect: false,
        afterSelectionChange: function (rowItem) {
            for (var i = 0; i < $scope.descriptions.colors.length; i++) {
                if ($scope.descriptions.colors[i].color.id == rowItem.entity.id) {
                    $scope.descriptionsGridData = $scope.descriptions.colors[i].descriptions;
                }
            }
        }
    };

    $scope.selectedColorGridData = [];

    $scope.updateColor = function ($event, color) {
        if ($event.target.checked) {
            color.supplierCode = "";
            color.price = "";
            color.MOQ = "";
            color.baseAmount = "";
            color.note = "";
            color.languages = "";
            $scope.selectedColorGridData.push(color);
            $scope.descriptions.colors.push({"color": color, "descriptions": []});
            addStandardDescriptions(color.id, color.name);
        } else {
            $scope.selectedColorGrid.selectedItems.length = 0;
            for (var i = 0; i < $scope.selectedColorGridData.length; i++) {
                if ($scope.selectedColorGridData[i].id == color.id) {
                    $scope.selectedColorGridData.splice(i, 1);
                }
            }
            for (var i = 0; i < $scope.descriptions.colors.length; i++) {
                if ($scope.descriptions.colors[i].color.id == color.id) {
                    $scope.descriptions.colors.splice(i, 1);
                }
            }
        }
    };

    $scope.selectAllColors = function () {
        $scope.selectedColorGridData = [];
        for (var i = 0; i < $scope.availibleColors.length; i++) {
            var colorCheckbox = $document.find('#cl' + $scope.availibleColors[i].id);
            colorCheckbox[0].checked = true;
            var color = angular.copy($scope.availibleColors[i]);
            color.supplierCode = "";
            color.price = "";
            color.MOQ = "";
            color.baseAmount = "";
            color.note = "";
            color.languages = "";
            $scope.selectedColorGridData.push(color);
            if (!isColorAlreadyInDescriptions(color.id)) {
                $scope.descriptions.colors.push({"color": color, "descriptions": []});
                addStandardDescriptions(color.id, color.name);
            }
        }
        checkAllColorsInColorLanguageCode();
    };

    function isColorAlreadyInDescriptions(colorId) {
        for (var i = 0; i < $scope.descriptions.colors.length; i++) {
            if ($scope.descriptions.colors[i].color.id == colorId) {
                return true;
            }
        }
        return false;
    }

    $scope.deselectAllColors = function () {
        for (var i = 0; i < $scope.availibleColors.length; i++) {
            var colorCheckbox = $document.find('#cl' + $scope.availibleColors[i].id);
            colorCheckbox[0].checked = false;
        }
        $scope.selectedColorGridData = [];
        $scope.description = {"languageCode": "", "languageName": "", "description": ""};
        $scope.descriptions = {"colors": []};
        $scope.descriptionsGridData = [];
        $scope.selectedColorGrid.selectedItems.length = 0;
    };

    function checkAllColorsInColorLanguageCode(){
        for(var i = 0; i < $scope.selectedColorGridData.length; i++){
            for(var j = 0; j < $scope.descriptions.colors.length; j++){
                if($scope.descriptions.colors[j].color.id == $scope.selectedColorGridData[i].id){
                    for(var k = 0; k < $scope.descriptions.colors[j].descriptions.length; k++){
                        if($scope.selectedColorGridData[i].languages.indexOf($scope.descriptions.colors[j].descriptions[k].languageCode) == -1){
                            $scope.selectedColorGridData[i].languages = $scope.selectedColorGridData[i].languages + $scope.descriptions.colors[j].descriptions[k].languageCode + ",";
                        }
                    }
                }
            }
        }
    }

    $scope.stockGrid = {
        data: 'stockGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "",
            width: 25,
            cellTemplate: '<input class="ngCellText" type="button" value="x" ng-click="removeStock(row)">',
            sortable: false
        },
            {field: 'name', displayName: 'name', width: 180},
            {field: 'location', displayName: 'location', width: 180},
            {
                field: 'creationDate',
                displayName: 'creation date',
                minWidth: 180,
                width: 180,
                maxWidth: 180,
                cellTemplate: "<div class='ngCellText'>{{row.entity.creationDate.dayOfMonth}} {{row.entity.creationDate.month}} {{row.entity.creationDate.year}}</div>"
            },
            {field: 'createdBy', displayName: 'created by', minWidth: 120, width: 120, maxWidth: 350},
            {
                field: 'lastModifiedDate',
                displayName: 'last modified date',
                minWidth: 180,
                width: 180,
                maxWidth: 180,
                cellTemplate: "<div class='ngCellText'>{{row.entity.lastModifiedDate.dayOfMonth}} {{row.entity.lastModifiedDate.month}} {{row.entity.lastModifiedDate.year}}</div>"
            },
            {field: 'modifiedBy', displayName: 'last modified by', minWidth: 120, width: 120, maxWidth: 350}]
    };

    $scope.stockGridData = [];

    $scope.changeStockSelection = function (stock) {
        for (var i = 0; i < $scope.stockGridData.length; i++) {
            if ($scope.stockGridData[i].id == stock.id) {
                return;
            }
        }
        $scope.stockGridData.push(stock);
    };

    $scope.removeStock = function (stock) {
        for (var i = 0; i < $scope.stockGridData.length; i++) {
            if ($scope.stockGridData[i].id == stock.entity.id) {
                $scope.stockGridData.splice(i, 1);
                return;
            }
        }
    };

    $scope.descriptionsGrid = {
        data: 'descriptionsGridData',
        enablePinning: false,
        enableColumnResize: true,
        columnDefs: [{
            field: '',
            displayName: "",
            width: 25,
            cellTemplate: '<input class="ngCellText" type="button" value="x" ng-click="removeDescription(row)">',
            sortable: false
        },
            {field: 'languageCode', displayName: 'language code', width: 250},
            {field: 'languageName', displayName: 'language name', width: 350},
            {field: 'description', displayName: 'description', minWidth: 180}]
    };

    $scope.descriptionsGridData = [];

    $scope.addDescription = function () {
        $scope.NoColorSelectedError = false;
        $scope.LanguageCodeFormatError = false;
        $scope.MissingDescriptionValuesError = false;
        $scope.DescriptionAlreadyExistsError = false;

        $log.log("descriptions");
        $log.log($scope.descriptions);
        $log.log($scope.selectedColor);

        if ($scope.selectedColor == undefined || $scope.selectedColor == null || $scope.selectedColor == "" || $scope.selectedColor == {} || $scope.selectedColor == []) {
            $scope.NoColorSelectedError = true;
        } else if ($scope.selectedColor[0].id == null || $scope.selectedColor[0].id == undefined || $scope.selectedColor[0].id == "") {
            $scope.NoColorSelectedError = true;
        } else if ($scope.description.languageCode == "" || $scope.description.languageCode.length != 2) {
            $scope.LanguageCodeFormatError = true;
        } else if ($scope.description.languageName == "" || $scope.description.description == "") {
            $scope.MissingDescriptionValuesError = true;
        } else if (isDescriptionAlreadyInList()) {
            $scope.DescriptionAlreadyExistsError = true;
        } else {
            for (var i = 0; i < $scope.descriptions.colors.length; i++) {
                if ($scope.descriptions.colors[i].color.id == $scope.selectedColor[0].id) {
                    $scope.descriptions.colors[i].descriptions.push($scope.description);

                    //no idea if this works!
                    if ($scope.selectedColor[0].languages.indexOf($scope.description.languageCode) == -1) {
                        for (var j = 0; j < $scope.selectedColorGridData.length; j++) {
                            if($scope.selectedColorGridData[j].id == $scope.selectedColor[0].id) {
                                $scope.selectedColorGridData[j].languages = $scope.selectedColorGridData[j].languages + $scope.description.languageCode + ",";
                            }
                        }
                    }
                    $scope.description = {"languageCode": "", "languageName": "", "description": ""};
                    $log.log($scope.descriptions);
                    return;
                }
            }

            //if you get here something went wrong.
            $log.log("You should NEVER be able to get here. But just in case. javascript fucks up again. If you do get here. Call trudy.");
            $log.log($scope.selectedColor);
            $log.log($scope.descriptions);
            $scope.descriptions.colors.push({"color": $scope.selectedColor, "descriptions": []});
            $scope.descriptions.colors[i].descriptions.push($scope.description);
            $scope.description = {"languageCode": "", "languageName": "", "description": ""};
            $log.log($scope.descriptions);
            return;
        }
    };

    function addStandardDescriptions(colorId, colorName){
        for (var i = 0; i < $scope.descriptions.colors.length; i++) {
            if ($scope.descriptions.colors[i].color.id == colorId) {
                $log.log($scope.selectedProductType);
                var germanDescription = {
                    "languageCode": "de",
                    "languageName": "German",
                    "description": "Die " + $scope.selectedProductType.name + " in " + colorName + " aus design " + $scope.selectedDesign.name
                };
                $scope.descriptions.colors[i].descriptions.push(germanDescription);
                var englishDescription = {
                    "languageCode": "en",
                    "languageName": "English",
                    "description": "The " + $scope.selectedProductType.name + " in " + colorName + " from design " + $scope.selectedDesign.name
                };
                $scope.descriptions.colors[i].descriptions.push(englishDescription);

                //no idea if this works!
                for (var j = 0; j < $scope.selectedColorGridData.length; j++) {
                    if ($scope.selectedColorGridData[j].id == colorId) {
                        $scope.selectedColorGridData[j].languages = $scope.selectedColorGridData[j].languages + "de,en,";
                    }
                }
                /*for(var j = 0; j < $scope.descriptions.colors.length; j++){
                    $log.log("change the descriptions1");
                    $log.log($scope.descriptions);
                    if($scope.descriptions.colors[j].color.id = $scope.selectedColor[0].id){
                        $log.log("change the descriptions");
                        $scope.descriptionsGridData = $scope.descriptions.colors[j].descriptions;
                    }
                }*/

                $log.log($scope.descriptions);
                return;
            }
        }
    }

    $scope.changeDescriptions = function(){
        for(var i = 0; i < $scope.descriptions.colors.length; i++){
            $scope.descriptions.colors[i].descriptions = [];
        }
        $log.log("mlksdjfmlqsdkjfmlqkdsj");
        $log.log($scope.descriptions);
        for(var i = 0; i < $scope.selectedColorGridData.length; i++){
            $scope.selectedColorGridData[i].languages = [];
            addStandardDescriptions($scope.selectedColorGridData[i].id, $scope.selectedColorGridData[i].name);
        }
    };

    function isDescriptionAlreadyInList() {
        for (var j = 0; j < $scope.descriptions.colors.length; j++) {
            if ($scope.descriptions.colors[j].color.id == $scope.selectedColor[0].id) {
                $log.log($scope.descriptions);
                for (var i = 0; i < $scope.descriptions.colors[j].descriptions.length; i++) {
                    if ($scope.descriptions.colors[j].descriptions[i].languageCode == $scope.description.languageCode) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    $scope.removeDescription = function (rowItem) {
        for (var i = 0; i < $scope.descriptionsGridData.length; i++) {
            if ($scope.descriptionsGridData[i].languageCode == rowItem.entity.languageCode) {
                if ($scope.selectedColor[0].languages.indexOf(rowItem.entity.languageCode) > -1) {
                    for (var j = 0; j < $scope.selectedColorGridData.length; j++) {
                        if ($scope.selectedColorGridData[j].id == $scope.selectedColor[0].id) {
                            $scope.selectedColorGridData[j].languages = $scope.selectedColorGridData[j].languages.replace(rowItem.entity.languageCode + ",", "");
                        }
                    }
                }
                $scope.descriptionsGridData.splice(i, 1);
                return;
            }
        }
    };

    $scope.save = function () {
        $scope.NoProductTypeSelectedError = false;
        $scope.NoSizeSelectedError = false;
        $scope.NoSupplierSelectedError = false;
        $scope.NoDesignSelectedError = false;
        $scope.NoCategorySelectedError = false;
        $scope.NoManufacturingTypeSelectedError = false;
        $scope.NoStockAddedError = false;
        $scope.AmountOfColorsAndDescriptionsMismatchError = false;
        $scope.NoColorsSelectedError = false;
        $scope.MissingDescriptionError = false;
        $scope.MissingProductInforError = false;
        $scope.ProductInfoFormatError = false;
        $scope.NotAllColorsHaveAPriceError = false;

        if ($scope.selectedProductType == "" || $scope.selectedProductType == null || $scope.selectedProductType == undefined || Object.keys($scope.selectedProductType).length === 0) {
            $scope.NoProductTypeSelectedError = true;
        } else if ($scope.selectedSize == "" || $scope.selectedSize == null || $scope.selectedSize == undefined || Object.keys($scope.selectedSize).length === 0) {
            $scope.NoSizeSelectedError = true;
        } else if ($scope.selectedSupplier == "" || $scope.selectedSupplier == null || $scope.selectedSupplier == undefined || Object.keys($scope.selectedSupplier).length === 0) {
            $scope.NoSupplierSelectedError = true;
        } else if ($scope.selectedDesign == "" || $scope.selectedDesign == null || $scope.selectedDesign == undefined || Object.keys($scope.selectedDesign).length === 0) {
            $scope.NoDesignSelectedError = true;
        } else if (isThereACategoryMissing()) {
            $scope.NoCategorySelectedError = true;
        } else if ($scope.selectedManufacturingType == "" || $scope.selectedManufacturingType == null || $scope.selectedManufacturingType == undefined || Object.keys($scope.selectedManufacturingType).length === 0) {
            $scope.NoManufacturingTypeSelectedError = true;
        } else if ($scope.productInfo.name == "" || $scope.productInfo.minTime == "" || $scope.productInfo.baseTime == "" || $scope.productInfo.maxTime == "") {
            $scope.MissingProductInforError = true;
        } else if (isNaN($scope.productInfo.baseTime) || isNaN($scope.productInfo.minTime) || isNaN($scope.productInfo.maxTime)) {
            $scope.ProductInfoFormatError = true;
        } else if ($scope.stockGridData.length == 0) {
            $scope.NoStockAddedError = true;
        } else if ($scope.descriptions.colors.length != $scope.selectedColorGridData.length) {
            $scope.AmountOfColorsAndDescriptionsMismatchError = true;
        } else if ($scope.selectedColorGridData.length <= 0) {
            $scope.NoColorsSelectedError = true;
        } else if (areThereColorsMissingADescription()) {
            $scope.MissingDescriptionError = true;
        } else if (areSomeColorsMissingAPrice()) {
            $scope.NotAllColorsHaveAPriceError = true;
        } else {
            //woow you survived the if else checks.
            if($scope.useSuggestedCategory){
                if (confirm("You are using the suggested category " + $scope.advicedCategory.name) == true) {
                    saveFinishedProducts($scope.selectedColorGridData, 0, true, 0);
                } else {
                    return;
                }
            } else if (!$scope.useSuggestedCategory) {
                if (confirm("You are using your own selected category " + $scope.selectedCategory.name) == true) {
                    saveFinishedProducts($scope.selectedColorGridData, 0, false, 0);
                } else {
                    return;
                }
            }
        }
    };

    function saveFinishedProducts(colorsList, index, useSuggestedCategory, successSaved){
        if(index < colorsList.length) {
            $log.log(colorsList.length + " index:" + index + " category:" + useSuggestedCategory);
            $log.log(createNewFinishedProductObject(colorsList[index].id, useSuggestedCategory));
            httpServiceFactory.createPostPromise('/products/create', 'userid=' + applicationInformation.getUserinformation().userId, createNewFinishedProductObject(colorsList[index].id, useSuggestedCategory), true, applicationInformation.getToken(), true)
                .success(function (data, status, headers, config) {
                    saveFinishedProducts(colorsList, index + 1, useSuggestedCategory, successSaved + 1);
                })
                .error(function (error, status) {
                    saveFinishedProducts(colorsList, index + 1, useSuggestedCategory, successSaved);
                });
        } else {
            notificationService.showMessageNotification(successSaved + " out of " + colorsList.length + " new products have been saved successfully.");
            return;
        }

    }

    function areSomeColorsMissingAPrice(){
        for(var i = 0; i < $scope.selectedColorGridData.length; i++){
            if($scope.selectedColorGridData[i].price == "" || $scope.selectedColorGridData[i].price == null || $scope.selectedColorGridData[i].price == 0 || $scope.selectedColorGridData[i].price == undefined){
                return true;
            }
        }
        return false;
    }

    function isThereACategoryMissing(){
        if($scope.selectedCategory == "" || $scope.selectedCategory == null || $scope.selectedCategory == undefined || Object.keys($scope.selectedCategory).length === 0) {
            if ($scope.advicedCategory == "" || $scope.advicedCategory == null || $scope.advicedCategory == undefined || Object.keys($scope.advicedCategory).length === 0) {
                return true;
            } else if (!$scope.useSuggestedCategory) {
                return true;
            }
        }
        return false;
    }

    function areThereColorsMissingADescription() {
        for (var i = 0; i < $scope.descriptions.colors.length; i++) {
            if ($scope.descriptions.colors[i].descriptions.length <= 0) {
                return true;
            }
        }
        return false;
    }

    function createNewFinishedProductObject(colorId, useSuggestedCategory) {
        var newProduct = {
            "name": $scope.productInfo.name,
            "minTime": Number($scope.productInfo.minTime),
            "baseTime": Number($scope.productInfo.baseTime),
            "maxTime": Number($scope.productInfo.maxTime),
            "price": 0,
            "productTypeId": Number($scope.selectedProductType.id),
            "sizeId": Number($scope.selectedSize.id),
            "supplierid": Number($scope.selectedSupplier.id),
            "designId": Number($scope.selectedDesign.id),
            "manufacturingtypeId": Number($scope.selectedManufacturingType.id),
            "StockIds": [],
            "colorId": Number(colorId),
            "descriptions": [],
            "categoryId": ""
        };
        if(useSuggestedCategory){
            newProduct.categoryId = Number($scope.advicedCategory.id);
        }else{
            newProduct.categoryId = Number($scope.selectedCategory.id);
        }
        for (var i = 0; i < $scope.stockGridData.length; i++) {
            newProduct.StockIds.push(Number($scope.stockGridData[i].id));
        }
        for (var i = 0; i < $scope.descriptions.colors.length; i++) {
            if ($scope.descriptions.colors[i].color.id == colorId) {
                for (var j = 0; j < $scope.descriptions.colors[i].descriptions.length; j++) {
                    newProduct.descriptions.push($scope.descriptions.colors[i].descriptions[j]);
                }
            }
        }
        for(var i = 0; i < $scope.selectedColorGridData.length; i++){
            if($scope.selectedColorGridData[i].id ==  colorId){
                newProduct.price = Number($scope.selectedColorGridData[i].price).toFixed(2);
            }
        }
        return newProduct;
    }

    $scope.calculateSuggestedCategory = function () {
        $scope.haveAdviceedCategory = false;
        $scope.advicedCategory  = {};
        if ($scope.selectedSize == "" || $scope.selectedSize == null || $scope.selectedSize == undefined || Object.keys($scope.selectedSize).length === 0 || $scope.productInfo.baseTime == "" || isNaN($scope.productInfo.baseTime)) {
            return;
        }else{
            for(var i = 0;  i < $scope.availibleCategories.length; i++){
                for(var j  = 0; j < $scope.availibleCategories[i].cslist.length; j++){
                    if($scope.availibleCategories[i].cslist[j].sizeName == $scope.selectedSize.name){
                        if($scope.availibleCategories[i].cslist[j].minTime <= $scope.productInfo.baseTime && $scope.productInfo.baseTime <= $scope.availibleCategories[i].cslist[j].maxTime){
                            $scope.advicedCategory =  $scope.availibleCategories[i];
                            $scope.haveAdviceedCategory = true;
                            $log.log($scope.advicedCategory);
                            //window.print();

                        }
                    }
                }
            }
        }

    };

    function init() {
    }

    init();
}]);