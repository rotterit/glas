/**
 * Created by Suited Coder Gunther Claessens
 *
 * @Description: The class responible for the routing of all links. Default page is the login page.
 */
'use strict';
var route = angular.module('clarity.route', ['ngRoute']);

route.config(['$routeProvider', function($routeProvider){
    $routeProvider.when('/login', {
        templateUrl: 'app/components/login/login.html',
        controller: 'LoginCtrl',
        css: 'app/components/login/login.css',
        navBar: {showNavBar: false}
        })
        .when('/landingPage', {
            templateUrl: 'app/components/landingPage/landingPage.html',
            controller: 'landingController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                permissionType: 2
            },
            navBar: {showNavBar: true}
        })
        .when('/prodcutOverView',{
            templateUrl: 'app/components/productoverview/productOverview.html',
            controller: 'productOverviewCtrl',
            css: 'app/components/productoverview/productOverview.css',
            access: {loginRequired: true,
                requiredPermissions: [2],
                permissionType: 0},
            navBar: {showNavBar: true}
        })
        .when('/prodcutnav', {
            templateUrl: 'app/shared/navbar/erpNavbar.html',
            controller: 'navbarCtrl',
            css: 'app/shared/navbar/erpNavBar.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2],
                permissionType: 0
            },
            navBar: {showNavBar: true}
        })
        .when('/rawMaterialOverview', {
            templateUrl: 'app/components/rawMaterial/rawMaterialOverview.html',
            controller: 'rawMaterialOverviewCtrl',
            css: 'app/components/rawMaterial/rawMaterial.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5, 6],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "overview";}
            }
        })
        .when('/createMaterialOverview', {
            templateUrl: 'app/components/rawMaterial/crudRawMaterial.html',
            controller: 'rawCrudController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                productTypes: function(crudService){return crudService.getProductTypes();},
                sizes: function(crudService){return crudService.getSizes();},
                series: function(crudService){return crudService.getSeries();},
                suppliers: function(crudService){return crudService.getSuppliers();},
                colors: function(crudService){return crudService.getColors();},
                stocks: function(crudService){return crudService.getRawStocks();},
                crudOption: function(){return "create";}
            }
        })
        .when('/orderRawOverview', {
            templateUrl: 'app/components/rawMaterial/rawMaterialOverview.html',
            controller: 'rawMaterialOverviewCtrl',
            css: 'app/components/rawMaterial/rawMaterial.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "order";}
            }
        })
        .when('/newOrdersOverview', {
            templateUrl: 'app/components/order/orderOverview.html',
            controller: 'orderOverviewController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5, 6],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "new";}
            }
        })
        .when('/confirmedOrdersOverview', {
            templateUrl: 'app/components/order/orderOverview.html',
            controller: 'orderOverviewController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5, 6],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "confirmed";}
            }
        })
        .when('/deliveredOrdersOverview', {
            templateUrl: 'app/components/order/orderOverview.html',
            controller: 'orderOverviewController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5, 6],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "deliver";}
            }
        })
        .when('/completeOrdersOverview', {
            templateUrl: 'app/components/order/orderOverview.html',
            controller: 'orderOverviewController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5, 6],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "complete";}
            }
        })
        .when('/createOrderRaw', {
            templateUrl: 'app/components/order/createEdit/crudOrder.html',
            controller: 'rawMaterialOrderCrudController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                order: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/editOrder', {
            templateUrl: 'app/components/order/createEdit/crudOrder.html',
            controller: 'rawMaterialOrderCrudController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                order: function(crudService){return crudService.getEditingOrder();},
                crudOption: function(){return "edit";}
            }
        })
        .when('/invoiceShoppingCart', {
            templateUrl: 'app/components/finishedProduct/finishedProductOverview.html',
            controller: 'finishedProductOverviewCtrl',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                state: function(){return "invoice";}
            }
        })
        .when('/createInvoice', {
            templateUrl: 'app/components/invoice/createInvoice.html',
            controller: 'createInvoiceController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                order: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/editInvoice', {
            templateUrl: 'app/components/invoice/editInvoice.html',
            controller: 'editInvoiceController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                order: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/invoiceDetail', {
            templateUrl: 'app/components/invoice/invoiceDetail.html',
            controller: 'invoiceDetailController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                invoiceDetail: function(crudService){
                    return crudService.getDetailInvoice();
                }
            }
        })
        .when('/invoiceSummary', {
            templateUrl: 'app/components/invoice/invoiceSummary.html',
            controller: 'invoiceSummaryController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                invoiceDetail: function(crudService){
                    return crudService.getDetailInvoice();
                }
            }
        })
        .when('/offerInvoices', {
            templateUrl: 'app/components/invoice/invoiceOverview.html',
            controller: 'invoiceOverviewController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3 ,5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "offers";}
            }
        })
        .when('/confirmedInvoices', {
            templateUrl: 'app/components/invoice/invoiceOverview.html',
            controller: 'invoiceOverviewController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "confirmed";}
            }
        })
        .when('/inProductionInvoices', {
            templateUrl: 'app/components/invoice/invoiceOverview.html',
            controller: 'invoiceOverviewController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "inProduction";}
            }
        })
        .when('/sendInvoices', {
            templateUrl: 'app/components/invoice/invoiceOverview.html',
            controller: 'invoiceOverviewController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "sent";}
            }
        })
        .when('/completedInvoices', {
            templateUrl: 'app/components/invoice/invoiceOverview.html',
            controller: 'invoiceOverviewController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                overviewType: function(){return "completed";}
            }
        })
        .when('/detailMaterialOverview', {
            templateUrl: 'app/components/rawMaterial/detailRawMaterial.html',
            controller: 'detailRawMaterialController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                rawMaterial: function(crudService){return crudService.getRawMaterialDetail();}
            }
        })
        .when('/finishedProductOverview', {
            templateUrl: 'app/components/finishedProduct/finishedProductOverview.html',
            controller: 'finishedProductOverviewCtrl',
            css: 'app/components/finishedProduct/finishedProduct.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5, 7],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                state: function(){return "overview";}
            }
        })
        .when('/createFinishedProduct', {
            templateUrl: 'app/components/finishedProduct/createFinishedProduct.html',
            controller: 'createFinishedProductController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                productTypes: function(crudService){return crudService.getProductTypes();},
                sizes: function(crudService){return crudService.getSizes();},
                suppliers: function(crudService){return crudService.getSuppliers();},
                colors: function(crudService){return crudService.getColors();},
                stocks: function(crudService){return crudService.getFinishedStocks();},
                categories: function(crudService){return crudService.getCategories();},
                manufacturingTypes: function(crudService){return crudService.getManufacturingTypes();},
                designs: function(crudService){return crudService.getDesigns();},
                crudOption: function(){return "create";}
            }
        })
        .when('/finishedProductDetail', {
            templateUrl: 'app/components/finishedProduct/finishedProductDetail.html',
            controller: 'finishedProductDetailController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5, 7],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                finishedProduct: function(crudService){return crudService.getFinishedProductDetail();}
            }
        })
        .when('/colorOverview', {
            templateUrl: 'app/components/color/colorOverview.html',
            controller: 'colorOverviewController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                colors: function (crudService) {
                    return crudService.getColors();
                }
            }
        })
        .when('/colorEdit',{
        templateUrl: 'app/components/color/createEdit/crudColor.html',
        controller: 'colorCrudController',
        css: 'assets/css/crudPages.css',
        access: {loginRequired: true,
            requiredPermissions: [2, 3, 5],
            permissionType: 0},
        navBar: {showNavBar: true},
        resolve: {
            color: function(crudService){
                return crudService.getEditingColor();
            },
            crudOption: function(){return "edit";}
        }
        })
        .when('/colorCreate', {
            templateUrl: 'app/components/color/createEdit/crudColor.html',
            controller: 'colorCrudController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                color: function () {
                    return null;
                },
                crudOption: function () {
                    return "create";
                }
            }
        })
        .when('/sizeOverview',{
        templateUrl: 'app/components/size/sizeOverview.html',
        controller: 'sizeOverviewController',
        css: 'assets/css/crudPages.css',
        access: {loginRequired: true,
            requiredPermissions: [2, 3, 5],
            permissionType: 0},
        navBar: {showNavBar: true},
        resolve: {
            sizes: function(crudService){
                return crudService.getSizes();
            }
        }
        })
        .when('/sizeEdit',{
        templateUrl: 'app/components/size/createEdit/crudSize.html',
        controller: 'sizeCrudController',
        css: 'assets/css/crudPages.css',
        access: {loginRequired: true,
            requiredPermissions: [2, 3, 5],
            permissionType: 0},
        navBar: {showNavBar: true},
        resolve: {
            size: function(crudService){
                return crudService.getEditingSize();
            },
            crudOption: function(){return "edit";}
        }
    })
        .when('/sizeCreate',{
        templateUrl: 'app/components/size/createEdit/crudSize.html',
        controller: 'sizeCrudController',
        css: 'assets/css/crudPages.css',
        access: {loginRequired: true,
            requiredPermissions: [2, 3, 5],
            permissionType: 0},
        navBar: {showNavBar: true},
        resolve: {
            size: function(){return null;},
            crudOption: function(){return "create";}
        }
        })
        .when('/supplierOverview',{
            templateUrl: 'app/components/supplier/supplierOverview.html',
            controller: 'supplierOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                suppliers: function(crudService){
                    return crudService.getSuppliers();
                }
            }
        })
        .when('/supplierEdit', {
            templateUrl: 'app/components/supplier/createEdit/crudSupplier.html',
            controller: 'supplierCrudController',
            css: 'assets/css/crudPages.css',
            access: {
                loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0
            },
            navBar: {showNavBar: true},
            resolve: {
                supplier: function (crudService) {
                    return crudService.getEditingSupplier();
                },
                crudOption: function () {
                    return "edit";
                }
            }
        })
        .when('/supplierCreate',{
        templateUrl: 'app/components/supplier/createEdit/crudSupplier.html',
        controller: 'supplierCrudController',
        css: 'assets/css/crudPages.css',
        access: {loginRequired: true,
            requiredPermissions: [2, 3, 5],
            permissionType: 0},
        navBar: {showNavBar: true},
        resolve: {
            supplier: function(){return null;},
            crudOption: function(){return "create";}
        }
        })
        .when('/qualityOverview',{
            templateUrl: 'app/components/quality/qualityOverview.html',
            controller: 'qualityOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 4, 5, 6],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                qualities: function(crudService){
                    return crudService.getQualities();
                }
            }
        })
        .when('/qualityEdit',{
            templateUrl: 'app/components/quality/createEdit/crudQuality.html',
            controller: 'qualityCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                quality: function(crudService){
                    return crudService.getEditingQuality();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/qualityCreate',{
            templateUrl: 'app/components/quality/createEdit/crudQuality.html',
            controller: 'qualityCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                quality: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/designOverview',{
            templateUrl: 'app/components/design/designOverview.html',
            controller: 'designOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5, 7],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                designs: function(crudService){
                    return crudService.getDesigns();
                }
            }
        })
        .when('/designEdit',{
            templateUrl: 'app/components/design/createEdit/crudDesign.html',
            controller: 'designCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                design: function(crudService){
                    return crudService.getEditingDesign();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/designCreate',{
            templateUrl: 'app/components/design/createEdit/crudDesign.html',
            controller: 'designCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                design: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/productTypeOverview',{
            templateUrl: 'app/components/productType/productTypeOverview.html',
            controller: 'productTypeOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5, 7],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                productTypes: function(crudService){
                    return crudService.getProductTypes();
                }
            }
        })
        .when('/productTypeEdit',{
            templateUrl: 'app/components/productType/createEdit/crudProductType.html',
            controller: 'productTypeCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                productType: function(crudService){
                    return crudService.getEditingProductType();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/productTypeCreate',{
            templateUrl: 'app/components/productType/createEdit/crudProductType.html',
            controller: 'productTypeCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                productType: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/locationOverview',{
            templateUrl: 'app/components/location/locationOverview.html',
            controller: 'locationOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                locations: function(crudService){
                    return crudService.getLocations();
                }
            }
        })
        .when('/locationEdit',{
            templateUrl: 'app/components/location/createEdit/crudLocation.html',
            controller: 'locationCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                location: function(crudService){
                    return crudService.getEditingLocation();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/locationCreate',{
            templateUrl: 'app/components/location/createEdit/crudLocation.html',
            controller: 'locationCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                location: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/roleOverview',{
            templateUrl: 'app/components/role/roleOverview.html',
            controller: 'roleOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                roles: function(crudService){
                    return crudService.getRoles();
                }
            }
        })
        .when('/roleEdit',{
            templateUrl: 'app/components/role/createEdit/crudRole.html',
            controller: 'roleCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                role: function(crudService){
                    return crudService.getEditingRole();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/roleCreate',{
            templateUrl: 'app/components/role/createEdit/crudRole.html',
            controller: 'roleCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                role: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/customerOverview',{
            templateUrl: 'app/components/customer/customerOverview.html',
            controller: 'customerOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5, 7],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                customers: function(crudService){
                    return crudService.getCustomers();
                }
            }
        })
        .when('/customerEdit',{
            templateUrl: 'app/components/customer/createEdit/crudCustomer.html',
            controller: 'customerCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5, 7],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                customer: function(crudService){
                    return crudService.getEditingCustomer();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/customerCreate',{
            templateUrl: 'app/components/customer/createEdit/crudCustomer.html',
            controller: 'customerCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5, 7],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                customer: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/unitOverview',{
            templateUrl: 'app/components/unit/unitOverview.html',
            controller: 'unitOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                units: function(crudService){
                    return crudService.getUnits();
                }
            }
        })
        .when('/stockOverview',{
            templateUrl: 'app/components/stock/stockOverview.html',
            controller: 'stockOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 4, 5, 6, 7],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                rawStocks: function(crudService){
                    return crudService.getRawStocks();
                },
                finishedStocks: function(crudService){
                    return crudService.getFinishedStocks();
                }
            }
        })
        .when('/stockEdit',{
            templateUrl: 'app/components/stock/createEdit/crudStock.html',
            controller: 'stockCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                stock: function(crudService){
                    return crudService.getEditingStock();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/stockCreate',{
            templateUrl: 'app/components/stock/createEdit/crudStock.html',
            controller: 'stockCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                stock: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/seriesOverview',{
            templateUrl: 'app/components/series/seriesOverview.html',
            controller: 'seriesOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5, 7],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                series: function(crudService){
                    return crudService.getSeries();
                }
            }
        })
        .when('/seriesEdit',{
            templateUrl: 'app/components/series/createEdit/crudSeries.html',
            controller: 'seriesCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                series: function(crudService){
                    return crudService.getEditingSeries();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/seriesCreate',{
            templateUrl: 'app/components/series/createEdit/crudSeries.html',
            controller: 'seriesCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                series: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/userOverview',{
            templateUrl: 'app/components/user/userOverview.html',
            controller: 'userOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                users: function(crudService){
                    return crudService.getUsers();
                }
            }
        })
        .when('/userEdit',{
            templateUrl: 'app/components/user/createEdit/crudUser.html',
            controller: 'userCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                user: function(crudService){
                    return crudService.getEditingUser();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/userCreate',{
            templateUrl: 'app/components/user/createEdit/crudUser.html',
            controller: 'userCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                user: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/categoryOverview',{
            templateUrl: 'app/components/category/categoryOverview.html',
            controller: 'categoryOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                categories: function(crudService){
                    return crudService.getCategories();
                }
            }
        })
        .when('/categoryEdit',{
            templateUrl: 'app/components/category/createEdit/crudCategory.html',
            controller: 'categoryCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                category: function(crudService){
                    return crudService.getEditingCategory();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/categoryCreate',{
            templateUrl: 'app/components/category/createEdit/crudCategory.html',
            controller: 'categoryCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                category: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/manufacturingTypeOverview',{
            templateUrl: 'app/components/manufacturingType/manufacturingTypeOverview.html',
            controller: 'manufacturingTypeOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5, 7],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                manufacturingTypes: function(crudService){
                    return crudService.getManufacturingTypes();
                }
            }
        })
        .when('/manufacturingTypeEdit',{
            templateUrl: 'app/components/manufacturingType/createEdit/crudManufacturingType.html',
            controller: 'manufacturingTypeCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                manufacturingType: function(crudService){
                    return crudService.getEditingManufacturingType();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/manufacturingTypeCreate',{
            templateUrl: 'app/components/manufacturingType/createEdit/crudManufacturingType.html',
            controller: 'manufacturingTypeCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                manufacturingType: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/materialNotificationOverview',{
            templateUrl: 'app/components/materialNotification/materialNotificationOverview.html',
            controller: 'materialNotificationOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                materialNotifications: function(crudService){
                    return crudService.getMaterialNotifications();
                }
            }
        })
        .when('/materialNotificationEdit',{
            templateUrl: 'app/components/materialNotification/createEdit/crudMaterialNotification.html',
            controller: 'materialNotificationCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                materialNotifications: function(crudService){
                    return crudService.getMaterialNotifications();
                },
                materialNotification: function(crudService){
                    return crudService.getEditingMaterialNotification();
                },
                crudOption: function(){return "edit";}
            }
        })
        .when('/materialNotificationCreate',{
            templateUrl: 'app/components/materialNotification/createEdit/crudMaterialNotification.html',
            controller: 'materialNotificationCrudController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                materialNotifications: function(crudService){
                    return crudService.getMaterialNotifications();
                },
                materialNotification: function(){return null;},
                crudOption: function(){return "create";}
            }
        })
        .when('/productionQueueOverview',{
            templateUrl: 'app/components/productionQueue/productionQueueOverview.html',
            controller: 'productionQueueOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                productionQueueItems: function(crudService){
                    return crudService.getProductionQueueItems();
                }
            }
        })
        .when('/createWorksheet',{
            templateUrl: 'app/components/productionQueue/createWorksheet.html',
            controller: 'createWorksheetController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true},
            resolve: {
                worksheetItems: function(crudService){
                    return crudService.getCreatingWorksheet();
                }
            }
        })
        .when('/detailWorksheet/:redirect',{
            templateUrl: 'app/components/productionQueue/worksheetDetail.html',
            controller: 'worksheetDetailController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 4, 5],
                permissionType: 0},
            navBar: {showNavBar: true}
        })
        .when('/worksheetQueueOverview',{
            templateUrl: 'app/components/productionQueue/worksheetQueueOverview.html',
            controller: 'worksheetQueueOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true}
        })
        .when('/cutterWorksheetQueueOverview',{
            templateUrl: 'app/components/productionQueue/cutterWorksheetQueueOverview.html',
            controller: 'cutterWorksheetQueueOverviewController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 4],
                permissionType: 0},
            navBar: {showNavBar: true}
        })
        .when('/rawMaterialStockCorrection',{
            templateUrl: 'app/components/stockCorrection/rawStockCorrection.html',
            controller: 'rawStockCorrectionController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true}
        })
        .when('/finishedProductStockCorrection',{
            templateUrl: 'app/components/stockCorrection/finishedStockCorrection.html',
            controller: 'finishedStockCorrectionController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true}
        })
        .when('/finishedProductStockOrder',{
            templateUrl: 'app/components/stockOrder/stockOrder.html',
            controller: 'stockOrderController',
            css: 'assets/css/crudPages.css',
            access: {loginRequired: true,
                requiredPermissions: [2, 3, 5],
                permissionType: 0},
            navBar: {showNavBar: true}
        })
        .otherwise({redirectTo: '/login'});
}]);