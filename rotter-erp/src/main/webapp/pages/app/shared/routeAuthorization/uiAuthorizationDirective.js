/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';
var uiAuth = angular.module('clarity.route.authorization');

uiAuth.directive('access',['applicationInformation', function(applicationInformation) {
    return {
        restrict: 'A',
        link: function(scop, element, attrs){
            var makeVisible = function(){element.removeClass('hidden');};
            var makeHidden = function(){element.addClass('hidden');};
            var roles = attrs.access.split(',');
            var determinAuth = function(){
                for(var i = 0; i < roles.length; i++) {
                    if (applicationInformation.getRole() == roles[i]) {
                        makeVisible();
                        return;
                    }
                }
                makeHidden();
            };
            determinAuth();
        }
    };
}]);