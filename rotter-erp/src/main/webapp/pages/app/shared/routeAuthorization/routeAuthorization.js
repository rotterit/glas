/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: This service will catch every routchange.
 * It will check the rout change access parameters and check if you are authorized to do this routeChange.
 */
'use strict';

var auth = angular.module('clarity.route.authorization');

auth.run(['$rootScope', '$location', '$log', 'routeAuthorizeService', function($rootScope, $location, $log, routeAuthorizeService){
    $rootScope.$on('$routeChangeStart', function(event, next){
        var authorized;
        if(next.access !== undefined){
            authorized = routeAuthorizeService.authorize(next.access.loginRequired, next.access.requiredPermissions, next.access.permissionType);
            if(authorized === routeAuthorizeService.enum.authorized.loginRequired){
                $location.path('/login');
            }else if(authorized === routeAuthorizeService.enum.authorized.notAuthorized){
                $log.log("You are not authorized");
                $location.path('/landingPage');
            }else if(authorized === routeAuthorizeService.enum.authorized.authorized){
                //you shall pass.
            }
        }
    });
}]);