/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: directive that does the search for raw aterials.
 * It binds to a list of search specs in the controller. (scope)
 */
'use strict';
var finishedProductSearch = angular.module('clarity.material.finished');

finishedProductSearch.directive('finishedProductSearch',['$log', '$document', 'httpServiceFactory', 'applicationInformation', 'notificationService', function($log, $document, httpServiceFactory, applicationInformation, notificationService) {
    function link(scope, element, attrs){
        scope.productSearchText = "";
        scope.sizeSearchText = "";
        scope.designSearchText = "";
        scope.supplierSearchText = "";
        scope.categorySearchText = "";

        scope.filteredProductId = "";
        scope.filteredSizeId = "";
        scope.filteredDesignId = "";
        scope.filteredSupplierId = "";
        scope.filteredCategoryId = "";

        scope.focusOnIdSearch = false;
        scope.focusOnDescriptionSearch = false;

        element.bind("keydown", function(event){
            if(event.which === 13){
                if(scope.focusOnIdSearch){
                    var searchArgs = $document.find('#rawmaterial-id-search')[0].value.split('-');
                    if(searchArgs.length == 4){
                        var designId = [];
                        designId[0] = searchDesignId(searchArgs[1], scope.searchPopulationData.designs);
                        var sizeId = [];
                        sizeId[0] = searchSizeCodeId(searchArgs[2], scope.searchPopulationData.sizes);
                        var productTypeId = [];
                        productTypeId[0] = searchProductTypeCodeId(searchArgs[0], scope.searchPopulationData.productTypes);
                        var colorId = [];
                        colorId[0] = searchColorCodeId(searchArgs[3], scope.searchPopulationData.colors);
                        var searchParameterJson = {"sizeids" : sizeId[0] != null? sizeId : sizeId = [],
                            "supplierids": [],
                            "productTypeids": productTypeId[0] != null? productTypeId : productTypeId = [],
                            "colorids": colorId[0] != null? colorId : colorId = [],
                            "manufacturingTypeids": [],
                            "designids": designId[0] != null? designId : designId = [],
                            "categoryids": []};
                        executeSearch(searchParameterJson);
                        $log.log(searchParameterJson);
                    }else{
                        notificationService.showWarningNotification("Search term does not have the right format.");
                        scope.$apply();
                    }
                }else if(scope.focusOnDescriptionSearch){
                    var searchDescription = $document.find('#rawmaterial-description-search')[0].value;
                    if(searchDescription != "") {
                        httpServiceFactory.createGetPromise('/products/bydescription', 'userid=' + applicationInformation.getUserinformation().userId + "&searchstring=" + searchDescription, undefined, true, applicationInformation.getToken())
                            .success(function (data, status, headers, config) {
                                $log.log(data);
                                scope.data = data;
                                scope.updateGrid(data);
                            }).error(function (error, status) {
                                $log.log(error);
                            });
                    }
                }else{
                    //select the element that they have selected at that point
                    //THIS IS GOING TO BE KAKA
                }
            }
        });

        scope.search = function(){
            var searchParameterJson = {"sizeids" : getSelectedIds(scope.searchPopulationData.sizes, 'sis'),
                "supplierids": getSelectedIds(scope.searchPopulationData.suppliers, 'sus'),
                "productTypeids": getSelectedIds(scope.searchPopulationData.productTypes, 'prs'),
                "colorids": getSelectedIds(scope.searchPopulationData.colors, 'cl'),
                "manufacturingTypeids": getSelectedIds(scope.searchPopulationData.manufacturingTypes, 'mt'),
                "designids": getSelectedIds(scope.searchPopulationData.designs, 'des'),
                "categoryids": getSelectedIds(scope.searchPopulationData.categorys, 'cat')};
            $log.log(searchParameterJson);
            executeSearch(searchParameterJson);
        };

        function executeSearch(searchParameters) {
            if(scope.overviewType == "overview") {
                httpServiceFactory.createPostPromise('/products/filtered', 'userid=' + applicationInformation.getUserinformation().userId, searchParameters, true, applicationInformation.getToken(), true)
                    .success(function (data, status, headers, config) {
                        $log.log("return van search data");
                        $log.log(data);
                        scope.finishedProductsSearchResultsGridData = data;
                    }).error(function (error, status) {
                        $log.log(error);
                    });
            } else if(scope.overviewType == "invoice"){
                httpServiceFactory.createPostPromise('/products/filteredperstock', 'userid=' + applicationInformation.getUserinformation().userId, searchParameters, true, applicationInformation.getToken(), true)
                    .success(function (data, status, headers, config) {
                        $log.log("return van search data invoice selection");
                        $log.log(data);
                        scope.finishedProductsSearchResultsGridData = data;
                    }).error(function (error, status) {
                        $log.log(error);
                    });
            }
        }

        function getSelectedIds(idList, idPreposition){
            var selection = [];
            for(var i = 0; i < idList.length; i++){
                var checkbox = $document.find('#' + idPreposition + idList[i].id);
                if(checkbox[0].checked){
                    selection[selection.length] = idList[i].id;
                }
            }
            return selection;
        }

        scope.selectAllColors = function(){
            for(var i = 0; i < scope.searchPopulationData.colors.length; i++){
                var colorCheckbox = $document.find('#cl' + scope.searchPopulationData.colors[i].id);
                colorCheckbox[0].checked = true;
            }
        };

        scope.deselectAllColors = function(){
            for(var i = 0; i < scope.searchPopulationData.colors.length; i++){
                var colorCheckbox = $document.find('#cl' + scope.searchPopulationData.colors[i].id);
                colorCheckbox[0].checked = false;
            }
        };

        scope.selectAllManufacturingTypes = function(){
            for(var i = 0; i < scope.searchPopulationData.manufacturingTypes.length; i++){
                var qualityCheckbox = $document.find('#mt' + scope.searchPopulationData.manufacturingTypes[i].id);
                qualityCheckbox[0].checked = true;
            }
        };

        scope.deselectAllManufacturingTypes = function(){
            for(var i = 0; i < scope.searchPopulationData.manufacturingTypes.length; i++){
                var qualityCheckbox = $document.find('#mt' + scope.searchPopulationData.manufacturingTypes[i].id);
                qualityCheckbox[0].checked = false;
            }
        };

        element.bind('keyup', function(event){
            switch (event.target.id) {
                case 'prs':
                    if (scope.productSearchText.length == 0) {
                        resetScrollBox("productSearchScrollBox");
                        return;
                    } else {
                        searchProductId(scope.productSearchText, scope.searchPopulationData.productTypes);
                        scroll("prs".concat(scope.filteredProductId), "productSearchScrollBox");
                    }
                    break;
                case 'sis':
                    if (scope.sizeSearchText.length == 0) {
                        resetScrollBox("sizeSearchScrollBox");
                        return;
                    } else {
                        searchSizeId(scope.sizeSearchText, scope.searchPopulationData.sizes);
                        scroll("sis".concat(scope.filteredSizeId), "sizeSearchScrollBox");
                    }
                    break;
                case 'des':
                    if (scope.designSearchText.length == 0) {
                        resetScrollBox("designSearchScrollBox");
                        return;
                    } else {
                        searchDesignId(scope.designSearchText, scope.searchPopulationData.designs);
                        scroll("des".concat(scope.filteredSeriesId), "designSearchScrollBox");
                    }
                    break;
                case 'sus':
                    if (scope.supplierSearchText.length == 0) {
                        resetScrollBox("supplierSearchScrollBox");
                        return;
                    } else {
                        searchSupplierId(scope.supplierSearchText, scope.searchPopulationData.suppliers);
                        scroll("sus".concat(scope.filteredSupplierId), "supplierSearchScrollBox");
                    }
                case 'cat':
                    if (scope.categorySearchText.length == 0) {
                        resetScrollBox("categorySearchScrollBox");
                        return;
                    } else {
                        searchCategoryId(scope.categorySearchText, scope.searchPopulationData.categorys);
                        scroll("cat".concat(scope.filteredCategoryId), "categorySearchScrollBox");
                    }
                    break;
            }
        });

        function searchProductId(searchString, searchList){
            if(isSearchForName(searchString)){
                searchString = searchString.substring(1);
                $log.log(searchString);
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].name)){
                        scope.filteredProductId = searchList[i].id;
                        return;
                    }
                }
            }else{
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].code)){
                        scope.filteredProductId = searchList[i].id;
                        return;
                    }
                }
            }
        }

        function searchSizeId(searchString, searchList){
            $log.log(searchString);
            if(isSearchForName(searchString)){
                searchString = searchString.substring(1);
                $log.log(searchString);
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].name)){
                        scope.filteredSizeId = searchList[i].id;
                        return;
                    }
                }
            }else{
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].code)){
                        scope.filteredSizeId = searchList[i].id;
                        return;
                    }
                }
            }
        }

        function searchCategoryId(searchString, searchList){
            for(var i = 0; i < searchList.length; i++){
                if(searchContains(searchString, searchList[i].name)){
                    scope.filteredCategoryId = searchList[i].id;
                    return;
                }
            }
        }

        function searchSupplierId(searchString, searchList){
            if(isSearchForName(searchString)){
                searchString = searchString.substring(1);
                $log.log(searchString);
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].name)){
                        scope.filteredSupplierId = searchList[i].id;
                        return;
                    }
                }
            }else{
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].supliercode)){
                        scope.filteredSupplierId = searchList[i].id;
                        return;
                    }
                }
            }
        }

        function searchProductTypeCodeId(searchString, searchList){
            $log.log("searchString: "  +  searchString);
            for(var i = 0; i < searchList.length; i++){
                $log.log("prouct search: " +  searchList[i]);
                if(searchString.toUpperCase() == searchList[i].code.toUpperCase()){
                    $log.log("is gelijk");
                    return searchList[i].id;
                }
            }
            return null;
        }

        function searchDesignId(searchString, searchList){
            $log.log("searchString: "  +  searchString);
            for(var i = 0; i < searchList.length; i++){
                if(searchString.toUpperCase() == searchList[i].designCode.toUpperCase()){
                    $log.log("is gelijk");
                    return searchList[i].id;
                }
            }
            return null;
        }

        function searchSizeCodeId(searchString, searchList){
            $log.log("searchString: "  +  searchString);
            for(var i = 0; i < searchList.length; i++){
                if(searchString.toUpperCase() == searchList[i].code.toUpperCase()){
                    $log.log("is gelijk");
                    return searchList[i].id;
                }
            }
            return null;
        }

        function searchColorCodeId(searchString, searchList){
            $log.log("searchString: "  +  searchString);
            for(var i = 0; i < searchList.length; i++){
                if(searchString.toUpperCase() == searchList[i].code.toUpperCase()){
                    $log.log("is gelijk");
                    return searchList[i].id;
                }
            }
            return null;
        }

        function isSearchForName(searchString){
            return (searchString.charAt(0).valueOf() == ':'.valueOf());
        }

        function searchContains(search, value){
            return (value.indexOf(search) > -1);
        }

        function scroll(elemntId, scrollBoxId){
            var scrollToElement = $document.find('#' + elemntId);
            $log.log(elemntId + " " + scrollToElement[0].offsetTop);
            var scrollingBox = $document.find('#' + scrollBoxId);
            $log.log(scrollBoxId + " " + scrollingBox[0].scrollTop);
            scrollingBox[0].scrollTop = scrollToElement[0].offsetTop;
        }

        function resetScrollBox(scrollBoxId){
            var scrollingBox = $document.find('#' + scrollBoxId);
            scrollingBox[0].scrollTop = 0;
        }
    }

    return {
        restrict: 'E',
        templateUrl: 'app/shared/searchBar/finishedProductSearchBar/finishedProductSearchBar.html',
        link: link
    };
}]);