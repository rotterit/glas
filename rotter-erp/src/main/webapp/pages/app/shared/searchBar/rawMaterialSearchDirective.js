/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: directive that does the search for raw aterials.
 * It binds to a list of search specs in the controller. (scope)
 */
'use strict';
var search = angular.module('clarity.material.raw');

search.directive('rawMaterialSearch',['$log', '$document', 'httpServiceFactory', 'applicationInformation', 'notificationService', function($log, $document, httpServiceFactory, applicationInformation, notificationService) {
    function link(scope, element, attrs){
        scope.filteredProductId = "";
        scope.filteredSizeId = "";
        scope.filteredSeriesId = "";
        scope.filteredSupplierId = "";

        scope.focusOnIdSearch = false;
        scope.focusOnDescriptionSearch = false;

        element.bind("keydown", function(event){
            if(event.which === 13){
                if(scope.focusOnIdSearch){
                    var test = [];
                    var searchArgs = $document.find('#rawmaterial-id-search')[0].value.split('-');
                    $log.log($document.find('#rawmaterial-id-search')[0].value.split('-'));
                    if(searchArgs.length == 3){
                        var sizeId = [];
                        sizeId[0] = searchSizeCodeId(searchArgs[1], scope.searchPopulationData.sizes);
                        var productTypeId = [];
                        productTypeId[0] = searchProductTypeCodeId(searchArgs[0], scope.searchPopulationData.productTypes);
                        var colorId = [];
                        colorId[0] = searchColorCodeId(searchArgs[2], scope.searchPopulationData.colors);
                        var searchJson = {sizeids : sizeId[0] != null? sizeId : sizeId = [],
                            supplierids: [],
                            productTypeids: productTypeId[0] != null? productTypeId : productTypeId = [],
                            seriesids: [],
                            colorids: colorId[0] != null? colorId : colorId = [],
                            qualityids: []};
                        excecuteSearch(searchJson);
                        $log.log(searchJson);
                    }else{
                        notificationService.showWarningNotification("Search term does not have the right format.");
                        scope.$apply();
                    }
                }else if(scope.focusOnDescriptionSearch){
                    var searchDescription = $document.find('#rawmaterial-description-search')[0].value;
                    if(searchDescription != "") {
                        if(scope.overviewType == "overview") {
                            httpServiceFactory.createGetPromise('/raw/bydescription', 'userid=' + applicationInformation.getUserinformation().userId + "&searchstring=" + searchDescription, undefined, true, applicationInformation.getToken())
                                .success(function (data, status, headers, config) {
                                    $log.log(data);
                                    scope.data = data;
                                    scope.updateGrid(data);
                                }).error(function (error, status) {
                                    $log.log(error);
                                });
                        }else if(scope.overviewType == "order"){
                            httpServiceFactory.createGetPromise('/raw/bydescriptionfororder', 'userid=' + applicationInformation.getUserinformation().userId + "&searchstring=" + searchDescription, undefined, true, applicationInformation.getToken())
                                .success(function (data, status, headers, config) {
                                    $log.log(data);
                                    scope.data = data;
                                    scope.updateGrid(data);
                                }).error(function (error, status) {
                                    $log.log(error);
                                });
                        }
                    }
                }else{
                    //select the element that they have selected at that point
                    //THIS IS GOING TO BE KAKA
                }
            }
        });

        scope.search = function(){
            var searchParameterJson = {sizeids : getSelectedIds(scope.searchPopulationData.sizes, 'sis'),
                supplierids: getSelectedIds(scope.searchPopulationData.suppliers, 'sus'),
                productTypeids: getSelectedIds(scope.searchPopulationData.productTypes, 'prs'),
                seriesids: getSelectedIds(scope.searchPopulationData.series, 'ses'),
                colorids: getSelectedIds(scope.searchPopulationData.colors, 'cl'),
                qualityids: getSelectedIds(scope.searchPopulationData.qualities, 'q')};
            $log.log(searchParameterJson);
            excecuteSearch(searchParameterJson);
        };

        function excecuteSearch(searchParameters) {
            if(scope.overviewType == "overview") {
                httpServiceFactory.createPostPromise('/raw/filtered', 'userid=' + applicationInformation.getUserinformation().userId, searchParameters, true, applicationInformation.getToken(), true)
                    .success(function (data, status, headers, config) {
                        $log.log(data);
                        scope.data = data;
                        scope.updateGrid(data);
                    }).error(function (error, status) {
                        $log.log(error);
                    });
            }else if(scope.overviewType == "order"){
                httpServiceFactory.createPostPromise('/raw/filteredfororder', 'userid=' + applicationInformation.getUserinformation().userId, searchParameters, true, applicationInformation.getToken(), true)
                    .success(function (data, status, headers, config) {
                        $log.log(data);
                        scope.data = data;
                        scope.updateGrid(data);
                    }).error(function (error, status) {
                        $log.log(error);
                    });
            }
        }

        function getSelectedIds(idList, idPreposition){
            var selection = [];
            for(var i = 0; i < idList.length; i++){
                var checkbox = $document.find('#' + idPreposition + idList[i].id);
                if(checkbox[0].checked){
                    selection[selection.length] = idList[i].id;
                }
            }
            return selection;
        }

        scope.selectAllColors = function(){
            for(var i = 0; i < scope.searchPopulationData.colors.length; i++){
                var colorCheckbox = $document.find('#cl' + scope.searchPopulationData.colors[i].id);
                colorCheckbox[0].checked = true;
            }
        };

        scope.deselectAllColors = function(){
            for(var i = 0; i < scope.searchPopulationData.colors.length; i++){
                var colorCheckbox = $document.find('#cl' + scope.searchPopulationData.colors[i].id);
                colorCheckbox[0].checked = false;
            }
        };

        scope.selectAllQualities = function(){
            for(var i = 0; i < scope.searchPopulationData.qualities.length; i++){
                var qualityCheckbox = $document.find('#q' + scope.searchPopulationData.qualities[i].id);
                qualityCheckbox[0].checked = true;
            }
        };

        scope.deselectAllQualities = function(){
            for(var i = 0; i < scope.searchPopulationData.qualities.length; i++){
                var qualityCheckbox = $document.find('#q' + scope.searchPopulationData.qualities[i].id);
                qualityCheckbox[0].checked = false;
            }
        };

        element.bind('keyup', function(event){
            switch (event.target.id) {
                case 'prs':
                    if (scope.productSearchText.length == 0) {
                        resetScrollBox("productSearchScrollBox");
                        return;
                    } else {
                        searchProductId(scope.productSearchText, scope.searchPopulationData.productTypes);
                        scroll("prs".concat(scope.filteredProductId), "productSearchScrollBox");
                    }
                    break;
                case 'sis':
                    if (scope.sizeSearchText.length == 0) {
                        resetScrollBox("sizeSearchScrollBox");
                        return;
                    } else {
                        searchSizeId(scope.sizeSearchText, scope.searchPopulationData.sizes);
                        scroll("sis".concat(scope.filteredSizeId), "sizeSearchScrollBox");
                    }
                    break;
                case 'ses':
                    if (scope.seriesSearchText.length == 0) {
                        resetScrollBox("seriesSearchScrollBox");
                        return;
                    } else {
                        searchSeriesId(scope.seriesSearchText, scope.searchPopulationData.series);
                        scroll("ses".concat(scope.filteredSeriesId), "seriesSearchScrollBox");
                    }
                    break;
                case 'sus':
                    if (scope.supplierSearchText.length == 0) {
                        resetScrollBox("supplierSearchScrollBox");
                        return;
                    } else {
                        searchSupplierId(scope.supplierSearchText, scope.searchPopulationData.suppliers);
                        scroll("sus".concat(scope.filteredSupplierId), "supplierSearchScrollBox");
                    }
                    break;
            }
        });

        function searchProductId(searchString, searchList){
            if(isSearchForName(searchString)){
                searchString = searchString.substring(1);
                $log.log(searchString);
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].name)){
                        scope.filteredProductId = searchList[i].id;
                        return;
                    }
                }
            }else{
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].productTypeCode)){
                        scope.filteredProductId = searchList[i].id;
                        return;
                    }
                }
            }
        }

        function searchSizeId(searchString, searchList){
            $log.log(searchString);
            if(isSearchForName(searchString)){
                searchString = searchString.substring(1);
                $log.log(searchString);
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].name)){
                        scope.filteredSizeId = searchList[i].id;
                        return;
                    }
                }
            }else{
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].code)){
                        scope.filteredSizeId = searchList[i].id;
                        return;
                    }
                }
            }
        }

        function searchSeriesId(searchString, searchList){
            for(var i = 0; i < searchList.length; i++){
                if(searchContains(searchString, searchList[i].name)){
                    scope.filteredSeriesId = searchList[i].id;
                    return;
                }
            }
        }

        function searchSupplierId(searchString, searchList){
            if(isSearchForName(searchString)){
                searchString = searchString.substring(1);
                $log.log(searchString);
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].name)){
                        scope.filteredSupplierId = searchList[i].id;
                        return;
                    }
                }
            }else{
                for(var i = 0; i < searchList.length; i++){
                    if(searchContains(searchString, searchList[i].supliercode)){
                        scope.filteredSupplierId = searchList[i].id;
                        return;
                    }
                }
            }
        }

        function searchProductTypeCodeId(searchString, searchList){
            $log.log("searchString: "  +  searchString);
            for(var i = 0; i < searchList.length; i++){
                $log.log("prouct search: " +  searchList[i]);
                if(searchString.toUpperCase() == searchList[i].code.toUpperCase()){
                    $log.log("is gelijk");
                    return searchList[i].id;
                }
            }
            return null;
        }

        function searchSizeCodeId(searchString, searchList){
            $log.log("searchString: "  +  searchString);
            for(var i = 0; i < searchList.length; i++){
                if(searchString.toUpperCase() == searchList[i].code.toUpperCase()){
                    $log.log("is gelijk");
                    return searchList[i].id;
                }
            }
            return null;
        }

        function searchColorCodeId(searchString, searchList){
            $log.log("searchString: "  +  searchString);
            for(var i = 0; i < searchList.length; i++){
                if(searchString.toUpperCase() == searchList[i].code.toUpperCase()){
                    $log.log("is gelijk");
                    return searchList[i].id;
                }
            }
            return null;
        }

        function isSearchForName(searchString){
            return (searchString.charAt(0).valueOf() == ':'.valueOf());
        }

        function searchContains(search, value){
            return (value.indexOf(search) > -1);
        }

        function scroll(elemntId, scrollBoxId){
            var scrollToElement = $document.find('#' + elemntId);
            $log.log(elemntId + " " + scrollToElement[0].offsetTop);
            var scrollingBox = $document.find('#' + scrollBoxId);
            $log.log(scrollBoxId + " " + scrollingBox[0].scrollTop);
            scrollingBox[0].scrollTop = scrollToElement[0].offsetTop;
        }

        function resetScrollBox(scrollBoxId){
            var scrollingBox = $document.find('#' + scrollBoxId);
            scrollingBox[0].scrollTop = 0;
        }
    }

    return {
        restrict: 'E',
        templateUrl: 'app/shared/searchBar/rawMaterialTemplate.html',
        link: link
    };
}]);