/**
 * Created by Suited Coder Gunther Claessens.
 */
'use strict';
var controller = angular.module('clarity.material.raw');

controller.controller('rawMaterialSearchCtrl', ['$scope', function($scope){
    $scope.productSearchText = "";
    $scope.sizeSearchText = "";
    $scope.seriesSearchText = "";
    $scope.supplierSearchText = "";

    $scope.searchSpecs={products:[{code:'WW', name: 'white wine', id:1},{code:'H', name: 'water bottle', id:2},{code:'RW', name: 'red wine', id:3},{code:'WY', name: 'whisky', id:4}],
        size:[{size:'S', name:'small', id:1}, {size:'M', name:'medium', id:2}, {size:'L', name:'large', id:3},
            {size:'X', name:'extra', id:4}, {size:'4548/60', name:'wiskey paris', id:5}, {size:'XL', name:'larger', id:6},
            {size:'XXL', name:'extra extra large', id:7}, {size:'XS', name:'extra small', id:8}, {size:'XXS', name:'extra extra small', id:9}],
        series:[{name:'paris', id:1}, {name:'teringzooi', id:2}],
        supplier:[{name:'pschott', code:'20', id:1}, {name:'eisch', code:'30', id:2}, {name:'KaHeKu', code:'40', id:3}, {name:'nachtmann', code:'50', id:4}],
        colors:[{code:"CL", name:"klar", id:1, colorCode:"E4E4E6"}, {code:"AM", name:"amber", id:2, colorCode:"E8B200"}, {code:"BL", name:"blue", id:3, colorCode:"2E09E0"}, {code:"GR", name:"green", id:4, colorCode:"00971C"}, {code:"RE", name:"reseda", id:5, colorCode:"D4E146"}],
        quality:[{id:1, name:"Alpha"}, {id:2, name:"Beta"}, {id:3, name:"Gamma"}, {id:4, name:"Delta"}, {id:5, name:"Omega"}]};
}]);