/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: this directive shows a popup box that will show a text that can be copied.
 * the trigger for this popupbox is a broadcast on the rootscope of 'copypasterino'.
 */
'use strict';
var copypasterino = angular.module('clarity.index');

copypasterino.directive('copypasterinoPopup',['$log', function($log) {
    function link(scope, element, attrs){
        scope.text = "";
        scope.copypasterinoVisibility = false;

        scope.$on('copypasterino', function (event, data) {
            $log.log("get text notification");
            $log.log(data);
            scope.text = data;
            scope.copypasterinoVisibility = true;
        });

        scope.close = function(){
            scope.copypasterinoVisibility = false;
            scope.text = "";
        };
    }

    return {
        restrict: 'EA',
        templateUrl: 'app/shared/copypasterino/copypasterinoTemplate.html',
        link: link
    };
}]);