/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: the directive that shows a loading screen to the user when a http request is active.
 */
'use strict';
var autoLogout = angular.module('clarity.index');

autoLogout.directive('loadingPopup',[function() {
    function link(scope, element, attrs){
        scope.loadingVisibility = false;
        scope.message = "Trudy is getting the data."

        scope.$on('StartLoading', function (event, data) {
            scope.loadingVisibility = true;
        });

        scope.$on('StopLoading', function (event, data) {
            scope.loadingVisibility = false;
        });
    }

    return {
        restrict: 'EA',
        templateUrl: 'app/shared/loadingModal/loadingTemplate.html',
        link: link
    };
}]);