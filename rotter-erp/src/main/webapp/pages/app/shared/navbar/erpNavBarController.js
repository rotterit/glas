angular.module('clarity')
    .controller('navbarCtrl',function($scope, $location, loginService, $log){
        //=== Variables ===//
        $scope.showNavBar = false;

        $scope.$on('$routeChangeStart', function(next, current){
            $scope.showNavBar = current.$$route.navBar.showNavBar;
        });

        $scope.search = {
            show : true,
            terms : ''
        };
        $scope.menus = [
            {
                title : "Home",
                action : "home"
            },{
                title : "Raw material",
                menu : [
                    {
                        title : "overview",
                        action : "raw.overview"
                    },
                    {
                        title : "create order",
                        action : "raw.order"
                    },
                    {
                        title : "order overview",
                        action : "raw.orderoverview"
                    },
                    {
                        title : "create raw material",
                        action : "raw.create"
                    }
                ]
            },
            {
                title : "finished products",
                menu : [
                    {
                        title : "overview",
                        action : "finished.overview"
                    },
                    {
                        title : "create finished product",
                        action : "finished.create"
                    }
                ]
            },
            {
                title : "create",
                menu : [
                    {
                        title : "category",
                        action : "create.category"
                    },
                    {
                        title : "color",
                        action : "create.color"
                    },
                    {
                        title : "customer",
                        action : "create.customer"
                    },
                    {
                        title : "design",
                        action : "create.design"
                    },
                    {
                        title : "location",
                        action : "create.location"
                    },
                    {
                        title : "manufacturing type",
                        action : "create.manufacturingType"
                    },
                    {
                        title : "material notification",
                        action : "create.materialNotification"
                    },
                    {
                        title : "product type",
                        action : "create.productType"
                    },
                    {
                        title : "quality",
                        action : "create.quality"
                    },
                    {
                        title : "role",
                        action : "create.role"
                    },
                    {
                        title : "series",
                        action : "create.series"
                    },
                    {
                        title : "size",
                        action : "create.size"
                    },
                    {
                        title : "stock",
                        action : "create.stock"
                    },
                    {
                        title : "supplier",
                        action : "create.supplier"
                    },
                    {
                        title : "unit",
                        action : "create.unit"
                    },
                    {
                        title : "user",
                        action : "create.user"
                    }
                ]
            },{
                title : "invoices",
                menu : [
                    {
                        title : "offers",
                        action : "invoices.offers"
                    },{
                        title : "confirmed invoices",
                        action : "invoices.confirmed"
                    },{
                        title : "in production invoices",
                        action : "invoices.inproduction"
                    },{
                        title : "send invoices",
                        action : "invoices.send"
                    },{
                        title : "completed invoices",
                        action : "invoices.completed"
                    },{
                        title : "create new invoice",
                        action : "invoices.create"
                    }
                ]
            },{
                title : "production",
                menu : [
                    {
                        title : "production queue",
                        action : "productionQueue.overview"
                    },
                    {
                        title : "worksheets",
                        action : "worksheets.overview"
                    },
                    {
                        title : "myWorksheets",
                        action : "worksheets.myWorksheets"
                    }
                ]
            },
            {
                title : "Logout",
                action : "logout"
            }
        ]; // end menus

        $scope.item = '';
        $scope.searchDisplay = 'Visible';

        $scope.searchfn = function(){
            alert('Attempting search on: "' + $scope.search.terms + '"');
        }; // searchfn

        $scope.navfn = function(action){
            switch(action){
                //home---------------------------------------------
                case 'home':
                    $location.path("/landingPage");
                    break;
                //raw---------------------------------------------
                case 'raw.overview':
                    $location.path("/rawMaterialOverview");
                    break;
                case 'raw.order':
                    $location.path("/orderRawOverview");
                    break;
                case 'raw.orderoverview':
                    $location.path("/newOrdersOverview");
                    break;
                case 'raw.create':
                    $location.path("/createMaterialOverview");
                    break;
                //finished----------------------------------------
                case 'finished.overview':
                    $location.path("/finishedProductOverview");
                    break;
                case 'finished.create':
                    $location.path("/createFinishedProduct");
                    break;
                //create------------------------------------------
                case 'create.category':
                    $location.path("/categoryOverview");
                    break;
                case 'create.color':
                    $location.path("/colorOverview");
                    break;
                case 'create.customer':
                    $location.path("/customerOverview");
                    break;
                case 'create.design':
                    $location.path("/designOverview");
                    break;
                case 'create.location':
                    $location.path("/locationOverview");
                    break;
                case 'create.manufacturingType':
                    $location.path("/manufacturingTypeOverview");
                    break;
                case 'create.materialNotification':
                    $location.path("/materialNotificationOverview");
                    break;
                case 'create.productType':
                    $location.path("/productTypeOverview");
                    break;
                case 'create.quality':
                    $location.path("/qualityOverview");
                    break;
                case 'create.role':
                    $location.path("/roleOverview");
                    break;
                case 'create.series':
                    $location.path("/seriesOverview");
                    break;
                case 'create.size':
                    $location.path("/sizeOverview");
                    break;
                case 'create.stock':
                    $location.path("/stockOverview");
                    break;
                case 'create.supplier':
                    $location.path("/supplierOverview");
                    break;
                case 'create.unit':
                    $location.path("/unitOverview");
                    break;
                case 'create.user':
                    $location.path("/userOverview");
                    break;
                //invoices----------------------------------------
                case 'invoices.offers':
                    $location.path("/offerInvoices");
                    break;
                case 'invoices.confirmed':
                    $location.path("/confirmedInvoices");
                    break;
                case 'invoices.inproduction':
                    $location.path("/inProductionInvoices");
                    break;
                case 'invoices.send':
                    $location.path("/sendInvoices");
                    break;
                case 'invoices.completed':
                    $location.path("/completedInvoices");
                    break;
                case 'invoices.create':
                    $location.path("/invoiceShoppingCart");
                    break;
                //production queue--------------------------------
                case 'productionQueue.overview':
                    $location.path("/productionQueueOverview");
                    break;
                case 'worksheets.overview':
                    $location.path("/worksheetQueueOverview");
                    break;
                case 'worksheets.myWorksheets':
                    $location.path("/cutterWorksheetQueueOverview");
                    break;
                //logout-----------------------------------------
                case 'logout':
                    loginService.logout();
                    break;
                default:
                    $scope.item = 'Default selection.';
                    break;
            } // end switch
        }; // end navfn

        $scope.toggleSearchForm = function(){
            $scope.search.show = !$scope.search.show;
            if(angular.equals($scope.search.show,true))
                $scope.searchDisplay = 'Visible';
            else
                $scope.searchDisplay = 'Hidden';
        }; // end toggleSearchForm

        $scope.addMenu = function(){
            $scope.menus.push({
                title : "Added On The Fly!",
                action : "default"
            });
        }; // end test

    }) // end navbarDirectiveTestCtrl

/**
 * Navbar Directive
 *
 * @requires: Bootstrap js
 **/
    .directive('angledNavbar',function(){
        return {
            restrict : 'AE',
            scope : {
                menus : '=',
                search : '=',
                searchfn : '&',
                navfn : '&'
            },
            templateUrl : 'app/shared/navbar/generalNavBar.html',
            controller : function($scope,$element,$attrs){
                //=== Scope/Attributes Defaults ===//

                $scope.defaults = {
                    menus : [],
                    search : {
                        show : false
                    }
                }; // end defaults

                // if no parent function was passed to directive for navfn, then create one to emit an event
                if(angular.isUndefined($attrs.navfn)){
                    $scope.navfn = function(action){
                        if(angular.isObject(action))
                            $scope.$emit('nav.menu',action);
                        else
                            $scope.$emit('nav.menu',{'action' : action});
                    }; // end navfn
                }

                // if no parent function was passed to directive for searchfn, then create one to emit a search event
                if(angular.isUndefined($attrs.searchfn)){
                    $scope.searchfn = function(){
                        $scope.$emit('nav.search.execute');
                    }; // end searchfn
                }

                //=== Observers & Listeners ===//



                //=== Methods ===//

                $scope.noop = function(){
                    angular.noop();
                }; // end noop

                $scope.navAction = function(action){
                    $scope.navfn({'action' : action});
                }; // end navAction


                /**
                 * Has Menus
                 * Checks to see if there were menus passed in for the navbar.
                 * @result  boolean
                 */
                $scope.hasMenus = function(){
                    return (angular.isDefined($attrs.menus));
                };

                /**
                 * Has Dropdown Menu
                 * Check to see if navbar item should have a dropdown menu
                 * @param  object  menu
                 * @result  boolean
                 */
                $scope.hasDropdownMenu = function(menu){
                    return (angular.isDefined(menu.menu) && angular.isArray(menu.menu));
                }; // end hasDropdownMenu

                /**
                 * Is Divider
                 * Check to see if dropdown menu item is to be a menu divider.
                 * @param  object  item
                 * @result  boolean
                 */
                $scope.isDivider = function(item){
                    return (angular.isDefined(item.divider) && angular.equals(item.divider,true));
                }; // end isDivider
            }
        };
    }); // end navbar

