/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: the directive that mgives the popup and countdown timer for the autologout function.
 * the directive listens for the broadcast of 'autoLogout' on the rootscope.
 * You have 2 minutes before a logout happens.
 */
'use strict';
var autoLogout = angular.module('clarity.index');

autoLogout.directive('autoLogoutPopup',['$log', '$timeout', '$location', 'applicationInformation', 'httpServiceFactory', 'autoLogoutService', function($log, $timeout, $location, applicationInformation, httpServiceFactory, autoLogoutService) {


    function link(scope, element, attrs){
        scope.minutes = 0;
        scope.seconds = 0;
        scope.autoLogoutVisibility = false;
        var timeLeft = applicationInformation.getReAuthenticationTimeOutTime();
        var timer = null;

        function startcounter(){
            if(timeLeft <= 0){
                close();
                scope.logout();
            }else{
                timeLeft--;
                scope.seconds = timeLeft % 60;
                scope.minutes = Math.floor(timeLeft/60);
                timer = $timeout(startcounter,1000);
            }
        }

        scope.$on('autoLogout', function (event, data) {
            $log.log("open autologout");
            timeLeft = applicationInformation.getReAuthenticationTimeOutTime();
            scope.autoLogoutVisibility = true;
            startcounter();
        });

        function close(){
            $log.log("close");
            scope.autoLogoutVisibility = false;
            if(timer != null){
                $timeout.cancel(timer);
            }
        }

        scope.reAuthenticate = function(){
            httpServiceFactory.createGetPromise('/suchcrypto/muchToken', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getUserinformation().token)
                .success(function (data, status, headers, config) {
                    applicationInformation.setToken(headers('token'));
                    close();
                }).error(function (error, status) {
                });
        };

        scope.logout = function(){
            close();
            applicationInformation.setUserInformation(undefined, undefined, undefined, undefined);
            $location.path('/login');
        };
    }

    return {
        restrict: 'EA',
        templateUrl: 'app/shared/autoLogout/autoLogoutTemplate.html',
        link: link
    };
}]);