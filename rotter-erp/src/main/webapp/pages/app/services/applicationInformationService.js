/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: here application variables are kept. Like the connection string to the api or times.
 */
'use strict';
var service = angular.module('clarity.applicationInformation');

service.factory('applicationInformation', ['$log',function($log){
    //uri for the login and the secure rest api
    var baseApiUrl = "http://ec2-52-28-129-182.eu-central-1.compute.amazonaws.com:8080/Clarity";
    var securedBaseApiUrl = "http://ec2-52-28-129-182.eu-central-1.compute.amazonaws.com:8080/Clarity/secure";
    //var baseApiUrl = "http://localhost:8080/Clarity";
    //var securedBaseApiUrl = "http://localhost:8080/Clarity/secure";
    //This is the time in millisecondes. this indicates how long befor the token expiration you will get a notification for reauthentication.
    var autologoutTime = 300000;
    //this is the time you have before you get automatically signed out. max 5 minutes. (time in minutes)
    var reAuthenticationTime = 120;

    //user information
    var userId = undefined;
    var user = undefined;
    var role = undefined;
    var token = undefined;

    return {
        getBaseApiUrl: function () {
            return baseApiUrl;
        },
        getSecuredBaseApiUrl: function () {
            return securedBaseApiUrl;
        },
        getAutologoutTime: function () {
            return autologoutTime;
        },
        getReAuthenticationTimeOutTime: function () {
            return reAuthenticationTime;
        },
        getUserinformation: function () {
            return {userId: userId, user: user, role: role, token: token};
        },
        setUserInformation: function (userid, userName, userRole, authToken) {
            userId = userid;
            user = userName;
            role = userRole;
            token = authToken;
        },
        getUsername: function () {
            return user;
        },
        getRole: function () {
            return role;
        },
        getToken: function () {
            return token;
        },
        setToken: function (authToken) {
            token = authToken;
        },
        logout: function () {
            userId = undefined;
            user = undefined;
            role = undefined;
            token = undefined;
        }
    }
}]);