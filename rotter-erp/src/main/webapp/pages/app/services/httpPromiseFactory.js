/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: This service will generate the right http calls with the correct parameters for secure calls.
 * All the functions return a promise to use in another controller or service.
 * The autologout service will get an update of the new token and the new expiration date.
 */
'use strict';
var httpFactory = angular.module('clarity.generalServices');

httpFactory.factory('httpServiceFactory', ['$http', 'applicationInformation', 'autoLogoutService', 'jwtHelper', '$log', 'loadingService', 'loggingService', function($http, applicationInformation, autoLogoutService, jwtHelper, $log, loadingService, loggingService) {
    function createUrlDataPath(pathData, token, isSecured) {
        if (isSecured) {
            if (pathData != undefined) {
                return '?' + pathData + '&Authorization=' + token;
            } else {
                return '?Authorization=' + token;
            }
        } else {
            if (pathData != undefined) {
                return '?' + pathData;
            } else {
                return '';
            }
        }
    }

    function setAutologout(token) {
        var d = jwtHelper.getTokenExpirationDate(token);
        if (!d) {
            return false;
        }
        $log.log("expiration date of token " + new Date(d));
        autoLogoutService.setTimer(d.valueOf() - applicationInformation.getAutologoutTime());
    }

    return {
        createGetPromise: function (urlExtension, urlPathData, postData, isSecured, token) {
            loadingService.startLoading(null);
            if (isSecured) {
                return $http({
                    method: 'GET',
                    url: applicationInformation.getSecuredBaseApiUrl() + urlExtension + createUrlDataPath(urlPathData, token, isSecured),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data, status, headers, config) {
                    setAutologout(headers('token'));
                    loadingService.stopLoading();
                }).error(function (data, status, headers, config) {
                    loggingService.error(status);
                    loadingService.stopLoading();
                });
            } else {
                return $http({
                    method: 'GET',
                    url: applicationInformation.getBaseApiUrl() + urlExtension + createUrlDataPath(urlPathData, token, isSecured),
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data, status, headers, config) {
                    setAutologout(headers('token'));
                    loadingService.stopLoading();
                }).error(function (data, status, headers, config) {
                    loggingService.error(status);
                    loadingService.stopLoading();
                });
            }
        },
        createPostPromise: function (urlExtension, urlPathData, postData, isSecured, token, isJsonData) {
            loadingService.startLoading(null);
            if(isJsonData){
                if (isSecured) {
                    return $http({
                        method: 'POST',
                        url: applicationInformation.getSecuredBaseApiUrl() + urlExtension + createUrlDataPath(urlPathData, token, isSecured),
                        data: postData,
                        headers: {'Content-Type': 'application/json'}
                    }).success(function (data, status, headers, config) {
                        setAutologout(headers('token'));
                        loadingService.stopLoading();
                    }).error(function (data, status, headers, config) {
                        loggingService.error(status);
                        loadingService.stopLoading();
                    });
                } else {
                    return $http({
                        method: 'POST',
                        url: applicationInformation.getBaseApiUrl() + urlExtension + createUrlDataPath(urlPathData, token, isSecured),
                        data: postData,
                        headers: {'Content-Type': 'application/json'}
                    }).success(function (data, status, headers, config) {
                        setAutologout(headers('token'));
                        loadingService.stopLoading();
                        loadingService.stopLoading();
                    }).error(function (data, status, headers, config) {
                        loggingService.error(status);
                        loadingService.stopLoading();
                    });
                }
            }else {
                if (isSecured) {
                    return $http({
                        method: 'POST',
                        url: applicationInformation.getSecuredBaseApiUrl() + urlExtension + createUrlDataPath(urlPathData, token, isSecured),
                        data: postData,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (data, status, headers, config) {
                        setAutologout(headers('token'));
                        loadingService.stopLoading();
                    }).error(function (data, status, headers, config) {
                        loggingService.error(status);
                        loadingService.stopLoading();
                    });
                } else {
                    return $http({
                        method: 'POST',
                        url: applicationInformation.getBaseApiUrl() + urlExtension + createUrlDataPath(urlPathData, token, isSecured),
                        data: postData,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).success(function (data, status, headers, config) {
                        setAutologout(headers('token'));
                        loadingService.stopLoading();
                    }).error(function (data, status, headers, config) {
                        loggingService.error(status);
                        loadingService.stopLoading();
                    });
                }
            }
        }
    }
}]);
