/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: This is the service that is responsible for triggering the loading modal.
 */
'use strict';
var service = angular.module('clarity.generalServices');

service.factory('loadingService', ['$rootScope', function($rootScope) {
    var message = null;

    return {
        startLoading: function (loadingMessage) {
            if (loadingMessage == null || loadingMessage == "") {
                message = "Loading...";
            }else{
                message = loadingMessage;
            }

            $rootScope.$broadcast('StartLoading');
        },
        getLoadingMessage: function () {
            return message;
        },
        stopLoading: function () {
            message = null;
            $rootScope.$broadcast('StopLoading');
        }
    }
}]);