/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: this is the service that is responible for checking the authorization level of a route change.
 */
'use strict';

var authService = angular.module('clarity.route.authorization');

authService.factory('routeAuthorizeService', ['applicationInformation', '$log', function(applicationInformation, $log){
    return{
        enum: {authorized:{authorized: 0, loginRequired: 1, notAuthorized: 2},
        permissionCheckType:{atLeastOne:0, combinationRequired:1, noPermissionNeeded:2}},
        authorize : function(loginRequired, requiredPermissions, permissionCheckType){
            if(loginRequired === true && applicationInformation.getToken() === undefined){
                return this.enum.authorized.loginRequired;
            }else if(loginRequired === true && applicationInformation.getToken() !== undefined && permissionCheckType === this.enum.permissionCheckType.noPermissionNeeded){
                return this.enum.authorized.authorized;
            }else if(requiredPermissions){
                var role = applicationInformation.getRole();
                for(var i = 0; i < requiredPermissions.length; i++){
                    if((permissionCheckType === this.enum.permissionCheckType.atLeastOne) &&(role === requiredPermissions[i])){
                        return this.enum.authorized.authorized;
                    }else if(permissionCheckType === this.enum.permissionCheckType.combinationRequired){
                        return this.enum.authorized.notAuthorized;
                        //this has to be implemented if you want to use it. So do not use!!!
                    }
                }
                return this.enum.authorized.loginRequired;
            }else if(loginRequired === false){
                return this.enum.authorized.authorized;
            }
        }
    }
}]);