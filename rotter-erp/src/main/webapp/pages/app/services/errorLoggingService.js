/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: Service responsible for logging errors and warnings to the server.
 */
'use strict';
var service = angular.module('clarity.logging');

service.factory('loggingService', ['$log', '$http', 'applicationInformation', 'loadingService', function($log, $http, applicationInformation, loadingService) {
    function log(type, message){
        loadingService.startLoading(null);
        if(type == "warn"){
            $http({
                method: 'POST',
                url: applicationInformation.getBaseApiUrl() + '/log/warn?message=' + message,
                headers: {'Content-Type': 'text/plain'}
            }).success(function (data, status, headers, config) {
                loadingService.stopLoading();
            }).error(function (data, status, headers, config) {
                loadingService.stopLoading();
                $log.log("Could not log your warning to the server.");
            });
        }else if(type == "error"){
            $http({
                method: 'POST',
                url: applicationInformation.getBaseApiUrl() + '/log/error?messsage=' + message,
                headers: {'Content-Type': 'text/plain'}
            }).success(function (data, status, headers, config) {
                loadingService.stopLoading();
            }).error(function (data, status, headers, config) {
                loadingService.stopLoading();
                $log.log("Could not log your error to the server.");
            });
        }
    }

    return {
        error: function(message){
            log("error", message);
        },
        warn: function(message){
            log("warn", message);
        }
    }
}]);
