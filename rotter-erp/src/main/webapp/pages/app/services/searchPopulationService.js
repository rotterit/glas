/**
 * Created by Suited Coders Gunther Claessens.
 *
 * @Description: a service responsible for having the searchable information to populate the search bars for products or raw material.
 */
'use strict';

var service = angular.module('clarity.services');

service.factory('searchPopulationService',[function() {
    var rawMaterialSearchParameters = undefined;

    return {
        getRawMaterialSearchParameters: function(){
            return rawMaterialSearchParameters;
        },
        setRawMaterialSearchParameters: function(searchParams){
            rawMaterialSearchParameters = searchParams;
        }
    }
}]);