/**
 * Created by Suited Coder Gunther Claessens
 *
 * @Description: Service responsible for the handling of the drop down notification bar. Here the right notification bars will be shown on the screen. The html for this you can find in the index.html page.
 */
'use strict';
var service = angular.module('clarity.services');

service.factory('notificationService', ['$log', 'notifications', function($log, notifications){
    return {
        showErrorNotification: function (notificationMessage) {
            notifications.showError({message: notificationMessage});
            $log.log("notification:error: " + notificationMessage);
        },
        showWarningNotification: function (notificationMessage) {
            notifications.showWarning({message: notificationMessage});
            $log.log("notification:warning: " + notificationMessage);
        },
        showMessageNotification: function (notificationMessage) {
            notifications.showSuccess({message: notificationMessage});
            $log.log("notification:message: " + notificationMessage);
        }
    }
}]);