/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: the service that holds the items for the shopping cart for raw material and finished products.
 */
'use strict';
var invoiceShoppingCartService = angular.module('clarity.helpers');

invoiceShoppingCartService.factory('invoiceShoppingCart', ['$log', 'notificationService', function($log, notificationService) {
    var shoppingCartState = "create";
    var invoiceInformation = {"customerId": null, "dueDate": null, "paymentDueDate": null, "priceWithoutVAT": 0, "localPrice": 0, "ValutaFactor": 1, "price": 0,
                                "VATpercentage": 19, "VAT": 0, "discountpercentage": 0, "discount": 0, "packingCostPercentage": 2, "packingCost": 0,
                                "upfrontpercentage": 0, "valuta": "Euro", "note": "", "invoiceCreateLines": [], "shippingCost": 0, "shippingMethod": "DPD"};
    var invoiceLines = [];
    var invoiceId = 0;
    var invoicenr = null;
    var fullEditingInvoice = {};

    return{
        addItemToInvoice: function(finishedProduct, quantity){
            if(Number(quantity) <= 0 || isNaN(quantity) || quantity == ""){return;}

            for(var i = 0; i < invoiceLines.length; i++){
                if(finishedProduct.id == invoiceLines[i].finishedProduct.id && finishedProduct.stock.id == invoiceLines[i].finishedProduct.stock.id){
                    invoiceLines[i].quantity = Number(invoiceLines[i].quantity) + Number(quantity);
                    return invoiceLines;
                }
            }

            return invoiceLines.push({"finishedProduct": finishedProduct, "quantity": Number(quantity), "inlineDiscount": 0});
        },
        removeProductFromInvoice: function(finishedProductId){
            for(var i = 0; i < invoiceLines.length; i++){
                if(Number(invoiceLines[i].finishedProduct.id) == Number(finishedProductId)){
                    invoiceLines.splice(i, 1);
                    return invoiceLines;
                }
            }
            return invoiceLines;
        },
        cancelInvoice: function(){
            invoiceInformation = {"customerId": null, "dueDate": null, "paymentDueDate": null, "priceWithoutVAT": null, "localPrice": null, "ValutaFactor": 1, "price": null,
                "VATpercentage": 19, "VAT": null, "discountpercentage": 0, "discount": null, "packingCostPercentage": 2, "packingCost": null,
                "upfrontpercentage": null, "valuta": "Euro", "note": "", "invoiceCreateLines": [], "shippingCost": 0, "shippingMethod": "DPD"};
            invoiceLines = [];
        },
        closeInvoice: function(){
            invoiceInformation = {"customerId": null, "dueDate": null, "paymentDueDate": null, "priceWithoutVAT": null, "localPrice": null, "ValutaFactor": 1, "price": null,
                "VATpercentage": 19, "VAT": null, "discountpercentage": 0, "discount": null, "packingCostPercentage": 2, "packingCost": null,
                "upfrontpercentage": null, "valuta": "Euro", "note": "", "invoiceCreateLines": [], "shippingCost": 0, "shippingMethod": "DPD"};
            invoiceLines = [];
            invoiceId = 0;
            invoicenr = null;
            fullEditingInvoice = {};
        },
        saveInvoiceInformation: function(invoiceInfo){
            invoiceInformation = invoiceInfo;
        },
        saveInvoiceLines: function(invoiceProductLines) {
            invoiceLines = invoiceProductLines;
        },
        saveInvoice: function(invoiceInfo, invoiceProductLines){
            invoiceInformation = invoiceInfo;
            invoiceLines = invoiceProductLines;
        },
        getInvoiceShoppingCart: function(){ return invoiceLines; },
        getInvoiceInformation: function() { return invoiceInformation; },
        getInvoiceDetailLineInformation: function(){
            return "You have " + invoiceLines.length + " items on the invoice.";
        },
        setInvoiceId: function(invoiceid) {
            invoiceId = Number(invoiceid);
        },
        getInvoiceId: function(){
             return invoiceId;
        },
        setInvoiceNr: function(invoiceNr) {
            invoicenr = invoiceNr;
        },
        getInvoiceNr: function(){
             return invoicenr;
        },
        setEditingInvoice: function(invoice) {
            fullEditingInvoice = invoice;
        },
        getEditingInvoice: function(){
             return fullEditingInvoice;
        }
    }
}]);