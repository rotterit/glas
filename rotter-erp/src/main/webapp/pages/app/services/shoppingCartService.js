/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: the service that holds the items for the shopping cart for raw material and finished products.
 */
'use strict';
var shoppingCartService = angular.module('clarity.helpers');

shoppingCartService.factory('shoppingCart', ['$log', 'notificationService', function($log, notificationService) {
    var shoppingCartState = "";
    var rawMaterialShoppingCart = [];
    var oldShoppingCart = [];
    var shoppingCartNote = {"id": null, "content":""};

    return{
        addToRawOrderCart: function(rawMaterial, quantity){
            if(Number(quantity) <= 0 || isNaN(quantity) || quantity == ""){return;}

            if(shoppingCartState == "" && rawMaterialShoppingCart.length == 0){
                shoppingCartState = "create";
            }

            for(var i = 0; i < rawMaterialShoppingCart.length; i++){
                if(rawMaterial.id == rawMaterialShoppingCart[i].rawMaterial.id){
                    rawMaterialShoppingCart[i].quantity = Number(rawMaterialShoppingCart[i].quantity) + Number(quantity);
                    return;
                }
            }

            if(rawMaterialShoppingCart.length > 0 && rawMaterial.supplierid == rawMaterialShoppingCart[0].rawMaterial.supplierid){
                rawMaterialShoppingCart.push({"rawMaterial":rawMaterial, "quantity":quantity});
            }else if(rawMaterialShoppingCart.length == 0){
                rawMaterialShoppingCart.push({"rawMaterial":rawMaterial, "quantity":quantity});
            }else{
                notificationService.showWarningNotification("You can't add this product. All products have to be from the same supplier.");
            }
        },
        removeFromRawOrderCart: function(rawMaterialId){
            for(var i = 0; i < rawMaterialShoppingCart.length; i++){
                if(rawMaterialShoppingCart[i].rawMaterial.id.toString() == rawMaterialId.toString()){
                    rawMaterialShoppingCart.splice(i, 1);
                    return rawMaterialShoppingCart;
                }
            }
            return rawMaterialShoppingCart;
        },
        cancelRawMaterialOrder: function(){
            shoppingCartState = "";
            shoppingCartNote = {"id": null, "content":""};
            rawMaterialShoppingCart = [];
            oldShoppingCart = [];
        },
        closeOrder: function(){
            shoppingCartState = "";
            shoppingCartNote = {"id": null, "content":""};
            rawMaterialShoppingCart = [];
            oldShoppingCart = [];
        },
        getRawMaterialOrderShoppingCart: function(){
            return rawMaterialShoppingCart;
        },
        getRawOrderDetailLine: function(){
            var totalPrice = 0;
            var totalGlasses = 0;
            for(var i = 0; i < rawMaterialShoppingCart.length; i++){
                totalGlasses += Number(rawMaterialShoppingCart[i].quantity);
                totalPrice += (Number(rawMaterialShoppingCart[i].rawMaterial.price) * Number(rawMaterialShoppingCart[i].quantity));
            }
            var detail = rawMaterialShoppingCart.length + " items in order for a total amount of " + totalGlasses + " costing " + totalPrice.toFixed(2);
            return detail;
        },
        getOldShoppingCart: function(){
            return oldShoppingCart;
        },
        getShoppingcartState: function(){
            return shoppingCartState;
        },
        setShoppingCart: function(newShoppingCart){
            shoppingCartState = "edit";
            rawMaterialShoppingCart = newShoppingCart;
            oldShoppingCart = angular.copy(newShoppingCart);
        },
        getShoppingCartNote: function(){
            return shoppingCartNote;
        },
        setShoppingCartNote: function(note){
            shoppingCartNote = note;
        }
    }
}]);