/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: This is the service that is responsible for triggering the autlogout procedure..
 * This will get a reset every time you make a call to the api.
 */
'use strict';
var service = angular.module('clarity.generalServices');

service.factory('autoLogoutService', ['$rootScope', '$timeout', '$log', function($rootScope, $timeout, $log) {
    var timer = null;

    return{
        setTimer: function(time) {
            if(timer != null){
                $timeout.cancel(timer);
                timer = null;
            }
            $log.log("sTime " + new Date(time) + " epoch " + new Date(time).valueOf());
            $log.log("cTime " + new Date() + " epoch " + new Date().valueOf());
            $log.log("timeDifference " + (new Date(time).valueOf() - new Date().valueOf()));

            timer = $timeout(function() {
                $log.log("trigger autoSignout procedure.");
                $rootScope.$broadcast('autoLogout');
                //$rootScope.$emit('autoLogout');
            }, (new Date(time).valueOf() - new Date().valueOf()));
        }
    }
}]);