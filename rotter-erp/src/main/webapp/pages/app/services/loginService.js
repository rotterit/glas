/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: the srvice that handles everything with login.
 * We keep the username, role, token and userid in here as well.
 */
'use strict';
var service = angular.module('clarity.securityServices');

service.factory('loginService', ['$http', '$q', 'applicationInformation', '$log', 'notificationService', '$location', 'httpServiceFactory', 'jwtHelper', function($http, $q, applicationInformation, $log, notificationService, $location, httpServiceFactory, jwtHelper) {
    function logMeIn(username, password) {
        var defer = $q.defer();
        httpServiceFactory.createPostPromise('/login', undefined, 'username=' + username + '&password=' + CryptoJS.SHA256(password).toString(), false, undefined)
            .success(function (data, status, headers, config) {
                applicationInformation.setUserInformation(data.id, data.username, data.roleid, headers('token'));
                defer.resolve("");
            }).error(function (error, status) {
                if (status == 401) {
                    notificationService.showErrorNotification(status + " Username or Password are wrong.");
                } else {
                    notificationService.showWarningNotification(status + " Something went wrong logging in.");
                }
                defer.reject("");
            });
        return defer.promise;
    }

    return{
        login: function login(username, password){
            return logMeIn(username, password);
        },
        logout: function(){
            applicationInformation.setUserInformation(undefined, undefined, undefined, undefined);
            $location.path('/login');
        }
    }
}]);