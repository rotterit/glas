/**
 * Created by Suited Coder Gunther Claessens.
 */
var service = angular.module('clarity.services');

service.factory('crudService', ['$log', 'httpServiceFactory', 'applicationInformation', 'notificationService', function($log, httpServiceFactory, applicationInformation, notificationService){
    var editingColor = null;
    var editingSize = null;
    var editingSupplier = null;
    var editingQuality = null;
    var editingDesign = null;
    var editingProductType = null;
    var editingLocation = null;
    var editingRole = null;
    var editingCustomer = null;
    var editingStock = null;
    var editingSerie = null;
    var editingUser = null;
    var editingCategory = null;
    var editingRawMaterial = null;
    var editingOrder = null;
    var editingManufacturingType = null;
    var editingMaterialNotification = null;
    var detailInvoice = null;
    var creatingWorksheet = null;
    var detailWorksheet = null;
    var correctingRawMaterial = null;
    var correctingFinishedProduct = null;
    var stockCreationFinishedProduct = null;
    var finishedProductDetail = null;

    return{
        getColors: function() {
            return httpServiceFactory.createGetPromise('/colors/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get colors from server.");
                });
        },
        getSizes: function() {
            return httpServiceFactory.createGetPromise('/sizes/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get sizes from server.");
                });
        },
        getSuppliers: function() {
            return httpServiceFactory.createGetPromise('/suppliers/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get suppliers from server.");
                });
        },
        getQualities: function() {
            return httpServiceFactory.createGetPromise('/qualities/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get qualities from server.");
                });
        },
        getDesigns: function() {
            return httpServiceFactory.createGetPromise('/designs/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get designs from server.");
                });
        },
        getProductTypes: function() {
            return httpServiceFactory.createGetPromise('/producttypes/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get product types from server.");
                });
        },
        getLocations: function() {
            return httpServiceFactory.createGetPromise('/locations/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get locations from server.");
                });
        },
        getRoles: function() {
            return httpServiceFactory.createGetPromise('/roles/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get roles from server.");
                });
        },
        getCustomers: function() {
            return httpServiceFactory.createGetPromise('/customers/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get customers from server.");
                });
        },
        getUnits: function() {
            return httpServiceFactory.createGetPromise('/units/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get units from server.");
                });
        },
        getRawStocks: function() {
            return httpServiceFactory.createGetPromise('/stocks/raw/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get raw stocks from server.");
                });
        },
        getFinishedStocks: function() {
            return httpServiceFactory.createGetPromise('/stocks/finished/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get finished stocks from server.");
                });
        },
        getSeries: function() {
            return httpServiceFactory.createGetPromise('/series/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get series from server.");
                });
        },
        getUsers: function() {
            return httpServiceFactory.createGetPromise('/users/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get users from server.");
                });
        },
        getCategories: function() {
            return httpServiceFactory.createGetPromise('/categories/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get categories from server.");
                });
        },
        getRawMaterialDetail: function() {
            return httpServiceFactory.createGetPromise('/raw/byid', 'userid=' + applicationInformation.getUserinformation().userId + "&rawmaterialid=" + editingRawMaterial.id, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get raw material detail from server.");
                });
        },
        getManufacturingTypes: function() {
            return httpServiceFactory.createGetPromise('/manufacturingtypes/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get manufacturing types from server.");
                });
        },
        getMaterialNotifications: function() {
            return httpServiceFactory.createGetPromise('/materialnotifications/all', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get material notifications from server.");
                });
        },
        getProductionQueueItems: function() {
            return httpServiceFactory.createGetPromise('/worksheets/getproductionqueue', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                .success(function (data, status, headers, config) {
                    $log.log(data);
                })
                .error(function (error, status) {
                    $log.log(error);
                    notificationService.showErrorNotification(status + ": oops... Could not get production queue items from server.");
                });
        },
        getLandingpageInfo: function() {
            if(applicationInformation.getRole() == 1 || applicationInformation.getRole() == 2) {
                return httpServiceFactory.createGetPromise('/landing/get', 'userid=' + applicationInformation.getUserinformation().userId, undefined, true, applicationInformation.getToken())
                    .success(function (data, status, headers, config) {
                        $log.log(data);
                    })
                    .error(function (error, status) {
                        notificationService.showErrorNotification(status + ": oops... Could not get landing page info from server.");
                    });
            } else {
                return null;
            }
        },
        setEditingColor: function(color){
            editingColor = color;
        },
        getEditingColor: function() {
            return editingColor;
        },
        setEditingSize: function(size){
            editingSize = size;
        },
        getEditingSize: function() {
            return editingSize;
        },
        setEditingSupplier: function(supplier){
            editingSupplier = supplier;
        },
        getEditingSupplier: function() {
            return editingSupplier;
        },
        setEditingQuality: function(quality){
            editingQuality = quality;
        },
        getEditingQuality: function() {
            return editingQuality;
        },
        setEditingDesign: function(design){
            editingDesign = design;
        },
        getEditingDesign: function() {
            return editingDesign;
        },
        setEditingProductType: function(productType){
            editingProductType = productType;
        },
        getEditingProductType: function() {
            return editingProductType;
        },
        setEditingLocation: function(location){
            editingLocation = location;
        },
        getEditingLocation: function() {
            return editingLocation;
        },
        setEditingRole: function(role){
            editingRole = role;
        },
        getEditingRole: function() {
            return editingRole;
        },
        setEditingCustomer: function(customer){
            editingCustomer = customer;
        },
        getEditingCustomer: function() {
            return editingCustomer;
        },
        setEditingStock: function(stock){
            editingStock = stock;
        },
        getEditingStock: function() {
            return editingStock;
        },
        setEditingSeries: function(serie){
            editingSerie = serie;
        },
        getEditingSeries: function() {
            return editingSerie;
        },
        setEditingUser: function(user){
            editingUser = user;
        },
        getEditingUser: function() {
            return editingUser;
        },
        setEditingCategory: function(category){
            editingCategory = category;
        },
        getEditingCategory: function() {
            return editingCategory;
        },
        setEditingRawMaterial: function(rawMaterial){
            editingRawMaterial = rawMaterial;
        },
        setEditingOrder: function(order){
            editingOrder = order;
        },
        getEditingOrder: function() {
            return editingOrder;
        },
        setEditingManufacturingType: function(manufacturingType){
            editingManufacturingType = manufacturingType;
        },
        getEditingManufacturingType: function() {
            return editingManufacturingType;
        },
        setEditingMaterialNotification: function(materialNotification){
            editingMaterialNotification = materialNotification;
        },
        getEditingMaterialNotification: function() {
            return editingMaterialNotification;
        },
        setDetailInvoice: function(invoice){
            detailInvoice = invoice;
        },
        getDetailInvoice: function() {
            return detailInvoice;
        },
        setCreatingWorksheet: function(worksheet){
            creatingWorksheet = worksheet;
        },
        getCreatingWorksheet: function() {
            return creatingWorksheet;
        },
        setDetailWorksheet: function(worksheet){
            detailWorksheet = worksheet;
        },
        getDetailWorksheet: function() {
            return detailWorksheet;
        },
        setCorrectingRawMaterial: function(rawMaterial){
            correctingRawMaterial = rawMaterial;
        },
        getCorrectingRawMaterial: function() {
            return correctingRawMaterial;
        },
        setCorrectingFinishedProduct: function(finishedProduct){
            correctingFinishedProduct = finishedProduct;
        },
        getCorrectingFinishedProduct: function() {
            return correctingFinishedProduct;
        },
        setFinishedProductDetail: function(finishedProduct){
            finishedProductDetail = finishedProduct;
        },
        getFinishedProductDetail: function() {
            return finishedProductDetail;
        },
        setStockCreationFinishedProduct: function(finishedProduct){
            stockCreationFinishedProduct = finishedProduct;
        },
        getStockCreationFinishedProduct: function() {
            return stockCreationFinishedProduct;
        }
    }
}]);