/**
 * Created by Suited Coder Gunther Claessens.
 *
 * @Description: this is the service that can be called if you want to display a textbox with text that can be copy pasted.
 */
'use strict';
var copypasterinoService = angular.module('clarity.generalServices');

copypasterinoService.factory('copypasterinoService', ['$rootScope', function($rootScope) {
    return{
        showText: function(text){
            $rootScope.$broadcast('copypasterino', text);
        }
    }
}]);