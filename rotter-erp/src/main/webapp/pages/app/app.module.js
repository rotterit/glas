/**
 * Created by Suited Coder Gunther Claessens
 *
 * @Description: the class where the app is created and al other modules are added to the app. Here the configurations of the app are set.
 */
'use strict';
var app = angular.module("clarity", ['clarity.route',
    'clarity.applicationInformation',
    'clarity.securityServices',
    'clarity.generalServices',
    'clarity.services',
    'clarity.logging',
    'clarity.helpers',
    'clarity.route.authorization',
    'clarity.index',
    'clarity.product',
    'clarity.overview',
    'clarity.crud',
    'clarity.search',
    'clarity.material.raw',
    'clarity.material.finished',
    'door3.css',
    'ngNotificationsBar',
    'ngGrid'
]);

//services
angular.module('clarity.applicationInformation', []);
angular.module('clarity.generalServices', []);
angular.module('clarity.helpers', []);
angular.module('clarity.securityServices', []);
angular.module('clarity.services', []);
angular.module('clarity.logging', []);
//controllers
angular.module('clarity.index', []);
angular.module('clarity.product', []);
angular.module('clarity.overview', []);
angular.module('clarity.crud', []);
angular.module('clarity.search', []);
angular.module('clarity.material.raw', []);
angular.module('clarity.material.finished', []);
angular.module('clarity.route.authorization', []);



app.config(['notificationsConfigProvider', function(notificationsConfigProvider){
    //configuration for the drop down notification bar.
    notificationsConfigProvider.setHideDelay(1200);
    notificationsConfigProvider.setAutoHide(true);
}]);