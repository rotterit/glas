/**
 * Created by Suited Coder Gunther Claessens
 *
 * @Description: controller class for the index page. This contains logic that is used in every page.
 */
'use strict';
var controller = angular.module('clarity.index');

controller.controller('IndexCtrl', ['$scope', '$log', 'notificationService', function($scope, $log, notificationService){
    $scope.showNavBar = false;

    $scope.showError = function (errorMessage) {
        notificationService.showErrorNotification(errorMessage);
    };

    $scope.showWarning = function (warningMessage) {
        notificationService.showWarningNotification(warningMessage);
    };

    $scope.showMessage = function (message) {
        notificationService.showMessageNotification(message);
    };
}]);