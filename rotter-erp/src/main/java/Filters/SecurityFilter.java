package Filters;

import Security.JWTVerifier;
import Security.JWTVerifyException;
import Util.UtilityProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Map;

/**
 * Created by thijs on 5-5-2015.
 */
public class SecurityFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        JWTVerifier verifier = new JWTVerifier(UtilityProvider.getSecret());
        String httpHeader = request.getParameter("Authorization");
        Map<String,Object> payload = null;
        try {
            payload  = verifier.verify(httpHeader);
        } catch (Exception e){
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token failed validation.");
            return;
        }
        if(payload!=null){
            if(payload.get("jti").equals(UtilityProvider.getIdentifier())) {
                request.setAttribute("userid", payload.get("sub"));
            }else{
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token failed validation.");
                return;
            }
        }else{
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token failed validation.");
            return;
        }
        filterChain.doFilter(request, response);
    }
}