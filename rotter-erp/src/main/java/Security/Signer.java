package Security;

import Util.UtilityProvider;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by thijs on 6-5-2015.
 */
@Service
public class Signer {

    public String sign(int userid){
        JWTSigner signer = new JWTSigner(UtilityProvider.getSecret());
        Map<String, Object> claims = new HashMap<>();
        claims.put("jti", UtilityProvider.getIdentifier());
        Long now = System.currentTimeMillis() /1000L;
        claims.put("exp", now+ 1800);
        claims.put("sub", ""+userid);
        String result = signer.sign(claims);
        return result;
    }
}
