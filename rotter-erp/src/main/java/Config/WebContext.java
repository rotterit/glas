package Config;

import DTO.Material.MaterialFilter;
import Filters.SimpleCORSFilter;
import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

/**
 * Created by thijs on 5-5-2015.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"DAL","Controllers","Security","Filters"})
@ImportResource("/WEB-INF/transactionmanager.xml")
public class WebContext extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations("/pages/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SimpleCORSFilter()).addPathPatterns("/**");
    }

    @Bean
    public Gson gson(){
        return new Gson();
    }

}
