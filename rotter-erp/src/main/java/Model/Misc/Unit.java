package Model.Misc;

import Model.Misc.Note;

import javax.persistence.*;

/**
 * Created by thijs on 8-5-2015.
 */
@Entity
@Table(name="t_unit")
public class Unit {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "unitId", nullable = false)
    private int id;
    @Column
    private String name;
    @Column
    private String textValue;
    @Column
    private double numericalValue;
    @ManyToOne
    private Note note;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public double getNumericalValue() {
        return numericalValue;
    }

    public void setNumericalValue(double numericalValue) {
        this.numericalValue = numericalValue;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}
