package Model.Misc;

import javax.persistence.*;

/**
 * Created by thijs on 29-4-2015.
 */
@Entity
@Table(name="t_note")
public class Note {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "noteId", nullable = false)
    private int id;

    @Column(columnDefinition="TEXT")
    private String content;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
