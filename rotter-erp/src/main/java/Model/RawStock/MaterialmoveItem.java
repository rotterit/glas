package Model.RawStock;

import Model.RawMaterial.RawMaterial;
import Model.Stock.Stock;

import javax.persistence.*;
/**
 * Created by thijs on 20-4-2015.
 */

@Entity
@Table(name="t_materialmoveitem")
public class MaterialmoveItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "materialmoveitemId", nullable = false)
    private Long id;
    @ManyToOne
    private RawMaterial rawMaterial;
    @Column
    private int amount;
    @ManyToOne
    private Stock stock;

    public MaterialmoveItem() {
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RawMaterial getRawMaterial() {
        return rawMaterial;
    }

    public void setRawMaterial(RawMaterial rawMaterial) {
        this.rawMaterial = rawMaterial;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
