package Model.RawStock;

import Model.Misc.Note;
import Model.Stock.Stock;
import Model.RawMaterial.Supplier;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.*;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_materialmove")
public class Materialmove {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "materialMoveId", nullable = false)
    private long id;
    @Column
    private boolean isDelivery;
    @Column
    private boolean isComplete;
    @ManyToOne
    private User completedBy;
    @Column
    private boolean confirmed;
    @ManyToOne
    private User confirmedBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @Column
    private boolean delivered;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime deliveryDate;
    @ManyToOne
    private User deliveryRecievedBy;
    @ManyToOne
    private Supplier supplier;
    @OneToMany
    private List<MaterialmoveItem> materialmoveItems;
    @ManyToOne
    private User createdBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;
    @ManyToOne
    private Note note;

    public Materialmove(){
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isDelivery() {
        return isDelivery;
    }

    public void setDelivery(boolean isDelivery) {
        this.isDelivery = isDelivery;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDateTime deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public List<MaterialmoveItem> getMaterialmoveItems() {
        return materialmoveItems;
    }

    public void setMaterialmoveItems(List<MaterialmoveItem> materialmoveItems) {
        this.materialmoveItems = materialmoveItems;
    }

    public User getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(User completedBy) {
        this.completedBy = completedBy;
    }

    public User getConfirmedBy() {
        return confirmedBy;
    }

    public void setConfirmedBy(User confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    public User getDeliveryRecievedBy() {
        return deliveryRecievedBy;
    }

    public void setDeliveryRecievedBy(User deliveryRecievedBy) {
        this.deliveryRecievedBy = deliveryRecievedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }
}
