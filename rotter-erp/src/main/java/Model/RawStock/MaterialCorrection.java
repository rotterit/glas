package Model.RawStock;

import Model.Misc.Note;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by thijs on 14-5-2015.
 */
@Entity
@Table(name="t_materialcorrection")
public class MaterialCorrection {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "materialCorrectionId", nullable = false)
    private long id;

    @Column
    private String reason;

    @ManyToOne
    private Materialmove materialmove;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @ManyToOne
    private User createdBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;

    @ManyToOne
    private Note note;

    public MaterialCorrection() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }


    public Materialmove getMaterialmove() {
        return materialmove;
    }

    public void setMaterialmove(Materialmove materialmove) {
        this.materialmove = materialmove;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}
