package Model.RawStock;

import Model.RawMaterial.RawMaterial;
import Model.Stock.Stock;

import javax.persistence.*;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_materialstockline")
public class MaterialStockLine {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "materialstocklineId", nullable = false)
    private int id;
    @OneToOne
    private Stock stock;
    @OneToOne
    private RawMaterial rawMaterial;
    @Column
    private int amount;

    public MaterialStockLine() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public RawMaterial getRawMaterial() {
        return rawMaterial;
    }

    public void setRawMaterial(RawMaterial rawMaterial) {
        this.rawMaterial = rawMaterial;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
