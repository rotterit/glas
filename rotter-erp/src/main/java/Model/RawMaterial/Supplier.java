package Model.RawMaterial;

import Model.Misc.Note;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_supplier")
public class Supplier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "supplierId", nullable = false)
    private int id;
    @Column
    private String suplierCode;
    @Column
    private String name;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @ManyToOne
    private Note contactnote;
    @ManyToOne
    private Note suppliernote;
    @ManyToOne
    private Note generalnote;

    @ManyToOne
    private User createdBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;

    public Supplier() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSuplierCode() {
        return suplierCode;
    }

    public void setSuplierCode(String suplierCode) {
        this.suplierCode = suplierCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Note getContactnote() {
        return contactnote;
    }

    public void setContactnote(Note contactnote) {
        this.contactnote = contactnote;
    }

    public Note getSuppliernote() {
        return suppliernote;
    }

    public void setSuppliernote(Note suppliernote) {
        this.suppliernote = suppliernote;
    }

    public Note getGeneralnote() {
        return generalnote;
    }

    public void setGeneralnote(Note generalnote) {
        this.generalnote = generalnote;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
