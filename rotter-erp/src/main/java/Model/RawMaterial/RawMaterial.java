package Model.RawMaterial;

import Model.FinishedProduct.Color;
import Model.FinishedProduct.MySize;
import Model.FinishedProduct.ProductType;
import Model.Misc.Note;
import Model.Stock.Stock;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_rawmaterial")
public class RawMaterial {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "rawmaterialId", nullable = false)
    private int id;
    @Column
    private String Description;
    @ManyToOne
    private ProductSupplierLine productSupplierLine;
    @ManyToOne
    private Color color;
    @ManyToOne
    private Supplier supplier;
    @ManyToOne
    private Quality quality;
    @Column
    private double price;
    @ManyToOne
    private Series series;
    @ManyToOne
    private MySize mySize;
    @ManyToOne
    private ProductType productType;
    @ManyToOne
    private Note note;
    @Column
    private int minimumOrderQuantity;
    @Column
    private int baseminStockAmount;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @ManyToOne
    private User createdBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;

    public RawMaterial() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Quality getQuality() {
        return quality;
    }

    public void setQuality(Quality quality) {
        this.quality = quality;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Series getSeries() {
        return series;
    }

    public void setSeries(Series series) {
        this.series = series;
    }

    public MySize getMySize() {
        return mySize;
    }

    public void setMySize(MySize mySize) {
        this.mySize = mySize;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public ProductSupplierLine getProductSupplierLine() {
        return productSupplierLine;
    }

    public void setProductSupplierLine(ProductSupplierLine productSupplierLine) {
        this.productSupplierLine = productSupplierLine;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public int getMinimumOrderQuantity() {
        return minimumOrderQuantity;
    }

    public void setMinimumOrderQuantity(int minimumOrderQuantity) {
        this.minimumOrderQuantity = minimumOrderQuantity;
    }

    public int getBaseminStockAmount() {
        return baseminStockAmount;
    }

    public void setBaseminStockAmount(int baseminStockAmount) {
        this.baseminStockAmount = baseminStockAmount;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
