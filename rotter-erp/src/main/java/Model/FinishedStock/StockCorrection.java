package Model.FinishedStock;

import Model.Misc.Note;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by thijs on 14-5-2015.
 */
@Entity
@Table(name="t_stockcorrection")
public class StockCorrection {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "stockCorrectionId", nullable = false)
    private long id;
    @ManyToOne
    private User createdBy;
    @Column
    private String reason;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;
    @ManyToOne
    private Stockmove stockmove;

    @ManyToOne
    private Note note;

    public StockCorrection(User createdBy, String reason, LocalDateTime creationDate, LocalDateTime lastModifiedDate, User modifiedBy, Stockmove stockmove, Note note) {
        this.createdBy = createdBy;
        this.reason = reason;
        this.creationDate = creationDate;
        this.lastModifiedDate = lastModifiedDate;
        this.modifiedBy = modifiedBy;
        this.stockmove = stockmove;
        this.note = note;
    }

    public StockCorrection() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Stockmove getStockmove() {
        return stockmove;
    }

    public void setStockmove(Stockmove stockmove) {
        this.stockmove = stockmove;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}
