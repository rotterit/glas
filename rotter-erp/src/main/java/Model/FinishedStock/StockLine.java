package Model.FinishedStock;

import Model.FinishedProduct.Product;
import Model.Stock.Stock;
import Model.User.User;

import javax.persistence.*;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_stockline")
public class StockLine {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "stocklineId", nullable = false)
    private int id;
    @ManyToOne
    private Stock stock;
    @ManyToOne
    private Product product;
    @Column
    private int amount;

    public StockLine() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
