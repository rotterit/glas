package Model.FinishedStock;

import Model.Invoicing.Invoice;
import Model.Stock.Stock;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.*;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_stockmove")
public class Stockmove {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "stockmoveId", nullable = false)
    private long id;
    @ManyToOne
    private User createdBy;
    @Column
    private boolean isOut;
    @Column
    private boolean isComplete;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime CompletionDate;
    @ManyToOne
    private User completedBy;
    @Column
    private boolean isPackingslip;
    @Column
    private boolean isReal;
    @ManyToMany
    private List<StockmoveItem> stockmoveItems;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;

    public Stockmove() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public boolean isOut() {
        return isOut;
    }

    public void setOut(boolean isOut) {
        this.isOut = isOut;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }

    public LocalDateTime getCompletionDate() {
        return CompletionDate;
    }

    public void setCompletionDate(LocalDateTime completionDate) {
        CompletionDate = completionDate;
    }

    public User getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(User completedBy) {
        this.completedBy = completedBy;
    }

    public boolean isPackingslip() {
        return isPackingslip;
    }

    public void setPackingslip(boolean isPackingslip) {
        this.isPackingslip = isPackingslip;
    }

    public boolean isReal() {
        return isReal;
    }

    public void setReal(boolean isReal) {
        this.isReal = isReal;
    }

    public List<StockmoveItem> getStockmoveItems() {
        return stockmoveItems;
    }

    public void setStockmoveItems(List<StockmoveItem> stockmoveItems) {
        this.stockmoveItems = stockmoveItems;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
