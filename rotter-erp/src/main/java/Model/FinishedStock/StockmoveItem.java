package Model.FinishedStock;

import Model.FinishedProduct.Product;
import Model.Stock.Stock;
import Model.User.User;

import javax.persistence.*;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_stockmoveitem")
public class StockmoveItem {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "stockmoveitemId", nullable = false)
    private long id;
    @OneToOne
    private Product product;
    @Column
    private int amount;
    @Column
    private boolean compromised;
    @ManyToOne
    private Stock stock;

    public StockmoveItem() {
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isCompromised() {
        return compromised;
    }

    public void setCompromised(boolean compromised) {
        this.compromised = compromised;
    }
}
