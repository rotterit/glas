package Model.Stock;

import Model.FinishedProduct.Product;
import Model.FinishedStock.StockLine;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by thijs on 21-4-2015.
 */
public class StockComparison {

    private Map<Product, Integer> stock1;
    private Map<Product, Integer> stock2;
    private Stock stock;

    public StockComparison() {
        this.stock1 = new HashMap<>();
        this.stock2 = new HashMap<>();
    }

    public void compare(){
        for(StockLine st : stock.getStockLines()){
            stock1.put(st.getProduct(), st.getAmount());
        }
        for(Stockmove st : stock.getStockmoves()){
            for (StockmoveItem item : st.getStockmoveItems()) {
                        if(stock2.containsKey(item.getProduct())){
                            int number = stock2.get(item.getProduct());
                            stock2.put(item.getProduct(),number+item.getAmount());
                        }else{
                            stock2.put(item.getProduct(),item.getAmount());
                        }
            }
        }
    }

    public Map<Product, Integer> getStock1() {
        return stock1;
    }

    public Map<Product, Integer> getStock2() {
        return stock2;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }


}
