package Model.Stock;

import Model.Misc.Note;
import Model.RawStock.MaterialStockLine;
import Model.RawStock.Materialmove;
import Model.FinishedStock.StockLine;
import Model.FinishedStock.Stockmove;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_stock")
public class Stock {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "stockId", nullable = false)
    private int id;
    @Column
    private String name;
    @ManyToOne
    private Location location;
    @Column
    private boolean isRaw;
    @OneToMany
    private List<Stockmove> stockmoves;
    @OneToMany
    private List<StockLine> stockLines;
    @OneToMany
    private List<Materialmove> materialmoves;
    @OneToMany
    private List<MaterialStockLine> materialStockLines;
    @ManyToOne
    private Note note;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @ManyToOne
    private User createdBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;

    public Stock() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isRaw() {
        return isRaw;
    }

    public void setRaw(boolean isRaw) {
        this.isRaw = isRaw;
    }

    public List<Stockmove> getStockmoves() {
        return stockmoves;
    }

    public void setStockmoves(List<Stockmove> stockmoves) {
        this.stockmoves = stockmoves;
    }

    public List<StockLine> getStockLines() {
        return stockLines;
    }

    public void setStockLines(List<StockLine> stockLines) {
        this.stockLines = stockLines;
    }

    public List<Materialmove> getMaterialmoves() {
        return materialmoves;
    }

    public void setMaterialmoves(List<Materialmove> materialmoves) {
        this.materialmoves = materialmoves;
    }

    public List<MaterialStockLine> getMaterialStockLines() {
        return materialStockLines;
    }

    public void setMaterialStockLines(List<MaterialStockLine> materialStockLines) {
        this.materialStockLines = materialStockLines;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
