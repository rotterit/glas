package Model.FinishedProduct;

import DTO.Material.RawMaterial;
import Model.RawMaterial.Supplier;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_product")
public class Product {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "productId", nullable = false)
    private long id;
    @ManyToOne
    private User createdBy;
    @Column
    private String productCode;
    @Column
    private String name;
    @Column
    private boolean active;
    @Column
    private boolean archived;
    //wss voor grouping
    @ManyToOne
    private Supplier supplier;
    @Column
    private int baseTime;
    @Column
    private int minTime;
    @Column
    private int maxTime;
    @Column
    private int avgTime;
    @Column
    //retail price without VAT
    private double price;
    @ManyToOne
    private Color color;
    @Column
    private double Actualprice;
    @ManyToOne
    private Model.RawMaterial.RawMaterial material;
    @ManyToOne
    private Category category;
    @OneToMany
    private List<Description> descriptions;
    @ManyToOne
    private MySize mySize;
    //vase, whiskey glass, coloured tumblr.....
    @ManyToOne
    private ProductType productType;
    //mouthblown, crystal, handmade
    @ManyToOne
    private ManufacturingType manufacturingtype;
    @ManyToOne
    private Product Active;
    @ManyToOne
    private Design design;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;

    public Product() {
    }

    public Product(User createdBy, String productCode, String name, boolean active, boolean archived, Supplier supplier, int baseTime, int minTime, int maxTime, int avgTime, double price, Color color, double actualprice, Model.RawMaterial.RawMaterial material, Category category, List<Description> descriptions, MySize mySize, ProductType productType, ManufacturingType manufacturingtype, Product active1, Design design, LocalDateTime creationDate, LocalDateTime lastModifiedDate, User modifiedBy) {
        this.createdBy = createdBy;
        this.productCode = productCode;
        this.name = name;
        this.active = active;
        this.archived = archived;
        this.supplier = supplier;
        this.baseTime = baseTime;
        this.minTime = minTime;
        this.maxTime = maxTime;
        this.avgTime = avgTime;
        this.price = price;
        this.color = color;
        Actualprice = actualprice;
        this.material = material;
        this.category = category;
        this.descriptions = descriptions;
        this.mySize = mySize;
        this.productType = productType;
        this.manufacturingtype = manufacturingtype;
        Active = active1;
        this.design = design;
        this.creationDate = creationDate;
        this.lastModifiedDate = lastModifiedDate;
        this.modifiedBy = modifiedBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public int getBaseTime() {
        return baseTime;
    }

    public void setBaseTime(int baseTime) {
        this.baseTime = baseTime;
    }

    public int getMinTime() {
        return minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    public int getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public int getAvgTime() {
        return avgTime;
    }

    public void setAvgTime(int avgTime) {
        this.avgTime = avgTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getActualprice() {
        return Actualprice;
    }

    public void setActualprice(double actualprice) {
        Actualprice = actualprice;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Description> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<Description> descriptions) {
        this.descriptions = descriptions;
    }

    public MySize getMySize() {
        return mySize;
    }

    public void setMySize(MySize mySize) {
        this.mySize = mySize;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public ManufacturingType getManufacturingtype() {
        return manufacturingtype;
    }

    public void setManufacturingtype(ManufacturingType manufacturingtype) {
        this.manufacturingtype = manufacturingtype;
    }

    public Product getActive() {
        return Active;
    }

    public void setActive(Product active) {
        Active = active;
    }

    public Design getDesign() {
        return design;
    }

    public void setDesign(Design design) {
        this.design = design;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Model.RawMaterial.RawMaterial getMaterial() {
        return material;
    }

    public void setMaterial(Model.RawMaterial.RawMaterial material) {
        this.material = material;
    }

    public Product calcPrice(double minutecost) {
        Actualprice = (material.getPrice() + (baseTime * minutecost));
        return this;
    }
}
