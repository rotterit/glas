package Model.User;

import javax.persistence.*;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_role")
public class Role{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "roleId", nullable = false)
    private int id;
    @Column
    private String name;

    public Role() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
