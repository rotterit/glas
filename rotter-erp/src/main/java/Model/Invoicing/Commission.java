package Model.Invoicing;

import Model.FinishedStock.Stockmove;
import Model.Misc.Note;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by thijs on 27-4-2015.
 */
@Entity
@Table(name="t_commission")
public class Commission {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "comissionId", nullable = false)
    private long id;
    @ManyToOne
    private User createdBy;
    @ManyToOne
    private Customer customer;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime startDate;
    @Column
    private boolean isReservation;
    @Column
    private boolean provideFakeInvoice;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime returnDate;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime requestedReturnDate;
    @OneToOne
    private Stockmove outStockmove;
    @OneToOne
    private Stockmove inStockmove;
    @OneToMany
    private List<PackingSlip> packingSlips;
    @OneToOne
    private Invoice invoice;
    @Column
    private boolean sent;
    @Column
    private boolean locked;
    @Column
    private int lockedBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime sendDate;
    @ManyToOne
    private User sentBy;
    @Column
    private boolean completed;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime completionDate;
    @ManyToOne
    private User completedBy;
    @ManyToOne
    private Note note;

    public Commission() {
    }

    public Commission(User createdBy, Customer customer, LocalDateTime creationDate, LocalDateTime lastModifiedDate, User modifiedBy, LocalDateTime startDate, boolean isReservation, boolean provideFakeInvoice, LocalDateTime returnDate, LocalDateTime requestedReturnDate, Stockmove outStockmove, Stockmove inStockmove, List<PackingSlip> packingSlips, Invoice invoice, boolean sent, boolean locked, int lockedBy, LocalDateTime sendDate, User sentBy, boolean completed, LocalDateTime completionDate, User completedBy, Note note) {
        this.createdBy = createdBy;
        this.customer = customer;
        this.creationDate = creationDate;
        this.lastModifiedDate = lastModifiedDate;
        this.modifiedBy = modifiedBy;
        this.startDate = startDate;
        this.isReservation = isReservation;
        this.provideFakeInvoice = provideFakeInvoice;
        this.returnDate = returnDate;
        this.requestedReturnDate = requestedReturnDate;
        this.outStockmove = outStockmove;
        this.inStockmove = inStockmove;
        this.packingSlips = packingSlips;
        this.invoice = invoice;
        this.sent = sent;
        this.locked = locked;
        this.lockedBy = lockedBy;
        this.sendDate = sendDate;
        this.sentBy = sentBy;
        this.completed = completed;
        this.completionDate = completionDate;
        this.completedBy = completedBy;
        this.note = note;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setProvideFakeInvoice(boolean provideFakeInvoice) {
        this.provideFakeInvoice = provideFakeInvoice;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public int getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(int lockedBy) {
        this.lockedBy = lockedBy;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public boolean isReservation() {
        return isReservation;
    }

    public void setReservation(boolean isReservation) {
        this.isReservation = isReservation;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }

    public LocalDateTime getRequestedReturnDate() {
        return requestedReturnDate;
    }

    public void setRequestedReturnDate(LocalDateTime requestedReturnDate) {
        this.requestedReturnDate = requestedReturnDate;
    }

    public Stockmove getOutStockmove() {
        return outStockmove;
    }

    public void setOutStockmove(Stockmove outStockmove) {
        this.outStockmove = outStockmove;
    }

    public Stockmove getInStockmove() {
        return inStockmove;
    }

    public void setInStockmove(Stockmove inStockmove) {
        this.inStockmove = inStockmove;
    }

    public List<PackingSlip> getPackingSlips() {
        return packingSlips;
    }

    public void setPackingSlips(List<PackingSlip> packingSlips) {
        this.packingSlips = packingSlips;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public LocalDateTime getSendDate() {
        return sendDate;
    }

    public void setSendDate(LocalDateTime sendDate) {
        this.sendDate = sendDate;
    }

    public User getSentBy() {
        return sentBy;
    }

    public void setSentBy(User sentBy) {
        this.sentBy = sentBy;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public LocalDateTime getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDateTime completionDate) {
        this.completionDate = completionDate;
    }

    public User getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(User completedBy) {
        this.completedBy = completedBy;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public boolean isProvideFakeInvoice() {
        return provideFakeInvoice;
    }
}
