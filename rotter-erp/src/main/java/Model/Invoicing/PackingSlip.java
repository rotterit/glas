package Model.Invoicing;

import Model.FinishedStock.Stockmove;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by thijs on 27-4-2015.
 */
@Entity
@Table(name="t_packingslip")
public class PackingSlip {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "packingslipId", nullable = false)
    private long id;

    @ManyToOne
    private Customer customer;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @ManyToOne
    private User createdBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime packedDate;

    @ManyToOne
    private User packedBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime shippedDate;

    @ManyToOne
    private User shippedBy;

    @Column
    private boolean isReady;

    @ManyToOne
    private Stockmove stockmove;

    public PackingSlip(Customer customer,
                       LocalDateTime creationDate,
                       User createdBy,
                       LocalDateTime lastModifiedDate,
                       User modifiedBy,
                       LocalDateTime packedDate,
                       User packedBy,
                       LocalDateTime shippedDate,
                       User shippedBy,
                       boolean isReady,
                       Stockmove stockmove) {
        this.customer = customer;
        this.creationDate = creationDate;
        this.createdBy = createdBy;
        this.lastModifiedDate = lastModifiedDate;
        this.modifiedBy = modifiedBy;
        this.packedDate = packedDate;
        this.packedBy = packedBy;
        this.shippedDate = shippedDate;
        this.shippedBy = shippedBy;
        this.isReady = isReady;
        this.stockmove = stockmove;
    }

    public PackingSlip() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public LocalDateTime getPackedDate() {
        return packedDate;
    }

    public void setPackedDate(LocalDateTime packedDate) {
        this.packedDate = packedDate;
    }

    public LocalDateTime getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(LocalDateTime shippedDate) {
        this.shippedDate = shippedDate;
    }

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean isReady) {
        this.isReady = isReady;
    }

    public Stockmove getStockmove() {
        return stockmove;
    }

    public void setStockmove(Stockmove stockmove) {
        this.stockmove = stockmove;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public User getPackedBy() {
        return packedBy;
    }

    public void setPackedBy(User packedBy) {
        this.packedBy = packedBy;
    }

    public User getShippedBy() {
        return shippedBy;
    }

    public void setShippedBy(User shippedBy) {
        this.shippedBy = shippedBy;
    }
}
