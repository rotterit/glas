package Model.Invoicing;

import Model.FinishedStock.StockmoveItem;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by thijs on 7-5-2015.
 *
 * The first time we were asked to build this part of the code the only way it seemed useful was fraud. So in this comment the developers would like to wipe their hands from any of that.
 */
@Entity
@Table(name="t_priceline")
public class PriceLine{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "pricelineId", nullable = false)
    private long id;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime LastModified;

    @Column
    private double price;

    @Column
    private double inLineDiscount;

    @Column
    private double inLineDiscountPercentage;

    @ManyToOne
    private StockmoveItem stockmoveItem;

    @ManyToOne
    private Invoice invoice;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getLastModified() {
        return LastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        LastModified = lastModified;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getInLineDiscount() {
        return inLineDiscount;
    }

    public void setInLineDiscount(double inLineDiscount) {
        this.inLineDiscount = inLineDiscount;
    }

    public double getInLineDiscountPercentage() {
        return inLineDiscountPercentage;
    }

    public void setInLineDiscountPercentage(double inLineDiscountPercentage) {
        this.inLineDiscountPercentage = inLineDiscountPercentage;
    }

    public StockmoveItem getStockmoveItem() {
        return stockmoveItem;
    }

    public void setStockmoveItem(StockmoveItem stockmoveItem) {
        this.stockmoveItem = stockmoveItem;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
