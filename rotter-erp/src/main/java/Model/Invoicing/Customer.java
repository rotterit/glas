package Model.Invoicing;


import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by thijs on 7-5-2015.
 */
@Entity
@Table(name="t_customer")
public class Customer {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "customerId", nullable = false)
    private int id;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @ManyToOne
    private User createdBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private double baseVAT;
    @Column
    private double baseDiscountPercentage;
    @Column
    private double baseLineDiscountPercentage;
    @Column
    private double basePackingCostPercentage;
    @Column
    private String companyName;
    @Column
    private String adress;
    @Column
    private String ZIP;
    @Column
    private String city;
    @Column
    private String country;
    @Column
    private String TEL;
    @Column
    private String email;
    @Column
    private String VATnumber;

    public Customer() {
    }

    public Customer(LocalDateTime creationDate, User createdBy, LocalDateTime lastModifiedDate, User modifiedBy, String name, String description, double baseVAT, double baseDiscountPercentage, double baseLineDiscountPercentage, double basePackingCostPercentage, String companyName, String adress, String ZIP, String city, String country, String TEL, String email, String VATnumber) {
        this.creationDate = creationDate;
        this.createdBy = createdBy;
        this.lastModifiedDate = lastModifiedDate;
        this.modifiedBy = modifiedBy;
        this.name = name;
        this.description = description;
        this.baseVAT = baseVAT;
        this.baseDiscountPercentage = baseDiscountPercentage;
        this.baseLineDiscountPercentage = baseLineDiscountPercentage;
        this.basePackingCostPercentage = basePackingCostPercentage;
        this.companyName = companyName;
        this.adress = adress;
        this.ZIP = ZIP;
        this.city = city;
        this.country = country;
        this.TEL = TEL;
        this.email = email;
        this.VATnumber = VATnumber;
    }

    public void edit(LocalDateTime creationDate, User createdBy, LocalDateTime lastModifiedDate, User modifiedBy, String name, String description, double baseVAT, double baseDiscountPercentage, double baseLineDiscountPercentage, double basePackingCostPercentage, String companyName, String adress, String ZIP, String city, String country, String TEL, String email, String VATnumber) {
        this.creationDate = creationDate;
        this.createdBy = createdBy;
        this.lastModifiedDate = lastModifiedDate;
        this.modifiedBy = modifiedBy;
        this.name = name;
        this.description = description;
        this.baseVAT = baseVAT;
        this.baseDiscountPercentage = baseDiscountPercentage;
        this.baseLineDiscountPercentage = baseLineDiscountPercentage;
        this.basePackingCostPercentage = basePackingCostPercentage;
        this.companyName = companyName;
        this.adress = adress;
        this.ZIP = ZIP;
        this.city = city;
        this.country = country;
        this.TEL = TEL;
        this.email = email;
        this.VATnumber = VATnumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescritpion() {
        return description;
    }

    public void setDescritpion(String descritpion) {
        description = descritpion;
    }

    public double getBaseDiscountPercentage() {
        return baseDiscountPercentage;
    }

    public void setBaseDiscountPercentage(double baseDiscountPercentage) {
        this.baseDiscountPercentage = baseDiscountPercentage;
    }

    public double getBaseLineDiscountPercentage() {
        return baseLineDiscountPercentage;
    }

    public void setBaseLineDiscountPercentage(double baseLineDiscountPercentage) {
        this.baseLineDiscountPercentage = baseLineDiscountPercentage;
    }

    public double getBasePackingCostPercentage() {
        return basePackingCostPercentage;
    }

    public void setBasePackingCostPercentage(double basePackingCostPercentage) {
        this.basePackingCostPercentage = basePackingCostPercentage;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        description = description;
    }

    public double getBaseVAT() {
        return baseVAT;
    }

    public void setBaseVAT(double baseVAT) {
        this.baseVAT = baseVAT;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getZIP() {
        return ZIP;
    }

    public void setZIP(String ZIP) {
        this.ZIP = ZIP;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTEL() {
        return TEL;
    }

    public void setTEL(String TEL) {
        this.TEL = TEL;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVATnumber() {
        return VATnumber;
    }

    public void setVATnumber(String VATnumber) {
        this.VATnumber = VATnumber;
    }
}
