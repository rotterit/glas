package Model.Invoicing;

import Model.FinishedStock.Stockmove;
import Model.Misc.Note;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 27-4-2015.
 */
@Entity
@Table(name="t_invoice")
public class Invoice {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "invoiceId", nullable = false)
    private long id;

    //invoice Nr (boekhouding)
    @Column
    private String invoicenr;

    //customer info
    @ManyToOne
    private Customer customer;

    //paymentinfo
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime paymentDueDate;
    @OneToMany
    private List<Payment> payment;

    //order info
    @OneToOne
    private Stockmove order;
    @OneToOne
    private Stockmove needed; // for productionqueue
    @OneToOne
    private Stockmove completed; // to check if ready

    //packing info
    @OneToMany
    private List<PackingSlip> packingSlip;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime sendDate;

    //prijs info
    @Column
    private double priceWithoutVAT;
    @Column
    private double VATpercentage;
    @Column
    private double VAT;
    //price in euro
    @Column
    private double price;
    @Column
    private double upfrontpercentage;
    @Column
    private double packingCost;
    @Column
    private double packingCostPercentage;
    @Column
    private double discount;
    @Column
    private double discountpercentage;
    // price in customer valuta
    @Column
    private double localPrice;
    @Column
    private String valuta;
    @Column
    private double shippingCost;
    @Column
    private String shippingMethod;

    //locking (niet in use)
    @Column
    private boolean locked;
    @Column
    private int lockedBy;

    @ManyToOne
    private Note note;



    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @ManyToOne
    private User createdBy;

    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;

    @Column
    private boolean sent;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime sentDate;
    @ManyToOne
    private User sentBy;

    @Column
    private boolean canceled;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime cancelationDate;
    @ManyToOne
    private User CanceledBy;

    @Column
    private boolean complete;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime completionDate;
    @ManyToOne
    private User completedBy;

    @Column
    private boolean confirmed;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime confirmationDate;
    @ManyToOne
    private User confirmedBy;

    @Column
    private boolean inProduction;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime productionOkDate;
    @ManyToOne
    private User okedForProductionBy;

    public Invoice() {
    }

    public Invoice(Customer customer, User user, Stockmove order, Stockmove needed, Stockmove completed,LocalDateTime sendDate, LocalDateTime paymentDueDate, double priceWithoutVAT, double VATpercentage, double VAT, double price, double upfrontpercentage, double packingCost, double packingCostPercentage, double discount, double discountpercentage, double localPrice, String valuta, Note note, double shippingCost, String shippingMethod, User VOID) {
        this.customer = customer;
        this.payment = new ArrayList<Payment>();
        this.order = order;
        this.needed = needed;
        this.completed = completed;
        this.paymentDueDate = paymentDueDate;
        this.packingSlip = new ArrayList<PackingSlip>();
        this.priceWithoutVAT = priceWithoutVAT;
        this.VATpercentage = VATpercentage;
        this.VAT = VAT;
        this.price = price;
        this.upfrontpercentage = upfrontpercentage;
        this.packingCost = packingCost;
        this.packingCostPercentage = packingCostPercentage;
        this.discount = discount;
        this.discountpercentage = discountpercentage;
        this.localPrice = localPrice;
        this.valuta = valuta;
        this.sent = false;
        this.sendDate = sendDate;
        this.sentBy = VOID;
        this.complete = false;
        this.completionDate = null;
        this.completedBy = VOID;
        this.locked = false;
        this.lockedBy = 3;
        this.canceled = false;
        this.cancelationDate = null;
        CanceledBy = VOID;
        this.note = note;
        this.creationDate = LocalDateTime.now();
        this.createdBy = user;
        this.lastModifiedDate = LocalDateTime.now();
        this.modifiedBy = user;
        this.shippingCost = shippingCost;
        this.shippingMethod = shippingMethod;
        this.sentDate = null;
        this.confirmed = false;
        this.confirmationDate = null;
        this.confirmedBy = VOID;
        this.inProduction = false;
        this.productionOkDate = null;
        this.okedForProductionBy = VOID;
        this.invoicenr = "";
    }

    public Invoice edit(User user, Stockmove order, Stockmove needed, Stockmove completed, LocalDateTime sendDate, LocalDateTime paymentDueDate,double priceWithoutVAT, double VATpercentage, double VAT, double price, double upfrontpercentage, double packingCost, double packingCostPercentage, double discount, double discountpercentage, double localPrice, String valuta, Note note, double shippingCost, String shippingMethod) {
        this.modifiedBy = user;
        this.lastModifiedDate = LocalDateTime.now();
        this.needed = needed;
        this.order = order;
        this.completed = completed;
        this.sendDate = sendDate;
        this.paymentDueDate = paymentDueDate;
        this.priceWithoutVAT = priceWithoutVAT;
        this.VATpercentage = VATpercentage;
        this.VAT = VAT;
        this.price = price;
        this.upfrontpercentage = upfrontpercentage;
        this.packingCost = packingCost;
        this.packingCostPercentage = packingCostPercentage;
        this.discount = discount;
        this.discountpercentage = discountpercentage;
        this.localPrice = localPrice;
        this.valuta = valuta;
        this.note = note;
        this.shippingCost = shippingCost;
        this.shippingMethod = shippingMethod;
        return this;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInvoicenr() {
        return invoicenr;
    }

    public void setInvoicenr(String invoicenr) {
        this.invoicenr = invoicenr;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LocalDateTime getPaymentDueDate() {
        return paymentDueDate;
    }

    public void setPaymentDueDate(LocalDateTime paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }

    public List<Payment> getPayment() {
        return payment;
    }

    public void setPayment(List<Payment> payment) {
        this.payment = payment;
    }

    public Stockmove getOrder() {
        return order;
    }

    public void setOrder(Stockmove order) {
        this.order = order;
    }

    public Stockmove getNeeded() {
        return needed;
    }

    public void setNeeded(Stockmove needed) {
        this.needed = needed;
    }

    public Stockmove getCompleted() {
        return completed;
    }

    public void setCompleted(Stockmove completed) {
        this.completed = completed;
    }

    public List<PackingSlip> getPackingSlip() {
        return packingSlip;
    }

    public void setPackingSlip(List<PackingSlip> packingSlip) {
        this.packingSlip = packingSlip;
    }

    public LocalDateTime getSendDate() {
        return sendDate;
    }

    public void setSendDate(LocalDateTime sendDate) {
        this.sendDate = sendDate;
    }

    public double getPriceWithoutVAT() {
        return priceWithoutVAT;
    }

    public void setPriceWithoutVAT(double priceWithoutVAT) {
        this.priceWithoutVAT = priceWithoutVAT;
    }

    public double getVATpercentage() {
        return VATpercentage;
    }

    public void setVATpercentage(double VATpercentage) {
        this.VATpercentage = VATpercentage;
    }

    public double getVAT() {
        return VAT;
    }

    public void setVAT(double VAT) {
        this.VAT = VAT;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getUpfrontpercentage() {
        return upfrontpercentage;
    }

    public void setUpfrontpercentage(double upfrontpercentage) {
        this.upfrontpercentage = upfrontpercentage;
    }

    public double getPackingCost() {
        return packingCost;
    }

    public void setPackingCost(double packingCost) {
        this.packingCost = packingCost;
    }

    public double getPackingCostPercentage() {
        return packingCostPercentage;
    }

    public void setPackingCostPercentage(double packingCostPercentage) {
        this.packingCostPercentage = packingCostPercentage;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getDiscountpercentage() {
        return discountpercentage;
    }

    public void setDiscountpercentage(double discountpercentage) {
        this.discountpercentage = discountpercentage;
    }

    public double getLocalPrice() {
        return localPrice;
    }

    public void setLocalPrice(double localPrice) {
        this.localPrice = localPrice;
    }

    public String getValuta() {
        return valuta;
    }

    public void setValuta(String valuta) {
        this.valuta = valuta;
    }

    public double getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(double shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public int getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(int lockedBy) {
        this.lockedBy = lockedBy;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public LocalDateTime getSentDate() {
        return sentDate;
    }

    public void setSentDate(LocalDateTime sentDate) {
        this.sentDate = sentDate;
    }

    public User getSentBy() {
        return sentBy;
    }

    public void setSentBy(User sentBy) {
        this.sentBy = sentBy;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public LocalDateTime getCancelationDate() {
        return cancelationDate;
    }

    public void setCancelationDate(LocalDateTime cancelationDate) {
        this.cancelationDate = cancelationDate;
    }

    public User getCanceledBy() {
        return CanceledBy;
    }

    public void setCanceledBy(User canceledBy) {
        CanceledBy = canceledBy;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public LocalDateTime getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDateTime completionDate) {
        this.completionDate = completionDate;
    }

    public User getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(User completedBy) {
        this.completedBy = completedBy;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public LocalDateTime getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(LocalDateTime confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public User getConfirmedBy() {
        return confirmedBy;
    }

    public void setConfirmedBy(User confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    public boolean isInProduction() {
        return inProduction;
    }

    public void setInProduction(boolean inProduction) {
        this.inProduction = inProduction;
    }

    public LocalDateTime getProductionOkDate() {
        return productionOkDate;
    }

    public void setProductionOkDate(LocalDateTime productionOkDate) {
        this.productionOkDate = productionOkDate;
    }

    public User getOkedForProductionBy() {
        return okedForProductionBy;
    }

    public void setOkedForProductionBy(User okedForProductionBy) {
        this.okedForProductionBy = okedForProductionBy;
    }
}
