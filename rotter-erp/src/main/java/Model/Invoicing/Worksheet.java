package Model.Invoicing;

import Model.FinishedStock.Stockmove;
import Model.Misc.Note;
import Model.RawStock.Materialmove;
import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import java.time.LocalDateTime;
import javax.persistence.*;

/**
 * Created by thijs on 20-4-2015.
 */
@Entity
@Table(name="t_worksheet")
public class Worksheet {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "worksheetId", nullable = false)
    private long id;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime createDate;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime startDate;
    @Column
    private boolean started;
    @Column
    private boolean completed;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime finishedDate;
    @ManyToOne
    private Materialmove materialmove;
    @ManyToOne
    private Stockmove stockmove;
    @ManyToOne
    private User creator;
    @ManyToOne
    private User executor;
    @ManyToOne
    private Note note;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;

    public Worksheet() {
    }

    public Worksheet(LocalDateTime createDate, LocalDateTime startDate, boolean started, boolean completed, LocalDateTime finishedDate, Materialmove materialmove, Stockmove stockmove, User creator, User executor, Note note, LocalDateTime lastModifiedDate, User modifiedBy) {
        this.createDate = createDate;
        this.startDate = startDate;
        this.started = started;
        this.completed = completed;
        this.finishedDate = finishedDate;
        this.materialmove = materialmove;
        this.stockmove = stockmove;
        this.creator = creator;
        this.executor = executor;
        this.note = note;
        this.lastModifiedDate = lastModifiedDate;
        this.modifiedBy = modifiedBy;
    }

    public void edit(LocalDateTime startDate, boolean started, boolean completed, LocalDateTime finishedDate, Materialmove materialmove, Stockmove stockmove, User executor, Note note, LocalDateTime lastModifiedDate, User modifiedBy) {
        this.startDate = startDate;
        this.started = started;
        this.completed = completed;
        this.finishedDate = finishedDate;
        this.materialmove = materialmove;
        this.stockmove = stockmove;
        this.executor = executor;
        this.note = note;
        this.lastModifiedDate = lastModifiedDate;
        this.modifiedBy = modifiedBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public LocalDateTime getFinishedDate() {
        return finishedDate;
    }

    public void setFinishedDate(LocalDateTime finishedDate) {
        this.finishedDate = finishedDate;
    }

    public Materialmove getMaterialmove() {
        return materialmove;
    }

    public void setMaterialmove(Materialmove materialmove) {
        this.materialmove = materialmove;
    }

    public Stockmove getStockmove() {
        return stockmove;
    }

    public void setStockmove(Stockmove stockmove) {
        this.stockmove = stockmove;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public User getExecutor() {
        return executor;
    }

    public void setExecutor(User executor) {
        this.executor = executor;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
