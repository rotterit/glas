package Model.Invoicing;

import Model.User.User;
import Util.LocalDateTimePersistenceConverter;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by thijs on 27-4-2015.
 */
@Entity
@Table(name="t_payment")
public class Payment {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "paymentId", nullable = false)
    private long id;
    @Column
    private double amount;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime creationDate;
    @ManyToOne
    private User createdBy;
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    @Column
    private LocalDateTime lastModifiedDate;
    @ManyToOne
    private User modifiedBy;

    @Column
    private boolean overdue;

    public Payment(double amount, LocalDateTime creationDate, User createdBy, LocalDateTime lastModifiedDate, User modifiedBy, boolean overdue) {
        this.amount = amount;
        this.creationDate = creationDate;
        this.createdBy = createdBy;
        this.lastModifiedDate = lastModifiedDate;
        this.modifiedBy = modifiedBy;
        this.overdue = overdue;
    }

    public Payment() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public User getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(User modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isOverdue() {
        return overdue;
    }

    public void setOverdue(boolean overdue) {
        this.overdue = overdue;
    }
}
