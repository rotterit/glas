package DAL;

import Model.Misc.Note;
import Model.RawMaterial.Supplier;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 6-5-2015.
 */
@Service
public class SupplierDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Supplier getSupplierById(int id) {

        Supplier l = (Supplier) sessionFactory.getCurrentSession().createQuery("From Supplier l where l.id = :id").setParameter("id", id).list().get(0);

        return l;
    }

    public List<Supplier> getSuppliers() {
        List<Supplier> Suppliers = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Supplier ").list();
        for (Object o : results) {
            Suppliers.add((Supplier) o);
        }
        return Suppliers;
    }

    public Supplier createSupplier(String supplierCode, String name, Note contactnote, Note suppliernote, Note generalnote, User u) throws HorribleException {
        if (sessionFactory.getCurrentSession().createQuery("From Supplier c where c.suplierCode = :code").setParameter("code", supplierCode).list().size() <= 0) {
            Supplier temp = new Supplier();
            //set values
            temp.setName(name);
            temp.setCreationDate(LocalDateTime.now());
            temp.setSuplierCode(supplierCode);
            temp.setContactnote(contactnote);
            temp.setGeneralnote(generalnote);
            temp.setSuppliernote(suppliernote);
            temp.setLastModifiedDate(LocalDateTime.now());
            temp.setModifiedBy(u);
            temp.setCreatedBy(u);
            temp.setCreationDate(LocalDateTime.now());
            sessionFactory.getCurrentSession().save(temp);

            return temp;
        } else {
            throw new HorribleException("the code already exists");
        }
    }

    public Supplier editSupplier(int Supplierid, String supplierCode, String name, Note contactnote, Note suppliernote, Note generalnote, User u) throws HorribleException {
        if (sessionFactory.getCurrentSession().createQuery("From Supplier c where c.suplierCode = :code And c.id != :id").setParameter("code", supplierCode).setParameter("id", Supplierid).list().size() <= 0) {
            Supplier temp = getSupplierById(Supplierid);
            //set values
            temp.setName(name);
            temp.setSuplierCode(supplierCode);
            temp.setContactnote(contactnote);
            temp.setGeneralnote(generalnote);
            temp.setSuppliernote(suppliernote);
            temp.setLastModifiedDate(LocalDateTime.now());
            temp.setModifiedBy(u);
            sessionFactory.getCurrentSession().update(temp);
            return temp;
        } else {
            throw new HorribleException("the code already exists");
        }
    }
}
