package DAL;

import Model.User.Role;
import Model.User.User;

import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 4-5-2015.
 */
@Service
public class UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public User getUserById(int userid) {
        User u = new User();
        try {
            u =  (User) sessionFactory.getCurrentSession().createQuery("From User u Where u.id = :id").setParameter("id", userid).list().get(0);
        }catch(IndexOutOfBoundsException e){
            return null;
        }
        return u;
    }


    public List<User> getUsers(){
        List<User>  users = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From User u where u.role.id!=1").list();
        for(Object o: results){
            users.add((User) o);
        }
        return users;
    }


    public User createUser(String name, String username, String password, int roleid) throws HorribleException {
        if(0 >= sessionFactory.getCurrentSession().createQuery("From User u where u.username = :username").setParameter("username", username).list().size()) {
            User u = new User();
            u.setPassword(password);
            u.setUsername(username);
            u.setName(name);
            u.setRole((Role) sessionFactory.getCurrentSession().createQuery("From Role r where r.id = :id").setParameter("id",roleid).list().get(0));
            sessionFactory.getCurrentSession().save(u);
            return u;
        }else{
            throw new HorribleException("The username already exists");
        }
    }


    public User editUser(int userid, String username, String name, int roleid) throws HorribleException {
        if(0 >= sessionFactory.getCurrentSession().createQuery("From User u where u.username = :username AND u.id != :id").setParameter("username", username).setParameter("id", userid).list().size()) {
            User u = getUserById(userid);
            u.setUsername(username);
            u.setName(name);
            u.setRole((Role) sessionFactory.getCurrentSession().createQuery("From Role r where r.id = :id").setParameter("id",roleid).list().get(0));
            sessionFactory.getCurrentSession().update(u);
            return u;
        }else{
            throw new HorribleException("The username already exists");
        }
    }


    public boolean checklogin(String username, String password) {
        User u = getUserByName(username);
        return u.getPassword().equals(password);
    }


    public User getUserByName(String username) {
        User u = new User();
        try {
            u = (User) sessionFactory.getCurrentSession().createQuery("From User u where u.username = :username").setParameter("username", username).list().get(0);
        }catch(IndexOutOfBoundsException e){
            throw new NullPointerException("no such user");
        }
        return u;
    }

    public User getVoid() {
        return (User) sessionFactory.getCurrentSession().createQuery("From User u where u.id =3").list().get(0);
    }

    public List<User> getCutters() {
        return (List<User>) sessionFactory.getCurrentSession().createQuery("from User u where u.role.id = 4").list();
    }
}
