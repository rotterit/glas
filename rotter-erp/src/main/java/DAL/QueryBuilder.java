package DAL;

import DTO.Material.MaterialFilter;
import Model.FinishedProduct.Product;
import DTO.Product.ProductFilter;
import Model.RawMaterial.RawMaterial;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 8-5-2015.
 */
@Service
public class QueryBuilder {

    @Autowired
    private SessionFactory sessionFactory;

    public List<RawMaterial> BuildQueryForRawMaterial(MaterialFilter m){
        List<List<Integer>> parameterlistlist = new ArrayList<>();
        String query = "From RawMaterial r Where ";
        if(m.getSizeids().size()>=1){
            query+= "r.mySize.id IN :sizes AND ";

        }
        if(m.getSupplierids().size()>=1){
            query+= "r.supplier.id IN :suppliers AND ";

        }
        if(m.getProductTypeids().size()>=1){
            query+= "r.productType.id IN :productTypes AND ";

        }
        if(m.getSeriesids().size() >= 1){
            query += "r.series.id IN :series AND ";

        }
        if(m.getColorids().size()>=1){
            query += "r.color.id IN :colors AND ";

        }
        if(m.getQualityids().size()>=1){
            query += "r.quality.id IN :qualities";

        }else {
            query += "1=1";
        }
        Query query1 = sessionFactory.getCurrentSession().createQuery(query);
        if(m.getSizeids().size()>=1){
            query1.setParameterList("sizes", m.getSizeids());
        }
        if(m.getSupplierids().size()>=1){
            query1.setParameterList("suppliers", m.getSupplierids());
        }
        if(m.getProductTypeids().size()>=1){
            query1.setParameterList("productTypes", m.getProductTypeids());
        }
        if(m.getSeriesids().size() >= 1) {
            query1.setParameterList("series", m.getSeriesids());
        }
        if(m.getColorids().size()>=1){
            query1.setParameterList("colors", m.getColorids());
        }
        if(m.getQualityids().size()>=1){
            query1.setParameterList("qualities", m.getQualityids());
        }
        List<RawMaterial> materials= new ArrayList<>();
        List<Object> results = query1.list();
        for(Object o: results){
            materials.add((RawMaterial) o);
        }
        return materials;
    }

    public List<RawMaterial> BuildQueryForOrderRawMaterial(MaterialFilter m){
        List<List<Integer>> parameterlistlist = new ArrayList<>();
        String query = "From RawMaterial r Where ";
        if(m.getSizeids().size()>=1){
            query+= "r.mySize.id IN :sizes AND ";

        }
        if(m.getSupplierids().size()>=1){
            query+= "r.supplier.id IN :suppliers AND ";

        }
        if(m.getProductTypeids().size()>=1){
            query+= "r.productType.id IN :productTypes AND ";

        }
        if(m.getSeriesids().size() >= 1){
            query += "r.series.id IN :series AND ";

        }
        if(m.getColorids().size()>=1){
            query += "r.color.id IN :colors AND ";

        }
        query += "r.quality.id = :quality";
        Query query1 = sessionFactory.getCurrentSession().createQuery(query);
        if(m.getSizeids().size()>=1){
            query1.setParameterList("sizes", m.getSizeids());
        }
        if(m.getSupplierids().size()>=1){
            query1.setParameterList("suppliers", m.getSupplierids());
        }
        if(m.getProductTypeids().size()>=1){
            query1.setParameterList("productTypes", m.getProductTypeids());
        }
        if(m.getSeriesids().size() >= 1) {
            query1.setParameterList("series", m.getSeriesids());
        }
        if(m.getColorids().size()>=1){
            query1.setParameterList("colors", m.getColorids());
        }
        query1.setParameter("quality", 1);
        List<RawMaterial> materials= new ArrayList<>();
        List<Object> results = query1.list();
        for(Object o: results){
            materials.add((RawMaterial) o);
        }
        return materials;
    }

    public List<Product> BuildQueryForProduct(ProductFilter m){
        String query = "From Product r Where r.active = true AND ";
        if(m.getSizeids().size()>=1){
            query+= "r.mySize.id IN :sizes AND ";

        }
        if(m.getSupplierids().size()>=1){
            query+= "r.supplier.id IN :suppliers AND ";

        }
        if(m.getProductTypeids().size()>=1){
            query+= "r.productType.id IN :productTypes AND ";

        }
        if(m.getDesignids().size() >= 1){
            query += "r.design.id IN :series AND ";

        }
        if(m.getColorids().size()>=1){
            query += "r.color.id IN :colors AND ";

        }
        if(m.getCategoryids().size()>=1){
            query += "r.category.id IN :categories AND";
        }
        if(m.getManufacturingTypeids().size()>=1){
            query += "r.manfacturingType.id IN :qualities";

        }else {
            query += " 1=1";
        }
        Query query1 = sessionFactory.getCurrentSession().createQuery(query);
        if(m.getSizeids().size()>=1){
            query1.setParameterList("sizes", m.getSizeids());
        }
        if(m.getSupplierids().size()>=1){
            query1.setParameterList("suppliers", m.getSupplierids());
        }
        if(m.getProductTypeids().size()>=1){
            query1.setParameterList("productTypes", m.getProductTypeids());
        }
        if(m.getDesignids().size() >= 1) {
            query1.setParameterList("series", m.getDesignids());
        }
        if(m.getColorids().size()>=1){
            query1.setParameterList("colors", m.getColorids());
        }
        if(m.getManufacturingTypeids().size()>=1){
            query1.setParameterList("qualities", m.getManufacturingTypeids());
        }
        if(m.getCategoryids().size()>=1){
            query1.setParameterList("categories", m.getCategoryids());
        }
        List<Product> products = new ArrayList<>();
        List<Object> results = query1.list();
        for(Object o : results){
            Product product = (Product) o;
            Hibernate.initialize(product.getDescriptions());
            products.add(product);
        }
        return products;
    }
}
