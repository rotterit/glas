package DAL;

import DAL.Util.StockMoveItemInvoiceConnector;
import Model.FinishedProduct.Product;
import Model.FinishedStock.StockLine;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.*;
import Model.Misc.Note;
import Model.Stock.Stock;
import Model.User.User;
import Util.HorribleException;
import Util.InvoiceStockException;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by thijs on 4-5-2015.
 */
@Service
public class InvoiceDAO {

    Logger logger = LoggerFactory.getLogger(InvoiceDAO.class);

    @Autowired
    private SessionFactory sessionFactory;

    public Invoice getInvoiceById(long invoiceid){
        Invoice i = (Invoice) sessionFactory.getCurrentSession().createQuery("From Invoice c where c.id = :id").setParameter("id",invoiceid).list().get(0);
        return i;
    }

    public List<Invoice> getOpenInvoices(){
        return (List<Invoice>) sessionFactory.getCurrentSession().createQuery("From Invoice i where i.confirmed = false AND i.canceled = false").list();
    }

    public List<Invoice> getInvoices(){
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Invoice").list();
        List<Invoice> invoices = new ArrayList<>();
        for(Object o: results){
            invoices.add((Invoice) o);
        }
        return invoices;
    }

    public Invoice createInvoice(Customer customer, User user, Stockmove order, Stockmove needed, Stockmove completed, long sendDate, long paymentDueDate, double priceWithoutVAT, double VATpercentage, double VAT, double price, double upfrontpercentage, double packingCost, double packingCostPercentage, double discount, double discountpercentage, double localPrice, String valuta, Note note, double shippingCost, String shippingMethod, User VOID){
        List<Payment> payments = new ArrayList<>();
        List<PackingSlip> packingSlips = new ArrayList<>();
        Invoice i = new Invoice(customer, user, order, needed, completed,LocalDateTime.ofInstant(Instant.ofEpochMilli(sendDate), ZoneId.of("Europe/Berlin")), LocalDateTime.ofInstant(Instant.ofEpochMilli(paymentDueDate), ZoneId.of("Europe/Berlin")), priceWithoutVAT, VATpercentage, VAT, price, upfrontpercentage, packingCost, packingCostPercentage,discount, discountpercentage, localPrice, valuta, note, shippingCost, shippingMethod, VOID);
        sessionFactory.getCurrentSession().save(i);
        return i;
    }


    public Invoice editInvoice(Long invoiceid,User user, Stockmove order, Stockmove needed, Stockmove completed, long sendDate, long paymentDueDate,double priceWithoutVAT, double VATpercentage, double VAT, double price, double upfrontpercentage, double packingCost, double packingCostPercentage, double discount, double discountpercentage, double localPrice, String valuta, Note note, double shippingCost, String shippingMethod) throws HorribleException {
        Invoice i = getInvoiceById(invoiceid);
        if(i.isInProduction()){
            throw new HorribleException("the invoice is already in production");
        }
        i.edit(user,order,needed,completed,LocalDateTime.ofInstant(Instant.ofEpochMilli(sendDate), ZoneId.of("Europe/Berlin")),LocalDateTime.ofInstant(Instant.ofEpochMilli(paymentDueDate), ZoneId.of("Europe/Berlin")), priceWithoutVAT, VATpercentage, VAT, price, upfrontpercentage, packingCost, packingCostPercentage, discount, discountpercentage, localPrice, valuta, note, shippingCost, shippingMethod);
        sessionFactory.getCurrentSession().update(i);
        return i;
    }

    public Invoice sendInvoice(long invoiceid, User user, double invoicestartnumber) throws HorribleException {
        Invoice i = getInvoiceById(invoiceid);
        if(!isReady(invoiceid)){
            throw new HorribleException("teh nie goe wer");
        }
        long number = (long) sessionFactory.getCurrentSession().createQuery("Select count(*) from Invoice i where i.sent = true").list().get(0);
        number += invoicestartnumber;
        String nr = "i"+number;
        i.setInvoicenr(nr);
        i.setSent(true);
        i.setSentDate(LocalDateTime.now());
        i.setSentBy(user);
        i.setLastModifiedDate(LocalDateTime.now());
        i.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(i);
        return i;
    }

    public boolean isReady(long invoiceid){
        boolean isReady = true;
        Invoice i = getInvoiceById(invoiceid);
        logger.warn(""+i.getPackingSlip().size());
        Map<Long, Integer> packingamounts = new HashMap<>();
        for(PackingSlip packingSlip : i.getPackingSlip()){
            for(StockmoveItem stockmoveItem : packingSlip.getStockmove().getStockmoveItems()){
                if(packingamounts.keySet().contains(stockmoveItem.getProduct().getId())){
                    int amount = packingamounts.get(stockmoveItem.getProduct().getId());
                    amount += stockmoveItem.getAmount();
                    packingamounts.put(stockmoveItem.getProduct().getId(), amount);
                    logger.warn("edit for product:" + stockmoveItem.getProduct().getId() + " amount: " + amount);
                }else{
                    packingamounts.put(stockmoveItem.getProduct().getId(), stockmoveItem.getAmount());
                    logger.warn("add for product:" +stockmoveItem.getProduct().getId() + " amount: " + stockmoveItem.getAmount());
                }
            }
        }
        logger.warn("packingamounts for invoice:"+invoiceid+" "+packingamounts);
        Map<Long, Integer> orderamounts = new HashMap<>();
        logger.warn(""+ i.getOrder().getStockmoveItems().size());
        for(StockmoveItem stockmoveItem: i.getOrder().getStockmoveItems()){
            if(orderamounts.keySet().contains(stockmoveItem.getProduct().getId())){
                int amount = orderamounts.get(stockmoveItem.getProduct().getId());
                amount += stockmoveItem.getAmount();
                orderamounts.put(stockmoveItem.getProduct().getId(), amount);
                logger.warn("edit for product:" + stockmoveItem.getProduct().getId() + " amount: " + amount);
            }else{
                orderamounts.put(stockmoveItem.getProduct().getId(), stockmoveItem.getAmount());
                logger.warn("add for product:" +stockmoveItem.getProduct().getId() + " amount: " + stockmoveItem.getAmount());
            }
        }
        logger.warn("orderamounts for invoice:"+ invoiceid +" "+orderamounts);
        for(StockmoveItem stockmoveItem : i.getOrder().getStockmoveItems()){
            logger.warn(""+orderamounts.get(stockmoveItem.getProduct().getId()));
            logger.warn(""+ packingamounts.get(stockmoveItem.getProduct().getId()));
            if(orderamounts.get(stockmoveItem.getProduct().getId())!= packingamounts.get(stockmoveItem.getProduct().getId())){
                isReady = false;
                logger.warn("tis te weinig wer");
            }
        }
        for(PackingSlip packingSlip : i.getPackingSlip()){
            if(packingSlip.getShippedBy().getId()==3){
                isReady = false;
                logger.warn("tis nog nie weg wer");
            }
        }
        if(((i.getPayment().stream().mapToDouble(m->m.getAmount()).sum())/i.getLocalPrice())>=i.getUpfrontpercentage()){
            logger.warn(""+((i.getPayment().stream().mapToDouble(m->m.getAmount()).sum())/i.getLocalPrice()));
            isReady = false;
        }
        logger.warn("invoicenr: "+ invoiceid + " ready? " + isReady);
        return isReady;
    }

    public Invoice confirmInvoice(long invoiceid, User user){
        Invoice i = getInvoiceById(invoiceid);
        i.setConfirmed(true);
        i.setConfirmedBy(user);
        i.setConfirmationDate(LocalDateTime.now());
        i.setLastModifiedDate(LocalDateTime.now());
        i.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(i);
        return i;
    }

    public Invoice produce(long invoiceid, User user, Stockmove needed, Stockmove completed) throws HorribleException {
        Invoice i = getInvoiceById(invoiceid);
        if(!i.isConfirmed()){
            throw new HorribleException("the invoice has not been confirmed");
        }
        if(checkFrontPayment(invoiceid)){
            throw new HorribleException("there's not enough payed");
        }
        i.setInProduction(true);
        i.setOkedForProductionBy(user);
        i.setProductionOkDate(LocalDateTime.now());
        i.setNeeded(needed);
        i.setCompleted(completed);
        i.setLastModifiedDate(LocalDateTime.now());
        i.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(i);
        return i;
    }

    public boolean checkFrontPayment(long invoiceId){
        Invoice invoice = getInvoiceById(invoiceId);
        return (((invoice.getPayment().stream().mapToDouble(m->m.getAmount()).sum())/invoice.getLocalPrice())*100)>invoice.getUpfrontpercentage();
    }

    public Invoice completeInvoice(long invoiceid, User user) throws HorribleException {
        Invoice i = getInvoiceById(invoiceid);
        double amount = i.getPayment().stream().mapToDouble(m->m.getAmount()).sum();
        if(amount>=i.getLocalPrice()){
            i.setComplete(true);
            i.setCompletionDate(LocalDateTime.now());
            i.setCompletedBy(user);
            sessionFactory.getCurrentSession().update(i);
        }else{
            throw new HorribleException("the invoice has not been completely payed for");
        }
        return i;
    }

    public Invoice requestLock(long invoiceid, int userid) throws HorribleException {
        Invoice i = getInvoiceById(invoiceid);
        if(i.isLocked()&& i.getLockedBy() != userid){
            throw new HorribleException(" the invoice is being edited");
        }else{
            if(i.getLockedBy() ==3) {
                i.setLocked(true);
                i.setLockedBy(userid);
                sessionFactory.getCurrentSession().update(i);
            }
        }
        return i;
    }

    public Invoice cancelInvoice(long invoiceid, User system){
        Invoice i = getInvoiceById(invoiceid);
        for(StockmoveItem stockmoveItem : i.getNeeded().getStockmoveItems()){
            stockmoveItem.setCompromised(true);
            sessionFactory.getCurrentSession().update(stockmoveItem);
        }
        i.setCanceled(true);
        i.setCanceledBy(system);
        i.setCancelationDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().update(i);
        return i;
    }


    public PriceLine createPriceLine(double price, double inlineDiscount, double inlineDiscountPercentage, StockmoveItem stockmoveItem) {
        PriceLine priceLine = new PriceLine();
        priceLine.setPrice(price);
        priceLine.setInLineDiscount(inlineDiscount);
        priceLine.setInLineDiscountPercentage(inlineDiscountPercentage);
        priceLine.setStockmoveItem(stockmoveItem);
        sessionFactory.getCurrentSession().save(priceLine);
        return priceLine;
    }

    public PriceLine getPriceLinesByStockMoveId(long id) {
        return (PriceLine) sessionFactory.getCurrentSession().createQuery("from PriceLine p where p.stockmoveItem.id = :id").setParameter("id",id).list().get(0);
    }

    public List<Invoice> getCompletedInvoices() {
        return (List<Invoice>) sessionFactory.getCurrentSession().createQuery("from Invoice i where i.complete = true AND i.canceled = false").list();
    }

    public List<Invoice> getSentInvoices() {
        return (List<Invoice>) sessionFactory.getCurrentSession().createQuery("from Invoice i where i.complete = false AND i.sent = true AND i.canceled = false").list();
    }

    public Invoice addPackingslip(long invoiceid, PackingSlip packingSlip, User user){
        Invoice invoice= getInvoiceById(invoiceid);
        invoice.getPackingSlip().add(packingSlip);
        invoice.setModifiedBy(user);
        invoice.setLastModifiedDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().update(invoice);
        return invoice;
    }

    public Invoice removePackingslip(long invoiceid,PackingSlip packingSlip, User user) {
        Invoice invoice = getInvoiceById(invoiceid);
        invoice.getPackingSlip().remove(packingSlip);
        invoice.setLastModifiedDate(LocalDateTime.now());
        invoice.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(invoice);
        return invoice;
    }

    public void completeStockmoveitem(StockmoveItem stockmoveItem, long id) {
        Invoice invoice = getInvoiceById(id);
        for(StockLine stockLine : stockmoveItem.getStock().getStockLines()){
            if(stockLine.getProduct().getId() == stockmoveItem.getProduct().getId()){
                stockLine.setAmount(stockLine.getAmount() + stockmoveItem.getAmount());
                sessionFactory.getCurrentSession().update(stockLine);
            }
        }
        invoice.getNeeded().getStockmoveItems().remove(stockmoveItem);
        invoice.getCompleted().getStockmoveItems().add(stockmoveItem);
        sessionFactory.getCurrentSession().update(invoice);
        sessionFactory.getCurrentSession().update(invoice.getNeeded());
        sessionFactory.getCurrentSession().update(invoice.getCompleted());
    }

    public List<Invoice> getDueInvoices() {
        //inproduction
        List<Invoice> invoices = getProductionInvoices();
        List<Invoice> selected = new ArrayList<>();

        for(Invoice invoice: invoices){
            logger.warn("" +invoice.getSendDate());
            logger.warn("" +LocalDateTime.now().with(DayOfWeek.SATURDAY).plusWeeks(1) );
            if(invoice.getSendDate().isBefore(LocalDateTime.now().with(DayOfWeek.SATURDAY).plusWeeks(1))){
                selected.add(invoice);
            }
        }
        logger.warn(""+selected.size());
        return selected;
    }

    public List<Invoice> getInvoiceDueForPayment() {
        List<Invoice> invoices = getSentInvoices();
        List<Invoice> selected = new ArrayList<>();

        for(Invoice invoice: invoices){
            logger.warn("" +invoice.getPaymentDueDate());
            logger.warn("" +LocalDateTime.now().with(DayOfWeek.SATURDAY).plusWeeks(1) );
            if(invoice.getPaymentDueDate().isBefore(LocalDateTime.now().with(DayOfWeek.SATURDAY))){
                selected.add(invoice);
            }
        }
        logger.warn(""+selected.size());
        return selected;
    }

    public List<Invoice> getConfirmedInvoices() {
        return (List<Invoice>) sessionFactory.getCurrentSession().createQuery("From Invoice i where i.confirmed = true AND i.canceled = false AND i.inProduction = false").list();
    }

    public List<Invoice> getProductionInvoices(){
        return (List<Invoice>) sessionFactory.getCurrentSession().createQuery("From Invoice i where i.inProduction = true AND i.canceled = false AND i.sent = false").list();
    }

    public Invoice getInvoiceByPackingSlipId(long packingslipid) throws HorribleException {
        List<Invoice> invoices = getProductionInvoices();
        for(Invoice i : invoices){
            if(i.getPackingSlip().stream().anyMatch(p->p.getId()== packingslipid)){
                return i;
            }
        }
        throw new HorribleException("no productioninvoice with this packinslip");
    }

    public void completeStockmoveitems(List<StockMoveItemInvoiceConnector> connectors) {
        StockmoveItem stockmoveItem;
        for(StockMoveItemInvoiceConnector connector : connectors){
            stockmoveItem = (StockmoveItem) sessionFactory.getCurrentSession().createQuery("From StockmoveItem  s where s.id = :id").setParameter("id", connector.getStockMoveItemId()).list().get(0);
            completeStockmoveitem(stockmoveItem, connector.getInvoiceId());
        }
    }
}
