package DAL;

import Model.User.Role;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 4-5-2015.
 */
@Service
public class RoleDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Role getRoleById(int roleid) {

        Role role =  (Role) sessionFactory.getCurrentSession().createQuery("From Role r WHERE r.id= :id").setParameter("id",roleid).list().get(0);

        return role;
    }

    public Role createRole(String name){
        Role r = new Role();
        r.setName(name);
        sessionFactory.getCurrentSession().save(r);
        return r;
    }

    public Role editRole(int roleid ,String name){
        Role r = getRoleById(roleid);
        r.setName(name);
        sessionFactory.getCurrentSession().update(r);
        return r;
    }

    public List<Role> getRoles(){
        List<Role>  roles = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Role r where r.id !=1").list();
        for(Object o: results){
            roles.add((Role) o);
        }
        return roles;
    }
}
