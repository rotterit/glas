package DAL;

import Model.RawMaterial.Series;

import Model.RawMaterial.Supplier;
import Model.User.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 6-5-2015.
 */
@Service
public class SeriesDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Series getSeriesById(int id){
        Series l = (Series) sessionFactory.getCurrentSession().createQuery("From Series l where l.id = :id").setParameter("id",id).list().get(0);
        return l;
    }

    public List<Series> getSeries(){
        List<Series> Seriess = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Series ").list();
        for(Object o: results){
            Seriess.add((Series) o);
        }
        return Seriess;
    }

    public Series createSeries(String name, String description, int supplierid, User u){
        Series temp = new Series();
        //set values
        temp.setName(name);
        temp.setDescription(description);
        temp.setSupplier((Supplier) sessionFactory.getCurrentSession().createQuery("From Supplier s where s.id = :id").setParameter("id", supplierid).uniqueResult());
        temp.setLastModifiedDate(LocalDateTime.now());
        temp.setCreatedBy(u);
        temp.setModifiedBy(u);
        temp.setCreationDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().save(temp);

        return temp;
    }

    public Series editSeries(int Seriesid, String name, String description, int supplierid, User u){
        Series temp = getSeriesById(Seriesid);
        //set values
        temp.setName(name);
        temp.setDescription(description);
        temp.setSupplier((Supplier) sessionFactory.getCurrentSession().createQuery("From Supplier s where s.id = :id").setParameter("id", supplierid).uniqueResult());
        temp.setModifiedBy(u);
        temp.setLastModifiedDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().update(temp);
        return temp;
    }
}
