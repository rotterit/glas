package DAL;

import Model.Stock.Location;

import Model.User.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 6-5-2015.
 */
@Service
public class LocationDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Location getLocationById(int id){
        Location l = (Location) sessionFactory.getCurrentSession().createQuery("From Location l where l.id = :id").setParameter("id",id).list().get(0);
        return l;
    }

    public List<Location> getLocations(){
        List<Location> locations = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Location ").list();
        for(Object o: results){
            locations.add((Location) o);
        }
        return locations;
    }

    public Location createLocation(String description, User u){
        Location l = new Location();
        l.setDescription(description);
        l.setLastModifiedDate(LocalDateTime.now());
        l.setCreatedBy(u);
        l.setModifiedBy(u);
        l.setCreationDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().save(l);

        return l;
    }

    public Location editLocation(int locationid, String description, User u){
        Location l = getLocationById(locationid);
        l.setDescription(description);
        l.setLastModifiedDate(LocalDateTime.now());
        l.setModifiedBy(u);
        sessionFactory.getCurrentSession().save(l);
        return l;
    }
}
