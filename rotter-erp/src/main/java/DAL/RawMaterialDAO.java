package DAL;

import DTO.Material.*;
import Model.FinishedProduct.*;
import Model.Misc.Note;
import Model.Misc.Unit;
import Model.RawMaterial.*;
import Model.RawMaterial.RawMaterial;
import Model.RawStock.MaterialNotification;
import Model.RawStock.MaterialStockLine;
import Model.Stock.Stock;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 12-5-2015.
 */
@Service
public class RawMaterialDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public RawMaterial getRawMaterialById(int id){

        RawMaterial r = (RawMaterial) sessionFactory.getCurrentSession().createQuery("From RawMaterial r where r.id = :id").setParameter("id",id).list().get(0);

        return r;
    }

    public List<RawMaterial> getRawMaterials(){
        List<RawMaterial> RawMaterials = new ArrayList<>();

        List<Object> results = sessionFactory.getCurrentSession().createQuery("From RawMaterial ").list();

        for(Object o: results){
            RawMaterials.add((RawMaterial) o);
        }
        return RawMaterials;
    }

    public RawMaterial createRawMaterial(String description,Color c, Supplier s, Quality q, double price, Series se, MySize si, ProductType pt, Note note, int moq, User u, int baseAmount) throws HorribleException {
        String productCode = generateProductCode(pt,si,c);
        if(s.getId()!=se.getSupplier().getId()){
            throw new HorribleException("the series and supplier do not match");
        }
        if(sessionFactory.getCurrentSession().createQuery("From RawMaterial r where r.mySize.id = :sizeid AND r.color.id = :colorid AND r.productType.id = :producttypeid AND r.series.id = :seriesid AND  r.supplier.id = :supplierid AND r.quality.id = :qualityid")
                .setParameter("colorid",c.getId())
                .setParameter("producttypeid", pt.getId())
                .setParameter("seriesid", se.getId())
                .setParameter("supplierid", s.getId())
                .setParameter("sizeid", si.getId())
                .setParameter("qualityid", q.getId()).list().size()<=0) {
            RawMaterial l = new RawMaterial();
            l.setDescription(description);
            l.setColor(c);
            l.setSupplier(s);
            l.setQuality(q);
            l.setPrice(price);
            l.setSeries(se);
            l.setMySize(si);
            l.setProductType(pt);
            l.setNote(note);
            l.setMinimumOrderQuantity(moq);
            l.setBaseminStockAmount(baseAmount);
            l.setCreationDate(LocalDateTime.now());
            l.setCreatedBy(u);
            l.setLastModifiedDate(LocalDateTime.now());
            l.setModifiedBy(u);
            sessionFactory.getCurrentSession().save(l);
            return l;
        }else{
            throw new HorribleException("The code already exists for this supplier");
        }
    }

    public RawMaterial editRawMaterial(int RawMaterialid, String description, double price, int moq, User u, int baseam, Note note){
        RawMaterial l = getRawMaterialById(RawMaterialid);
        l.setDescription(description);
        l.setPrice(price);
        l.setMinimumOrderQuantity(moq);
        l.setModifiedBy(u);
        l.setLastModifiedDate(LocalDateTime.now());
        l.setBaseminStockAmount(baseam);
        l.setNote(note);
        sessionFactory.getCurrentSession().update(l);
        return l;
    }

    public RawMaterial editRawMaterial(int RawMaterialid, double price, int moq, User u, int baseam){
        RawMaterial l = getRawMaterialById(RawMaterialid);
        l.setPrice(price);
        l.setMinimumOrderQuantity(moq);
        l.setModifiedBy(u);
        l.setLastModifiedDate(LocalDateTime.now());
        l.setBaseminStockAmount(baseam);
        sessionFactory.getCurrentSession().update(l);
        return l;
    }

    public String generateProductCode(ProductType pt, MySize s, Color c){
        return "" + pt.getCode()+"-"+s.getCode()+"-"+c.getCode();
    }

    public RawMaterial addSupplierLine(int RawMaterialid, ProductSupplierLine p){
        RawMaterial l = getRawMaterialById(RawMaterialid);
        l.setProductSupplierLine(p);
        sessionFactory.getCurrentSession().update(l);
        return l;
    }

    public int getMinStockAmount(int rawMaterialid){
        List<MaterialNotification> ms = (List<MaterialNotification>) sessionFactory.getCurrentSession().createQuery("From MaterialNotification  m where m.rawMaterial.id = :id").setParameter("id", rawMaterialid).list();
        for(MaterialNotification m : ms){
            if(m.getStartdate().isBefore(LocalDateTime.now()) && m.getEnddate().isAfter(LocalDateTime.now())){
                return m.getAmount();
            }
        }
        RawMaterial l = (RawMaterial) sessionFactory.getCurrentSession().createQuery("From RawMaterial r where r.id = :id").setParameter("id",rawMaterialid).list().get(0);
        return l.getBaseminStockAmount();
    }

    public int getStockAmount(int rawMaterialid) throws HorribleException {
        List<MaterialStockLine> rawMaterials = (List<MaterialStockLine>) sessionFactory.getCurrentSession().createQuery("From MaterialStockLine m where m.rawMaterial.id = :id").setParameter("id", rawMaterialid).list().stream().map(MaterialStockLine.class::cast).collect(Collectors.toList());
        int amount = 0;
        for(MaterialStockLine r : rawMaterials){
            amount += r.getAmount();
        }
        return amount;
    }

    public String getLineColor(int rawMaterialid) throws HorribleException {
        int amount = getStockAmount(rawMaterialid);
        RawMaterial l = getRawMaterialById(rawMaterialid);
        int checkamount = 0;
        Unit u = (Unit) sessionFactory.getCurrentSession().createQuery("From Unit u where u.name = 'WarnAmount'").list().get(0);
        double warn = u.getNumericalValue();
        List<MaterialNotification> ms = (List<MaterialNotification>) sessionFactory.getCurrentSession().createQuery("From MaterialNotification  m where m.rawMaterial.id = :id").setParameter("id", rawMaterialid).list();
        for(MaterialNotification m : ms){
            if(m.getPredate().isBefore(LocalDateTime.now()) && m.getEnddate().isAfter(LocalDateTime.now())){
               if(amount<m.getAmount()){
                   return "#D41918";
               }else{
                   if(amount<m.getAmount()+warn){
                       return "#FFB801";
                   }else{
                       return "";
                   }
               }

            }

        }
        if(amount<l.getBaseminStockAmount()){
            return "#D41918";
        }else{
            if(amount<l.getBaseminStockAmount()+warn){
                return "#FFB801";
            }else{
                return "";
            }
        }
    }

    public List<RawMaterial> getByDescription(String search) {
        return (List<RawMaterial>) sessionFactory.getCurrentSession().createQuery("From RawMaterial r where r.Description Like :search").setParameter("search","%"+search+"%").list();
    }

    public List<RawMaterial> getByDescriptionForOrder(String search) {
        return (List<RawMaterial>) sessionFactory.getCurrentSession().createQuery("From RawMaterial r where r.Description Like :search And r.quality.id = 1").setParameter("search","%"+search+"%").list();
    }

    public ProductSupplierLine createProductSupplierLine(ProductType pt, MySize s, Color c, String supplierCode, Supplier su, RawMaterial r, User u){
        ProductSupplierLine p = new ProductSupplierLine();
        p.setBarcode("");
        p.setInternalReference(generateProductCode(pt,s,c));
        p.setRawMaterialCode(supplierCode);
        p.setCreationDate(LocalDateTime.now());
        p.setCreatedBy(u);
        p.setLastModifiedDate(LocalDateTime.now());
        p.setModifiedBy(u);
        p.setSupplier(su);
        p.setRawMaterial(r);
        sessionFactory.getCurrentSession().save(p);
        return p;
    }

    public List<MaterialStockLine> getStockLineById(int id) {
        return (List<MaterialStockLine>) sessionFactory.getCurrentSession().createQuery("from MaterialStockLine m where m.rawMaterial.id = :id").setParameter("id", id).list();
    }

    public RawMaterial getForDifferentQuality(int materialid, int qualityid){
        RawMaterial m = getRawMaterialById(materialid);
        return (RawMaterial) sessionFactory.getCurrentSession().createQuery("From RawMaterial r where r.mySize.id = :sizeid AND r.color.id = :colorid AND r.productType.id = :producttypeid AND r.series.id = :seriesid AND  r.supplier.id = :supplierid AND r.quality.id = :qualityid")
                .setParameter("colorid",m.getColor().getId())
                .setParameter("producttypeid", m.getProductType().getId())
                .setParameter("seriesid", m.getSeries().getId())
                .setParameter("supplierid", m.getSupplier().getId())
                .setParameter("qualityid", qualityid)
                .setParameter("sizeid", m.getMySize().getId()).list().get(0);

    }


    public DTO.Material.RawMaterial getRawMaterialFullById(int id) throws HorribleException {
            List<StockDetail> ms = getStockLineById(id).stream().map(StockDetail::new).collect(Collectors.toList());
            int stockAmount = getStockAmount(id);
            return new DTO.Material.RawMaterial(getRawMaterialById(id), stockAmount, getMinStockAmount(id), getLineColor(id),ms);
    }

    public void checkProductPrices(List<RawMaterial> rawMaterials, double minutecost){
        List<Integer> ids = rawMaterials.stream().mapToInt(s -> s.getId()).boxed().collect(Collectors.toList());
        List<Product> products = sessionFactory.getCurrentSession().createQuery("From Product p where p.material.id IN :id").setParameterList("id", ids).list();
        products.stream().map(p->p.calcPrice(minutecost));
        for(Product p : products){
            sessionFactory.getCurrentSession().update(p);
        }
    }

    public List<RawMaterial> getAllColorVariants(int id){
        RawMaterial r = getRawMaterialById(id);
        return sessionFactory.getCurrentSession().createQuery("from RawMaterial r where r.productType.id = :productType AND r.supplier.id = :supplier AND r.mySize.id = :size AND r.quality.id = :quality")
                .setParameter("productType", r.getProductType().getId())
                .setParameter("supplier", r.getSupplier().getId())
                .setParameter("quality", r.getQuality().getId())
                .setParameter("size", r.getMySize().getId()).list();
    }

    public List<RawMaterial> getAllQualityVariants(int id){
        RawMaterial r = getRawMaterialById(id);
        return sessionFactory.getCurrentSession().createQuery("from RawMaterial r where r.productType.id = :productType AND r.supplier.id = :supplier AND r.mySize.id = :size AND r.color.id = :color")
                .setParameter("productType", r.getProductType().getId())
                .setParameter("supplier", r.getSupplier().getId())
                .setParameter("color", r.getColor().getId())
                .setParameter("size", r.getMySize().getId()).list();
    }

    public void editMOQForSupplier(int supplierid, int moq) {
        for(RawMaterial r : (List<RawMaterial>) sessionFactory.getCurrentSession().createQuery("from RawMaterial r where r.supplier.id = :supplier").setParameter("supplier", supplierid).list()){
            r.setMinimumOrderQuantity(moq);
            sessionFactory.getCurrentSession().update(r);
        }
    }

    public DTO.Material.RawMaterial getRawMaterialForOrderFullById(int id) throws HorribleException {
        List<StockDetail> ms = getStockLineById(id).stream().map(StockDetail::new).collect(Collectors.toList());
        int stockAmount = 0;
        for(RawMaterial m : getAllQualityVariants(id)) {
            stockAmount += getStockAmount(m.getId());
        }
        return new DTO.Material.RawMaterial(getRawMaterialById(id), stockAmount, getMinStockAmount(id), getLineColor(id),ms);
    }

    public void AllForThisQuality(Quality q) throws HorribleException {
        for(RawMaterial r : getRawMaterials()){
            RawMaterial m = createRawMaterial(r.getDescription(),r.getColor(),r.getSupplier(), q, r.getPrice(),r.getSeries(), r.getMySize(), r.getProductType(), r.getNote(), r.getMinimumOrderQuantity(), r.getCreatedBy(), r.getBaseminStockAmount());
            addSupplierLine(m.getId(),r.getProductSupplierLine());
        }
    }

    public RawMaterial getRawMaterialByProps(Color c, ProductType pt, MySize s, Supplier supplier) throws HorribleException {
        try {
            return (RawMaterial) sessionFactory.getCurrentSession().createQuery("from RawMaterial r where r.productType.id = :productType AND r.supplier.id = :supplier AND r.mySize.id = :size AND r.color.id = :color AND r.quality.id = 1")
                    .setParameter("productType", pt.getId())
                    .setParameter("supplier", supplier.getId())
                    .setParameter("color", c.getId())
                    .setParameter("size", s.getId()).list().get(0);
        }catch(IndexOutOfBoundsException e){
            throw new HorribleException(e.getMessage());
        }
    }

    public void refreshMaterialCode(){
        List<RawMaterial> rawMaterials = getRawMaterials();
        for(RawMaterial r : rawMaterials){
            ProductSupplierLine supplierLine = r.getProductSupplierLine();
            supplierLine.setInternalReference(generateProductCode(r.getProductType(), r.getMySize(), r.getColor()));
        }
    }
}
