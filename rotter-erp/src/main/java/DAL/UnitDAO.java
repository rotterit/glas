package DAL;

import Model.Misc.Note;
import Model.Misc.Unit;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 14-5-2015.
 */
@Service
public class UnitDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Unit getUnitById(int unitId){
        return (Unit) sessionFactory.getCurrentSession().createQuery("From Unit u where u.id = :id").setParameter("id",unitId).list().get(0);
    }

    public List<Unit> getUnits(){
        List<Unit> units = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Unit ").list();
        for(Object o: results){
            units.add((Unit) o);
        }
        return units;
    }

    public Unit edit(int unitid, String name,String textValue, double numericValue, Note note){
        Unit u = getUnitById(unitid);
        u.setName(name);
        u.setTextValue(textValue);
        u.setNumericalValue(numericValue);
        u.setNote(note);
        sessionFactory.getCurrentSession().update(u);
        return u;
    }

    public Unit getWorkprice() {
        return (Unit) sessionFactory.getCurrentSession().createQuery("from Unit u where u.id = 1").list().get(0);
    }

    public Unit getinvoicestart() {
        return (Unit) sessionFactory.getCurrentSession().createQuery("from Unit u where u.id = 3").list().get(0);
    }
}
