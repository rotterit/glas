package DAL;


import DAL.Util.InvoiceAndStockCorrections;
import DTO.Worksheet.ProductionQueueLine;
import Model.FinishedStock.StockCorrection;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.Invoice;
import Model.Invoicing.Worksheet;
import Model.Misc.Note;
import Model.RawStock.Materialmove;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 22-4-2015.
 */
@Service
public class WorksheetDAO {

    private Logger logger = LoggerFactory.getLogger(WorksheetDAO.class);

    @Autowired
    private SessionFactory sessionFactory;

    public Worksheet getWorksheetById(long id){
        Worksheet w = (Worksheet) sessionFactory.getCurrentSession().createQuery("From Worksheet w Where w.id = :id").setParameter("id", id).list().get(0);
        return w;
    }

    public List<Worksheet> getWorksheets(){
        List<Worksheet> worksheets = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Worksheet").list();
        for(Object o : results){
            worksheets.add((Worksheet) o);
        }
        return worksheets;
    }

    public Worksheet createWorksheet(Materialmove materialmove, Stockmove stockmove, User creator, User executor, Note note){
        Worksheet w = new Worksheet(
                LocalDateTime.now(),
                null,
                false,
                false,
                null,
                materialmove,
                stockmove,
                creator,
                executor,
                note,
                LocalDateTime.now(),
                creator
        );
        sessionFactory.getCurrentSession().save(w);
        return w;
    }

    public Worksheet startWorksheet(long worksheetid, User user){
        Worksheet w = getWorksheetById(worksheetid);
        w.setStarted(true);
        w.setStartDate(LocalDateTime.now());
        w.setLastModifiedDate(LocalDateTime.now());
        w.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(w);
        return w;
    }

    public Worksheet finishWorksheet(long worksheetid, User user){
        Worksheet w = getWorksheetById(worksheetid);
        w.setCompleted(true);
        w.setFinishedDate(LocalDateTime.now());
        w.setLastModifiedDate(LocalDateTime.now());
        w.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(w);
        return w;
    }

    public Worksheet editWorksheet(long worksheetid, long MaterialmoveId, long StockmoveId, long createDateMillis, long startDateMillis, long finishedDateMillis, boolean started, boolean completed){
        Worksheet w = getWorksheetById(worksheetid);
        return w;
    }


    public List<ProductionQueueLine> getProductionQueue() {
        List<StockmoveItem> worksheetstockmoveitems = sessionFactory.getCurrentSession().createQuery("select w.stockmove.stockmoveItems From Worksheet w where w.completed = false").list();
        List<ProductionQueueLine> productionQueueLines = new ArrayList<>();
        List<Invoice> invoices = sessionFactory.getCurrentSession().createQuery("From Invoice i where i.sent = false AND i.canceled = false").list();
        logger.warn("invoices: " + invoices.size());
        logger.warn("needed: " + invoices.stream().mapToInt(m->m.getNeeded().getStockmoveItems().size()).sum());
        List<StockCorrection> stockCorrections = sessionFactory.getCurrentSession().createQuery("from StockCorrection s where s.stockmove.isComplete =false").list();
        logger.warn("stockCorrections: "+ stockCorrections.size());
        boolean add = false;
        for(Invoice invoice : invoices){
            for(StockmoveItem stockmoveItem : invoice.getNeeded().getStockmoveItems()){
                add = true;
                for(StockmoveItem stockmoveItem1 : worksheetstockmoveitems) {
                    if(stockmoveItem.getId()==stockmoveItem1.getId()) {
                        add = false;
                    }
                }
                if(add) {
                    productionQueueLines.add(new ProductionQueueLine(stockmoveItem, invoice));
                }
            }
        }
        for(StockCorrection stockCorrection : stockCorrections){
            for(StockmoveItem stockmoveItem : stockCorrection.getStockmove().getStockmoveItems()){
                add = true;
                for(StockmoveItem stockmoveItem1 : worksheetstockmoveitems) {
                    if (stockmoveItem.getId() != stockmoveItem1.getId()) {
                        add = false;
                    }
                }
                if(add) {
                    productionQueueLines.add(new ProductionQueueLine(stockmoveItem, stockCorrection));
                }
            }
        }
        logger.warn("result: "+ productionQueueLines.size());
        return productionQueueLines;
    }

    public void deleteWorksheet(long worksheetid) {
        Worksheet w= getWorksheetById(worksheetid);
        sessionFactory.getCurrentSession().delete(w);
    }

    public List<Worksheet> getNewWorksheets() {
        return (List<Worksheet>) sessionFactory.getCurrentSession().createQuery("From Worksheet w where w.started=false").list();
    }

    public List<Worksheet> getStartedWorksheets(){
        return (List<Worksheet>) sessionFactory.getCurrentSession().createQuery("From Worksheet w where w.started=true and w.completed = false").list();
    }

    public List<Worksheet> getCompletedWorksheets(){
        return (List<Worksheet>) sessionFactory.getCurrentSession().createQuery("From Worksheet w where w.completed = true").list();
    }

    public List<Worksheet> getWorksheetsForCutters(int userid) {
        return (List<Worksheet>) sessionFactory.getCurrentSession().createQuery("From Worksheet w where w.completed = false AND w.executor.id = :userid").setParameter("userid", userid).list();
    }
}
