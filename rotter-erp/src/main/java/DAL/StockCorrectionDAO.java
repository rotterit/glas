package DAL;

import Model.FinishedStock.StockCorrection;
import Model.FinishedStock.StockLine;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;
import Model.Misc.Note;
import Model.User.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by thijs on 21-5-2015.
 */
@Service
public class StockCorrectionDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public List<StockCorrection> getStockCorrections(){
        return (List<StockCorrection>) sessionFactory.getCurrentSession().createQuery("from StockCorrection s").list();
    }

    public List<StockCorrection> getOpenStockCorrections(){
        return (List<StockCorrection>) sessionFactory.getCurrentSession().createQuery("from StockCorrection s where s.stockmove.isComplete = false").list();
    }

    public StockCorrection getStockCorrectionById(long id){
        return (StockCorrection) sessionFactory.getCurrentSession().createQuery("from StockCorrection s where s.id = :id").setParameter("id", id).list().get(0);
    }

    public StockCorrection createStockCorrection(User createdBy, LocalDateTime creationDate, LocalDateTime lastModifiedDate, User modifiedBy, Stockmove stockmove, Note note){
        StockCorrection stockCorrection = new StockCorrection(createdBy,"",creationDate,lastModifiedDate,modifiedBy,stockmove,note);
        sessionFactory.getCurrentSession().save(stockCorrection);
        return stockCorrection;
    }


    public void completeStockcorrection(long id) {
        StockCorrection stockCorrection = getStockCorrectionById(id);
        StockmoveItem stockmoveItem = stockCorrection.getStockmove().getStockmoveItems().get(0);
        for(StockLine stockLine : stockmoveItem.getStock().getStockLines()){
            if(stockLine.getProduct().getId()== stockmoveItem.getProduct().getId()){
                stockLine.setAmount(stockLine.getAmount()+stockmoveItem.getAmount());
                sessionFactory.getCurrentSession().update(stockLine);
            }
        }
    }

    public void completeStockcorrections(List<Long> stockCorrectionids) {
        for(long id : stockCorrectionids){
            completeStockcorrection(id);
        }
    }
}
