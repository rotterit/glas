package DAL;

import Model.FinishedProduct.ManufacturingType;
import Model.Stock.Location;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 20-5-2015.
 */
@Service
public class ManufacturingTypeDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public ManufacturingType getManufacturingTypeById(int id){
        return (ManufacturingType) sessionFactory.getCurrentSession().createQuery("From ManufacturingType m where m.id = :id").setParameter("id", id).list().get(0);
    }

    public List<ManufacturingType> getManufacturingTypes(){
        List<ManufacturingType> locations = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From ManufacturingType").list();
        for(Object o: results){
            locations.add((ManufacturingType) o);
        }
        return locations;
    }

    public ManufacturingType create(String description, String name, String code, User u) throws HorribleException {
        if(sessionFactory.getCurrentSession().createQuery("From ManufacturingType m where m.Code = :code").setParameter("code",code).list().size()<=0) {
            ManufacturingType m = new ManufacturingType();
            m.setDescription(description);
            m.setCode(code);
            m.setName(name);
            m.setCreatedBy(u);
            m.setCreationDate(LocalDateTime.now());
            m.setLastModifiedDate(LocalDateTime.now());
            m.setModifiedBy(u);
            sessionFactory.getCurrentSession().save(m);
            return m;
        }else{
            throw new HorribleException("you can't make a ManufacturingType with a code that's already in use");
            }
    }

    public ManufacturingType edit(int id, String description, String name, String code, User u) throws HorribleException {
        if(sessionFactory.getCurrentSession().createQuery("From ManufacturingType m where m.Code = :code AND m.id != :id").setParameter("code",code).setParameter("id", id).list().size()<=0) {
            ManufacturingType m = getManufacturingTypeById(id);
            m.setDescription(description);
            m.setCode(code);
            m.setName(name);
            m.setLastModifiedDate(LocalDateTime.now());
            m.setModifiedBy(u);
            sessionFactory.getCurrentSession().update(m);
            return m;
        }else{
            throw new HorribleException("you can't edit a ManufacturingType's code to one that's already in use");
        }
    }
}
