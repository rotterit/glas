package DAL;

import Model.RawMaterial.RawMaterial;
import Model.RawStock.MaterialNotification;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 18-5-2015.
 */
@Service
public class MaterialNotificationDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public MaterialNotification getById(int materialNotificationId){
        return (MaterialNotification) sessionFactory.getCurrentSession().createQuery("From MaterialNotification m where m.id = :id").setParameter("id", materialNotificationId).list().get(0);
    }

    public MaterialNotification create(long predate, long startdate, long enddate, RawMaterial rawMaterial, int amount, User u) throws HorribleException {
        LocalDateTime preDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(predate), ZoneId.of("Europe/Berlin"));
        LocalDateTime startDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(startdate), ZoneId.of("Europe/Berlin"));
        LocalDateTime endDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(enddate), ZoneId.of("Europe/Berlin"));
        List<MaterialNotification> ms = sessionFactory.getCurrentSession().createQuery("From MaterialNotification m").list();
        for (int i = 0; i < ms.size(); i++) {
            MaterialNotification materialNotification =  ms.get(i);
            if((materialNotification.getStartdate().isAfter(startDate)&& materialNotification.getStartdate().isBefore(endDate))||(materialNotification.getEnddate().isBefore(endDate)&&materialNotification.getEnddate().isAfter(startDate))){
                throw new HorribleException("the dates provide an overlap");
            }
        }
        MaterialNotification m = new MaterialNotification();
        m.setPredate(preDate);
        m.setStartdate(startDate);
        m.setEnddate(endDate);
        m.setAmount(amount);
        m.setRawMaterial(rawMaterial);
        m.setCreationDate(LocalDateTime.now());
        m.setCreatedBy(u);
        m.setModifiedBy(u);
        m.setLastModifiedDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().save(m);
        return m;
    }

    public MaterialNotification edit(long predate, long startdate, long enddate,int amount, User u) throws HorribleException{
        LocalDateTime preDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(predate), ZoneId.of("ECT"));
        LocalDateTime startDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(startdate), ZoneId.of("ECT"));
        LocalDateTime endDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(enddate),ZoneId.of("ECT"));
        List<MaterialNotification> ms = sessionFactory.getCurrentSession().createQuery("From MaterialNotification m").list();
        for (int i = 0; i < ms.size(); i++) {
            MaterialNotification materialNotification =  ms.get(i);
            if((materialNotification.getStartdate().isAfter(startDate)&& materialNotification.getStartdate().isBefore(endDate))||(materialNotification.getEnddate().isBefore(endDate)&&materialNotification.getEnddate().isAfter(startDate))){
                throw new HorribleException("the dates provide an overlap");
            }
        }
        MaterialNotification m = new MaterialNotification();
        m.setPredate(preDate);
        m.setStartdate(startDate);
        m.setEnddate(endDate);
        m.setAmount(amount);
        m.setModifiedBy(u);
        m.setLastModifiedDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().update(m);
        return m;
    }

    public List<MaterialNotification> getAll() {
        return (List<MaterialNotification>) sessionFactory.getCurrentSession().createQuery("From MaterialNotification m").list();
    }

    public MaterialNotification remove(int id) {
        MaterialNotification materialNotification = getById(id);
        sessionFactory.getCurrentSession().delete(materialNotification);
        return materialNotification;
    }
}
