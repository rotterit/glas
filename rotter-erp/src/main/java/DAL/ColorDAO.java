package DAL;

import Model.FinishedProduct.Color;

import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 30-4-2015.
 */
@Service
public class ColorDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Color getColorById(int id){

        Color c = (Color) sessionFactory.getCurrentSession().createQuery("From Color c where c.id = :id").setParameter("id",id).list().get(0);

        return c;
    }

    public List<Color> getColors(){
        List<Color> categories = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Color ").list();
        for(Object o: results){
            categories.add((Color) o);
        }
        return categories;
    }

    public Color createColor(String code, String name, String value, User u) throws HorribleException {
        if(sessionFactory.getCurrentSession().createQuery("From Color c where c.code = :code").setParameter("code", code).list().size()<=0) {
            Color c = new Color();
            c.setValue(value);
            c.setCreationDate(LocalDateTime.now());
            c.setCode(code);
            c.setName(name);
            c.setModifiedBy(u);
            c.setLastModifiedDate(LocalDateTime.now());
            c.setCreationDate(LocalDateTime.now());
            c.setCreatedBy(u);
            sessionFactory.getCurrentSession().save(c);
            return c;
        }else{
            throw new HorribleException("the code already exists");
        }
    }

    public Color editColor(int colorid,String code, String name, String value, User u) throws HorribleException {
        if(sessionFactory.getCurrentSession().createQuery("From Color c where c.code = :code AND c.id != :id").setParameter("code", code).setParameter("id",colorid).list().size()<=0) {
            Color c = getColorById(colorid);
            c.setValue(value);
            c.setCode(code);
            c.setName(name);
            c.setLastModifiedDate(LocalDateTime.now());
            c.setModifiedBy(u);
            sessionFactory.getCurrentSession().save(c);
            return c;
        }else{
            throw new HorribleException("the code already exists");
        }
    }
}
