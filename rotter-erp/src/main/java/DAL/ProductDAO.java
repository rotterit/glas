package DAL;

import DTO.Product.DescriptionNoId;
import Model.FinishedProduct.*;
import Model.FinishedStock.StockLine;
import Model.FinishedStock.StockmoveItem;
import Model.Misc.Unit;
import Model.RawMaterial.RawMaterial;
import Model.RawMaterial.Supplier;
import Model.Stock.Stock;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 21-4-2015.
 */
@Service
public class ProductDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private String regex = "^[A-Z]{2,}-[0-9]{5}-[A-Z0-9\\\\]+-[A-Z]{2}$";

    public ProductDAO() {
    }

    public Product getProductById(long productid) {
        Product p = (Product) sessionFactory.getCurrentSession().createQuery("From Product p WHERE p.id= :id").setParameter("id", productid).list().get(0);
        return p;
    }

    public List<Product> getProducts() {
        List<Product> products = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Product").list();
        for (Object o : results) {
            Product product = (Product) o;
            Hibernate.initialize(product.getDescriptions());
            products.add(product);
        }
        return products;
    }

    public Product editProduct(long productid, String name, int basetimemins, int mintimemins, int maxtimemins, double price, User u , User system, Unit workprice) {
        Product p = getProductById(productid);
        Product pnew = new Product();
        pnew.setActive(true);
        pnew.setColor(p.getColor());
        pnew.setAvgTime(((basetimemins * 4) + (mintimemins * 2) + (maxtimemins * 2)) / 8);
        pnew.setName(name);
        pnew.setBaseTime(basetimemins);
        pnew.setMaxTime(maxtimemins);
        pnew.setMinTime(mintimemins);
        pnew.setPrice(price);
        pnew.setProductCode(p.getProductCode());
        pnew.setArchived(false);
        //formule voor prijs
        pnew.setMaterial(p.getMaterial());
        pnew.setActualprice((p.getMaterial().getPrice() + (basetimemins * workprice.getNumericalValue())));
        pnew.setCategory(p.getCategory());
        List<DescriptionNoId> descriptions = new ArrayList<>();
        for(Description description: p.getDescriptions()){
            DescriptionNoId descriptionNoId = new DescriptionNoId();
            descriptionNoId.setLanguageCode(description.getLanguageCode());
            descriptionNoId.setLanguageName(description.getLanguageName());
            descriptionNoId.setDescription(description.getDescription());
            descriptions.add(descriptionNoId);
        }
        pnew.setDescriptions(createDescriptions(descriptions,u));
        pnew.setMySize(p.getMySize());
        pnew.setProductType(p.getProductType());
        pnew.setManufacturingtype(p.getManufacturingtype());
        pnew.setActive(pnew);
        pnew.setSupplier(p.getSupplier());
        pnew.setDesign(p.getDesign());
        pnew.setModifiedBy(u);
        pnew.setCreatedBy(p.getCreatedBy());
        pnew.setLastModifiedDate(LocalDateTime.now());
        pnew.setCreationDate(p.getCreationDate());
        sessionFactory.getCurrentSession().save(pnew);
        p.setActive(pnew);
        p.setArchived(true);
        p.setActive(false);
        sessionFactory.getCurrentSession().update(p);
        return pnew;
    }

    public Product editProduct(long productid, String name, int basetimemins, int mintimemins, int maxtimemins, User u , User system, Unit workprice) {
        Product p = getProductById(productid);
        Product pnew = new Product();
        pnew.setActive(true);
        pnew.setColor(p.getColor());
        pnew.setAvgTime(((basetimemins * 4) + (mintimemins * 2) + (maxtimemins * 2)) / 8);
        pnew.setName(name);
        pnew.setBaseTime(basetimemins);
        pnew.setMaxTime(maxtimemins);
        pnew.setMinTime(mintimemins);
        pnew.setProductCode(p.getProductCode());
        pnew.setArchived(false);
        //formule voor prijs
        pnew.setMaterial(p.getMaterial());
        pnew.setActualprice((p.getMaterial().getPrice() + (basetimemins * workprice.getNumericalValue())));
        pnew.setCategory(p.getCategory());
        pnew.setDescriptions(p.getDescriptions());
        pnew.setMySize(p.getMySize());
        pnew.setProductType(p.getProductType());
        pnew.setManufacturingtype(p.getManufacturingtype());
        pnew.setActive(pnew);
        pnew.setSupplier(p.getSupplier());
        pnew.setDesign(p.getDesign());
        pnew.setModifiedBy(u);
        pnew.setCreatedBy(p.getCreatedBy());
        pnew.setLastModifiedDate(LocalDateTime.now());
        pnew.setCreationDate(p.getCreationDate());
        sessionFactory.getCurrentSession().save(pnew);
        p.setActive(pnew);
        p.setArchived(true);
        sessionFactory.getCurrentSession().update(p);
        return pnew;
    }

    public Product createProduct(Supplier s,
                                 int basetime,
                                 int mintime,
                                 int maxtime,
                                 double price,
                                 Color c,
                                 List<Description> descriptions,
                                 MySize size,
                                 ProductType p,
                                 ManufacturingType m,
                                 Design d,
                                 String name,
                                 User u,
                                 RawMaterial r,
                                 String productCode,
                                 Unit workprice,
                                 Category ca) throws HorribleException {
        if (sessionFactory.getCurrentSession().createQuery("from Product p where p.productCode = :id").setParameter("id", productCode).list().size() <= 0) {
            Product product = new Product(u, productCode, name, true, false, s, basetime, mintime, maxtime, (maxtime + mintime + basetime + basetime) / 4, price, c, (r.getPrice() + (basetime * workprice.getNumericalValue())), r, ca, descriptions, size, p, m, null, d, LocalDateTime.now(), LocalDateTime.now(), u);
            sessionFactory.getCurrentSession().save(product);
            return product;
        } else {
            throw new HorribleException("this code is already in use");
        }

    }

    public String generateProductCode(ProductType p, Design d, MySize size, Color c) {
        return "" + p.getCode() + "-" + d.getDesignCode() + "-" + size.getCode() + "-" + c.getCode();
    }

    public List<Description> createDescriptions(List<DescriptionNoId> descriptions, User u) {
        List<Description> descriptionList = new ArrayList<>();
        for (DescriptionNoId d : descriptions) {
            Description desc = new Description();
            desc.setLanguageCode(d.getLanguageCode());
            desc.setLanguageName(d.getLanguageName());
            desc.setDescription(d.getDescription());
            desc.setModifiedBy(u);
            desc.setCreatedBy(u);
            desc.setCreationDate(LocalDateTime.now());
            desc.setLastModifiedDate(LocalDateTime.now());
            sessionFactory.getCurrentSession().save(desc);
            descriptionList.add(desc);
        }
        return descriptionList;
    }

    public List<Product> getColorVariants(long id) {
        Product p = getProductById(id);
        return sessionFactory.getCurrentSession().createQuery("From Product p where p.mySize.id = :sizeid AND p.design.id = :design AND p.productType.id = :productType AND p.active = true")
                .setParameter("sizeid", p.getMySize().getId())
                .setParameter("design", p.getDesign().getId())
                .setParameter("productType", p.getProductType().getId()).list();
    }

    public void bindProduct(List<Description> descriptions, long id){
        Product p = getProductById(id);
        for(Description d : descriptions){
            d.setProduct(p);
            sessionFactory.getCurrentSession().update(d);
        }
    }

    public List<Product> getByCategorySize(int categorysizeId){
        CategorySize cs = (CategorySize) sessionFactory.getCurrentSession().createQuery("From CategorySize s where s.id = :id").setParameter("id", categorysizeId).list().get(0);
        return sessionFactory.getCurrentSession().createQuery("From Product p  where p.category.id = :cid and p.mySize.id = :sid and  p.active = true").setParameter("cid", cs.getCategory().getId()).setParameter("sid", cs.getMySize().getId()).list();
    }

    public Product setPrice(long id, double price, User u){
        Product p = getProductById(id);
        Product pnew = new Product();
        pnew.setActive(true);
        pnew.setColor(p.getColor());
        pnew.setAvgTime(p.getAvgTime());
        pnew.setName(p.getName());
        pnew.setBaseTime(p.getBaseTime());
        pnew.setMaxTime(p.getBaseTime());
        pnew.setMinTime(p.getMinTime());
        pnew.setPrice(price);
        pnew.setProductCode(p.getProductCode());
        pnew.setArchived(false);
        //formule voor prijs
        pnew.setMaterial(p.getMaterial());
        pnew.setActualprice(p.getActualprice());
        pnew.setCategory(p.getCategory());
        pnew.setDescriptions(p.getDescriptions());
        pnew.setMySize(p.getMySize());
        pnew.setProductType(p.getProductType());
        pnew.setManufacturingtype(p.getManufacturingtype());
        pnew.setActive(pnew);
        pnew.setSupplier(p.getSupplier());
        pnew.setDesign(p.getDesign());
        pnew.setModifiedBy(u);
        pnew.setCreatedBy(p.getCreatedBy());
        pnew.setLastModifiedDate(LocalDateTime.now());
        pnew.setCreationDate(p.getCreationDate());
        sessionFactory.getCurrentSession().save(pnew);
        p.setActive(pnew);
        p.setArchived(true);
        sessionFactory.getCurrentSession().update(p);
        return pnew;
    }

    public List<Product> getByDescription(String searchstring) {
        return  sessionFactory.getCurrentSession().createQuery("Select Distinct d.product From Description d where d.description = :desc").setParameter("desc", "%"+searchstring+"%").list();

    }

    public int getStockAmount(long id) {
        List<StockLine> stockLines = sessionFactory.getCurrentSession().createQuery("from StockLine s where s.product.id = :id").setParameter("id", id).list();
        return stockLines.stream().mapToInt(s->s.getAmount()).sum();
    }

    public int getReservedStock(long id) {
        List<StockmoveItem> stockmoveItems = sessionFactory.getCurrentSession().createQuery("Select i.completed.stockmoveItems From Invoice i where i.sent = false AND  i.canceled = false AND i.inProduction=true").list();
        //stockmoveItems.addAll(sessionFactory.getCurrentSession().createQuery("Select c.outStockmove.stockmoveItems From Commission c where c.isReservation = true AND c.completed = false").list());
        return stockmoveItems.stream().filter(s->s.getProduct().getId() == id ).mapToInt(s-> s.getAmount()).sum();
    }

    public int getOnComission(long id){
        List<StockmoveItem> stockmoveItems = sessionFactory.getCurrentSession().createQuery("Select c.outStockmove.stockmoveItems From Commission c where c.isReservation = false AND c.completed = false").list();
        return stockmoveItems.stream().filter(s->s.getProduct().getId() == id ).mapToInt(s-> s.getAmount()).sum();
    }

    public int getInProduction(long id){
        List<StockmoveItem> stockmoveItems = sessionFactory.getCurrentSession().createQuery("Select w.stockmove.stockmoveItems  from Worksheet w where w.completed = false").list();
        return stockmoveItems.stream().filter(s->s.getProduct().getId() == id).mapToInt(s->s.getAmount()).sum();
    }

    public int getInProductionQueue(long id ){
        List<StockmoveItem> stockmoveItems = sessionFactory.getCurrentSession().createQuery("Select i.needed.stockmoveItems From Invoice i where i.sent = false AND  i.canceled = false AND i.inProduction=true").list();
        stockmoveItems.addAll(sessionFactory.getCurrentSession().createQuery("Select  s.stockmove.stockmoveItems From StockCorrection  s where s.stockmove.isComplete = false").list());
        return stockmoveItems.stream().filter(p->p.getProduct().getId() == id ).mapToInt(p->p.getAmount()).sum();
    }

    public Description getDescriptionById(int descriptionid){
        return (Description) sessionFactory.getCurrentSession().createQuery("From Description d where d.id = :id").setParameter("id", descriptionid).list().get(0);
    }

    public Description editDescription(int descriptionid, String description) {
        Description d = getDescriptionById(descriptionid);
        d.setDescription(description);
        sessionFactory.getCurrentSession().update(d);
        return d;
    }

    public void recalculatePrice(Unit ut) {
        List<Product> products = getProducts();
        for(Product p : products){
            p.setActualprice((p.getMaterial().getPrice() + (p.getBaseTime() * ut.getNumericalValue())));
            sessionFactory.getCurrentSession().update(p);
        }
    }

    public int getReservedStock(long id, Stock stock) {
        List<StockmoveItem> stockmoveItems = sessionFactory.getCurrentSession().createQuery("Select i.completed.stockmoveItems From Invoice i where i.sent = false").list();
        stockmoveItems.addAll(sessionFactory.getCurrentSession().createQuery("Select c.outStockmove.stockmoveItems From Commission c where c.isReservation = true AND c.completed = false").list());
        return stockmoveItems.stream().filter(s->s.getProduct().getId() == id ).filter(t -> t.getStock().getId() == stock.getId()).mapToInt(s -> s.getAmount()).sum();
    }

    public int getOnComission(long id, Stock stock) {
        List<StockmoveItem> stockmoveItems = sessionFactory.getCurrentSession().createQuery("Select c.outStockmove.stockmoveItems From Commission c where c.isReservation = false AND c.completed = false").list();
        return stockmoveItems.stream().filter(s -> s.getProduct().getId() == id).filter(t -> t.getStock().getId() == stock.getId()).mapToInt(s-> s.getAmount()).sum();
    }

    public int getInProduction(long id,Stock stock){
        List<StockmoveItem> stockmoveItems = sessionFactory.getCurrentSession().createQuery("Select w.stockmove.stockmoveItems  from Worksheet w where w.completed = false").list();
        return stockmoveItems.stream().filter(s->s.getProduct().getId() == id).filter(t -> t.getStock().getId() == stock.getId()).mapToInt(s->s.getAmount()).sum();
    }

    public int getInProductionQueue(long id,Stock stock ){
        List<StockmoveItem> stockmoveItems = sessionFactory.getCurrentSession().createQuery("Select i.needed.stockmoveItems From Invoice i where i.sent = false").list();
        stockmoveItems.addAll(sessionFactory.getCurrentSession().createQuery("Select  s.stockmove.stockmoveItems From StockCorrection  s where s.stockmove.isComplete = false").list());
        return stockmoveItems.stream().filter(p->p.getProduct().getId() == id ).filter(t -> t.getStock().getId() == stock.getId()).mapToInt(p->p.getAmount()).sum();
    }

    public void refreshProductCode() {
//        ProductType p, Design d, MySize size, Color c
        List<Product> products = getProducts();
        for(Product p : products){
            p.setProductCode(generateProductCode(p.getProductType(), p.getDesign(), p.getMySize(), p.getColor()));
            sessionFactory.getCurrentSession().update(p);
        }
    }
}
