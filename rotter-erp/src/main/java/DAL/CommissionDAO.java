package DAL;

import DAL.Util.CommissionToInvoice;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.Commission;
import Model.Invoicing.Customer;
import Model.Invoicing.PackingSlip;
import Model.Misc.Note;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thijs on 27-5-2015.
 */
@Service
public class CommissionDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Commission getById(long id){
        return (Commission) sessionFactory.getCurrentSession().createQuery("From Commission c where c.id = :id").setParameter("id", id).list().get(0);
    }

    public List<Commission> getCommissions(){
        return (List<Commission>) sessionFactory.getCurrentSession().createQuery("From Commission c").list();
    }

    public Commission createCommission(User user, Customer customer, long startmillis, long returnmillis, Stockmove outStockmove, User VOID, Note note){
        Commission commission = new Commission(user,
                customer,
                LocalDateTime.now(),
                LocalDateTime.now(),
                user,
                LocalDateTime.ofInstant(Instant.ofEpochMilli(startmillis), ZoneId.of("Europe/Berlin")) ,
                false,
                false,
                null,
                LocalDateTime.ofInstant(Instant.ofEpochMilli(returnmillis), ZoneId.of("Europe/Berlin")),
                outStockmove,
                null,
                new ArrayList<PackingSlip>(),
                null,
                false,
                false,
                3,
                null,
                VOID,
                false,
                null,
                VOID,
                note);
        sessionFactory.getCurrentSession().save(commission);
        return commission;
    }

    public Commission editCommission(long id, Stockmove stockmove, long startmillis, long returnmillis, Note note, User user) throws HorribleException {
        Commission commission = getById(id);
        if(!commission.isSent()){
            commission.setLastModifiedDate(LocalDateTime.now());
            commission.setModifiedBy(user);
            commission.setRequestedReturnDate(LocalDateTime.ofInstant(Instant.ofEpochMilli(returnmillis), ZoneId.of("Europe/Berlin")));
            commission.setStartDate(LocalDateTime.ofInstant(Instant.ofEpochMilli(startmillis), ZoneId.of("Europe/Berlin")));
            commission.setOutStockmove(stockmove);
            commission.setNote(note);
            sessionFactory.getCurrentSession().update(commission);
            return commission;
        }else{
            throw new HorribleException("The commission has already been sent");
        }
    }

    public Commission sendCommission(long id, User user) throws HorribleException {
        Commission commission = getById(id);
        Map<Long, Integer> packingamounts = new HashMap<>();
        for(PackingSlip packingSlip : commission.getPackingSlips()){
            for(StockmoveItem stockmoveItem : packingSlip.getStockmove().getStockmoveItems()){
                if(packingamounts.keySet().contains(stockmoveItem.getProduct().getId())){
                    int amount = packingamounts.get(stockmoveItem.getProduct().getId());
                    amount += stockmoveItem.getAmount();
                    packingamounts.put(stockmoveItem.getProduct().getId(), amount);
                }
            }
        }
        Map<Long, Integer> orderamounts = new HashMap<>();
        for(StockmoveItem stockmoveItem: commission.getOutStockmove().getStockmoveItems()){
            if(orderamounts.keySet().contains(stockmoveItem.getProduct().getId())){
                int amount = orderamounts.get(stockmoveItem.getProduct().getId());
                amount += stockmoveItem.getAmount();
                orderamounts.put(stockmoveItem.getProduct().getId(), amount);
            }
        }
        for(StockmoveItem stockmoveItem : commission.getOutStockmove().getStockmoveItems()){
            if(orderamounts.get(stockmoveItem.getProduct().getId())!= packingamounts.get(stockmoveItem.getProduct().getId())){
                throw new HorribleException("the packinslips do not cover the entire order");
            }
        }
        for(PackingSlip packingSlip : commission.getPackingSlips()){
            if(packingSlip.getShippedBy().getId()==3){
                throw new HorribleException("not all packaging slips have been shipped");
            }
        }
        commission.setSent(true);
        commission.setSentBy(user);
        commission.setSendDate(LocalDateTime.now());
        commission.setLastModifiedDate(LocalDateTime.now());
        commission.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(commission);
        return commission;
    }

    public Commission requestLock(long id, int userid) throws HorribleException {
        Commission c = getById(id);
        if(c.isLocked()&& c.getLockedBy() != userid){
            throw new HorribleException(" the commission is being edited");
        }else{
            if(c.getLockedBy() ==3) {
                c.setLocked(true);
                c.setLockedBy(userid);
                sessionFactory.getCurrentSession().update(c);
            }
        }
        return c;
    }

    public Commission completeCommission(long id, User user) throws HorribleException {
        Commission commission = getById(id);
        if(invoiceFromCommission(id).size()==0|| commission.getInvoice() != null) {
            commission.setCompleted(true);
            commission.setCompletedBy(user);
            commission.setCompletionDate(LocalDateTime.now());
            commission.setLastModifiedDate(LocalDateTime.now());
            commission.setModifiedBy(user);
            sessionFactory.getCurrentSession().update(commission);
            return commission;
        }else{
            throw new HorribleException("there's still unresolved issues");
        }
    }

    public Commission recieveCommission(long id, User user, Stockmove stockmove){
        Commission commission = getById(id);
        commission.setLastModifiedDate(LocalDateTime.now());
        commission.setModifiedBy(user);
        commission.setReturnDate(LocalDateTime.now());
        commission.setInStockmove(stockmove);
        sessionFactory.getCurrentSession().update(commission);
        return commission;
    }

    public List<CommissionToInvoice> invoiceFromCommission(long id){
        Commission commission = getById(id);
        Stockmove out = commission.getOutStockmove();
        Stockmove in = commission.getInStockmove();
        List<CommissionToInvoice> commissionToInvoices = new ArrayList<>();
        boolean used = false;
        for(StockmoveItem outitem : out.getStockmoveItems()){
            used = false;
            for(StockmoveItem initem : in.getStockmoveItems()){
                if(outitem.getProduct().getId()== initem.getProduct().getId()&&outitem.getAmount()!=initem.getAmount()){
                    commissionToInvoices.add(new CommissionToInvoice(initem.getProduct().getId(),outitem.getAmount()-initem.getAmount(), outitem.getStock().getId()));
                    used = true;
                }
            }
            if(!used){
                commissionToInvoices.add(new CommissionToInvoice(outitem.getProduct().getId(), outitem.getAmount(), outitem.getStock().getId()));
            }
        }
        return commissionToInvoices;
    }

    public List<Commission> getNew() {
        return (List<Commission>) sessionFactory.getCurrentSession().createQuery("From Commission c where c.sent= false AND c.completed = false").list();
    }

    public List<Commission> getSent() {
        return (List<Commission>) sessionFactory.getCurrentSession().createQuery("From Commission c where c.sent = true AND c.completed = false AND c.returnDate = null").list();
    }

    public List<Commission> getRecieved() {
        return (List<Commission>) sessionFactory.getCurrentSession().createQuery("From Commission c where c.sent = true AND c.completed = false AND c.returnDate != null").list();
    }

    public List<Commission> getCompleted() {
        return (List<Commission>) sessionFactory.getCurrentSession().createQuery("From Commission c where c.completed = true").list();
    }

    public Commission removePackingslip(long commissionid, PackingSlip packingSlip, User user) {
        Commission commission = getById(commissionid);
        commission.getPackingSlips().remove(packingSlip);
        commission.setLastModifiedDate(LocalDateTime.now());
        commission.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(commission);
        return commission;
    }

    public Commission addPackingslip(long id, PackingSlip packingSlip, User user) {
        Commission commission = getById(id);
        commission.getPackingSlips().add(packingSlip);
        commission.setLastModifiedDate(LocalDateTime.now());
        commission.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(commission);
        return commission;
    }

    public Commission createReservation(User user, Customer customer, long startmillis, long returnmillis, Stockmove stockmove, User VOID, Note note) {
        Commission commission = new Commission(user,
                customer,
                LocalDateTime.now(),
                LocalDateTime.now(),
                user,
                LocalDateTime.ofInstant(Instant.ofEpochMilli(startmillis), ZoneId.of("Europe/Berlin")) ,
                true,
                false,
                null,
                LocalDateTime.ofInstant(Instant.ofEpochMilli(returnmillis), ZoneId.of("Europe/Berlin")),
                stockmove,
                null,
                new ArrayList<PackingSlip>(),
                null,
                false,
                false,
                3,
                null,
                VOID,
                false,
                null,
                VOID,
                note);
        sessionFactory.getCurrentSession().save(commission);
        return commission;
    }
}
