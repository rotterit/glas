package DAL;

import Model.Invoicing.Invoice;
import Model.Invoicing.Payment;

import Model.User.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 4-5-2015.
 */
@Service
public class PaymentDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Payment getPaymentById(long id) {
        Payment p = (Payment) sessionFactory.getCurrentSession().createQuery("From Payment p where p.id = :id").setParameter("id", id).list().get(0);
        return p;
    }

    public List<Payment> getPayments(){
        List<Payment>  payments= new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Payment ").list();
        for(Object o: results){
            payments.add((Payment) o);
        }
        return payments;
    }

    public Payment createPayment(Invoice invoice, double amount, User user){
        Payment p = new Payment(amount, LocalDateTime.now(), user, LocalDateTime.now(), user, invoice.getPaymentDueDate().isBefore(LocalDateTime.now()));
        sessionFactory.getCurrentSession().save(p);
        invoice.getPayment().add(p);
        sessionFactory.getCurrentSession().update(invoice);
        return p;
    }

    public Payment editPayment(int paymentid, double amount, User user) {
        Payment p = getPaymentById(paymentid);
        p.setAmount(amount);
        p.setModifiedBy(user);
        p.setLastModifiedDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().update(p);
        return p;
    }

    public void deletePayment(int paymentid, Invoice invoice){
        Payment payment = getPaymentById(paymentid);
        invoice.getPayment().remove(payment);
        sessionFactory.getCurrentSession().update(invoice);
        sessionFactory.getCurrentSession().delete(payment);
    }
}
