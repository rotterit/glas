package DAL;

import Model.Invoicing.Customer;
import Model.Invoicing.PackingSlip;
import Model.FinishedStock.Stockmove;

import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 4-5-2015.
 */
@Service
public class PackingSlipDAO {

    @Autowired
    private SessionFactory sessionFactory;

    private final StockmoveDAO stDAL;

    public PackingSlipDAO() {
        stDAL = new StockmoveDAO();
    }

    public PackingSlip getPackingSlipById(Long packingslipid) {
        PackingSlip ps = (PackingSlip) sessionFactory.getCurrentSession().createQuery("From PackingSlip ps where ps.id = :id").setParameter("id", packingslipid).list().get(0);
        return ps;
    }

    public List<PackingSlip> getPackingSlips(){
        List<PackingSlip>  packingSlips= new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From PackingSlip").list();
        for(Object o: results){
            packingSlips.add((PackingSlip) o);
        }
        return packingSlips;
    }

    public PackingSlip createPackingSlip(Customer customer,User user, User VOID,Stockmove stockmove){
        PackingSlip ps = new PackingSlip(customer,
                LocalDateTime.now(),
                user,
                LocalDateTime.now(),
                user,
                null,
                VOID,
                null,
                VOID,
                false,
                stockmove
                );
        sessionFactory.getCurrentSession().save(ps);
        return ps;
    }

    public PackingSlip editPackingSlip(long packingslipid, Stockmove stockmove, User user){
        PackingSlip ps = getPackingSlipById(packingslipid);
        ps.setStockmove(stockmove);
        ps.setLastModifiedDate(LocalDateTime.now());
        ps.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(ps);
        return ps;
    }

    public PackingSlip setPacked(long packingslipid, User user){
        PackingSlip ps = getPackingSlipById(packingslipid);
        ps.setReady(true);
        ps.setPackedBy(user);
        ps.setPackedDate(LocalDateTime.now());
        ps.setLastModifiedDate(LocalDateTime.now());
        ps.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(ps);
        return ps;
    }

    public PackingSlip setshipped(long packingslipid, User user) throws HorribleException {
        PackingSlip ps = getPackingSlipById(packingslipid);
        if(ps.isReady()){
            ps.setShippedBy(user);
            ps.setShippedDate(LocalDateTime.now());
            ps.setLastModifiedDate(LocalDateTime.now());
            ps.setModifiedBy(user);
        }else{
            throw new HorribleException("package is not ready yet");
        }
        return ps;
    }

    public void deletePackingslip(long packingslipid) {
        PackingSlip packingSlip = getPackingSlipById(packingslipid);
        sessionFactory.getCurrentSession().delete(packingSlip);
    }
}
