package DAL;


import DTO.StockMove.Stockmove;
import Model.FinishedProduct.Product;
import Model.FinishedStock.StockLine;
import Model.Misc.Note;
import Model.RawMaterial.RawMaterial;
import Model.RawStock.MaterialStockLine;
import Model.RawStock.Materialmove;
import Model.Stock.Location;
import Model.Stock.Stock;
import Model.Stock.StockComparison;
import Model.User.User;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 21-4-2015.
 */
@Service
public class StockDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public StockDAO() {
    }

    public Stock getStockById(int stockid) {
        Stock s = (Stock) sessionFactory.getCurrentSession().createQuery("From Stock s where s.id = :id").setParameter("id", stockid).list().get(0);
        return s;
    }

    public Stock createStock(boolean isRaw, String name, int locationid, Note n, User u) {
        Stock s = new Stock();
        s.setRaw(isRaw);
        Query getlocation = sessionFactory.getCurrentSession().createQuery("From Location l WHERE l.id = :id");
        Location loc = (Location) getlocation.setParameter("id", locationid).list().get(0);
        s.setLocation(loc);
        s.setName(name);
        s.setNote(n);
        s.setLastModifiedDate(LocalDateTime.now());
        s.setModifiedBy(u);
        s.setCreatedBy(u);
        s.setCreationDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().save(s);
        return s;
    }

    public Stock editStock(int stockid, String name, Note n, User u) {
        Stock s = getStockById(stockid);
        s.setName(name);
        s.setNote(n);
        s.setLastModifiedDate(LocalDateTime.now());
        s.setModifiedBy(u);
        sessionFactory.getCurrentSession().update(s);
        return s;
    }

    public List<Stock> getAllRawStocks() {
        List<Stock> stocks = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("from Stock s where s.isRaw = true and s.id != 1").list();
        for (Object o : results) {
            Stock t =  (Stock) o;
            Hibernate.initialize(t.getMaterialStockLines());
            stocks.add(t);
        }
        return stocks;
    }

    public List<Stock> getAllFinishedStocks() {
        List<Stock> stocks = new ArrayList<>();

        List<Object> results = sessionFactory.getCurrentSession().createQuery("from Stock s where s.isRaw = false AND s.id !=1").list();

        for (Object o : results) {
            Stock t =  (Stock) o;
            Hibernate.initialize(t.getStockLines());
            stocks.add(t);
        }
        return stocks;
    }

    public StockComparison checkStock(int stockid) {
        Stock stock = (Stock) sessionFactory.getCurrentSession().createQuery("From Stock s where s.id = :id").setParameter("id", stockid).list().get(0);
        StockComparison comp = new StockComparison();
        comp.setStock(stock);
        comp.compare();
        return comp;
    }

    public Stock createMaterialStockLine(int stockid, RawMaterial r){
        MaterialStockLine mm = new MaterialStockLine();
        Stock s = getStockById(stockid);
        mm.setRawMaterial(r);
        mm.setAmount(0);
        mm.setStock(s);
        sessionFactory.getCurrentSession().save(mm);
        s.getMaterialStockLines().add(mm);
        sessionFactory.getCurrentSession().update(s);
        return s;
    }

    public boolean checkIfExists(int stockid, RawMaterial r) {
        return sessionFactory.getCurrentSession().createQuery("From MaterialStockLine ms where ms.rawMaterial.id = :materialid AND ms.stock.id = :stockid").setParameter("materialid" ,r.getId()).setParameter("stockid", stockid).list().size()<=0;
    }

    public StockLine createStockLine(Stock sl, Product product) {
        StockLine st = new StockLine();
        st.setProduct(product);
        st.setAmount(0);
        st.setStock(sl);
        sessionFactory.getCurrentSession().save(st);
        sl.getStockLines().add(st);
        sessionFactory.getCurrentSession().update(sl);
        return st;
    }

    public List<Stock> getStockByRawmaterial(int id) {
        List<Stock> stocks = (List<Stock>) sessionFactory.getCurrentSession().createQuery("SELECT  DISTINCT m.stock From MaterialStockLine m where m.rawMaterial.id = :id").setParameter("id",id).list().stream().map(Stock.class::cast).collect(Collectors.toList());
        for(Stock s : stocks){
            Hibernate.initialize(s.getMaterialStockLines());
            Hibernate.initialize(s.getStockLines());
            Hibernate.initialize(s.getStockmoves());
            for(Model.FinishedStock.Stockmove stockmove : s.getStockmoves()){
                Hibernate.initialize(stockmove.getStockmoveItems());
            }
            Hibernate.initialize(s.getMaterialmoves());
            for(Materialmove m :s.getMaterialmoves()){
                Hibernate.initialize(m.getMaterialmoveItems());
            }
        }
        return stocks;
    }

    public MaterialStockLine getMaterialStockLine(int materialid, int stockid) {
        return (MaterialStockLine) sessionFactory.getCurrentSession().createQuery("from MaterialStockLine m where m.stock.id = :stockid AND m.rawMaterial.id = :materialid").setParameter("stockid", stockid).setParameter("materialid", materialid).list().get(0);
    }

    public StockLine getStockLine(long productid, int stockid) {
        return (StockLine) sessionFactory.getCurrentSession().createQuery("from StockLine m where m.stock.id = :stockid AND m.product.id = :materialid").setParameter("stockid", stockid).setParameter("materialid", productid).list().get(0);
    }

    public MaterialStockLine subtractAmount(int materialStockLineId, int amount) {
        MaterialStockLine materialStockLine = getMaterialStockLineById(materialStockLineId);
        materialStockLine.setAmount(materialStockLine.getAmount()-amount);
        sessionFactory.getCurrentSession().update(materialStockLine);
        return materialStockLine;
    }

    private MaterialStockLine getMaterialStockLineById(int materialStockLineId) {
        return (MaterialStockLine) sessionFactory.getCurrentSession().createQuery("from MaterialStockLine m where m.id = :id").setParameter("id",materialStockLineId).list().get(0);
    }

    public List<StockLine> getStocklineByProduct(long id){
        return (List<StockLine>) sessionFactory.getCurrentSession().createQuery("From StockLine  s where s.product.id = :id").setParameter("id", id).list();
    }

    public List<Stock> getStockByProduct(long id) {
        return (List<Stock>) sessionFactory.getCurrentSession().createQuery("Select s.stock From StockLine s where s.product.id = :id").setParameter("id", id).list();
    }

    public void addAmount(int stockid, int materialid, int amount) {
        MaterialStockLine materialStockLine = getMaterialStockLine(materialid, stockid);
        materialStockLine.setAmount(materialStockLine.getAmount() + amount);
        sessionFactory.getCurrentSession().update(materialStockLine);
    }

    public void addAmount(int stockid, long productid, int amount){
        StockLine stockLine = getStockLine(productid,stockid);
        stockLine.setAmount(stockLine.getAmount() + amount);
        sessionFactory.getCurrentSession().update(stockLine);
    }

    public List<Stock> getFreeStockByProduct(long productid) {
        List<Stock> stocks = getAllFinishedStocks();
        return stocks.stream().filter(s->!s.getStockLines().stream().anyMatch(m->m.getProduct().getId()==productid)).collect(Collectors.toList());
    }

    public List<Stock> getFreeStockByMaterial(int materialid) {
        List<Stock> stocks = getAllRawStocks();
        return stocks.stream().filter(s->!s.getMaterialStockLines().stream().anyMatch(m->m.getRawMaterial().getId()==materialid)).collect(Collectors.toList());
    }

    public StockLine createStockLine2(Stock stock, int amount, Product product) {
        StockLine st = new StockLine();
        st.setProduct(product);
        st.setAmount(amount);
        st.setStock(stock);
        sessionFactory.getCurrentSession().save(st);
        stock.getStockLines().add(st);
        sessionFactory.getCurrentSession().update(stock);
        return st;
    }

    public List<Stock> getRawStockByProduct(int materialid) {
        return (List<Stock>) sessionFactory.getCurrentSession().createQuery("Select distinct l.stock from MaterialStockLine l where l.rawMaterial.id = :materialid").setParameter("materialid", materialid).list();
    }
}
