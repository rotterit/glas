package DAL;

import Model.Invoicing.Customer;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 14-5-2015.
 */
@Service
public class CustomerDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Customer getCustomerById(int customerid) {
        return (Customer) sessionFactory.getCurrentSession().createQuery("From Customer c where c.id = :id").setParameter("id", customerid).list().get(0);
    }

    public List<Customer> getCustomers() {
        List<Customer> categories = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Customer ").list();
        for (Object o : results) {
            categories.add((Customer) o);
        }
        return categories;
    }

    public Customer createCustomer(User user, String name, String description, double baseVAT, double baseDiscountPercentage, double baseLineDiscountPercentage, double basePackingCostPercentage, String companyName, String adress, String ZIP, String city, String country, String TEL, String email, String VATnumber) {
        Customer c = new Customer(LocalDateTime.now(), user, LocalDateTime.now(), user, name, description, baseVAT, baseDiscountPercentage, baseLineDiscountPercentage, basePackingCostPercentage, companyName, adress, ZIP, city, country, TEL, email, VATnumber);
        sessionFactory.getCurrentSession().save(c);
        return c;
    }

    public Customer editCustomer(int id, User user, String name, String description, double baseVAT, double baseDiscountPercentage, double baseLineDiscountPercentage, double basePackingCostPercentage, String companyName, String adress, String ZIP, String city, String country, String TEL, String email, String VATnumber){
        Customer c = getCustomerById(id);
        c.edit(LocalDateTime.now(), user, LocalDateTime.now(), user, name, description, baseVAT, baseDiscountPercentage, baseLineDiscountPercentage, basePackingCostPercentage, companyName, adress, ZIP, city, country, TEL, email, VATnumber);
        sessionFactory.getCurrentSession().update(c);
        return c;
    }

    public List<Customer> getCustomersByName(String searchterm) {
        return sessionFactory.getCurrentSession().createQuery("From Customer c where c.name like :name").setParameter("name", "%" + searchterm + "%").list();
    }
}
