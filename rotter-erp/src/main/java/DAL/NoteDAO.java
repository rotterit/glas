package DAL;

import Model.Misc.Note;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 6-5-2015.
 */
@Service
public class NoteDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Note getNoteById(int id){
        Note l = (Note) sessionFactory.getCurrentSession().createQuery("From Note l where l.id = :id").setParameter("id",id).list().get(0);
        return l;
    }

    public List<Note> getNotes(){
        List<Note> Notes = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Note ").list();
        for(Object o: results){
            Notes.add((Note) o);
        }
        return Notes;
    }

    public Note createNote(String content){
        Note temp = new Note();
        //set values
        temp.setContent(content);

        sessionFactory.getCurrentSession().save(temp);

        return temp;
    }

    public Note editNote(int Noteid,String content){
        Note temp = getNoteById(Noteid);
        //set values
        temp.setContent(content);

        sessionFactory.getCurrentSession().update(temp);

        return temp;
    }
}
