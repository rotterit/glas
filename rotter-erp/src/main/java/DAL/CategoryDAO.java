package DAL;

import Model.FinishedProduct.Category;
import Model.FinishedProduct.CategorySize;
import Model.FinishedProduct.MySize;
import Model.Misc.Note;
import Model.Stock.Stock;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 29-4-2015.
 */
@Service
public class CategoryDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public CategoryDAO(){

    }

    public Category getCategoryById(int id){
        Category category = (Category) sessionFactory.getCurrentSession().createQuery("From Category c where c.id = :id").setParameter("id",id).list().get(0);
        return category;
    }

    public List<Category> getCategories(){
        List<Category> categories = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Category ").list();
        for(Object o: results){
            Category c = (Category) o;
            Hibernate.initialize(c.getCategorySizes());
            categories.add(c);
        }
        return categories;
    }

    public Category createCategory(int basemins, String name, Note note, User u){
        Category c = new Category();
        c.setCreationDate(LocalDateTime.now());
        c.setBaseTime(basemins);
        c.setName(name);
        c.setCreatedBy(u);
        c.setModifiedBy(u);
        c.setLastModifiedDate(LocalDateTime.now());
        c.setCreationDate(LocalDateTime.now());
        List<CategorySize> cslist = new ArrayList<>();
        c.setNote(note);
        sessionFactory.getCurrentSession().save(c);
        return c;
    }

    public Category editCategory(int categoryid,int basemins, String description, Note note, User u){
        Category c = getCategoryById(categoryid);
        c.setBaseTime(basemins);
        c.setName(description);
        c.setModifiedBy(u);
        c.setLastModifiedDate(LocalDateTime.now());
        c.setNote(note);
        sessionFactory.getCurrentSession().update(c);
        return c;
    }

    public CategorySize createCategorySize(double basePrice, int maxtimemins, int mintimemins, int sizeid, Category c,User u) throws HorribleException {
        if(sessionFactory.getCurrentSession().createQuery("From CategorySize c where c.mySize.id = :id AND ((c.minTime< :mintimemins AND :mintimemins <c.maxTime)OR(c.minTime < :maxtimemins AND :maxtimemins < c.maxTime)OR(:maxtimemins = c.maxTime)OR(:mintimemins= c.minTime)OR(:maxtimemins> c.maxTime AND :mintimemins <c.minTime))").setParameter("id", sizeid).setParameter("mintimemins", mintimemins).setParameter("maxtimemins", maxtimemins).list().size()<=0) {
            CategorySize cs = new CategorySize();
            cs.setMaxTime(maxtimemins);
            cs.setMinTime(mintimemins);
            cs.setCreationDate(LocalDateTime.now());
            cs.setLastModifiedDate(LocalDateTime.now());
            cs.setCreatedBy(u);
            cs.setModifiedBy(u);
            cs.setCategory(c);
            cs.setBasePrice(basePrice);
            cs.setMySize((MySize) sessionFactory.getCurrentSession().createQuery("From MySize s where s.id = :id").setParameter("id", sizeid).list().get(0));
            sessionFactory.getCurrentSession().save(cs);
            return cs;
        }else{
            throw new HorribleException("de tijden overlappen");
        }
    }

    public CategorySize editCategorySize(int csid, int maxtimemins,int mintimemins, User u){
        CategorySize cs = getCategorySizeById(csid);
        cs.setMaxTime(maxtimemins);
        cs.setMinTime( mintimemins);
        cs.setLastModifiedDate(LocalDateTime.now());
        cs.setModifiedBy(u);
        sessionFactory.getCurrentSession().save(cs);
        return cs;
    }

    private CategorySize getCategorySizeById(int csid) {
        CategorySize cs = (CategorySize) sessionFactory.getCurrentSession().createQuery("From CategorySize cs Where cs.id = :id").setParameter("id", csid).list().get(0);
        return cs;
    }

    private List<CategorySize> getCategorySizeByCategory(int categoryid){
        Category c = (Category) sessionFactory.getCurrentSession().createQuery("From Category c where c.id = :id").setParameter("id",categoryid).list().get(0);
        return c.getCategorySizes();
    }

    private List<CategorySize> getCategorySizeBySize(int sizeid){
        List<CategorySize> cslist = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From CategorySize cs where cs.mySize.id = :id").setParameter("id",sizeid).list();
        for(Object o : results){
            cslist.add((CategorySize) o);
        }
        return cslist;
    }

    private Category AdviseCategory(int sizeid, int basemins) throws HorribleException {
        List<CategorySize> cslist = getCategorySizeBySize(sizeid);
        for(CategorySize cs : cslist){
            if(cs.getMinTime()<=basemins && basemins<=cs.getMaxTime()){
                return cs.getCategory();
            }
        }
        throw new HorribleException("No suitable category found");
    }

    public Category BindCategorySizes(int Categoryid, List<CategorySize> categorySizes, User u){
        Category c = getCategoryById(Categoryid);
        c.setCategorySizes(categorySizes);
        c.setModifiedBy(u);
        c.setLastModifiedDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().update(c);
        return c;
    }
}
