package DAL;

import Model.FinishedProduct.ProductType;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 8-5-2015.
 */
@Service
public class ProductTypeDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public ProductType getProductTypeById(int id) {
        ProductType l = (ProductType) sessionFactory.getCurrentSession().createQuery("From ProductType l where l.id = :id").setParameter("id", id).list().get(0);
        return l;
    }

    public List<ProductType> getProductTypes() {
        List<ProductType> ProductTypes = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From ProductType ").list();
        for (Object o : results) {
            ProductTypes.add((ProductType) o);
        }
        return ProductTypes;
    }

    public ProductType createProductType(String producttypecode, String name, String description, User u) throws HorribleException {
        if (sessionFactory.getCurrentSession().createQuery("From ProductType c where c.code = :code").setParameter("code", producttypecode).list().size() <= 0) {
            ProductType temp = new ProductType();
            //set values
            temp.setCreationDate(LocalDateTime.now());
            temp.setCode(producttypecode);
            temp.setName(name);
            temp.setDescription(description);
            temp.setLastModifiedDate(LocalDateTime.now());
            temp.setModifiedBy(u);
            temp.setCreatedBy(u);
            temp.setCreationDate(LocalDateTime.now());
            sessionFactory.getCurrentSession().save(temp);
            return temp;
        } else {
            throw new HorribleException("the code already exists");
        }
    }

    public ProductType editProductType(int ProductTypeid, String producttypecode, String name, String description, User u) throws HorribleException {
        if (sessionFactory.getCurrentSession().createQuery("From ProductType c where c.code = :code AND c.id != :id").setParameter("code", producttypecode).setParameter("id", ProductTypeid).list().size() <= 0) {
            ProductType temp = getProductTypeById(ProductTypeid);
            //set values
            temp.setCode(producttypecode);
            temp.setName(name);
            temp.setDescription(description);
            temp.setLastModifiedDate(LocalDateTime.now());
            temp.setModifiedBy(u);
            sessionFactory.getCurrentSession().update(temp);
            return temp;
        } else {
            throw new HorribleException("the code already exists");
        }
    }
}
