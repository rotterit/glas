package DAL;

import Model.FinishedProduct.MySize;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 6-5-2015.
 */
@Service
public class SizeDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public MySize getSizeById(int id) {

        MySize l = (MySize) sessionFactory.getCurrentSession().createQuery("From MySize l where l.id = :id").setParameter("id", id).list().get(0);

        return l;
    }

    public List<MySize> getSizes() {
        List<MySize> mySizes = new ArrayList<>();

        List<Object> results = sessionFactory.getCurrentSession().createQuery("From MySize ").list();

        for (Object o : results) {
            mySizes.add((MySize) o);
        }
        return mySizes;
    }

    public MySize createSize(String name, String code, User u) throws HorribleException {
        if (sessionFactory.getCurrentSession().createQuery("From MySize s where s.Code = :code").setParameter("code", code).list().size() <= 0) {
            MySize temp = new MySize();
            temp.setCreationDate(LocalDateTime.now());
            temp.setName(name);
            temp.setCode(code);
            temp.setLastModifiedDate(LocalDateTime.now());
            temp.setModifiedBy(u);
            temp.setCreatedBy(u);
            temp.setCreationDate(LocalDateTime.now());
            sessionFactory.getCurrentSession().save(temp);

            return temp;
        } else {
            throw new HorribleException("Code is al bezet");
        }

    }

    public MySize editSize(int Sizeid, String name, String code, User u) throws HorribleException {
        if (sessionFactory.getCurrentSession().createQuery("From MySize s where s.Code = :code AND s.id != :id").setParameter("code", code).setParameter("id", Sizeid).list().size() <= 0) {
            MySize temp = getSizeById(Sizeid);
            //set values
            temp.setName(name);
            temp.setCode(code);
            temp.setLastModifiedDate(LocalDateTime.now());
            temp.setModifiedBy(u);
            sessionFactory.getCurrentSession().update(temp);
            return temp;
        } else {
            throw new HorribleException("Code is al bezet");
        }
    }

}
