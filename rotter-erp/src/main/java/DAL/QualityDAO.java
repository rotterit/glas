package DAL;

import Model.RawMaterial.Quality;

import Model.User.User;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 6-5-2015.
 */
@Service
public class QualityDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Quality getQualityById(int id){
        Quality l = (Quality) sessionFactory.getCurrentSession().createQuery("From Quality l where l.id = :id").setParameter("id",id).list().get(0);
        return l;
    }

    public List<Quality> getQualitys(){
        List<Quality> Qualitys = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Quality ").list();
        for(Object o: results){
            Qualitys.add((Quality) o);
        }
        return Qualitys;
    }

    public Quality createQuality(String name, User u){
        Quality temp = new Quality();
        //set values
        temp.setName(name);
        temp.setLastModifiedDate(LocalDateTime.now());
        temp.setModifiedBy(u);
        temp.setCreatedBy(u);
        temp.setCreationDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().save(temp);
        return temp;
    }

    public Quality editQuality(int Qualityid, String name, User u){
        Quality temp = getQualityById(Qualityid);
        //set values
        temp.setName(name);
        temp.setLastModifiedDate(LocalDateTime.now());
        temp.setModifiedBy(u);
        sessionFactory.getCurrentSession().update(temp);
        return temp;
    }

    public List<Quality> getSearchQualitys() {
        List<Quality> Qualitys = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Quality q ").list();
        for(Object o: results){
            Qualitys.add((Quality) o);
        }
        return Qualitys;
    }
}
