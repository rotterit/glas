package DAL;

import Model.FinishedProduct.Product;
import Model.Stock.Stock;
import Model.FinishedStock.StockLine;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;

import Model.User.User;
import Util.HorribleException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by thijs on 23-4-2015.
 */
@Service
public class StockmoveDAO {

    private Logger logger = LoggerFactory.getLogger(StockmoveDAO.class);

    @Autowired
    private SessionFactory sessionFactory;

    public StockmoveDAO() {

    }

    public Stockmove getStockmoveById(long stockmoveid){
        Stockmove s = (Stockmove) sessionFactory.getCurrentSession().createQuery("From Stockmove sm where sm.id = :id").setParameter("id",stockmoveid).list().get(0);
        return s;
    }

    public List<Stockmove> getStockmoves(){
        List<Stockmove> stockmoves = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Stockmove ").list();
        for(Object o: results){
            stockmoves.add((Stockmove) o);
        }
        return stockmoves;
    }

    public StockmoveItem getStockMoveItemById(long id){
        StockmoveItem stockmoveItem = (StockmoveItem) sessionFactory.getCurrentSession().createQuery("From StockmoveItem i where i.id = :id ").setParameter("id",id).list().get(0);
        return stockmoveItem;
    }

    public Stockmove createStockmove(boolean isOut, boolean completed, boolean isreal, List<StockmoveItem> stockmoveitems, User user, boolean isPackingslip, User VOID){
        Stockmove s = new Stockmove();
        s.setCreationDate(LocalDateTime.now());
        s.setLastModifiedDate(LocalDateTime.now());
        s.setCreatedBy(user);
        s.setModifiedBy(user);
        s.setCompletedBy(VOID);
        s.setCompletionDate(null);
        s.setPackingslip(isPackingslip);
        s.setOut(isOut);
        s.setReal(isreal);
        s.setComplete(completed);
        s.setCreationDate(LocalDateTime.now());
        Query getStock = sessionFactory.getCurrentSession().createQuery("From Stock s where s.id = :id");
        s.setStockmoveItems(stockmoveitems);
        sessionFactory.getCurrentSession().save(s);
        return s;
    }

    public Stockmove createStockmove2(boolean isOut, boolean completed, boolean isreal, List<Long> stockmoveitems, User user, boolean isPackingslip, User VOID){
        Stockmove s = new Stockmove();
        s.setCreationDate(LocalDateTime.now());
        s.setLastModifiedDate(LocalDateTime.now());
        s.setCreatedBy(user);
        s.setModifiedBy(user);
        s.setCompletedBy(VOID);
        s.setCompletionDate(null);
        s.setPackingslip(isPackingslip);
        s.setOut(isOut);
        s.setReal(isreal);
        s.setComplete(completed);
        s.setCreationDate(LocalDateTime.now());
        List<StockmoveItem> stockmoveitemList = new ArrayList<>();
        for(Long stockmoveItem: stockmoveitems){
            stockmoveitemList.add((StockmoveItem) sessionFactory.getCurrentSession().createQuery("From StockmoveItem  s where s.id =:id").setParameter("id", stockmoveItem).list().get(0));
        }
        logger.warn("Stockmoveitemssize: " + stockmoveitemList.size());
        s.setStockmoveItems(stockmoveitemList);
        sessionFactory.getCurrentSession().save(s);
        return s;
    }

    public List<Stockmove> splitByStock(Stockmove s, User user, User VOID){
        logger.warn("started splitting");
        Map<Long, Integer> reservedNow = new HashMap<>();
        List<StockmoveItem> neededstockmoveItems = new ArrayList<>();
        List<StockmoveItem> completestockmoveItems = new ArrayList<>();
        int counter =1;
        for(StockmoveItem stockmoveItem : s.getStockmoveItems()){
            logger.warn("line: " + counter);
            List<StockLine> stockmoveItems = sessionFactory.getCurrentSession().createQuery("From StockLine s where s.product.id = :productid").setParameter("productid", stockmoveItem.getProduct().getId()).list();
            int amount = stockmoveItems.stream().mapToInt(t->t.getAmount()).sum();
            logger.warn("amount: " + amount);

            int reservedAmount = 0;
            if(reservedNow.containsKey(stockmoveItem.getProduct().getId())) {
                reservedAmount = reservedNow.get(stockmoveItem.getProduct().getId());
            }
            int stockDifference = stockmoveItem.getAmount() - (amount- (getReservedStock(stockmoveItem.getProduct().getId())+reservedAmount));
            logger.warn("stockdifference: " + stockDifference);
            if(stockDifference<=0){
                StockmoveItem stockmoveItem1 = createStockmoveItem(stockmoveItem.getProduct(), stockmoveItem.getAmount(), stockmoveItem.getStock());
                if(reservedNow.containsKey(stockmoveItem.getProduct().getId())) {
                    reservedNow.put(stockmoveItem.getProduct().getId(), reservedAmount + stockmoveItem.getAmount());
                }else{
                    reservedNow.put(stockmoveItem.getProduct().getId(), stockmoveItem.getAmount());
                }
                logger.warn("added only to complete");
                completestockmoveItems.add(stockmoveItem1);
            }
            if(stockDifference>0&&amount!=0){
                StockmoveItem stockmoveItem1 = createStockmoveItem(stockmoveItem.getProduct(), (amount- (getReservedStock(stockmoveItem.getProduct().getId())+reservedAmount)), stockmoveItem.getStock());
                if(reservedNow.containsKey(stockmoveItem.getProduct().getId())) {
                    reservedNow.put(stockmoveItem.getProduct().getId(), (amount - getReservedStock(stockmoveItem.getProduct().getId())));
                }else{
                    reservedNow.put(stockmoveItem.getProduct().getId(), amount- (getReservedStock(stockmoveItem.getProduct().getId())));
                }
                StockmoveItem stockmoveItem2 = createStockmoveItem(stockmoveItem.getProduct(), stockDifference, stockmoveItem.getStock());
                completestockmoveItems.add(stockmoveItem1);
                neededstockmoveItems.add(stockmoveItem2);
                logger.warn("added to both");
            }
            if(amount==0){
                if(reservedNow.containsKey(stockmoveItem.getProduct().getId())) {
                    reservedNow.put(stockmoveItem.getProduct().getId(), (amount - getReservedStock(stockmoveItem.getProduct().getId())));
                }else{
                    reservedNow.put(stockmoveItem.getProduct().getId(), amount- (getReservedStock(stockmoveItem.getProduct().getId())));
                }
                StockmoveItem stockmoveItem2 = createStockmoveItem(stockmoveItem.getProduct(), stockDifference, stockmoveItem.getStock());
                neededstockmoveItems.add(stockmoveItem2);
                logger.warn("added to neede");
            }
        }
        Stockmove needed = createStockmove(true,false,false,neededstockmoveItems, user, false, VOID);
        Stockmove complete = createStockmove(true,false,false,completestockmoveItems, user, false, VOID);
        List<Stockmove> stockmoves = new ArrayList<>();
        stockmoves.add(needed);
        stockmoves.add(complete);
        logger.warn("it has been done");
        logger.warn("needed: " + needed.getStockmoveItems().size());
        logger.warn("completed: " + complete.getStockmoveItems().size());
        logger.warn("order: " + s.getStockmoveItems().size());
        return stockmoves;
    }

    public int getReservedStock(long id) {
        List<StockmoveItem> stockmoveItems = sessionFactory.getCurrentSession().createQuery("Select i.completed.stockmoveItems From Invoice i where i.sent = false").list();
        stockmoveItems.addAll(sessionFactory.getCurrentSession().createQuery("Select c.outStockmove.stockmoveItems From Commission c where c.isReservation = true AND c.completed = false").list());
        return stockmoveItems.stream().filter(s->s.getProduct().getId() == id ).mapToInt(s -> s.getAmount()).sum();
    }

    public StockmoveItem createStockmoveItem(Product product, int amount, Stock stock){
        StockmoveItem stockmoveItem = new StockmoveItem();
        stockmoveItem.setProduct(product);
        stockmoveItem.setStock(stock);
        stockmoveItem.setAmount(amount);
        sessionFactory.getCurrentSession().save(stockmoveItem);
        return stockmoveItem;
    }

    public Stockmove completeOrderMove(long stockmoveid, User user){
        Stockmove s = getStockmoveById(stockmoveid);
        s.setComplete(true);
        s.setCompletedBy(user);
        s.setCompletionDate(LocalDateTime.now());
        for(StockmoveItem si : s.getStockmoveItems()){
            StockLine sl = (StockLine) sessionFactory.getCurrentSession().createQuery("From StockLine s where s.product.id = :id").setParameter("id", si.getProduct().getId()).list().get(0);
            sl.setAmount(sl.getAmount()-si.getAmount());
            sessionFactory.getCurrentSession().update(sl);
        }
        sessionFactory.getCurrentSession().update(s);
        return s;
    }

    public Stockmove completeProductionStockmove(long stockmoveid, User user){
        Stockmove s = getStockmoveById(stockmoveid);
        s.setComplete(true);
        s.setCompletedBy(user);
        s.setCompletionDate(LocalDateTime.now());
        for(StockmoveItem si : s.getStockmoveItems()){
            StockLine sl = (StockLine) sessionFactory.getCurrentSession().createQuery("From StockLine s where s.product.id = :id").setParameter("id", si.getProduct().getId()).list().get(0);
            sl.setAmount(sl.getAmount()+si.getAmount());
            sessionFactory.getCurrentSession().update(sl);
        }
        sessionFactory.getCurrentSession().update(s);
        return s;
    }

    public Stockmove completeStockCorrection(long id, User user) {
        Stockmove s = getStockmoveById(id);
        s.setComplete(true);
        s.setCompletedBy(user);
        s.setCompletionDate(LocalDateTime.now());
        for(StockmoveItem si : s.getStockmoveItems()){
            StockLine sl = (StockLine) sessionFactory.getCurrentSession().createQuery("From StockLine s where s.product.id = :id").setParameter("id", si.getProduct().getId()).list().get(0);
            sl.setAmount(sl.getAmount()+si.getAmount());
            sessionFactory.getCurrentSession().update(sl);
        }
        sessionFactory.getCurrentSession().update(s);
        return s;
    }

    public Stockmove cancel(long id, User user) {
        Stockmove stockmove = getStockmoveById(id);
        stockmove.setComplete(true);
        stockmove.setCompletedBy(user);
        stockmove.setCompletionDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().update(stockmove);
        return stockmove;
    }

    public StockmoveItem editStockMoveItem(long id, int amount) {
        StockmoveItem stockmoveItem = getStockMoveItemById(id);
        stockmoveItem.setAmount(amount);
        sessionFactory.getCurrentSession().update(stockmoveItem);
        return stockmoveItem;
    }

    public Stockmove editStockmove(long id, List<StockmoveItem> stockmoveItems, User user) {
        Stockmove stockmove = getStockmoveById(id);
        stockmove.setStockmoveItems(stockmoveItems);
        stockmove.setLastModifiedDate(LocalDateTime.now());
        stockmove.setModifiedBy(user);
        sessionFactory.getCurrentSession().update(stockmove);
        return stockmove;
    }
}
