package DAL.Util;

import DTO.Invoice.Invoice;
import Model.FinishedStock.StockCorrection;
import Model.Invoicing.Worksheet;

import java.util.List;

/**
 * Created by thijs on 26-5-2015.
 */
public class InvoiceAndStockCorrections {

    private List<Long> stockmoveitemids;

    private Worksheet worksheet;

    public List<Long> getStockmoveitemids() {
        return stockmoveitemids;
    }

    public void setStockmoveitemids(List<Long> stockmoveitemids) {
        this.stockmoveitemids = stockmoveitemids;
    }

    public Worksheet getWorksheet() {
        return worksheet;
    }

    public void setWorksheet(Worksheet worksheet) {
        this.worksheet = worksheet;
    }
}
