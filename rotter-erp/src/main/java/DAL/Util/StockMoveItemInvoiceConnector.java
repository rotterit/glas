package DAL.Util;

/**
 * Created by thijs on 4-6-2015.
 */
public class StockMoveItemInvoiceConnector {

    private long stockMoveItemId;

    private long invoiceId;

    public StockMoveItemInvoiceConnector(long stockMoveItemId, long invoiceId) {
        this.stockMoveItemId = stockMoveItemId;
        this.invoiceId = invoiceId;
    }

    public long getStockMoveItemId() {
        return stockMoveItemId;
    }

    public long getInvoiceId() {
        return invoiceId;
    }
}
