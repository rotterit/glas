package DAL.Util;

/**
 * Created by thijs on 27-5-2015.
 */
public class CommissionToInvoice {

    private long productId;

    private int amount;

    private int stockid;

    public CommissionToInvoice(long productId, int amount, int stockid) {
        this.productId = productId;
        this.amount = amount;
        this.stockid = stockid;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getStockid() {
        return stockid;
    }

    public void setStockid(int stockid) {
        this.stockid = stockid;
    }
}
