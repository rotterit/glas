package DAL;

import Model.FinishedProduct.Design;
import Model.User.User;
import Util.HorribleException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 6-5-2015.
 */
@Service
public class DesignDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public Design getDesignById(int id) {
        Design l = (Design) sessionFactory.getCurrentSession().createQuery("From Design l where l.id = :id").setParameter("id", id).list().get(0);
        return l;
    }

    public List<Design> getDesigns() {
        List<Design> Designs = new ArrayList<>();
        List<Object> results = sessionFactory.getCurrentSession().createQuery("From Design ").list();
        for (Object o : results) {
            Designs.add((Design) o);
        }
        return Designs;
    }

    public Design createDesign(String designcode, String description, String name, User u) throws HorribleException {
        if (sessionFactory.getCurrentSession().createQuery("From Design c where c.designCode = :code").setParameter("code", designcode).list().size() <= 0) {
            Design temp = new Design();
            //set values
            temp.setCreationDate(LocalDateTime.now());
            temp.setDesignCode(designcode);
            temp.setDescription(description);
            temp.setName(name);
            temp.setLastModifiedDate(LocalDateTime.now());
            temp.setCreatedBy(u);
            temp.setModifiedBy(u);
            temp.setCreationDate(LocalDateTime.now());
            sessionFactory.getCurrentSession().save(temp);
            return temp;
        } else {
            throw new HorribleException("the code already exists");
        }
    }

    public Design editDesign(int Designid, String designcode, String description, String name, User u) throws HorribleException {
        if (sessionFactory.getCurrentSession().createQuery("From Design c where c.designCode = :code AND c.id != :id").setParameter("code", designcode).setParameter("id", Designid).list().size() <= 0) {
            Design temp = getDesignById(Designid);
            //set values
            temp.setDesignCode(designcode);
            temp.setDescription(description);
            temp.setName(name);
            temp.setLastModifiedDate(LocalDateTime.now());
            temp.setModifiedBy(u);
            sessionFactory.getCurrentSession().update(temp);
            return temp;
        } else {
            throw new HorribleException("the code already exists");
        }
    }

}
