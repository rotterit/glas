package DAL;


import Model.Invoicing.Worksheet;
import Model.Misc.Note;
import Model.RawMaterial.RawMaterial;
import Model.RawMaterial.Supplier;
import Model.RawStock.MaterialCorrection;
import Model.RawStock.MaterialStockLine;
import Model.RawStock.Materialmove;
import Model.RawStock.MaterialmoveItem;
import Model.Stock.Stock;
import Model.User.User;
import Util.HorribleException;
import javafx.geometry.HPos;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 29-4-2015.
 */
@Service
public class MaterialmoveDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public MaterialmoveDAO() {

    }

    public Materialmove getMaterialmoveById(long materialmoveid){
        Materialmove mm = (Materialmove) sessionFactory.getCurrentSession().createQuery("From Materialmove mm where mm.id = :id").setParameter("id",materialmoveid).list().get(0);
        Hibernate.initialize(mm.getMaterialmoveItems());
        return mm;
    }

    public MaterialmoveItem getMaterialmoveItemById(long id){
        return (MaterialmoveItem) sessionFactory.getCurrentSession().createQuery("From MaterialmoveItem m where m.id = :id").setParameter("id",id).list().get(0);
    }

    public Materialmove createOrder(User voiduser, Supplier s, User u, Note n){
        Materialmove m = new Materialmove();
        m.setDelivery(true);
        m.setComplete(false);
        m.setCompletedBy(voiduser);
        m.setConfirmed(false);
        m.setConfirmedBy(voiduser);
        m.setCreationDate(LocalDateTime.now());
        m.setDelivered(false);
        m.setDeliveryDate(null);
        m.setDeliveryRecievedBy(voiduser);
        m.setSupplier(s);
        m.setMaterialmoveItems(new ArrayList<>());
        m.setCreatedBy(u);
        m.setModifiedBy(u);
        m.setLastModifiedDate(LocalDateTime.now());
        m.setNote(n);
        sessionFactory.getCurrentSession().save(m);
        return m;
    }

    public Materialmove createMaterialMove(User voiduser, Supplier s, User u, Note n){
        Materialmove m = new Materialmove();
        m.setDelivery(false);
        m.setComplete(false);
        m.setCompletedBy(voiduser);
        m.setConfirmed(false);
        m.setConfirmedBy(voiduser);
        m.setCreationDate(LocalDateTime.now());
        m.setDelivered(false);
        m.setDeliveryDate(null);
        m.setDeliveryRecievedBy(voiduser);
        m.setSupplier(s);
        m.setMaterialmoveItems(new ArrayList<>());
        m.setCreatedBy(u);
        m.setModifiedBy(u);
        m.setLastModifiedDate(LocalDateTime.now());
        m.setNote(n);
        sessionFactory.getCurrentSession().save(m);
        return m;
    }

    public Materialmove completeMaterialMove(long materialmoveid, User u){
        Materialmove m = getMaterialmoveById(materialmoveid);
        m.setComplete(true);
        m.setCompletedBy(u);
        sessionFactory.getCurrentSession().update(m);
        return m;
    }

    public Materialmove bindMoveItemsToOrder(long materialmoveid, List<MaterialmoveItem> items){
        Materialmove mm = getMaterialmoveById(materialmoveid);
        mm.setMaterialmoveItems(items);
        sessionFactory.getCurrentSession().update(mm);
        return mm;
    }

    public MaterialCorrection getById(long materialcorrectionid){
        return (MaterialCorrection) sessionFactory.getCurrentSession().createQuery("From MaterialCorrection m where m.id = :id").setParameter("id",materialcorrectionid).list().get(0);
    }

    public MaterialCorrection createStockCorrection(String reason, Materialmove materialmove,User u, Note n ){
        MaterialCorrection m = new MaterialCorrection();
        m.setReason(reason);
        m.setCreatedBy(u);
        m.setModifiedBy(u);
        m.setLastModifiedDate(LocalDateTime.now());
        m.setCreationDate(LocalDateTime.now());
        m.setNote(n);
        Materialmove mm = materialmove;
        m.setMaterialmove(mm);
        sessionFactory.getCurrentSession().save(mm);
        sessionFactory.getCurrentSession().save(m);
        return m;
    }

//    public MaterialCorrection bindMoveItems(long materialcorrectionid, List<MaterialmoveItem> items){
//        MaterialCorrection m = getById(materialcorrectionid);
//        Materialmove mm = m.getMaterialmove();
//        mm.setMaterialmoveItems(items);
//        sessionFactory.getCurrentSession().update(mm);
//        sessionFactory.getCurrentSession().update(m);
//        return m;
//    }

    public MaterialmoveItem createMaterialMoveItem(RawMaterial r, int amount, Stock s){
        MaterialmoveItem m = new MaterialmoveItem();
        m.setRawMaterial(r);
        m.setAmount(amount);
        m.setStock(s);
        sessionFactory.getCurrentSession().save(m);
        return m;
    }

//    public MaterialCorrection completeMaterialCorrection(long materialcorrectionid){
//        MaterialCorrection m = getById(materialcorrectionid);
//        for(MaterialmoveItem mm : m.getMaterialmove().getMaterialmoveItems()){
//            MaterialStockLine ms = (MaterialStockLine) sessionFactory.getCurrentSession().createQuery("From MaterialStockLine ms where ms.rawMaterial.id = :materialid AND ms.stock.id = :stockid").setParameter("materialid", mm.getRawMaterial().getId()).setParameter("stockid",mm.getStock().getId()).list().get(0);
//            ms.setAmount(ms.getAmount()+ mm.getAmount());
//            sessionFactory.getCurrentSession().update(ms);
//        }
//        Materialmove mg = m.getMaterialmove();
//        mg.setComplete(true);
//        sessionFactory.getCurrentSession().update(mg);
//        return m;
//    }

    public MaterialmoveItem editMaterialMoveItem(long id, int amount, Stock s ){
        MaterialmoveItem m = getMaterialmoveItemById(id);
        m.setAmount(amount);
        m.setStock(s);
        sessionFactory.getCurrentSession().update(m);
        return m;
    }

    public Materialmove confirmOrder(long id, User u, long deliverymillis) throws HorribleException {
        Materialmove mm = getMaterialmoveById(id);
        if(!mm.isConfirmed()) {
            mm.setConfirmed(true);
            mm.setConfirmedBy(u);
            mm.setDeliveryDate(LocalDateTime.ofInstant(Instant.ofEpochMilli(deliverymillis), ZoneId.of("Europe/Berlin")));
            mm.setModifiedBy(u);
            mm.setLastModifiedDate(LocalDateTime.now());
            sessionFactory.getCurrentSession().update(mm);
            return mm;
        }else{
            throw new HorribleException("the order is already confirmed");
        }
    }

    public Materialmove confirmDeliveryOrder(long id, User u) throws HorribleException {
        Materialmove mm = getMaterialmoveById(id);
        if(!mm.isConfirmed()){
            throw new HorribleException("The order is not confirmed yet");
        }
        mm.setDelivered(true);
        mm.setDeliveryDate(LocalDateTime.now());
        mm.setDeliveryRecievedBy(u);
        mm.setModifiedBy(u);
        mm.setLastModifiedDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().update(mm);
        return mm;
    }

    public Materialmove completeOrder(long id, User u) throws HorribleException {
        Materialmove mm = getMaterialmoveById(id);
        if(!mm.isDelivered()){
            throw new HorribleException("the order isn't delivered yet");
        }
        for(MaterialmoveItem mi : mm.getMaterialmoveItems()){
            if(mi.getRawMaterial().getQuality().getId()==1){
                throw new HorribleException("Not all the materials have been checked");
            }
        }
        mm.setCompletedBy(u);
        mm.setComplete(true);
        for(MaterialmoveItem mi : mm.getMaterialmoveItems()){
            MaterialStockLine ms = (MaterialStockLine) sessionFactory.getCurrentSession().createQuery("From MaterialStockLine m where m.rawMaterial.id = :id AND m.stock.id = :stockid").setParameter("id", mi.getRawMaterial().getId()).setParameter("stockid",mi.getStock().getId()).list().get(0);
            ms.setAmount(ms.getAmount()+ mi.getAmount());
            sessionFactory.getCurrentSession().update(ms);
        }
        mm.setModifiedBy(u);
        mm.setLastModifiedDate(LocalDateTime.now());
        sessionFactory.getCurrentSession().update(mm);
        return mm;
    }

    public Materialmove cancelOrder(long id){
        Materialmove mm = getMaterialmoveById(id);
        for(MaterialmoveItem m : mm.getMaterialmoveItems()){
            sessionFactory.getCurrentSession().delete(m);
        }
        sessionFactory.getCurrentSession().delete(mm);
        return mm;
    }

    public List<Materialmove> getNotConfirmedOrders(){
        List<Object> os = sessionFactory.getCurrentSession().createQuery("From Materialmove m where m.isDelivery = true AND m.confirmed = false").list();
        List<Materialmove> returnlist = new ArrayList<>();
        for(Object o : os){
            Materialmove m = (Materialmove) o;
            Hibernate.initialize(m.getMaterialmoveItems());
            returnlist.add(m);
        }
        return returnlist;
    }

    public List<Materialmove> getConfirmedOrders(){
        List<Object> os = sessionFactory.getCurrentSession().createQuery("From Materialmove m where m.isDelivery = true AND m.confirmed = true AND m.delivered =false").list();
        List<Materialmove> returnlist = new ArrayList<>();
        for(Object o : os){
            Materialmove m = (Materialmove) o;
            Hibernate.initialize(m.getMaterialmoveItems());
            returnlist.add(m);
        }
        return returnlist;
    }

    public List<Materialmove> getDeliveredOrders(){
        List<Object> os = sessionFactory.getCurrentSession().createQuery("From Materialmove m where m.isDelivery = true AND m.isComplete = false AND m.delivered = true").list();
        List<Materialmove> returnlist = new ArrayList<>();
        for(Object o : os){
            Materialmove m = (Materialmove) o;
            Hibernate.initialize(m.getMaterialmoveItems());
            returnlist.add(m);
        }
        return returnlist;
    }

    public List<Materialmove> getCompletedOrders(){
        List<Object> os = sessionFactory.getCurrentSession().createQuery("From Materialmove m where m.isDelivery = true AND m.isComplete = true ").list();
        List<Materialmove> returnlist = new ArrayList<>();
        for(Object o : os){
            Materialmove m = (Materialmove) o;
            Hibernate.initialize(m.getMaterialmoveItems());
            returnlist.add(m);
        }
        return returnlist;
    }

    public boolean compareMaterialMoveItems(List<MaterialmoveItem> m1 , List<MaterialmoveItem> m2){
        int amount1 = 0;
        for(MaterialmoveItem m: m1){
            amount1 += m.getAmount();
        }
        int amount2 = 0;
        for(MaterialmoveItem m: m1){
            amount2 += m.getAmount();
        }
        return amount1 == amount2;
    }

    public Materialmove replaceMaterialmoveItems(long id, List<MaterialmoveItem> mnew) {
        Materialmove m = getMaterialmoveById(id);
        for(MaterialmoveItem mi : m.getMaterialmoveItems()){
            sessionFactory.getCurrentSession().delete(mi);
        }
        m.setMaterialmoveItems(mnew);
        sessionFactory.getCurrentSession().update(m);
        return m;
    }

    public Materialmove addMoveItemsToOrder(long id, List<MaterialmoveItem> mnew){
        Materialmove m = getMaterialmoveById(id);
        m.getMaterialmoveItems().addAll(mnew);
        sessionFactory.getCurrentSession().update(m);
        return m;
    }

    public void deleteMaterialMoveItem(Materialmove m, MaterialmoveItem mt) {
        Materialmove mm = getMaterialmoveById(m.getId());
        m.getMaterialmoveItems().remove(mt);
        sessionFactory.getCurrentSession().update(m);
        sessionFactory.getCurrentSession().delete(mt);
    }
}
