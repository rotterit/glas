package DTO.MaterialMove;

import Model.Misc.Note;

import java.util.List;

/**
 * Created by thijs on 19-5-2015.
 */
public class OrderNoId {

    private int supplierId;

    private List<MaterialMoveItemNoId> materialmoveItems;

    private String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public List<MaterialMoveItemNoId> getMaterialmoveItems() {
        return materialmoveItems;
    }

    public void setMaterialmoveItems(List<MaterialMoveItemNoId> materialmoveItems) {
        this.materialmoveItems = materialmoveItems;
    }
}
