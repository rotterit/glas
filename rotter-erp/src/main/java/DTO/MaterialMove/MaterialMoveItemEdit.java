package DTO.MaterialMove;

/**
 * Created by thijs on 19-5-2015.
 */
public class MaterialMoveItemEdit {

    private Long id;

    private int amount;

    private int rawMaterialId;

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
