package DTO.MaterialMove;

import java.util.List;

/**
 * Created by thijs on 22-5-2015.
 */
public class QualityCheckSetUp {

    private MaterialPossibilities materialPossibilities;

    private QualityPossibilites qualities;

    private StockPossibilities possibilities;

    private List<MaterialmoveItem> materialmoveItems;

    public List<MaterialmoveItem> getMaterialmoveItems() {
        return materialmoveItems;
    }

    public void setMaterialmoveItems(List<MaterialmoveItem> materialmoveItems) {
        this.materialmoveItems = materialmoveItems;
    }

    public MaterialPossibilities getMaterialPossibilities() {
        return materialPossibilities;
    }

    public void setMaterialPossibilities(MaterialPossibilities materialPossibilities) {
        this.materialPossibilities = materialPossibilities;
    }

    public QualityPossibilites getQualities() {
        return qualities;
    }

    public void setQualities(QualityPossibilites qualities) {
        this.qualities = qualities;
    }

    public StockPossibilities getPossibilities() {
        return possibilities;
    }

    public void setPossibilities(StockPossibilities possibilities) {
        this.possibilities = possibilities;
    }
}
