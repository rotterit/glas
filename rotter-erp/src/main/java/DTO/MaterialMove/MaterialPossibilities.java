package DTO.MaterialMove;

import java.util.List;

/**
 * Created by thijs on 22-5-2015.
 */
public class MaterialPossibilities {

    private List<MiniMaterial> miniMaterialList;

    public MaterialPossibilities(List<MiniMaterial> miniMaterialList) {
        this.miniMaterialList = miniMaterialList;
    }

    public List<MiniMaterial> getMiniMaterialList() {
        return miniMaterialList;
    }

    public void setMiniMaterialList(List<MiniMaterial> miniMaterialList) {
        this.miniMaterialList = miniMaterialList;
    }
}
