package DTO.MaterialMove;

import Model.Misc.Note;

import java.util.List;

/**
 * Created by thijs on 19-5-2015.
 */
public class StockCorrectionNoId {

    private int materialId;

    private int amount;

    private String reason;

    private String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
