package DTO.MaterialMove;

import DTO.Material.RawMaterial;

/**
 * Created by thijs on 20-4-2015.
 */


public class MaterialmoveItem {

    private Long id;

    private RawMaterial rawMaterial;

    private int amount;

    private String StockName;
    private int StockId;

    private double unitPrice;

    private double price;

    public MaterialmoveItem(Model.RawStock.MaterialmoveItem m, RawMaterial r) {
        this.id = m.getId();
        this.rawMaterial = r;
        this.amount = m.getAmount();
        this.StockName = m.getStock().getName();
        this.StockId = m.getStock().getId();
        this.unitPrice = m.getRawMaterial().getPrice();
        this.price = m.getRawMaterial().getPrice()*amount;
    }

    public RawMaterial getRawMaterial() {
        return rawMaterial;
    }

    public void setRawMaterial(RawMaterial rawMaterial) {
        this.rawMaterial = rawMaterial;
    }

    public String getStockName() {
        return StockName;
    }

    public void setStockName(String stockName) {
        StockName = stockName;
    }

    public int getStockId() {
        return StockId;
    }

    public void setStockId(int stockId) {
        StockId = stockId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
