package DTO.MaterialMove;

import DTO.Quality.Quality;

import java.util.List;

/**
 * Created by thijs on 22-5-2015.
 */
public class QualityPossibilites {

    private List<Quality> qualities;

    public QualityPossibilites(List<Quality> qualities) {
        this.qualities = qualities;
    }

    public List<Quality> getQualities() {
        return qualities;
    }

    public void setQualities(List<Quality> qualities) {
        this.qualities = qualities;
    }
}
