package DTO.MaterialMove;

import java.util.List;

/**
 * Created by thijs on 19-5-2015.
 */
public class QualitySetContainer {

    private long orderId;

    private List<QualitySetLine> qualitySetLines;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public List<QualitySetLine> getQualitySetLines() {
        return qualitySetLines;
    }

    public void setQualitySetLines(List<QualitySetLine> qualitySetLines) {
        this.qualitySetLines = qualitySetLines;
    }
}
