package DTO.MaterialMove;

import Model.Misc.Note;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by thijs on 17-5-2015.
 */
public class Order {

    private long id;

    private int supplierId;

    private String createdBy;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private LocalDateTime deliveryDate;

    private String confirmedBy;

    private String completedBy;

    private String deliveryRecievedBy;

    private String supplierName;

    private Note note;

    private List<MaterialmoveItem> materialmoveItems;

    public Order(Model.RawStock.Materialmove m, List<MaterialmoveItem> mi) {
        this.id = m.getId();
        this.supplierId = m.getSupplier().getId();
        this.createdBy = m.getCreatedBy().getName();
        this.creationDate = m.getCreationDate();
        this.lastModifiedDate = m.getLastModifiedDate();
        this.modifiedBy = m.getModifiedBy().getName();
        this.deliveryDate = m.getDeliveryDate();
        if(m.isConfirmed()) {
            this.confirmedBy = m.getConfirmedBy().getName();
        }else{
            this.confirmedBy ="";
        }
        if(m.isComplete()) {
            this.completedBy = m.getCompletedBy().getName();
        }else{
            this.completedBy = "";
        }
        if(m.isDelivered()) {
            this.deliveryRecievedBy = m.getDeliveryRecievedBy().getName();
        }else{
            this.deliveryRecievedBy = "";
        }
        this.supplierName = m.getSupplier().getName();
        this.materialmoveItems = mi;
        this.note = m.getNote();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public LocalDateTime getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDateTime deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getConfirmedBy() {
        return confirmedBy;
    }

    public void setConfirmedBy(String confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    public String getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(String completedBy) {
        this.completedBy = completedBy;
    }

    public String getDeliveryRecievedBy() {
        return deliveryRecievedBy;
    }

    public void setDeliveryRecievedBy(String deliveryRecievedBy) {
        this.deliveryRecievedBy = deliveryRecievedBy;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public List<MaterialmoveItem> getMaterialmoveItems() {
        return materialmoveItems;
    }

    public void setMaterialmoveItems(List<MaterialmoveItem> materialmoveItems) {
        this.materialmoveItems = materialmoveItems;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}
