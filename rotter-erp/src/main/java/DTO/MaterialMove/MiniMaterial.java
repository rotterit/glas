package DTO.MaterialMove;

/**
 * Created by thijs on 22-5-2015.
 */
public class MiniMaterial {

    private int rawMaterialId;

    private int qualityId;

    private String rawMaterialName;

    public MiniMaterial(int rawMaterialId, int qualityId, String rawMaterialName) {
        this.rawMaterialId = rawMaterialId;
        this.qualityId = qualityId;
        this.rawMaterialName = rawMaterialName;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    public int getQualityId() {
        return qualityId;
    }

    public void setQualityId(int qualityId) {
        this.qualityId = qualityId;
    }

    public String getRawMaterialName() {
        return rawMaterialName;
    }

    public void setRawMaterialName(String rawMaterialName) {
        this.rawMaterialName = rawMaterialName;
    }
}
