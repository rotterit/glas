package DTO.MaterialMove;

import java.util.List;

/**
 * Created by thijs on 22-5-2015.
 */
public class StockPossibilities {

    private List<StockPossibility> stockPossibilities;

    public StockPossibilities(List<StockPossibility> stockPossibilities) {
        this.stockPossibilities = stockPossibilities;
    }

    public List<StockPossibility> getStockPossibilities() {
        return stockPossibilities;
    }

    public void setStockPossibilities(List<StockPossibility> stockPossibilities) {
        this.stockPossibilities = stockPossibilities;
    }
}
