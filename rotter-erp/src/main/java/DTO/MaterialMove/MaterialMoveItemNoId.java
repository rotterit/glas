package DTO.MaterialMove;

/**
 * Created by thijs on 19-5-2015.
 */
public class MaterialMoveItemNoId {

    private int rawMaterialId;

    private int amount;

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
