package DTO.MaterialMove;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 20-4-2015.
 */

public class Materialmove {

    private long id;

    private List<MaterialmoveItemSmall> materialmoveItems;

    private LocalDateTime creationDate;

    private String createdBy;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    public Materialmove(Model.RawStock.Materialmove m){
        this.id =m.getId();
        this.materialmoveItems = m.getMaterialmoveItems().stream().map(MaterialmoveItemSmall::new).collect(Collectors.toList());
        this.creationDate = m.getCreationDate();
        this.createdBy = m.getCreatedBy().getName();
        this.lastModifiedDate = m.getLastModifiedDate();
        this.modifiedBy = m.getModifiedBy().getName();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<MaterialmoveItemSmall> getMaterialmoveItems() {
        return materialmoveItems;
    }

    public void setMaterialmoveItems(List<MaterialmoveItemSmall> materialmoveItems) {
        this.materialmoveItems = materialmoveItems;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
