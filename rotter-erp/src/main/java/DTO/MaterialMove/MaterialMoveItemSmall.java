package DTO.MaterialMove;

/**
 * Created by thijs on 20-5-2015.
 */
public class MaterialmoveItemSmall {

    private Long id;

    private int amount;

    private String rawMaterialCode;
    private int rawMaterialId;

    private String StockName;
    private int StockId;

    public MaterialmoveItemSmall(Model.RawStock.MaterialmoveItem m) {
        this.id = m.getId();
        this.rawMaterialCode = m.getRawMaterial().getProductSupplierLine().getInternalReference();
        this.rawMaterialId = m.getRawMaterial().getId();
        this.amount = m.getAmount();
        this.StockName = m.getStock().getName();
        this.StockId = m.getStock().getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getRawMaterialCode() {
        return rawMaterialCode;
    }

    public void setRawMaterialCode(String rawMaterialCode) {
        this.rawMaterialCode = rawMaterialCode;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    public String getStockName() {
        return StockName;
    }

    public void setStockName(String stockName) {
        StockName = stockName;
    }

    public int getStockId() {
        return StockId;
    }

    public void setStockId(int stockId) {
        StockId = stockId;
    }
}
