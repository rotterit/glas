package DTO.MaterialMove;

import DTO.Stock.Stock;
import DTO.Stock.StockMini;

import java.util.List;

/**
 * Created by thijs on 22-5-2015.
 */
public class StockPossibility {

    private int rawMaterialId;

    private List<StockMini> stocks;

    public StockPossibility(int rawMaterialId, List<StockMini> stocks) {
        this.rawMaterialId = rawMaterialId;
        this.stocks = stocks;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    public List<StockMini> getStocks() {
        return stocks;
    }

    public void setStocks(List<StockMini> stocks) {
        this.stocks = stocks;
    }
}
