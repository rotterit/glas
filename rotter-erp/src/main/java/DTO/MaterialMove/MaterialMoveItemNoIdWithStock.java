package DTO.MaterialMove;

/**
 * Created by thijs on 20-5-2015.
 */
public class MaterialMoveItemNoIdWithStock {

    private int rawMaterialId;

    private int amount;

    private int stockId;

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
