package DTO.MaterialMove;

import Model.Misc.Note;

import java.util.List;

/**
 * Created by thijs on 19-5-2015.
 */
public class OrderEdit {

    private long id;

    private List<MaterialMoveItemNoId> moveItems;

    private int supplierId;

    private Note note;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<MaterialMoveItemNoId> getMoveItems() {
        return moveItems;
    }

    public void setMoveItems(List<MaterialMoveItemNoId> moveItems) {
        this.moveItems = moveItems;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }
}
