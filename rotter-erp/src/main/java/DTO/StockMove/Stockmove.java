package DTO.StockMove;

import Model.User.User;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 20-4-2015.
 */

public class Stockmove {

    private long id;

    private LocalDateTime creationDate;

    private List<StockmoveItem> stockmoveItems;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private String createdBy;

    public Stockmove(Model.FinishedStock.Stockmove s) {
        this.id = s.getId();
        this.creationDate= s.getCreationDate();
        this.stockmoveItems = s.getStockmoveItems().stream().map(StockmoveItem::new).collect(Collectors.toList());
        this.lastModifiedDate = s.getLastModifiedDate();
        this.createdBy = s.getCreatedBy().getName();
        this.modifiedBy = s.getModifiedBy().getName();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public List<StockmoveItem> getStockmoveItems() {
        return stockmoveItems;
    }

    public void setStockmoveItems(List<StockmoveItem> stockmoveItems) {
        this.stockmoveItems = stockmoveItems;
    }

    public void addStockmoveItem(StockmoveItem item){
        this.stockmoveItems.add(item);
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
