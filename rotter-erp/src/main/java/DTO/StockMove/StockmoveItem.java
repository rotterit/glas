package DTO.StockMove;

/**
 * Created by thijs on 20-4-2015.
 */

public class StockmoveItem {


    private long id;

    private String product;

    private long productId;

    private int amount;

    private int stockId;

    private boolean compromised;

    public StockmoveItem(Model.FinishedStock.StockmoveItem s) {
        this.id = s.getId();
        this.product = s.getProduct().getProductCode();
        this.productId = s.getProduct().getId();
        this.amount = s.getAmount();
        this.stockId = s.getStock().getId();
        this.compromised = s.isCompromised();
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public boolean isCompromised() {
        return compromised;
    }

    public void setCompromised(boolean compromised) {
        this.compromised = compromised;
    }
}
