package DTO.User;

/**
 * Created by thijs on 20-4-2015.
 */

public class User {

    private int id;

    private String name;

    private String username;

    private int roleid;

    private String rolename;

    public User(Model.User.User user) {
        this.name = user.getName();
        this.id = user.getId();
        this.username = user.getUsername();
        this.roleid = user.getRole().getId();
        this.rolename = user.getRole().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getRoleid() {
        return roleid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }
}
