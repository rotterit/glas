package DTO.Supplier;

import Model.Misc.Note;

/**
 * Created by thijs on 14-5-2015.
 */
public class SupplierNoId {

    private String supplierCode;

    private String name;

    private String contactnote;

    private String suppliernote;

    private String generalnote;

    public SupplierNoId() {
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        this.supplierCode = supplierCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactnote() {
        return contactnote;
    }

    public void setContactnote(String contactnote) {
        this.contactnote = contactnote;
    }

    public String getSuppliernote() {
        return suppliernote;
    }

    public void setSuppliernote(String suppliernote) {
        this.suppliernote = suppliernote;
    }

    public String getGeneralnote() {
        return generalnote;
    }

    public void setGeneralnote(String generalnote) {
        this.generalnote = generalnote;
    }
}
