package DTO.Supplier;

import Model.Misc.Note;

import java.time.LocalDateTime;

/**
 * Created by thijs on 20-4-2015.
 */
public class Supplier {

    private int id;

    private String supplierCode;

    private String name;

    private Note contactnote;

    private Note suppliernote;

    private Note generalnote;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String createdBy;

    private String modifiedBy;

    public Supplier() {
    }

    public Supplier(Model.RawMaterial.Supplier s) {
        this.id = s.getId();
        this.supplierCode = s.getSuplierCode();
        this.name = s.getName();
        this.creationDate = s.getCreationDate();
        this.contactnote = s.getContactnote();
        this.suppliernote = s.getSuppliernote();
        this.generalnote = s.getGeneralnote();
        this.lastModifiedDate = s.getLastModifiedDate();
        this.createdBy = s.getCreatedBy().getName();
        this.modifiedBy =s.getModifiedBy().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    public void setSupplierCode(String suplierCode) {
        this.supplierCode = suplierCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Note getContactnote() {
        return contactnote;
    }

    public void setContactnote(Note contactnote) {
        this.contactnote = contactnote;
    }

    public Note getSuppliernote() {
        return suppliernote;
    }

    public void setSuppliernote(Note suppliernote) {
        this.suppliernote = suppliernote;
    }

    public Note getGeneralnote() {
        return generalnote;
    }

    public void setGeneralnote(Note generalnote) {
        this.generalnote = generalnote;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}

