package DTO.Invoice;

import DTO.StockMove.Stockmove;

import java.time.LocalDateTime;

/**
 * Created by thijs on 22-5-2015.
 */
public class PackingSlip {

    private long id;

    private LocalDateTime creationDate;

    private String createdBy;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private LocalDateTime packedDate;

    private String packedBy;

    private LocalDateTime shippedDate;

    private String shippedBy;

    private boolean isReady;

    private Stockmove stockmove;

    public PackingSlip(Model.Invoicing.PackingSlip p) {
        this.id = p.getId();
        this.creationDate = p.getCreationDate();
        this.createdBy = p.getCreatedBy().getName();
        this.lastModifiedDate = p.getLastModifiedDate();
        this.modifiedBy = p.getModifiedBy().getName();
        this.packedDate = p.getPackedDate();
        this.packedBy = p.getPackedBy().getName();
        this.shippedDate = p.getShippedDate();
        this.shippedBy = p.getShippedBy().getName();
        this.isReady = p.isReady();
        this.stockmove = new Stockmove(p.getStockmove());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public LocalDateTime getPackedDate() {
        return packedDate;
    }

    public void setPackedDate(LocalDateTime packedDate) {
        this.packedDate = packedDate;
    }

    public String getPackedBy() {
        return packedBy;
    }

    public void setPackedBy(String packedBy) {
        this.packedBy = packedBy;
    }

    public LocalDateTime getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(LocalDateTime shippedDate) {
        this.shippedDate = shippedDate;
    }

    public String getShippedBy() {
        return shippedBy;
    }

    public void setShippedBy(String shippedBy) {
        this.shippedBy = shippedBy;
    }

    public boolean isReady() {
        return isReady;
    }

    public void setReady(boolean isReady) {
        this.isReady = isReady;
    }

    public Stockmove getStockmove() {
        return stockmove;
    }

    public void setStockmove(Stockmove stockmove) {
        this.stockmove = stockmove;
    }
}
