package DTO.Invoice;

/**
 * Created by thijs on 23-5-2015.
 */
public class InvoiceEdit {

    private long invoiceId;

    private InvoiceNoId invoice;

    public long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public InvoiceNoId getInvoice() {
        return invoice;
    }

    public void setInvoice(InvoiceNoId invoice) {
        this.invoice = invoice;
    }
}
