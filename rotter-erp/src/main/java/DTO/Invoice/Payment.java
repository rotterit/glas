package DTO.Invoice;

import java.time.LocalDateTime;

/**
 * Created by thijs on 22-5-2015.
 */
public class Payment {

    private long id;

    private double amount;

    private LocalDateTime creationDate;

    private String createdBy;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private boolean overdue;

    public Payment(Model.Invoicing.Payment p) {
        this.id = p.getId();
        this.amount = p.getAmount();
        this.creationDate = p.getCreationDate();
        this.createdBy = p.getCreatedBy().getName();
        this.lastModifiedDate = p.getLastModifiedDate();
        this.modifiedBy = p.getModifiedBy().getName();
        this.overdue = p.isOverdue();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isOverdue() {
        return overdue;
    }

    public void setOverdue(boolean overdue) {
        this.overdue = overdue;
    }
}
