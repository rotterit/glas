package DTO.Invoice;

import DTO.StockMove.Stockmove;

import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by thijs on 22-5-2015.
 */
public class InvoiceNoId {

    private int customerId;

    private long paymentDueDate;

    private double priceWithoutVAT;

    private double VATpercentage;

    private double VAT;

    private double ValutaFactor;

    private double upfrontpercentage;

    private double packingCost;

    private double packingCostPercentage;

    private double discount;

    private double discountpercentage;

    private double localPrice;

    private double price;

    private String valuta;

    private String note;

    private long dueDate;

    private List<InvoiceCreateLine> invoiceCreateLines;

    private double shippingCost;

    private String shippingMethod;

    public InvoiceNoId(int customerId, String note, List<InvoiceCreateLine> invoiceCreateLines) {
        this.customerId = customerId;
        this.paymentDueDate = 0;
        this.priceWithoutVAT = 0;
        this.VATpercentage = 0;
        this.VAT = 0;
        ValutaFactor = 0;
        this.upfrontpercentage = 0;
        this.packingCost = 0;
        this.packingCostPercentage = 0;
        this.discount = 0;
        this.discountpercentage = 0;
        this.localPrice = 0;
        this.price = 0;
        this.valuta = "";
        this.note = note;
        this.dueDate = 0;
        this.invoiceCreateLines = invoiceCreateLines;
        this.shippingCost = 0;
        this.shippingMethod = "";
    }

    public InvoiceNoId() {
    }

    public double getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(double shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public long getPaymentDueDate() {
        return paymentDueDate;
    }

    public void setPaymentDueDate(long paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }

    public double getPriceWithoutVAT() {
        return priceWithoutVAT;
    }

    public void setPriceWithoutVAT(double priceWithoutVAT) {
        this.priceWithoutVAT = priceWithoutVAT;
    }

    public double getVATpercentage() {
        return VATpercentage;
    }

    public void setVATpercentage(double VATpercentage) {
        this.VATpercentage = VATpercentage;
    }

    public double getVAT() {
        return VAT;
    }

    public void setVAT(double VAT) {
        this.VAT = VAT;
    }

    public double getValutaFactor() {
        return ValutaFactor;
    }

    public void setValutaFactor(double valutaFactor) {
        ValutaFactor = valutaFactor;
    }

    public double getUpfrontpercentage() {
        return upfrontpercentage;
    }

    public void setUpfrontpercentage(double upfrontpercentage) {
        this.upfrontpercentage = upfrontpercentage;
    }

    public double getPackingCost() {
        return packingCost;
    }

    public void setPackingCost(double packingCost) {
        this.packingCost = packingCost;
    }

    public double getPackingCostPercentage() {
        return packingCostPercentage;
    }

    public void setPackingCostPercentage(double packingCostPercentage) {
        this.packingCostPercentage = packingCostPercentage;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getDiscountpercentage() {
        return discountpercentage;
    }

    public void setDiscountpercentage(double discountpercentage) {
        this.discountpercentage = discountpercentage;
    }

    public double getLocalPrice() {
        return localPrice;
    }

    public void setLocalPrice(double localPrice) {
        this.localPrice = localPrice;
    }

    public String getValuta() {
        return valuta;
    }

    public void setValuta(String valuta) {
        this.valuta = valuta;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<InvoiceCreateLine> getInvoiceCreateLines() {
        return invoiceCreateLines;
    }

    public void setInvoiceCreateLines(List<InvoiceCreateLine> invoiceCreateLines) {
        this.invoiceCreateLines = invoiceCreateLines;
    }
}
