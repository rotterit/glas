package DTO.Invoice;

import Model.Invoicing.*;

import java.util.List;

/**
 * Created by thijs on 25-5-2015.
 */
public class PackingSlipNoId {

    private List<PackingSlipLine> packingSlipLineList;

    public List<PackingSlipLine> getPackingSlipLineList() {
        return packingSlipLineList;
    }

    public void setPackingSlipLineList(List<PackingSlipLine> packingSlipLineList) {
        this.packingSlipLineList = packingSlipLineList;
    }
}
