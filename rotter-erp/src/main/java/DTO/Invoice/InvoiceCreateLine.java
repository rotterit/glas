package DTO.Invoice;

/**
 * Created by thijs on 23-5-2015.
 */
public class InvoiceCreateLine {

    private long productId;

    private int amount;

    private double inlineDiscount;

    private double inlineDiscountPercentage;

    private double inlinePrice;

    private int stockId;

    public InvoiceCreateLine(long productId, int amount, int stockId) {
        this.productId = productId;
        this.amount = amount;
        this.inlineDiscount = 0;
        this.inlineDiscountPercentage = 0;
        this.inlinePrice = 0;
        this.stockId = stockId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getInlineDiscount() {
        return inlineDiscount;
    }

    public void setInlineDiscount(double inlineDiscount) {
        this.inlineDiscount = inlineDiscount;
    }

    public double getInlineDiscountPercentage() {
        return inlineDiscountPercentage;
    }

    public void setInlineDiscountPercentage(double inlineDiscountPercentage) {
        this.inlineDiscountPercentage = inlineDiscountPercentage;
    }

    public double getInlinePrice() {
        return inlinePrice;
    }

    public void setInlinePrice(double inlinePrice) {
        this.inlinePrice = inlinePrice;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }
}
