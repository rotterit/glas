package DTO.Invoice;

/**
 * Created by thijs on 25-5-2015.
 */
public class PackingSlipLine {

    private long productid;
    private int amount;

    public long getProductid() {
        return productid;
    }

    public void setProductid(long productid) {
        this.productid = productid;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
