package DTO.Invoice;

import DTO.Product.MiniProduct;

/**
 * Created by thijs on 22-5-2015.
 */
public class InvoiceDetailLine {

    private MiniProduct product;

    private int amount;

    private double inlineDiscount;

    private double inlineDiscountPercentage;

    private double inlinePrice;

    private int stockId;

    public double getInlineDiscount() {
        return inlineDiscount;
    }

    public void setInlineDiscount(double inlineDiscount) {
        this.inlineDiscount = inlineDiscount;
    }

    public double getInlineDiscountPercentage() {
        return inlineDiscountPercentage;
    }

    public void setInlineDiscountPercentage(double inlineDiscountPercentage) {
        this.inlineDiscountPercentage = inlineDiscountPercentage;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }

    public MiniProduct getProduct() {
        return product;
    }

    public void setProduct(MiniProduct product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getInlinePrice() {
        return inlinePrice;
    }

    public void setInlinePrice(double inlinePrice) {
        this.inlinePrice = inlinePrice;
    }
}
