package DTO.Invoice;

import DTO.Customer.Customer;
import DTO.StockMove.Stockmove;
import Model.FinishedStock.StockmoveItem;
import Model.Misc.Note;


import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 22-5-2015.
 */
public class Invoice {

    private double valutaFactor;
    private long id;

    //invoice Nr (boekhouding)

    private String invoicenr;

    //customer info

    private Customer customer;
    private int customerId;

    //paymentinfo

    private LocalDateTime paymentDueDate;

    private List<Payment> payment;

    //order info

    private List<InvoiceDetailLine> order;

    private Stockmove needed;

    private Stockmove completed;

    //packing info

    private List<PackingSlip> packingSlip;

    private LocalDateTime sendDate;

    //prijs info

    private double priceWithoutVAT;

    private double VATpercentage;

    private double VAT;
    //price in euro

    private double price;

    private double upfrontpercentage;

    private double packingCost;

    private double packingCostPercentage;

    private double discount;

    private double discountpercentage;
    // price in customer valuta

    private double localPrice;

    private String valuta;

    private double shippingCost;

    private String shippingMethod;

    private Note note;

    private LocalDateTime creationDate;

    private String createdBy;


    private LocalDateTime lastModifiedDate;

    private String modifiedBy;


    private boolean sent;

    private LocalDateTime sentDate;

    private String sentBy;


    private boolean canceled;

    private LocalDateTime cancelationDate;

    private String CanceledBy;


    private boolean complete;

    private LocalDateTime completionDate;

    private String completedBy;


    private boolean confirmed;

    private LocalDateTime confirmationDate;

    private String confirmedBy;


    private boolean inProduction;

    private LocalDateTime productionOkDate;

    private String okedForProductionBy;

    private boolean overDue;

    private boolean pastTwoWeeks;

    private boolean ready;

    private boolean payed;

//    public Invoice(Model.Invoicing.Invoice i, List<InvoiceDetailLine> detailLines, double baseinvoicenr ) {
//        this.id = i.getId();
//        this.invoicenumber = (long) (baseinvoicenr + i.getId());
//        this.customer = i.getCustomer().getName();
//        this.payments = i.getPayment().stream().map(Payment::new).collect(Collectors.toList());
//        this.Order = detailLines;
//        Needed = new Stockmove(i.getNeeded());
//        this.completed = new Stockmove(i.getCompleted());
//        this.paymentDueDate = i.getPaymentDueDate();
//        this.packingSlip = i.getPackingSlip().stream().map(PackingSlip::new).collect(Collectors.toList());
//        this.priceWithoutVAT = i.getPriceWithoutVAT();
//        this.VATpercentage = i.getVATpercentage();
//        this.VAT = i.getVAT();
//        this.upfrontpercentage = i.getUpfrontpercentage();
//        this.packingCost = i.getPackingCost();
//        this.packingCostPercentage = i.getPackingCostPercentage();
//        this.discount = i.getDiscount();
//        this.discountpercentage = i.getDiscountpercentage();
//        this.localPrice = i.getLocalPrice();
//        this.valuta = i.getValuta();
//        this.sent = i.isSent();
//        this.sendDate = i.getSendDate();
//        this.sentBy = i.getSentBy().getName();
//        this.complete = i.isComplete();
//        this.completionDate = i.getCompletionDate();
//        this.completedBy = i.getCompletedBy().getName();
//        this.canceled = i.isCanceled();
//        this.cancelationDate = i.getCancelationDate();
//        CanceledBy = i.getCanceledBy().getName();
//        this.note = i.getNote();
//        this.creationDate = i.getCreationDate();
//        this.createdBy = i.getCreatedBy().getName();
//        this.lastModifiedDate = i.getLastModifiedDate();
//        this.modifiedBy = i.getModifiedBy().getName();
//        this.customerId = i.getCustomer().getId();
//        this.valutaFactor = localPrice/i.getPrice();
//        this.price = i.getPrice();
//        this.overDue = i.getPaymentDueDate().isBefore(LocalDateTime.now());
//        this.pastTwoWeeks = i.getPaymentDueDate().plusWeeks(2).isBefore(LocalDateTime.now());
//    }


    public Invoice(Model.Invoicing.Invoice i, List<InvoiceDetailLine> detailLines, boolean ready) {
        this.id = i.getId();
        this.invoicenr = i.getInvoicenr();
        this.customer = new Customer(i.getCustomer());
        this.customerId = i.getCustomer().getId();
        this.paymentDueDate = i.getPaymentDueDate();
        this.payment = i.getPayment().stream().map(Payment::new).collect(Collectors.toList());
        this.order = detailLines;
        this.packingSlip = i.getPackingSlip().stream().map(PackingSlip::new).collect(Collectors.toList());
        this.sendDate = i.getSendDate();
        this.priceWithoutVAT = i.getPriceWithoutVAT();
        this.VATpercentage = i.getVATpercentage();
        this.VAT = i.getVAT();
        this.price = i.getPrice();
        this.upfrontpercentage = i.getUpfrontpercentage();
        this.packingCost = i.getPackingCost();
        this.packingCostPercentage = i.getPackingCostPercentage();
        this.discount = i.getDiscount();
        this.discountpercentage = i.getDiscountpercentage();
        this.localPrice = i.getLocalPrice();
        this.valuta = i.getValuta();
        this.valutaFactor = i.getPrice()/i.getLocalPrice();
        this.shippingCost = i.getShippingCost();
        this.shippingMethod = i.getShippingMethod();
        this.note = i.getNote();
        this.creationDate = i.getCreationDate();
        this.createdBy = i.getCreatedBy().getName();
        this.lastModifiedDate = i.getLastModifiedDate();
        this.modifiedBy = i.getModifiedBy().getName();
        this.sent = i.isSent();
        this.sentDate = i.getSentDate();
        this.sentBy = i.getSentBy().getName();
        this.canceled = i.isCanceled();
        this.cancelationDate = i.getCancelationDate();
        this.CanceledBy = i.getCanceledBy().getName();
        this.complete = i.isComplete();
        this.completionDate = i.getCompletionDate();
        this.completedBy = i.getCompletedBy().getName();
        this.confirmed = i.isConfirmed();
        this.confirmationDate = i.getConfirmationDate();
        this.confirmedBy = i.getConfirmedBy().getName();
        this.inProduction = i.isInProduction();
        this.productionOkDate = i.getProductionOkDate();
        this.okedForProductionBy = i.getOkedForProductionBy().getName();
        this.overDue = i.getPaymentDueDate().isBefore(LocalDateTime.now());
        this.pastTwoWeeks = i.getPaymentDueDate().plusWeeks(2).isBefore(LocalDateTime.now());
        this.needed = new Stockmove(i.getNeeded());
        this.completed = new Stockmove(i.getCompleted());
        this.ready = ready;
        this.payed = (i.getPayment().stream().mapToDouble(m->m.getAmount()).sum()>=i.getLocalPrice());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getInvoicenr() {
        return invoicenr;
    }

    public void setInvoicenr(String invoicenr) {
        this.invoicenr = invoicenr;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public LocalDateTime getPaymentDueDate() {
        return paymentDueDate;
    }

    public void setPaymentDueDate(LocalDateTime paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }

    public List<Payment> getPayment() {
        return payment;
    }

    public void setPayment(List<Payment> payment) {
        this.payment = payment;
    }

    public List<InvoiceDetailLine> getOrder() {
        return order;
    }

    public void setOrder(List<InvoiceDetailLine> order) {
        this.order = order;
    }

    public List<PackingSlip> getPackingSlip() {
        return packingSlip;
    }

    public void setPackingSlip(List<PackingSlip> packingSlip) {
        this.packingSlip = packingSlip;
    }

    public LocalDateTime getSendDate() {
        return sendDate;
    }

    public void setSendDate(LocalDateTime sendDate) {
        this.sendDate = sendDate;
    }

    public double getPriceWithoutVAT() {
        return priceWithoutVAT;
    }

    public void setPriceWithoutVAT(double priceWithoutVAT) {
        this.priceWithoutVAT = priceWithoutVAT;
    }

    public double getVATpercentage() {
        return VATpercentage;
    }

    public void setVATpercentage(double VATpercentage) {
        this.VATpercentage = VATpercentage;
    }

    public double getVAT() {
        return VAT;
    }

    public void setVAT(double VAT) {
        this.VAT = VAT;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getUpfrontpercentage() {
        return upfrontpercentage;
    }

    public void setUpfrontpercentage(double upfrontpercentage) {
        this.upfrontpercentage = upfrontpercentage;
    }

    public double getPackingCost() {
        return packingCost;
    }

    public void setPackingCost(double packingCost) {
        this.packingCost = packingCost;
    }

    public double getPackingCostPercentage() {
        return packingCostPercentage;
    }

    public void setPackingCostPercentage(double packingCostPercentage) {
        this.packingCostPercentage = packingCostPercentage;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getDiscountpercentage() {
        return discountpercentage;
    }

    public void setDiscountpercentage(double discountpercentage) {
        this.discountpercentage = discountpercentage;
    }

    public double getLocalPrice() {
        return localPrice;
    }

    public void setLocalPrice(double localPrice) {
        this.localPrice = localPrice;
    }

    public String getValuta() {
        return valuta;
    }

    public void setValuta(String valuta) {
        this.valuta = valuta;
    }

    public double getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(double shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isSent() {
        return sent;
    }

    public void setSent(boolean sent) {
        this.sent = sent;
    }

    public LocalDateTime getSentDate() {
        return sentDate;
    }

    public void setSentDate(LocalDateTime sentDate) {
        this.sentDate = sentDate;
    }

    public String getSentBy() {
        return sentBy;
    }

    public void setSentBy(String sentBy) {
        this.sentBy = sentBy;
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public LocalDateTime getCancelationDate() {
        return cancelationDate;
    }

    public void setCancelationDate(LocalDateTime cancelationDate) {
        this.cancelationDate = cancelationDate;
    }

    public String getCanceledBy() {
        return CanceledBy;
    }

    public void setCanceledBy(String canceledBy) {
        CanceledBy = canceledBy;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public LocalDateTime getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDateTime completionDate) {
        this.completionDate = completionDate;
    }

    public String getCompletedBy() {
        return completedBy;
    }

    public void setCompletedBy(String completedBy) {
        this.completedBy = completedBy;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public LocalDateTime getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(LocalDateTime confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public String getConfirmedBy() {
        return confirmedBy;
    }

    public void setConfirmedBy(String confirmedBy) {
        this.confirmedBy = confirmedBy;
    }

    public boolean isInProduction() {
        return inProduction;
    }

    public void setInProduction(boolean inProduction) {
        this.inProduction = inProduction;
    }

    public LocalDateTime getProductionOkDate() {
        return productionOkDate;
    }

    public void setProductionOkDate(LocalDateTime productionOkDate) {
        this.productionOkDate = productionOkDate;
    }

    public String getOkedForProductionBy() {
        return okedForProductionBy;
    }

    public void setOkedForProductionBy(String okedForProductionBy) {
        this.okedForProductionBy = okedForProductionBy;
    }

    public boolean isOverDue() {
        return overDue;
    }

    public void setOverDue(boolean overDue) {
        this.overDue = overDue;
    }

    public boolean isPastTwoWeeks() {
        return pastTwoWeeks;
    }

    public void setPastTwoWeeks(boolean pastTwoWeeks) {
        this.pastTwoWeeks = pastTwoWeeks;
    }

    public double getValutaFactor() {
        return valutaFactor;
    }

    public void setValutaFactor(double valutaFactor) {
        this.valutaFactor = valutaFactor;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public Stockmove getNeeded() {
        return needed;
    }

    public void setNeeded(Stockmove needed) {
        this.needed = needed;
    }

    public Stockmove getCompleted() {
        return completed;
    }

    public void setCompleted(Stockmove completed) {
        this.completed = completed;
    }

    public boolean isPayed() {
        return payed;
    }

    public void setPayed(boolean payed) {
        this.payed = payed;
    }
}
