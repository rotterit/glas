package DTO.Misc;

import DTO.Invoice.Invoice;

import java.util.List;

/**
 * Created by thijs on 2-6-2015.
 */
public class LandingInfo {

    private int productionQueueItemsCount;

    private List<Invoice> DueInvoiceList;

    private List<Invoice> paymentDueList;

    public LandingInfo() {
    }

    public int getProductionQueueItemsCount() {
        return productionQueueItemsCount;
    }

    public void setProductionQueueItemsCount(int productionQueueItemsCount) {
        this.productionQueueItemsCount = productionQueueItemsCount;
    }

    public List<Invoice> getDueInvoiceList() {
        return DueInvoiceList;
    }

    public void setDueInvoiceList(List<Invoice> dueInvoiceList) {
        DueInvoiceList = dueInvoiceList;
    }

    public List<Invoice> getPaymentDueList() {
        return paymentDueList;
    }

    public void setPaymentDueList(List<Invoice> paymentDueList) {
        this.paymentDueList = paymentDueList;
    }
}
