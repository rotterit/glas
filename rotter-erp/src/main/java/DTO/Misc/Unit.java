package DTO.Misc;

import Model.Misc.Note;

/**
 * Created by thijs on 14-5-2015.
 */
public class Unit {

    private int id;

    private String name;

    private String textValue;

    private double numericalValue;

    private Note note;

    public Unit(Model.Misc.Unit u) {
        this.id =u.getId();
        this.name = u.getName();
        this.textValue = u.getTextValue();
        this.numericalValue = u.getNumericalValue();
        this.note = u.getNote();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public double getNumericalValue() {
        return numericalValue;
    }

    public void setNumericalValue(double numericalValue) {
        this.numericalValue = numericalValue;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}
