package DTO.MaterialNotification;

/**
 * Created by thijs on 17-5-2015.
 */
public class MaterialNotificationNoId {

    private long predate;

    private long startdate;

    private long enddate;

    private int amount;

    private int rawMaterialId;

    public long getPredate() {
        return predate;
    }

    public void setPredate(long predate) {
        this.predate = predate;
    }

    public long getStartdate() {
        return startdate;
    }

    public void setStartdate(long startdate) {
        this.startdate = startdate;
    }

    public long getEnddate() {
        return enddate;
    }

    public void setEnddate(long enddate) {
        this.enddate = enddate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }
}
