package DTO.MaterialNotification;

import DTO.Material.RawMaterial;
import Model.User.User;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by thijs on 20-4-2015.
 */

public class MaterialNotification {

    private int id;

    private LocalDateTime predate;

    private LocalDateTime startdate;

    private LocalDateTime enddate;

    private int amount;

    private int rawMaterialId;

    private String rawMaterialCode;

    private LocalDateTime creationDate;

    private String createdBy;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    public MaterialNotification(Model.RawStock.MaterialNotification m) {
        this.id = m.getId();
        this.predate = m.getPredate();
        this.startdate = m.getStartdate();
        this.enddate = m.getEnddate();
        this.amount = m.getAmount();
        this.rawMaterialId = m.getRawMaterial().getId();
        this.rawMaterialCode = m.getRawMaterial().getProductSupplierLine().getInternalReference();
        this.creationDate = m.getCreationDate();
        this.createdBy = m.getCreatedBy().getName();
        this.lastModifiedDate = m.getLastModifiedDate();
        this.modifiedBy = m.getModifiedBy().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getPredate() {
        return predate;
    }

    public void setPredate(LocalDateTime predate) {
        this.predate = predate;
    }

    public LocalDateTime getStartdate() {
        return startdate;
    }

    public void setStartdate(LocalDateTime startdate) {
        this.startdate = startdate;
    }

    public LocalDateTime getEnddate() {
        return enddate;
    }

    public void setEnddate(LocalDateTime enddate) {
        this.enddate = enddate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    public String getRawMaterialCode() {
        return rawMaterialCode;
    }

    public void setRawMaterialCode(String rawMaterialCode) {
        this.rawMaterialCode = rawMaterialCode;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
