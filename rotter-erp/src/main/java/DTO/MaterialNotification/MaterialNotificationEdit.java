package DTO.MaterialNotification;

/**
 * Created by thijs on 18-5-2015.
 */
public class MaterialNotificationEdit {

    private int id;

    private long predate;

    private long startdate;

    private long enddate;

    private int amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getPredate() {
        return predate;
    }

    public void setPredate(long predate) {
        this.predate = predate;
    }

    public long getStartdate() {
        return startdate;
    }

    public void setStartdate(long startdate) {
        this.startdate = startdate;
    }

    public long getEnddate() {
        return enddate;
    }

    public void setEnddate(long enddate) {
        this.enddate = enddate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

}
