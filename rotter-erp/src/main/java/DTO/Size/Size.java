package DTO.Size;

import Model.FinishedProduct.MySize;

import java.time.LocalDateTime;

/**
 * Created by thijs on 8-5-2015.
 */
public class Size {

    private int id;

    private LocalDateTime creationDate;

    private String name;

    private String Code;

    private LocalDateTime modifiedDate;

    private String modifiedBy;

    private String createdBy;

    public Size(MySize s) {
        this.id = s.getId();
        this.creationDate = s.getCreationDate();
        this.name = s.getName();
        this.Code = s.getCode();
        this.modifiedDate = s.getLastModifiedDate();
        this.createdBy =s.getCreatedBy().getName();
        this.modifiedBy = s.getModifiedBy().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public LocalDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(LocalDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
