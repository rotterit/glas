package DTO.StockCorrection;

import Model.FinishedStock.StockmoveItem;

import java.util.List;

/**
 * Created by thijs on 27-5-2015.
 */
public class FinishedStockCorrectionNoId {

    private String note;

    private String reason;

    private List<StockmoveItemNoId> stockmoveItemNoIds;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public List<StockmoveItemNoId> getStockmoveItemNoIds() {
        return stockmoveItemNoIds;
    }

    public void setStockmoveItemNoIds(List<StockmoveItemNoId> stockmoveItemNoIds) {
        this.stockmoveItemNoIds = stockmoveItemNoIds;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
