package DTO.StockCorrection;

/**
 * Created by thijs on 27-5-2015.
 */
public class StockmoveItemNoId {

    private int productId;

    private int amount;

    private int stockId;

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }
}
