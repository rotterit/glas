package DTO.Commission;

import Model.User.User;

import java.util.List;

/**
 * Created by thijs on 27-5-2015.
 */
public class CommissionNoId {

    private int customerId;

    private long startmillis;

    private long returnmillis;

    private String note;

    private List<StockmoveItemNoId> stockmoveItemNoIdList;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public long getStartmillis() {
        return startmillis;
    }

    public void setStartmillis(long startmillis) {
        this.startmillis = startmillis;
    }

    public long getReturnmillis() {
        return returnmillis;
    }

    public void setReturnmillis(long returnmillis) {
        this.returnmillis = returnmillis;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<StockmoveItemNoId> getStockmoveItemNoIdList() {
        return stockmoveItemNoIdList;
    }

    public void setStockmoveItemNoIdList(List<StockmoveItemNoId> stockmoveItemNoIdList) {
        this.stockmoveItemNoIdList = stockmoveItemNoIdList;
    }
}
