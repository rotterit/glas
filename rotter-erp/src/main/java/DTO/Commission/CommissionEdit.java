package DTO.Commission;

import DTO.StockMove.StockmoveItem;
import Model.Misc.Note;

import java.util.List;

/**
 * Created by thijs on 27-5-2015.
 */
public class CommissionEdit {

    private long id;

    private long startmillis;

    private long returnmillis;

    private Note note;

    private List<StockmoveItem> stockmoveItems;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStartmillis() {
        return startmillis;
    }

    public void setStartmillis(long startmillis) {
        this.startmillis = startmillis;
    }

    public long getReturnmillis() {
        return returnmillis;
    }

    public void setReturnmillis(long returnmillis) {
        this.returnmillis = returnmillis;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public List<StockmoveItem> getStockmoveItems() {
        return stockmoveItems;
    }

    public void setStockmoveItems(List<StockmoveItem> stockmoveItems) {
        this.stockmoveItems = stockmoveItems;
    }
}
