package DTO.Commission;

import java.util.List;

/**
 * Created by thijs on 28-5-2015.
 */
public class CommissionRecieve {

    private long commissionId;

    private List<StockmoveItemNoId> stockmoveItemNoIdList;

    public long getCommissionId() {
        return commissionId;
    }

    public void setCommissionId(long commissionId) {
        this.commissionId = commissionId;
    }

    public List<StockmoveItemNoId> getStockmoveItemNoIdList() {
        return stockmoveItemNoIdList;
    }

    public void setStockmoveItemNoIdList(List<StockmoveItemNoId> stockmoveItemNoIdList) {
        this.stockmoveItemNoIdList = stockmoveItemNoIdList;
    }
}
