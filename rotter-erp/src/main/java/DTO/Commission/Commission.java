package DTO.Commission;

import DTO.Customer.Customer;
import DTO.Invoice.Invoice;
import DTO.Invoice.InvoiceDetailLine;
import DTO.Invoice.PackingSlip;
import DTO.StockMove.Stockmove;
import Model.Misc.Note;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 28-5-2015.
 */
public class Commission {

    private long id;

    private String createdBy;

    private Customer customer;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private LocalDateTime startDate;

    private boolean isReservation;

    private boolean provideFakeInvoice;

    private LocalDateTime returnDate;

    private LocalDateTime requestedReturnDate;

    private Stockmove outStockmove;

    private Stockmove inStockmove;

    private List<PackingSlip> packingSlips;

    private Invoice invoice;

    private boolean sent;

    private LocalDateTime sendDate;

    private String sentBy;

    private boolean completed;

    private LocalDateTime completionDate;

    private String completedBy;

    private Note note;

    public Commission(Model.Invoicing.Commission c,List<InvoiceDetailLine> invoiceDetailLines, double invoicestart, boolean invoiceready) {
        this.id = c.getId();
        this.createdBy = c.getCreatedBy().getName();
        this.customer = new Customer(c.getCustomer());
        this.creationDate = c.getCreationDate();
        this.lastModifiedDate = c.getLastModifiedDate();
        this.modifiedBy = c.getModifiedBy().getName();
        this.startDate = c.getStartDate();
        this.isReservation = c.isReservation();
        this.provideFakeInvoice = c.isProvideFakeInvoice();
        this.returnDate = c.getReturnDate();
        this.requestedReturnDate = c.getRequestedReturnDate();
        this.outStockmove = new Stockmove(c.getOutStockmove());
        this.inStockmove = new Stockmove(c.getInStockmove());
        this.packingSlips = c.getPackingSlips().stream().map(PackingSlip::new).collect(Collectors.toList());
        if(c.getInvoice()!= null) {
            this.invoice = new Invoice(c.getInvoice(), invoiceDetailLines, invoiceready);
        }else{
            this.invoice = null;
        }
        this.sent = c.isSent();
        this.sendDate = c.getSendDate();
        this.sentBy = c.getSentBy().getName();
        this.completed = c.isCompleted();
        this.completionDate = c.getCompletionDate();
        this.completedBy = c.getCompletedBy().getName();
        this.note = c.getNote();
    }
}
