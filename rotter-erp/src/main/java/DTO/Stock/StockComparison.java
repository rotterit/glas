package DTO.Stock;

import DTO.Product.Product;

import java.util.Map;

/**
 * Created by thijs on 21-4-2015.
 */
public class StockComparison {

    private Map<Product, Integer> stock1;
    private Map<Product, Integer> stock2;
    private int stockId;

    public StockComparison() {
    }

    public Map<Product, Integer> getStock1() {
        return stock1;
    }

    public void setStock1(Map<Product, Integer> stock1) {
        this.stock1 = stock1;
    }

    public Map<Product, Integer> getStock2() {
        return stock2;
    }

    public void setStock2(Map<Product, Integer> stock2) {
        this.stock2 = stock2;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }
}
