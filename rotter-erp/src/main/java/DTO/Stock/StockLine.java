package DTO.Stock;


/**
 * Created by thijs on 20-4-2015.
 */

public class StockLine {

    private int id;

    private int stockId;

    private String product;

    private long productId;

    private int amount;

    private String size;

    private String color;

    private String producttype;

    private String design;

    public StockLine(Model.FinishedStock.StockLine s) {
        this.id = s.getId();
        this.stockId = s.getStock().getId();
        this.product = s.getProduct().getProductCode();
        this.productId = s.getProduct().getId();
        this.amount = s.getAmount();
        this.size = s.getProduct().getMySize().getName();
        this.color = s.getProduct().getColor().getName();
        this.producttype = s.getProduct().getProductType().getName();
        this.design = s.getProduct().getDesign().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getProducttype() {
        return producttype;
    }

    public void setProducttype(String producttype) {
        this.producttype = producttype;
    }

    public String getDesign() {
        return design;
    }

    public void setDesign(String design) {
        this.design = design;
    }
}
