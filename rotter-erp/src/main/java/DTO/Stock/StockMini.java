

package DTO.Stock;

/**
 * Created by thijs on 22-5-2015.
 */
public class StockMini {

    private int stockid;

    private String stockname;

    public StockMini(Model.Stock.Stock s) {
        this.stockid = s.getId();
        this.stockname = s.getName();
    }

    public StockMini(Stock s) {

    }

    public int getStockid() {
        return stockid;
    }

    public void setStockid(int stockid) {
        this.stockid = stockid;
    }

    public String getStockname() {
        return stockname;
    }

    public void setStockname(String stockname) {
        this.stockname = stockname;
    }
}
