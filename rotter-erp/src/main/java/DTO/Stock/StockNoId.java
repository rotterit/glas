package DTO.Stock;

import Model.Misc.Note;

/**
 * Created by thijs on 14-5-2015.
 */
public class StockNoId {

    private String name;

    private int locationid;

    private boolean raw;

    private String note;

    public StockNoId() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLocationid() {
        return locationid;
    }

    public void setLocationid(int locationid) {
        this.locationid = locationid;
    }

    public boolean isRaw() {
        return raw;
    }

    public void setRaw(boolean isRaw) {
        this.raw = isRaw;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
