package DTO.Stock;

import Model.Misc.Note;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 20-4-2015.
 */

public class Stock {

    private int id;

    private String name;

    private String location;

    private int locationid;

    private List<StockLine> stockLines;

    private boolean raw;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String createdBy;

    private String modifiedBy;

    private Note note;

    public Stock(Model.Stock.Stock s) {
        this.stockLines = s.getStockLines().stream().map(StockLine::new).collect(Collectors.toList());
        this.id = s.getId();
        this.name = s.getName();
        this.location = s.getLocation().getDescription();
        this.locationid = s.getLocation().getId();
        this.raw = s.isRaw();
        this.note = s.getNote();
        this.creationDate = s.getCreationDate();
        this.lastModifiedDate = s.getLastModifiedDate();
        this.createdBy = s.getCreatedBy().getName();
        this.modifiedBy = s.getModifiedBy().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getLocationid() {
        return locationid;
    }

    public void setLocationid(int locationid) {
        this.locationid = locationid;
    }

    public List<StockLine> getStockLines() {
        return stockLines;
    }

    public void setStockLines(List<StockLine> stockLines) {
        this.stockLines = stockLines;
    }

    public boolean isRaw() {
        return raw;
    }

    public void setRaw(boolean isRaw) {
        this.raw = isRaw;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
