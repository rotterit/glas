package DTO.Stock;

import Model.Misc.Note;

/**
 * Created by thijs on 14-5-2015.
 */
public class StockEdit {

    private int id;

    private String name;

    private Note note;

    public StockEdit() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNote(Note note) {
        this.note = note;
    }

    public Note getNote() {
        return note;
    }
}
