package DTO.Worksheet;

import DTO.Quality.Quality;

import java.util.List;

/**
 * Created by thijs on 26-5-2015.
 */
public class WorksheetLine {

    public long stockmoveitemid;

    public List<QualityLine> qualityLines;

    public long getStockmoveitemid() {
        return stockmoveitemid;
    }

    public void setStockmoveitemid(long stockmoveitemid) {
        this.stockmoveitemid = stockmoveitemid;
    }

    public List<QualityLine> getQualityLines() {
        return qualityLines;
    }

    public void setQualityLines(List<QualityLine> qualityLines) {
        this.qualityLines = qualityLines;
    }
}
