package DTO.Worksheet;

import DTO.MaterialMove.Materialmove;
import DTO.StockMove.Stockmove;

import java.time.LocalDateTime;

/**
 * Created by thijs on 20-4-2015.
 */

public class Worksheet {

    private long id;

    private LocalDateTime createDate;

    private LocalDateTime startDate;

    private LocalDateTime finishedDate;

    private Materialmove materialmove;

    private Stockmove stockmove;

    private String creator;

    private String executor;

    private String note;
    private int noteId;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private boolean started;

    private boolean completed;

    private boolean compromised;

    public Worksheet(Model.Invoicing.Worksheet w) {
        this.id = w.getId();
        this.createDate = w.getCreateDate();
        this.startDate = w.getStartDate();
        this.finishedDate = w.getFinishedDate();
        this.materialmove = new Materialmove(w.getMaterialmove());
        this.stockmove = new Stockmove(w.getStockmove());
        this.creator = w.getCreator().getName();
        this.executor = w.getExecutor().getName();
        this.note = w.getNote().getContent();
        this.noteId = w.getNote().getId();
        this.lastModifiedDate = w.getLastModifiedDate();
        this.modifiedBy = w.getModifiedBy().getName();
        this.started = w.isStarted();
        this.completed = w.isCompleted();
        this.compromised = w.getStockmove().getStockmoveItems().stream().anyMatch(s->s.isCompromised());
    }

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getFinishedDate() {
        return finishedDate;
    }

    public void setFinishedDate(LocalDateTime finishedDate) {
        this.finishedDate = finishedDate;
    }

    public Materialmove getMaterialmove() {
        return materialmove;
    }

    public void setMaterialmove(Materialmove materialmove) {
        this.materialmove = materialmove;
    }

    public Stockmove getStockmove() {
        return stockmove;
    }

    public void setStockmove(Stockmove stockmove) {
        this.stockmove = stockmove;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public boolean isCompromised() {
        return compromised;
    }

    public void setCompromised(boolean compromised) {
        this.compromised = compromised;
    }
}
