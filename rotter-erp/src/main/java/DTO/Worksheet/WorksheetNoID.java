package DTO.Worksheet;

import java.util.List;

/**
 * Created by thijs on 26-5-2015.
 */
public class WorksheetNoID {

    private int executorid;

    private String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getExecutorid() {
        return executorid;
    }

    public void setExecutorid(int executorid) {
        this.executorid = executorid;
    }

    private List<WorksheetLine> worksheetLineList;

    public List<WorksheetLine> getWorksheetLineList() {
        return worksheetLineList;
    }

    public void setWorksheetLineList(List<WorksheetLine> worksheetLineList) {
        this.worksheetLineList = worksheetLineList;
    }
}
