package DTO.Worksheet;

/**
 * Created by thijs on 26-5-2015.
 */
public class QualityLine {

    private int qualityId;

    private int amount;

    private int stockId;

    public int getQualityId() {
        return qualityId;
    }

    public void setQualityId(int qualityId) {
        this.qualityId = qualityId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }
}
