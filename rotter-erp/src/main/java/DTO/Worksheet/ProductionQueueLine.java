package DTO.Worksheet;

import Model.FinishedStock.StockCorrection;
import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.Invoice;

import java.time.LocalDateTime;

/**
 * Created by thijs on 26-5-2015.
 */
public class ProductionQueueLine {

    private long productid;

    private String productCode;

    private String productName;

    private String productType;
    private int productTypeId;

    private String design;
    private int designId;

    private String size;
    private int sizeId;

    private String color;
    private int colorId;

    private int amount;

    private String customer;

    private double percentagePayed;

    private LocalDateTime creationDate;

    private long stockmoveItemId;

    private int productionTime;

    private long stockCorrectionId;

    public ProductionQueueLine(StockmoveItem stockmoveItem, Invoice invoice) {
        this.productid = stockmoveItem.getProduct().getId();
        this.productCode = stockmoveItem.getProduct().getProductCode();
        this.productName = stockmoveItem.getProduct().getName();
        this.productType = stockmoveItem.getProduct().getProductType().getName();
        this.productTypeId = stockmoveItem.getProduct().getProductType().getId();
        this.design = stockmoveItem.getProduct().getDesign().getName();
        this.designId = stockmoveItem.getProduct().getDesign().getId();
        this.size = stockmoveItem.getProduct().getMySize().getName();
        this.sizeId = stockmoveItem.getProduct().getMySize().getId();
        this.color = stockmoveItem.getProduct().getColor().getName();
        this.colorId = stockmoveItem.getProduct().getColor().getId();
        this.amount = stockmoveItem.getAmount();
        this.customer = invoice.getCustomer().getName();
        this.percentagePayed = (invoice.getPayment().stream().mapToDouble(m->m.getAmount()).sum())/(invoice.getPrice()*100);
        this.creationDate = invoice.getCreationDate();
        this.stockmoveItemId = stockmoveItem.getId();
        this.productionTime = stockmoveItem.getAmount() * stockmoveItem.getProduct().getAvgTime();
        this.stockCorrectionId = 0;
    }

    public ProductionQueueLine(StockmoveItem stockmoveItem, StockCorrection stockCorrection){
        this.productid = stockmoveItem.getProduct().getId();
        this.productCode = stockmoveItem.getProduct().getProductCode();
        this.productName = stockmoveItem.getProduct().getName();
        this.productType = stockmoveItem.getProduct().getProductType().getName();
        this.productTypeId = stockmoveItem.getProduct().getProductType().getId();
        this.design = stockmoveItem.getProduct().getDesign().getName();
        this.designId = stockmoveItem.getProduct().getDesign().getId();
        this.size = stockmoveItem.getProduct().getMySize().getName();
        this.sizeId = stockmoveItem.getProduct().getMySize().getId();
        this.color = stockmoveItem.getProduct().getColor().getName();
        this.colorId = stockmoveItem.getProduct().getColor().getId();
        this.amount = stockmoveItem.getAmount();
        this.customer = "rotter";
        this.percentagePayed = 0;
        this.creationDate = stockCorrection.getCreationDate();
        this.stockmoveItemId = stockmoveItem.getId();
        this.productionTime = stockmoveItem.getAmount() * stockmoveItem.getProduct().getAvgTime();
        this.stockCorrectionId = stockCorrection.getId();
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getDesign() {
        return design;
    }

    public void setDesign(String design) {
        this.design = design;
    }

    public int getDesignId() {
        return designId;
    }

    public void setDesignId(int designId) {
        this.designId = designId;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public long getProductid() {
        return productid;
    }

    public void setProductid(long productid) {
        this.productid = productid;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public double getPercentagePayed() {
        return percentagePayed;
    }

    public void setPercentagePayed(double percentagePayed) {
        this.percentagePayed = percentagePayed;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public long getStockmoveItemId() {
        return stockmoveItemId;
    }

    public void setStockmoveItemId(int stockmoveItemId) {
        this.stockmoveItemId = stockmoveItemId;
    }

    public void setStockmoveItemId(long stockmoveItemId) {
        this.stockmoveItemId = stockmoveItemId;
    }

    public int getProductionTime() {
        return productionTime;
    }

    public void setProductionTime(int productionTime) {
        this.productionTime = productionTime;
    }

    public long getStockCorrectionId() {
        return stockCorrectionId;
    }

    public void setStockCorrectionId(long stockCorrectionId) {
        this.stockCorrectionId = stockCorrectionId;
    }
}
