package DTO.Series;

/**
 * Created by thijs on 14-5-2015.
 */
public class SeriesNoId {

    private String name;

    private String description;

    private int supplierid;

    public SeriesNoId() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(int supplierid) {
        this.supplierid = supplierid;
    }

}
