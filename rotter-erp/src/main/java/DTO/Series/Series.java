package DTO.Series;

import java.time.LocalDateTime;

/**
 * Created by thijs on 8-5-2015.
 */
public class Series {

    private int id;

    private String name;

    private String description;

    private String suppliername;

    private int supplierid;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String createdBy;

    private String modifiedBy;

    public Series(Model.RawMaterial.Series s) {
        this.id = s.getId();
        this.name = s.getName();
        this.description = s.getDescription();
        this.supplierid = s.getSupplier().getId();
        this.suppliername = s.getSupplier().getName();
        this.creationDate = s.getCreationDate();
        this.lastModifiedDate = s.getLastModifiedDate();
        this.createdBy = s.getCreatedBy().getName();
        this.modifiedBy = s.getModifiedBy().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(int supplierid) {
        this.supplierid = supplierid;
    }

    public String getSuppliername() {
        return suppliername;
    }

    public void setSuppliername(String suppliername) {
        this.suppliername = suppliername;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
