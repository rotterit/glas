package DTO.Color;

import java.time.LocalDateTime;

/**
 * Created by thijs on 13-5-2015.
 */
public class ColorNoId {


    private String name;

    private String code;
    //hexadecimale waarde voor frontend;

    private String value;

    public ColorNoId() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
