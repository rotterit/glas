package DTO.Color;

import Model.User.User;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

/**
 * Created by thijs on 20-4-2015.
 */

public class Color {

    private int id;

    private String createdBy;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private String name;

    private String code;
    //hexadecimale waarde voor frontend;

    private String value;

    public Color(Model.FinishedProduct.Color c) {
        this.id = c.getId();
        this.createdBy = c.getCreatedBy().getName();
        this.lastModifiedDate = c.getLastModifiedDate();
        this.name = c.getName();
        this.code = c.getCode();
        this.value = c.getValue();
        this.modifiedBy = c.getModifiedBy().getName();
        this.creationDate = c.getCreationDate();
    }

    public Color() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
