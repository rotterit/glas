package DTO.Product;

import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.Invoice;

/**
 * Created by thijs on 21-5-2015.
 */
public class ProductInvoiceDetail {

    private long invoiceId;

    private int amount;

    private String customername;

    public ProductInvoiceDetail(int invoiceId, int amount, String customername) {
        this.invoiceId = invoiceId;
        this.amount = amount;
        this.customername = customername;
    }

    public ProductInvoiceDetail(Invoice i, StockmoveItem si) {
        this.invoiceId = i.getId();
        this.amount = si.getAmount();
        this.customername = i.getCustomer().getName();
    }

    public long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(int invoiceId) {
        this.invoiceId = invoiceId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }
}
