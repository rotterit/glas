package DTO.Product;

import Model.Stock.Stock;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 29-5-2015.
 */
public class MiniProduct {
    private long id;
    private String productCode;
    private String name;
    private String supplier;
    private int baseTime;
    private int minTime;
    private int avgTime;
    private int maxTime;
    private double price;
    private int colorid;
    private String color;
    private double Actualprice;
    private int categoryId;
    private String categoryName;
    private List<Description> descriptions;
    private int size;
    private String sizeCode;
    private int productType;
    private String productTypeName;
    private int manufacturingtype;
    private String manufacturingTypeName;
    private int design;
    private String designName;
    private LocalDateTime creationDate;
    private LocalDateTime lastModifiedDate;
    private String modifiedBy;
    private String createdBy;
    private DTO.Stock.Stock stock;

    //productcode, description, color, design,size, producttype,

    public MiniProduct(Model.FinishedProduct.Product p, Stock stock) {
        this.id=p.getId();
        this.productCode=p.getProductCode();
        this.name=p.getName();
        this.supplier=p.getSupplier().getName();
        this.baseTime=p.getBaseTime();
        this.minTime=p.getMinTime();
        this.maxTime=p.getMaxTime();
        this.avgTime=p.getAvgTime();
        this.price=p.getPrice();
        this.colorid = p.getColor().getId();
        this.color=p.getColor().getName();
        this.Actualprice=p.getActualprice();
        this.categoryId=p.getCategory().getId();
        this.categoryName = p.getCategory().getName();
        this.descriptions=p.getDescriptions().stream().map(DTO.Product.Description::new).collect(Collectors.toList());
        this.size=p.getMySize().getId();
        this.sizeCode = p.getMySize().getName();
        this.productType=p.getProductType().getId();
        this.productTypeName=p.getProductType().getName();
        this.manufacturingtype=p.getManufacturingtype().getId();
        this.manufacturingTypeName = p.getManufacturingtype().getName();
        this.design=p.getDesign().getId();
        this.designName = p.getDesign().getName();
        this.creationDate = p.getCreationDate();
        this.lastModifiedDate = p.getLastModifiedDate();
        this.modifiedBy = p.getModifiedBy().getName();
        this.createdBy = p.getCreatedBy().getName();
        this.stock = new DTO.Stock.Stock(stock);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public int getBaseTime() {
        return baseTime;
    }

    public void setBaseTime(int baseTime) {
        this.baseTime = baseTime;
    }

    public int getMinTime() {
        return minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    public int getAvgTime() {
        return avgTime;
    }

    public void setAvgTime(int avgTime) {
        this.avgTime = avgTime;
    }

    public int getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getColorid() {
        return colorid;
    }

    public void setColorid(int colorid) {
        this.colorid = colorid;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getActualprice() {
        return Actualprice;
    }

    public void setActualprice(double actualprice) {
        Actualprice = actualprice;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<Description> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<Description> descriptions) {
        this.descriptions = descriptions;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSizeCode() {
        return sizeCode;
    }

    public void setSizeCode(String sizeCode) {
        this.sizeCode = sizeCode;
    }

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public int getManufacturingtype() {
        return manufacturingtype;
    }

    public void setManufacturingtype(int manufacturingtype) {
        this.manufacturingtype = manufacturingtype;
    }

    public String getManufacturingTypeName() {
        return manufacturingTypeName;
    }

    public void setManufacturingTypeName(String manufacturingTypeName) {
        this.manufacturingTypeName = manufacturingTypeName;
    }

    public int getDesign() {
        return design;
    }

    public void setDesign(int design) {
        this.design = design;
    }

    public String getDesignName() {
        return designName;
    }

    public void setDesignName(String designName) {
        this.designName = designName;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public DTO.Stock.Stock getStock() {
        return stock;
    }

    public void setStock(DTO.Stock.Stock stock) {
        this.stock = stock;
    }
}
