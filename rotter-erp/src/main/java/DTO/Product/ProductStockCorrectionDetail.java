package DTO.Product;

import Model.FinishedStock.StockCorrection;
import Model.FinishedStock.StockmoveItem;

/**
 * Created by thijs on 21-5-2015.
 */
public class ProductStockCorrectionDetail {

    private long stockCorrectionId;

    private int amount;

    private String reason;

    public ProductStockCorrectionDetail(int stockCorrectionId, int amount, String reason) {
        this.stockCorrectionId = stockCorrectionId;
        this.amount = amount;
        this.reason = reason;
    }

    public ProductStockCorrectionDetail(StockCorrection s, StockmoveItem si) {
        this.stockCorrectionId = s.getId();
        this.amount = si.getAmount();
        this.reason = s.getReason();
    }

    public long getStockCorrectionId() {
        return stockCorrectionId;
    }

    public void setStockCorrectionId(int stockCorrectionId) {
        this.stockCorrectionId = stockCorrectionId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
