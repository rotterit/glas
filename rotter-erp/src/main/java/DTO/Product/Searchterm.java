package DTO.Product;

import DTO.Category.Category;
import DTO.Color.Color;
import DTO.ManufacturingType.ManufacturingType;
import DTO.ProductType.ProductType;
import DTO.Size.Size;
import DTO.Supplier.Supplier;
import DTO.Design.Design;

import java.util.List;

/**
 * Created by thijs on 22-5-2015.
 */
public class SearchTerm {

    private List<ProductType> productTypes;

    private List<Size> sizes;

    private List<Design> designs;

    private List<ManufacturingType> manufacturingTypes;

    private List<Category> categorys;

    private List<Supplier> suppliers;

    private List<Color> colors;

    public SearchTerm(List<ProductType> productTypes, List<Size> sizes, List<Design> designs, List<ManufacturingType> manufacturingTypes, List<Category> categorys, List<Supplier> suppliers, List<Color> colors) {
        this.productTypes = productTypes;
        this.sizes = sizes;
        this.designs = designs;
        this.manufacturingTypes = manufacturingTypes;
        this.categorys = categorys;
        this.suppliers = suppliers;
        this.colors = colors;
    }

    public List<ProductType> getProductTypes() {
        return productTypes;
    }

    public void setProductTypes(List<ProductType> productTypes) {
        this.productTypes = productTypes;
    }

    public List<Size> getSizes() {
        return sizes;
    }

    public void setSizes(List<Size> sizes) {
        this.sizes = sizes;
    }

    public List<Design> getDesigns() {
        return designs;
    }

    public void setDesigns(List<Design> designs) {
        this.designs = designs;
    }

    public List<ManufacturingType> getManufacturingTypes() {
        return manufacturingTypes;
    }

    public void setManufacturingTypes(List<ManufacturingType> manufacturingTypes) {
        this.manufacturingTypes = manufacturingTypes;
    }

    public List<Category> getCategorys() {
        return categorys;
    }

    public void setCategorys(List<Category> categorys) {
        this.categorys = categorys;
    }

    public List<Supplier> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<Supplier> suppliers) {
        this.suppliers = suppliers;
    }

    public List<Color> getColors() {
        return colors;
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }
}
