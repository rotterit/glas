package DTO.Product;

import Model.FinishedProduct.Description;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 20-4-2015.
 */

public class Product {

    private long id;

    private String productCode;

    private String name;

    private String supplier;

    private int baseTime;

    private int minTime;

    private int maxTime;

    private int avgTime;

    //retail price without VAT
    private double price;

    private String color;
    private int colorid;

    private double Actualprice;

    private int categoryId;
    private String categoryName;

    private List<DTO.Product.Description>descriptions;

    private int size;
    private String sizeCode;

    //vase, whiskey glass, coloured tumblr.....
    private int productType;
    private String productTypeName;

    //mouthblown, crystal, handmade
    private int manufacturingtype;
    private String manufacturingTypeName;

    private int design;
    private String designName;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private String createdBy;

    private int stockAmount;

    private int reservedStock;

    //private int onComission;

    private int inProduction;

    private int inProductionQueue;

    public Product(Model.FinishedProduct.Product p, int stockAmount, int reservedStock, int onComission, int inProduction, int inProductionQueue, List<ProductInvoiceDetail> productInvoiceDetails, List<ProductStockCorrectionDetail> productStockCorrectionDetails) {
        this.id=p.getId();
        this.productCode=p.getProductCode();
        this.name=p.getName();
        this.supplier=p.getSupplier().getName();
        this.baseTime=p.getBaseTime();
        this.minTime=p.getMinTime();
        this.maxTime=p.getMaxTime();
        this.avgTime=p.getAvgTime();
        this.price=p.getPrice();
        this.colorid = p.getColor().getId();
        this.color=p.getColor().getName();
        this.Actualprice=p.getActualprice();
        this.categoryId=p.getCategory().getId();
        this.categoryName = p.getCategory().getName();
        this.descriptions=p.getDescriptions().stream().map(DTO.Product.Description::new).collect(Collectors.toList());
        this.size=p.getMySize().getId();
        this.sizeCode = p.getMySize().getName();
        this.productType=p.getProductType().getId();
        this.productTypeName=p.getProductType().getName();
        this.manufacturingtype=p.getManufacturingtype().getId();
        this.manufacturingTypeName = p.getManufacturingtype().getName();
        this.design=p.getDesign().getId();
        this.designName = p.getDesign().getName();
        this.creationDate = p.getCreationDate();
        this.lastModifiedDate = p.getLastModifiedDate();
        this.modifiedBy = p.getModifiedBy().getName();
        this.createdBy = p.getCreatedBy().getName();
        this.stockAmount = stockAmount;
        this.reservedStock =reservedStock;
        //this.onComission = onComission;
        this.inProduction = inProduction;
        this.inProductionQueue = inProductionQueue;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public int getBaseTime() {
        return baseTime;
    }

    public void setBaseTime(int baseTime) {
        this.baseTime = baseTime;
    }

    public int getMinTime() {
        return minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    public int getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public int getAvgTime() {
        return avgTime;
    }

    public void setAvgTime(int avgTime) {
        this.avgTime = avgTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getColorid() {
        return colorid;
    }

    public void setColorid(int colorid) {
        this.colorid = colorid;
    }

    public double getActualprice() {
        return Actualprice;
    }

    public void setActualprice(double actualprice) {
        Actualprice = actualprice;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<DTO.Product.Description> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<DTO.Product.Description> descriptions) {
        this.descriptions = descriptions;
    }

    public int getStockAmount() {
        return stockAmount;
    }

    public void setStockAmount(int stockAmount) {
        this.stockAmount = stockAmount;
    }

    public int getReservedStock() {
        return reservedStock;
    }

    public void setReservedStock(int reservedStock) {
        this.reservedStock = reservedStock;
    }

//    public int getOnComission() {
//        return onComission;
//    }
//
//    public void setOnComission(int onComission) {
//        this.onComission = onComission;
//    }

    public int getInProduction() {
        return inProduction;
    }

    public void setInProduction(int inProduction) {
        this.inProduction = inProduction;
    }

    public int getInProductionQueue() {
        return inProductionQueue;
    }

    public void setInProductionQueue(int inProductionQueue) {
        this.inProductionQueue = inProductionQueue;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSizeCode() {
        return sizeCode;
    }

    public void setSizeCode(String sizeCode) {
        this.sizeCode = sizeCode;
    }

    public int getProductType() {
        return productType;
    }

    public void setProductType(int productType) {
        this.productType = productType;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public int getManufacturingtype() {
        return manufacturingtype;
    }

    public void setManufacturingtype(int manufacturingtype) {
        this.manufacturingtype = manufacturingtype;
    }

    public String getManufacturingTypeName() {
        return manufacturingTypeName;
    }

    public void setManufacturingTypeName(String manufacturingTypeName) {
        this.manufacturingTypeName = manufacturingTypeName;
    }

    public int getDesign() {
        return design;
    }

    public void setDesign(int design) {
        this.design = design;
    }

    public String getDesignName() {
        return designName;
    }

    public void setDesignName(String designName) {
        this.designName = designName;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
