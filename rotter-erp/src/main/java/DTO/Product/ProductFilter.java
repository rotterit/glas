package DTO.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 22-5-2015.
 */
public class ProductFilter {

    private List<Integer> productTypeids;

    private List<Integer> sizeids;

    private List<Integer> designids;

    private List<Integer> manufacturingTypeids;

    private List<Integer> categoryids;

    private List<Integer> supplierids;

    private List<Integer> colorids;

    public ProductFilter() {
        this.productTypeids = new ArrayList<>();
        this.sizeids = new ArrayList<>();
        this.designids = new ArrayList<>();
        this.manufacturingTypeids = new ArrayList<>();
        this.categoryids = new ArrayList<>();
        this.supplierids = new ArrayList<>();
        this.colorids = new ArrayList<>();
    }

    public List<Integer> getProductTypeids() {
        return productTypeids;
    }

    public void setProductTypeids(List<Integer> productTypeids) {
        this.productTypeids = productTypeids;
    }

    public List<Integer> getSizeids() {
        return sizeids;
    }

    public void setSizeids(List<Integer> sizeids) {
        this.sizeids = sizeids;
    }

    public List<Integer> getDesignids() {
        return designids;
    }

    public List<Integer> getCategoryids() {
        return categoryids;
    }

    public void setCategoryids(List<Integer> categoryids) {
        this.categoryids = categoryids;
    }

    public void setDesignids(List<Integer> designids) {
        this.designids = designids;
    }

    public List<Integer> getManufacturingTypeids() {
        return manufacturingTypeids;
    }

    public void setManufacturingTypeids(List<Integer> manufacturingTypeids) {
        this.manufacturingTypeids = manufacturingTypeids;
    }

    public List<Integer> getSupplierids() {
        return supplierids;
    }

    public void setSupplierids(List<Integer> supplierids) {
        this.supplierids = supplierids;
    }

    public List<Integer> getColorids() {
        return colorids;
    }

    public void setColorids(List<Integer> colorids) {
        this.colorids = colorids;
    }
}
