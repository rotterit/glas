package DTO.Product;

import java.time.LocalDateTime;

/**
 * Created by thijs on 28-5-2015.
 */
public class Description {

    private long id;

    private String createdBy;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private String languageCode;

    private String description;

    private String languageName;

    public Description(Model.FinishedProduct.Description d) {
        this.id = d.getId();
        this.createdBy = d.getCreatedBy().getName();
        this.creationDate = d.getCreationDate();
        this.lastModifiedDate = d.getLastModifiedDate();
        this.modifiedBy = d.getModifiedBy().getName();
        this.languageCode = d.getLanguageCode();
        this.description = d.getDescription();
        this.languageName = d.getLanguageName();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }
}
