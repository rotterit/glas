package DTO.Product;

import Model.FinishedProduct.Description;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by thijs on 21-5-2015.
 */
public class ProductEdit {

    private long id;

    private String name;

    private int baseTime;

    private int minTime;

    private int maxTime;

    //retail price without VAT
    private double price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBaseTime() {
        return baseTime;
    }

    public void setBaseTime(int baseTime) {
        this.baseTime = baseTime;
    }

    public int getMinTime() {
        return minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    public int getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
