package DTO.Product;

import Model.RawMaterial.RawMaterial;

import java.util.List;

/**
 * Created by thijs on 20-5-2015.
 */
public class ProductNoId {

    private String name;
    //wss voor grouping

    private int supplierid;

    private int baseTime;

    private int minTime;

    private int maxTime;

    //retail price without VAT
    private double price;

    private int colorId;

    private double materialprice;

    private int categoryId;

    private List<DescriptionNoId> descriptions;

    private int sizeId;
    //vase, whiskey glass, coloured tumblr.....

    private int productTypeId;
    //mouthblown, crystal, handmade

    private int manufacturingtypeId;

    private int designId;

    private List<Integer> StockIds;

    public List<Integer> getStockIds() {
        return StockIds;
    }

    public void setStockIds(List<Integer> stockIds) {
        StockIds = stockIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(int supplierid) {
        this.supplierid = supplierid;
    }

    public int getBaseTime() {
        return baseTime;
    }

    public void setBaseTime(int baseTime) {
        this.baseTime = baseTime;
    }

    public int getMinTime() {
        return minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    public int getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public double getMaterialprice() {
        return materialprice;
    }

    public void setMaterialprice(double materialprice) {
        this.materialprice = materialprice;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public List<DescriptionNoId> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<DescriptionNoId> descriptions) {
        this.descriptions = descriptions;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public int getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(int productTypeId) {
        this.productTypeId = productTypeId;
    }

    public int getManufacturingtypeId() {
        return manufacturingtypeId;
    }

    public void setManufacturingtypeId(int manufacturingtypeId) {
        this.manufacturingtypeId = manufacturingtypeId;
    }

    public int getDesignId() {
        return designId;
    }

    public void setDesignId(int designId) {
        this.designId = designId;
    }
}
