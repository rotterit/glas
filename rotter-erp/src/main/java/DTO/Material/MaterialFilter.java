package DTO.Material;

import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 8-5-2015.
 */
public class MaterialFilter {

    private List<Integer> productTypeids;

    private List<Integer> sizeids;

    private List<Integer> seriesids;

    private List<Integer> supplierids;

    private List<Integer> qualityids;

    private List<Integer> colorids;

    public MaterialFilter() {
        this.productTypeids = new ArrayList<>();
        this.sizeids = new ArrayList<>();
        this.seriesids = new ArrayList<>();
        this.supplierids = new ArrayList<>();
        this.qualityids = new ArrayList<>();
        this.colorids = new ArrayList<>();
    }

    public List<Integer> getProductTypeids() {
        return productTypeids;
    }

    public void setProductTypeids(List<Integer> productTypeids) {
        this.productTypeids = productTypeids;
    }

    public List<Integer> getSizeids() {
        return sizeids;
    }

    public void setSizeids(List<Integer> sizeids) {
        this.sizeids = sizeids;
    }

    public List<Integer> getSeriesids() {
        return seriesids;
    }

    public void setSeriesids(List<Integer> seriesids) {
        this.seriesids = seriesids;
    }

    public List<Integer> getSupplierids() {
        return supplierids;
    }

    public void setSupplierids(List<Integer> supplierids) {
        this.supplierids = supplierids;
    }

    public List<Integer> getQualityids() {
        return qualityids;
    }

    public void setQualityids(List<Integer> qualityids) {
        this.qualityids = qualityids;
    }

    public List<Integer> getColorids() {
        return colorids;
    }

    public void setColorids(List<Integer> colorids) {
        this.colorids = colorids;
    }

}


