package DTO.Material;

import Model.Misc.Note;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by thijs on 20-4-2015.
 */

public class RawMaterial {

    private String linecolor;

    private int id;

    private String productCode;

    private String rawmaterialcode;

    private String Description;

    private String color;
    private int colorid;

    private String supplier;
    private int supplierid;

    private String quality;
    private int qualityid;

    private double price;

    private String series;
    private int seriesid;

    private String size;
    private int sizeid;

    private String productType;
    private int productTypeid;

    private int amountInStock;

    private int minimumAmountInStock;

    private int minimumorderQuantity;

    private LocalDateTime lastmodified;

    private String lastModifiedBy;

    private LocalDateTime creationDate;

    private String createdBy;

    private String note;

    private int noteId;

    private List<StockDetail> stockDetails;

    public RawMaterial(Model.RawMaterial.RawMaterial r, int amountInStock, int minimumAmountInStock, String color, List<StockDetail> stockDetails) {
        this.id = r.getId();
        this.rawmaterialcode = r.getProductSupplierLine().getRawMaterialCode();
        this.productCode = r.getProductSupplierLine().getInternalReference();
        this.Description = r.getDescription();
        this.color = r.getColor().getName();
        this.colorid = r.getColor().getId();
        this.supplier = r.getSupplier().getName();
        this.supplierid = r.getSupplier().getId();
        this.quality = r.getQuality().getName();
        this.qualityid = r.getQuality().getId();
        this.price = r.getPrice();
        this.series = r.getSeries().getName();
        this.seriesid = r.getSeries().getId();
        this.size = r.getMySize().getName();
        this.sizeid = r.getMySize().getId();
        this.productType = r.getProductType().getName();
        this.productTypeid = r.getProductType().getId();
        this.amountInStock = amountInStock;
        this.minimumAmountInStock = minimumAmountInStock;
        this.lastmodified = r.getLastModifiedDate();
        this.lastModifiedBy = r.getModifiedBy().getName();
        this.creationDate = r.getCreationDate();
        this.createdBy = r.getCreatedBy().getName();
        this.linecolor = color;
        this.minimumorderQuantity = r.getMinimumOrderQuantity();
        this.note = r.getNote().getContent();
        this.noteId = r.getNote().getId();
        this.stockDetails = stockDetails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getRawmaterialcode() {
        return rawmaterialcode;
    }

    public void setRawmaterialcode(String rawmaterialcode) {
        this.rawmaterialcode = rawmaterialcode;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getColorid() {
        return colorid;
    }

    public void setColorid(int colorid) {
        this.colorid = colorid;
    }

    public int getSupplierid() {
        return supplierid;
    }

    public void setSupplierid(int supplierid) {
        this.supplierid = supplierid;
    }

    public int getQualityid() {
        return qualityid;
    }

    public void setQualityid(int qualityid) {
        this.qualityid = qualityid;
    }

    public int getSeriesid() {
        return seriesid;
    }

    public void setSeriesid(int seriesid) {
        this.seriesid = seriesid;
    }

    public int getSizeid() {
        return sizeid;
    }

    public void setSizeid(int sizeid) {
        this.sizeid = sizeid;
    }

    public int getProductTypeid() {
        return productTypeid;
    }

    public void setProductTypeid(int productTypeid) {
        this.productTypeid = productTypeid;
    }

    public int getAmountInStock() {
        return amountInStock;
    }

    public void setAmountInStock(int amountInStock) {
        this.amountInStock = amountInStock;
    }

    public int getMinimumAmountInStock() {
        return minimumAmountInStock;
    }

    public void setMinimumAmountInStock(int minimumAmountInStock) {
        this.minimumAmountInStock = minimumAmountInStock;
    }

    public LocalDateTime getLastmodified() {
        return lastmodified;
    }

    public void setLastmodified(LocalDateTime lastmodified) {
        this.lastmodified = lastmodified;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getLinecolor() {
        return linecolor;
    }

    public void setLinecolor(String linecolor) {
        this.linecolor = linecolor;
    }

    public int getMinimumorderQuantity() {
        return minimumorderQuantity;
    }

    public void setMinimumorderQuantity(int minimumorderQuantity) {
        this.minimumorderQuantity = minimumorderQuantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public int getNoteId() {
        return noteId;
    }

    public void setNoteId(int noteId) {
        this.noteId = noteId;
    }

    public List<StockDetail> getStockDetails() {
        return stockDetails;
    }

    public void setStockDetails(List<StockDetail> stockDetails) {
        this.stockDetails = stockDetails;
    }
}
