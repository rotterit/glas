package DTO.Material;

import Model.RawStock.MaterialStockLine;
import Model.RawStock.MaterialmoveItem;

/**
 * Created by thijs on 19-5-2015.
 */
public class StockDetail {

    private int stockId;

    private String stockName;

    private int amount;

    private String locationName;

    public StockDetail(MaterialStockLine m){
        this.stockId = m.getStock().getId();
        this.stockName = m.getStock().getName();
        this.amount = m.getAmount();
        this.locationName = m.getStock().getLocation().getDescription();
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }
}
