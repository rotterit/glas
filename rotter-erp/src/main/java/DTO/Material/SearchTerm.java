package DTO.Material;

import DTO.Color.Color;
import DTO.ProductType.ProductType;
import DTO.Quality.Quality;
import DTO.Series.Series;
import DTO.Size.Size;
import DTO.Supplier.Supplier;
import Model.FinishedProduct.MySize;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thijs on 8-5-2015.
 */
public class SearchTerm {

    private List<Size> sizes;

    private List<Supplier> suppliers;

    private List<ProductType> productTypes;

    private List<Series> series;

    private List<Color> colors;

    private List<Quality> qualities;

    public SearchTerm(List<Model.RawMaterial.Series> series, List<MySize> mySizes, List<Model.RawMaterial.Supplier> suppliers, List<Model.FinishedProduct.ProductType> productTypes, List<Model.FinishedProduct.Color> colors, List<Model.RawMaterial.Quality> qualities) {
        this.series = new ArrayList<>();
        for(Model.RawMaterial.Series s : series){
            this.series.add(new Series(s));
        }
        this.sizes =  new ArrayList<>();
        for(MySize s : mySizes){
            this.sizes.add(new Size(s));
        }
        this.suppliers =  new ArrayList<>();
        for(Model.RawMaterial.Supplier s : suppliers){
            this.suppliers.add(new Supplier(s));
        }
        this.productTypes =  new ArrayList<>();
        for(Model.FinishedProduct.ProductType s : productTypes){
            this.productTypes.add(new ProductType(s));
        }
        this.colors =  new ArrayList<>();
        for(Model.FinishedProduct.Color s : colors){
            this.colors.add(new Color(s));
        }
        this.qualities =  new ArrayList<>();
        for(Model.RawMaterial.Quality s : qualities){
            this.qualities.add(new Quality(s));
        }
    }

    public List<Size> getSizes() {
        return sizes;
    }

    public void setSizes(List<Size> sizes) {
        this.sizes = sizes;
    }

    public List<Supplier> getSuppliers() {
        return suppliers;
    }

    public void setSuppliers(List<Supplier> suppliers) {
        this.suppliers = suppliers;
    }

    public List<ProductType> getProductTypes() {
        return productTypes;
    }

    public void setProductTypes(List<ProductType> productTypes) {
        this.productTypes = productTypes;
    }

    public List<Series> getSeries() {
        return series;
    }

    public void setSeries(List<Series> series) {
        this.series = series;
    }

    public List<Color> getColors() {
        return colors;
    }

    public void setColors(List<Color> colors) {
        this.colors = colors;
    }

    public List<Quality> getQualities() {
        return qualities;
    }

    public void setQualities(List<Quality> qualities) {
        this.qualities = qualities;
    }
}
