package DTO.Category;

import Model.Misc.Note;

import java.util.List;

/**
 * Created by thijs on 14-5-2015.
 */
public class CategoryNoId {

    private int baseTime;

    private String name;

    private List<CategorySizeNoId> categorySizes;

    private String note;

    public int getBaseTime() {
        return baseTime;
    }

    public void setBaseTime(int baseTime) {
        this.baseTime = baseTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CategorySizeNoId> getCategorySizes() {
        return categorySizes;
    }

    public void setCategorySizes(List<CategorySizeNoId> categorySizes) {
        this.categorySizes = categorySizes;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
