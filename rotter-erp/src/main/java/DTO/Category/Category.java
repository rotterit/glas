package DTO.Category;
import Model.Misc.Note;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by thijs on 14-5-2015.
 */
public class Category {

    private int id;

    private String createdBy;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private int baseTime;

    private String name;
    
    private List<CategorySize> cslist;

    private Note note;

    public Category(Model.FinishedProduct.Category c) {
        this.id = c.getId();
        this.createdBy = c.getCreatedBy().getName();
        this.creationDate = c.getCreationDate();
        this.lastModifiedDate = c.getLastModifiedDate();
        this.modifiedBy = c.getModifiedBy().getName();
        this.baseTime = c.getBaseTime();
        this.name = c.getName();
        this.note = c.getNote();
        this.cslist = c.getCategorySizes().stream().map(CategorySize::new).collect(Collectors.toList());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public int getBaseTime() {
        return baseTime;
    }

    public void setBaseTime(int baseTime) {
        this.baseTime = baseTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CategorySize> getCslist() {
        return cslist;
    }

    public void setCslist(List<CategorySize> cslist) {
        this.cslist = cslist;
    }

    public Note getNote() {
        return note;
    }

    public void setNote(Note note) {
        this.note = note;
    }
}
