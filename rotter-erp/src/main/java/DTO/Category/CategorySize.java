package DTO.Category;

/**
 * Created by thijs on 14-5-2015.
 */
public class CategorySize {

    private int id;

    private int sizeId;

    private String sizeCode;

    private String sizeName;

    private double basePrice;

    private int minTime;

    private int maxTime;

    public CategorySize(Model.FinishedProduct.CategorySize cs) {
        this.id = cs.getId();
        this.sizeId = cs.getMySize().getId();
        this.sizeCode = cs.getMySize().getCode();
        this.minTime = cs.getMinTime();
        this.maxTime = cs.getMaxTime();
        this.sizeName = cs.getMySize().getName();
        this.basePrice = cs.getBasePrice();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeCode() {
        return sizeCode;
    }

    public void setSizeCode(String sizeCode) {
        this.sizeCode = sizeCode;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public int getMinTime() {
        return minTime;
    }

    public void setMinTime(int minTime) {
        this.minTime = minTime;
    }

    public int getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(int maxTime) {
        this.maxTime = maxTime;
    }
}
