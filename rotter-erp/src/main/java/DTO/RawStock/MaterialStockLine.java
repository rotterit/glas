package DTO.RawStock;

/**
 * Created by thijs on 20-4-2015.
 */

public class MaterialStockLine {

    private int id;

    private int stockId;

    private String rawMaterial;

    private String quality;

    private double price;

    private String productType;

    private String size;

    private String supplier;

    private String color;

    private int rawMaterialId;

    private int amount;

    public MaterialStockLine(Model.RawStock.MaterialStockLine m) {
        this.quality = m.getRawMaterial().getQuality().getName();
        this.id = m.getId();
        this.stockId = m.getStock().getId();
        this.rawMaterial = m.getRawMaterial().getProductSupplierLine().getInternalReference();
        this.amount = m.getAmount();
        this.rawMaterialId = m.getRawMaterial().getId();
        this.price = m.getRawMaterial().getPrice();
        this.productType = m.getRawMaterial().getProductType().getName();
        this.size = m.getRawMaterial().getMySize().getName();
        this.supplier = m.getRawMaterial().getSupplier().getName();
        this.color = m.getRawMaterial().getColor().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStockId() {
        return stockId;
    }

    public void setStockId(int stockId) {
        this.stockId = stockId;
    }

    public String getRawMaterial() {
        return rawMaterial;
    }

    public void setRawMaterial(String rawMaterial) {
        this.rawMaterial = rawMaterial;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
