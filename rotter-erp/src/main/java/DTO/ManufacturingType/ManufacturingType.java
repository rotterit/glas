package DTO.ManufacturingType;

import java.time.LocalDateTime;

/**
 * Created by thijs on 20-5-2015.
 */
public class ManufacturingType {

    private int id;

    private String createdBy;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String modifiedBy;

    private String code;

    private String name;

    private String description;

    public ManufacturingType(Model.FinishedProduct.ManufacturingType m) {
        this.id = m.getId();
        this.createdBy = m.getCreatedBy().getName();
        this.creationDate = m.getCreationDate();
        this.lastModifiedDate = m.getLastModifiedDate();
        this.modifiedBy = m.getModifiedBy().getName();
        this.code = m.getCode();
        this.name = m.getName();
        this.description = m.getDescription();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
