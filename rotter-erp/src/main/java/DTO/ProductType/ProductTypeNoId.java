package DTO.ProductType;

/**
 * Created by thijs on 14-5-2015.
 */
public class ProductTypeNoId {

    private String code;

    private String name;

    private String description;

    public ProductTypeNoId() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
