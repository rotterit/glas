package DTO.ProductType;

import java.time.LocalDateTime;

/**
 * Created by thijs on 8-5-2015.
 */
public class ProductType {

    private int id;

    private LocalDateTime creationDate;

    private String code;

    private String name;

    private String description;

    private LocalDateTime modifiedDate;

    private String modifiedBy;

    private String createdBy;
    

    public ProductType(Model.FinishedProduct.ProductType p) {
        this.id = p.getId();
        this.creationDate = p.getCreationDate();
        this.code = p.getCode();
        this.name = p.getName();
        this.description = p.getDescription();
        this.createdBy = p.getCreatedBy().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(LocalDateTime modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
