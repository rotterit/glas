package DTO.Role;

import javax.persistence.*;

/**
 * Created by thijs on 20-4-2015.
 */

public class Role {

    private int id;

    private String name;

    public Role() {
    }

    public Role(Model.User.Role r) {
        this.id = r.getId();
        this.name = r.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
