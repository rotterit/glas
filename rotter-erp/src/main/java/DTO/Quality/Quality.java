package DTO.Quality;

import java.time.LocalDateTime;

/**
 * Created by thijs on 20-4-2015.
 */

public class Quality {

    private int id;

    private String name;

    private LocalDateTime creationDate;

    private LocalDateTime lastModifiedDate;

    private String lastModifiedBy;

    private String createdBy;

    public Quality(Model.RawMaterial.Quality s) {
        this.id = s.getId();
        this.name = s.getName();
        this.creationDate = s.getCreationDate();
        this.lastModifiedDate = s.getLastModifiedDate();
        this.lastModifiedBy = s.getModifiedBy().getName();
        this.createdBy = s.getCreatedBy().getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(LocalDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
