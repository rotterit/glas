package DTO.Design;

/**
 * Created by thijs on 14-5-2015.
 */
public class DesignNoId {

    private String designCode;

    private String name;

    private String description;

    public DesignNoId() {
    }

    public String getDesignCode() {
        return designCode;
    }

    public void setDesignCode(String designCode) {
        this.designCode = designCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
