package DTO.Customer;

import Model.User.User;

import javax.persistence.Column;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

/**
 * Created by thijs on 14-5-2015.
 */
public class Customer {

    private int id;

    private String name;

    private String description;

    private double baseVAT;

    private double baseDiscountPercentage;

    private double baseLineDiscountPercentage;

    private double basePackingCostPercentage;

    private String companyName;

    private String adress;

    private String ZIP;

    private String city;

    private String country;

    private String TEL;

    private String email;

    private String VATnumber;

    public Customer(Model.Invoicing.Customer c) {
        this.id = c.getId();
        this.name = c.getName();
        this.description = c.getDescription();
        this.baseVAT = c.getBaseVAT();
        this.baseDiscountPercentage = c.getBaseDiscountPercentage();
        this.baseLineDiscountPercentage = c.getBaseLineDiscountPercentage();
        this.basePackingCostPercentage = c.getBasePackingCostPercentage();
        this.companyName = c.getCompanyName();
        this.adress = c.getAdress();
        this.ZIP = c.getZIP();
        this.city = c.getCity();
        this.country = c.getCountry();
        this.TEL = c.getTEL();
        this.email = c.getEmail();
        this.VATnumber = c.getVATnumber();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getBaseVAT() {
        return baseVAT;
    }

    public void setBaseVAT(double baseVAT) {
        this.baseVAT = baseVAT;
    }

    public double getBaseDiscountPercentage() {
        return baseDiscountPercentage;
    }

    public void setBaseDiscountPercentage(double baseDiscountPercentage) {
        this.baseDiscountPercentage = baseDiscountPercentage;
    }

    public double getBaseLineDiscountPercentage() {
        return baseLineDiscountPercentage;
    }

    public void setBaseLineDiscountPercentage(double baseLineDiscountPercentage) {
        this.baseLineDiscountPercentage = baseLineDiscountPercentage;
    }

    public double getBasePackingCostPercentage() {
        return basePackingCostPercentage;
    }

    public void setBasePackingCostPercentage(double basePackingCostPercentage) {
        this.basePackingCostPercentage = basePackingCostPercentage;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getZIP() {
        return ZIP;
    }

    public void setZIP(String ZIP) {
        this.ZIP = ZIP;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTEL() {
        return TEL;
    }

    public void setTEL(String TEL) {
        this.TEL = TEL;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVATnumber() {
        return VATnumber;
    }

    public void setVATnumber(String VATnumber) {
        this.VATnumber = VATnumber;
    }
}
