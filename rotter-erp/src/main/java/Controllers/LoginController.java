package Controllers;


import DAL.UserDAO;
import DTO.User.User;
import Security.Signer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;


/**
 * Created by thijs on 5-5-2015.
 */
@RestController
public class LoginController {

    private Logger logger = LoggerFactory.getLogger(LoginController.class);

    private Logger uilogger = LoggerFactory.getLogger("UI");

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Signer Signer;

    @Transactional
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<User> login(String username, String password){
        Model.User.User u;
        try {
            u = userDAO.getUserByName(username);
        }catch(NullPointerException e){
            logger.warn("bad login for user", username);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<User>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
        if(userDAO.checklogin(u.getUsername(), password)){
            User us = new User(u);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(u.getId()));
            logger.info("login accepted for user", username);
            return new ResponseEntity<User>(us,responseHeaders ,HttpStatus.OK);
        }else{
            logger.warn("bad login for user", username);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<User>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/log/warn", method = RequestMethod.POST)
    public ResponseEntity<String> warn(@RequestParam("message") String message){
        uilogger.warn("UI logs warning: "+ message);
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value = "/log/error", method = RequestMethod.POST)
    public ResponseEntity<String> error(@RequestParam("message") String message){
        uilogger.error("UI logs error: "+ message);
        HttpHeaders responseHeaders = new HttpHeaders();
        return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
    }

}
