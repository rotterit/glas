package Controllers;

import DAL.*;
import DTO.MaterialMove.MaterialMoveItemNoIdWithStock;
import DTO.MaterialMove.StockCorrectionNoId;
import DTO.RawStock.RawStock;
import DTO.StockCorrection.FinishedStockCorrectionNoId;
import DTO.StockCorrection.StockmoveItemNoId;
import Model.FinishedStock.StockCorrection;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;
import Model.Misc.Note;
import Model.RawStock.MaterialCorrection;
import Model.RawStock.Materialmove;
import Model.RawStock.MaterialmoveItem;
import Model.Stock.Stock;
import Model.User.User;
import Security.Signer;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sun.swing.StringUIClientPropertyKey;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 19-5-2015.
 */
@RestController
@RequestMapping(value= "/secure/stockcorrections")
public class StockCorrectionController {

    private Logger logger = LoggerFactory.getLogger(StockCorrectionController.class);

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private MaterialmoveDAO materialmoveDAO;

    @Autowired
    private Gson gson;

    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private RawMaterialDAO rawMaterialDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private Signer signer;

    @Autowired
    private StockmoveDAO stockmoveDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private StockCorrectionDAO stockCorrectionDAO;

    @Transactional
    @RequestMapping(value="/raw/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestParam(value="materialid") int materialid, @RequestParam(value="amount") int amount, @RequestParam(value="stockid") int stockid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            MaterialmoveItem materialmoveItem = materialmoveDAO.createMaterialMoveItem(rawMaterialDAO.getRawMaterialById(materialid),amount, stockDAO.getStockById(stockid));
            Note note = noteDAO.createNote("user: " + userDAO.getUserById(userid).getName() + " on: " + LocalDateTime.now().toString());
            Materialmove materialmove = materialmoveDAO.createMaterialMove(userDAO.getUserById(3),null,userDAO.getUserById(userid), note);
            MaterialCorrection materialCorrection = materialmoveDAO.createStockCorrection("therefore", materialmove, userDAO.getUserById(userid), note);
            stockDAO.addAmount(stockid, materialid, amount);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/finished/createstockorder", method = RequestMethod.POST)
    public ResponseEntity<String> createOrder(@RequestParam(value="userid") int userid, @RequestParam(value="productid") long productid, @RequestParam(value="amount") int amount, @RequestParam(value="stockid") int stockid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            StockmoveItem stockmoveItem = stockmoveDAO.createStockmoveItem(productDAO.getProductById(productid),amount, stockDAO.getStockById(stockid));
            Note note = noteDAO.createNote("user: " + userDAO.getUserById(userid).getName() + " on: " + LocalDateTime.now().toString());
            List<StockmoveItem> stockmoveItems = new ArrayList<>();
            stockmoveItems.add(stockmoveItem);
            Stockmove stockmove = stockmoveDAO.createStockmove(false, false, true, stockmoveItems, userDAO.getUserById(userid), false, userDAO.getUserById(3));
            StockCorrection stockCorrection = stockCorrectionDAO.createStockCorrection(userDAO.getUserById(userid), LocalDateTime.now(),LocalDateTime.now(), userDAO.getUserById(userid),stockmove, note );
            stockDAO.addAmount(stockid, productid, amount);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/finished/cancelstockorder", method = RequestMethod.POST)
    public ResponseEntity<String> cancelOrder(@RequestParam(value="userid") int userid, @RequestParam(value="stockorderid") long stockcorrectionid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            StockCorrection stockCorrection = stockCorrectionDAO.getStockCorrectionById(stockcorrectionid);
            Stockmove stockmove = stockmoveDAO.cancel(stockCorrection.getStockmove().getId(), userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/getstockspermaterial", method = RequestMethod.GET)
    public ResponseEntity<List<RawStock>> getRawStock(@RequestParam(value="userid") int userid, @RequestParam(value="materialid") int materialid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Stock> stocks = stockDAO.getStockByRawmaterial(materialid);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(stocks.stream().map(RawStock::new).collect(Collectors.toList()),responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/getstocksperproduct", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Stock.Stock>> getFinishedStock(@RequestParam(value="userid") int userid, @RequestParam(value="productid") long id){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Stock> stocks = stockDAO.getStockByProduct(id);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(stocks.stream().map(DTO.Stock.Stock::new).collect(Collectors.toList()),responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/finished/createstockcorrection", method = RequestMethod.POST)
    public ResponseEntity<String> createStockCorrection(@RequestParam(value="userid") int userid,  @RequestParam(value="productid") long productid, @RequestParam(value="amount") int amount, @RequestParam(value="stockid") int stockid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            StockmoveItem stockmoveItem = stockmoveDAO.createStockmoveItem(productDAO.getProductById(productid),amount, stockDAO.getStockById(stockid));
            Note note = noteDAO.createNote("user: " + userDAO.getUserById(userid).getName() + " on: " + LocalDateTime.now().toString());
            List<StockmoveItem> stockmoveItems = new ArrayList<>();
            stockmoveItems.add(stockmoveItem);
            Stockmove stockmove = stockmoveDAO.createStockmove(false, true, true, stockmoveItems, userDAO.getUserById(userid), false, userDAO.getUserById(3));
            StockCorrection stockCorrection = stockCorrectionDAO.createStockCorrection(userDAO.getUserById(userid), LocalDateTime.now(),LocalDateTime.now(), userDAO.getUserById(userid),stockmove, note );
            stockDAO.addAmount(stockid, productid, amount);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }
}
