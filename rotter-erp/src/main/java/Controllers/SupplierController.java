package Controllers;

import DAL.NoteDAO;
import DAL.SupplierDAO;
import DAL.UserDAO;
import DTO.Supplier.Supplier;
import DTO.Supplier.SupplierNoId;
import Model.Misc.Note;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value="/secure/suppliers")
public class SupplierController {

    private Logger logger = LoggerFactory.getLogger(SupplierController.class);

    @Autowired
    private Security.Signer Signer;

    @Autowired
    private SupplierDAO supplierDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Gson gson;

    @Autowired
    private NoteDAO noteDAO;

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing a supplier with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Supplier si = gson.fromJson(str, Supplier.class);
            try {
                Note contactNote = noteDAO.editNote(si.getContactnote().getId(), si.getContactnote().getContent());
                Note supplierNote = noteDAO.editNote(si.getSuppliernote().getId(), si.getSuppliernote().getContent());
                Note generalNote = noteDAO.editNote(si.getGeneralnote().getId(), si.getGeneralnote().getContent());
                Model.RawMaterial.Supplier s = supplierDAO.editSupplier(si.getId(), si.getSupplierCode(), si.getName(), contactNote, supplierNote, generalNote,userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to edit supplier with code to " + si.getSupplierCode() + " but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to edit supplier with code to " + si.getSupplierCode() + " but it already exists");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", Signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders , HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is creating a supplier with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            SupplierNoId si = gson.fromJson(str, SupplierNoId.class);
            try {
                Note contactNote = noteDAO.createNote(si.getContactnote());
                Note supplierNote = noteDAO.createNote(si.getSuppliernote());
                Note generalNote = noteDAO.createNote(si.getGeneralnote());
                Model.RawMaterial.Supplier s = supplierDAO.createSupplier(si.getSupplierCode(),si.getName(),contactNote,supplierNote,generalNote,userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to create a supplier with code to " + si.getSupplierCode() + " but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to create a supplier with code to " + si.getSupplierCode() + " but it already exists");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", Signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders , HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<List<Supplier>> get(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Supplier> sizes = supplierDAO.getSuppliers().stream().map(Supplier::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(userid));
            return new ResponseEntity<List<Supplier>>(sizes,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/byid", method = RequestMethod.GET)
    public ResponseEntity<Supplier> get(@RequestParam(value="userid") int userid, @RequestParam(value="supplierid") int supplierid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(userid));
            return new ResponseEntity<>(new Supplier(supplierDAO.getSupplierById(supplierid)),responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }
}
