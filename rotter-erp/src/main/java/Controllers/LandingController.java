package Controllers;

import DAL.*;
import DTO.Invoice.InvoiceDetailLine;
import DTO.Location.Location;
import DTO.Misc.LandingInfo;
import DTO.Product.MiniProduct;
import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.Invoice;
import Security.Signer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by thijs on 2-6-2015.
 */
@RestController
@RequestMapping(value="/secure/landing")
public class LandingController {

    private Logger logger = LoggerFactory.getLogger(LandingController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private InvoiceDAO invoiceDAO;

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private WorksheetDAO worksheetDAO;

    @Transactional
    @RequestMapping(value="/get", method = RequestMethod.GET)
    public ResponseEntity<LandingInfo> get(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //

        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Invoice> invoices = invoiceDAO.getDueInvoices();
            LandingInfo landingInfo = new LandingInfo();
            List<DTO.Invoice.Invoice> invoiceList = new ArrayList<>();
            for (Invoice i : invoices) {
                List<InvoiceDetailLine> invoiceDetailLines = new ArrayList<>();
                for (StockmoveItem stockmoveItem : i.getOrder().getStockmoveItems()) {
                    InvoiceDetailLine invoiceDetailLine = new InvoiceDetailLine();
                    invoiceDetailLine.setProduct(new MiniProduct(stockmoveItem.getProduct(), stockmoveItem.getStock()));
                    invoiceDetailLine.setAmount(stockmoveItem.getAmount());
                    invoiceDetailLine.setInlinePrice(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getPrice());
                    invoiceDetailLine.setStockId(stockmoveItem.getStock().getId());
                    invoiceDetailLine.setInlineDiscountPercentage(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscountPercentage());
                    invoiceDetailLine.setInlineDiscount(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscount());
                    invoiceDetailLines.add(invoiceDetailLine);
                }
                DTO.Invoice.Invoice invoice = new DTO.Invoice.Invoice(i, invoiceDetailLines,invoiceDAO.isReady(i.getId()));
                invoiceList.add(invoice);
            }
            landingInfo.setDueInvoiceList(invoiceList);
            List<DTO.Invoice.Invoice> invoiceList1 = new ArrayList<>();
            invoices.clear();
            invoices = invoiceDAO.getInvoiceDueForPayment();
            for (Invoice i : invoices) {
                List<InvoiceDetailLine> invoiceDetailLines = new ArrayList<>();
                for (StockmoveItem stockmoveItem : i.getOrder().getStockmoveItems()) {
                    InvoiceDetailLine invoiceDetailLine = new InvoiceDetailLine();
                    invoiceDetailLine.setProduct(new MiniProduct(stockmoveItem.getProduct(), stockmoveItem.getStock()));
                    invoiceDetailLine.setAmount(stockmoveItem.getAmount());
                    invoiceDetailLine.setInlinePrice(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getPrice());
                    invoiceDetailLine.setStockId(stockmoveItem.getStock().getId());
                    invoiceDetailLine.setInlineDiscountPercentage(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscountPercentage());
                    invoiceDetailLine.setInlineDiscount(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscount());
                    invoiceDetailLines.add(invoiceDetailLine);
                }
                DTO.Invoice.Invoice invoice = new DTO.Invoice.Invoice(i, invoiceDetailLines,invoiceDAO.isReady(i.getId()));
                invoiceList1.add(invoice);
            }
            landingInfo.setPaymentDueList(invoiceList1);
            int count = worksheetDAO.getProductionQueue().size();
            landingInfo.setProductionQueueItemsCount(count);
            logger.warn("" + landingInfo.getDueInvoiceList().size());
            logger.warn(""+landingInfo.getPaymentDueList().size());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(landingInfo,responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }
}
