package Controllers;

import DAL.UserDAO;
import DTO.User.User;
import DTO.User.UserNoId;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/users")
public class UserController {

    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private Security.Signer Signer;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Gson gson;

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing user with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            User u = gson.fromJson(str, User.class);
            try {
                Model.User.User um = userDAO.editUser(u.getId(), u.getUsername(), u.getName(), u.getRoleid());
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to edit user with username " + u.getUsername() + " but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to edit user with username " + u.getUsername() + " but it already exists");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", Signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders , HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is creating a User with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            UserNoId u = gson.fromJson(str, UserNoId.class);
            try {
                Model.User.User um = userDAO.createUser(u.getName(), u.getUsername(), u.getPassword(), u.getRoleid());
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to create a user with username " + u.getUsername() + " but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to create a user with username " + u.getUsername() + " but it already exists");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", Signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders , HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<List<User>> get(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<User> sizes = userDAO.getUsers().stream().map(User::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(userid));
            return new ResponseEntity<List<User>>(sizes,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }
}
