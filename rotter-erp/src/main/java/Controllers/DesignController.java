package Controllers;

import DAL.DesignDAO;
import DAL.ProductDAO;
import DAL.UserDAO;
import DTO.Design.DesignNoId;
import DTO.Design.Design;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/designs")
public class DesignController {

    private Logger logger = LoggerFactory.getLogger(DesignController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private DesignDAO DesignDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Gson gson;

    @Autowired
    private ProductDAO productDAO;

    @Transactional
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() + " is editing Design with data: " + str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Design s = gson.fromJson(str, Design.class);
            try {
                Model.FinishedProduct.Design t = DesignDAO.editDesign(s.getId(), s.getDesignCode(), s.getDescription(), s.getName(),userDAO.getUserById(userid));
                productDAO.refreshProductCode();
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to edit a design with code to " + s.getDesignCode() + " but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to edit a design with code to " + s.getDesignCode() + " but it already exists");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() + " is creating a Design with data: " + str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            DesignNoId s = gson.fromJson(str, DesignNoId.class);
            try {
                Model.FinishedProduct.Design q = DesignDAO.createDesign(s.getDesignCode(), s.getDescription(), s.getName(),userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to create a design with code to " + s.getDesignCode() + " but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to create a design with code to " + s.getDesignCode() + " but it already exists");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Design>> get(@RequestParam(value = "userid") int userid) {
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5,7}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Design> sizes = DesignDAO.getDesigns().stream().map(Design::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<List<Design>>(sizes, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }
}
