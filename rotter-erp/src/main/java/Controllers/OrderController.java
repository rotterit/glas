package Controllers;

import DAL.*;
import DTO.Material.RawMaterial;
import DTO.MaterialMove.*;
import DTO.Quality.Quality;
import DTO.Stock.StockMini;
import DTO.StockMove.StockmoveItem;
import Model.Misc.Note;
import Model.RawStock.Materialmove;
import Model.RawStock.MaterialmoveItem;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 19-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/orders")
public class OrderController {

    private Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private MaterialmoveDAO materialmoveDAO;

    @Autowired
    private SupplierDAO supplierDAO;

    @Autowired
    private RawMaterialDAO rawMaterialDAO;

    @Autowired
    private QualityDAO qualityDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private Gson gson;

    @Transactional
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            OrderNoId o = gson.fromJson(str, OrderNoId.class);
            Note n = noteDAO.createNote(o.getNote());
            Materialmove m = materialmoveDAO.createOrder(userDAO.getVoid(), supplierDAO.getSupplierById(o.getSupplierId()), userDAO.getUserById(userid), n);
            List<MaterialmoveItem> materialmoveItems = new ArrayList<>();
            for (MaterialMoveItemNoId mm : o.getMaterialmoveItems()) {
                materialmoveItems.add(materialmoveDAO.createMaterialMoveItem(rawMaterialDAO.getRawMaterialById(mm.getRawMaterialId()), mm.getAmount(), stockDAO.getStockById(1)));
            }
            m = materialmoveDAO.bindMoveItemsToOrder(m.getId(), materialmoveItems);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<Order> edit(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            OrderEdit o = gson.fromJson(str, OrderEdit.class);
            materialmoveDAO.cancelOrder(o.getId());
            Note n = noteDAO.editNote(o.getNote().getId(), o.getNote().getContent());
            Materialmove m = materialmoveDAO.createOrder(userDAO.getVoid(), supplierDAO.getSupplierById(o.getSupplierId()), userDAO.getUserById(userid), n);
            List<MaterialmoveItem> materialmoveItems = new ArrayList<>();
            for (MaterialMoveItemNoId mm : o.getMoveItems()) {
                materialmoveItems.add(materialmoveDAO.createMaterialMoveItem(rawMaterialDAO.getRawMaterialById(mm.getRawMaterialId()), mm.getAmount(), stockDAO.getStockById(1)));
            }
            m = materialmoveDAO.bindMoveItemsToOrder(m.getId(), materialmoveItems);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/confirm", method = RequestMethod.POST)
    public ResponseEntity<String> confirm(@RequestParam(value = "userid") int userid, @RequestParam(value="orderid") long id, @RequestParam(value="deliverymillis") long deliverymillis){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            try {
            Materialmove m = materialmoveDAO.confirmOrder(id,userDAO.getUserById(userid),deliverymillis);
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to set an order to delivered that has not been confirmed yet");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to set an order to delivered that has not been confirmed yet");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Deprecated
    @Transactional
    @RequestMapping(value = "/delivered", method = RequestMethod.POST)
    public ResponseEntity<String> delivered(@RequestParam(value = "userid") int userid, @RequestParam(value="orderid") long id){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            try {
                Materialmove m = materialmoveDAO.confirmDeliveryOrder(id, userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to set an order to delivered that has not been confirmed yet");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to set an order to delivered that has not been confirmed yet");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/complete", method = RequestMethod.POST)
    public ResponseEntity<String> completed(@RequestParam(value = "userid") int userid, @RequestParam(value="orderid") long id){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            try {
                Materialmove m = materialmoveDAO.completeOrder(id , userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to complete an order but not all materials have a quality");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to complete an order but not all materials have a quality");
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    public ResponseEntity<String> cancel(@RequestParam(value = "userid") int userid, @RequestParam(value="orderid") long id){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Materialmove m = materialmoveDAO.cancelOrder(id);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/new", method = RequestMethod.GET)
    public ResponseEntity<List<Order>> getNew(@RequestParam(value = "userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Order> os = new ArrayList<>();
            for(Materialmove m : materialmoveDAO.getNotConfirmedOrders()){
                List<DTO.MaterialMove.MaterialmoveItem> list= new ArrayList<>();
                for(MaterialmoveItem mm : m.getMaterialmoveItems()){
                    try {
                        list.add(new DTO.MaterialMove.MaterialmoveItem(mm, rawMaterialDAO.getRawMaterialFullById(mm.getRawMaterial().getId())));
                    } catch (HorribleException e) {
                        logger.error("no stockline for this material", mm.getRawMaterial().getId());
                        throw new RuntimeException("no stockline for this material: "+ mm.getRawMaterial().getId());
//                        HttpHeaders responseHeaders = new HttpHeaders();
//                        responseHeaders.add("token", signer.sign(userid));
//                        return new ResponseEntity<>(null, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }
                os.add(new Order(m,list));
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(os, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/confirmed", method = RequestMethod.GET)
    public ResponseEntity<List<Order>> getConfirmed(@RequestParam(value = "userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,6}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Order> os = new ArrayList<>();
            for(Materialmove m : materialmoveDAO.getConfirmedOrders()){
                List<DTO.MaterialMove.MaterialmoveItem> list= new ArrayList<>();
                for(MaterialmoveItem mm : m.getMaterialmoveItems()){
                    try {
                        list.add(new DTO.MaterialMove.MaterialmoveItem(mm, rawMaterialDAO.getRawMaterialFullById(mm.getRawMaterial().getId())));
                    } catch (HorribleException e) {
                        logger.error("no stockline for this material", mm.getRawMaterial().getId());
                        throw new RuntimeException("no stockline for this material: "+ mm.getRawMaterial().getId());
//                        HttpHeaders responseHeaders = new HttpHeaders();
//                        responseHeaders.add("token", signer.sign(userid));
//                        return new ResponseEntity<>(null, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }
                os.add(new Order(m,list));
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(os, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/delivered", method = RequestMethod.GET)
     public ResponseEntity<List<Order>> getDelivered(@RequestParam(value = "userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,6}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Order> os = new ArrayList<>();
            for(Materialmove m : materialmoveDAO.getDeliveredOrders()){
                List<DTO.MaterialMove.MaterialmoveItem> list= new ArrayList<>();
                for(MaterialmoveItem mm : m.getMaterialmoveItems()){
                    try {
                        list.add(new DTO.MaterialMove.MaterialmoveItem(mm, rawMaterialDAO.getRawMaterialFullById(mm.getRawMaterial().getId())));
                    } catch (HorribleException e) {
                        logger.error("no stockline for this material", mm.getRawMaterial().getId());
                        throw new RuntimeException("no stockline for this material: "+ mm.getRawMaterial().getId());
//                        HttpHeaders responseHeaders = new HttpHeaders();
//                        responseHeaders.add("token", signer.sign(userid));
//                        return new ResponseEntity<>(null, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }
                os.add(new Order(m,list));
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(os, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/completed", method = RequestMethod.GET)
    public ResponseEntity<List<Order>> getCompleted(@RequestParam(value = "userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Order> os = new ArrayList<>();
            for(Materialmove m : materialmoveDAO.getCompletedOrders()){
                List<DTO.MaterialMove.MaterialmoveItem> list= new ArrayList<>();
                for(MaterialmoveItem mm : m.getMaterialmoveItems()){
                    try {
                        list.add(new DTO.MaterialMove.MaterialmoveItem(mm, rawMaterialDAO.getRawMaterialFullById(mm.getRawMaterial().getId())));
                    } catch (HorribleException e) {
                        logger.error("no stockline for this material", mm.getRawMaterial().getId());
                        throw new RuntimeException("no stockline for this material: "+ mm.getRawMaterial().getId());
//                        HttpHeaders responseHeaders = new HttpHeaders();
//                        responseHeaders.add("token", signer.sign(userid));
//                        return new ResponseEntity<>(null, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
                    }
                }
                os.add(new Order(m,list));
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(os, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/setquality", method = RequestMethod.POST)
    public ResponseEntity<String> setQuality(@RequestParam(value = "userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,6}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            QualitySetContainer q = gson.fromJson(str,QualitySetContainer.class);
            List<MaterialmoveItem> mnew = new ArrayList<>();
            for(QualitySetLine qs : q.getQualitySetLines()){
                mnew.add(materialmoveDAO.createMaterialMoveItem(rawMaterialDAO.getForDifferentQuality(qs.getRawMaterialId(),qs.getQualityId()),qs.getAmount(), stockDAO.getStockById(qs.getStockId())));
            }
            if(materialmoveDAO.compareMaterialMoveItems(materialmoveDAO.getMaterialmoveById(q.getOrderId()).getMaterialmoveItems(), mnew)){
                materialmoveDAO.replaceMaterialmoveItems(q.getOrderId(),mnew);
                try {
                    Materialmove m = materialmoveDAO.confirmDeliveryOrder(q.getOrderId(), userDAO.getUserById(userid));
                } catch (HorribleException e) {
                    logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to set an order to delivered that has not been confirmed yet");
                    throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to set an order to delivered that has not been confirmed yet");
                }
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.add("token", signer.sign(userid));
                return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
            }else{
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to set quality but the amount is not correct");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to set quality but the amount is not correct");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
            }
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/getqualitychecksetup", method = RequestMethod.GET)
    public ResponseEntity<QualityCheckSetUp> getSetup(@RequestParam(value = "userid") int userid, @RequestParam(value = "orderid") long id){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,6}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            QualityCheckSetUp q = new QualityCheckSetUp();
            List<MiniMaterial> miniMaterials = new ArrayList<>();
            for(MaterialmoveItem m : materialmoveDAO.getMaterialmoveById(id).getMaterialmoveItems()){
                for(Model.RawMaterial.RawMaterial r : rawMaterialDAO.getAllQualityVariants(m.getRawMaterial().getId()).stream().filter(s->s.getQuality().getId()!=1).collect(Collectors.toList())){
                    miniMaterials.add(new MiniMaterial(r.getId(), r.getQuality().getId(), m.getRawMaterial().getProductSupplierLine().getInternalReference()));
                }
            }
            q.setMaterialPossibilities(new MaterialPossibilities(miniMaterials));
            List<StockPossibility> stockPossibilities = new ArrayList<>();
            for(MaterialmoveItem m : materialmoveDAO.getMaterialmoveById(id).getMaterialmoveItems()) {
                List<StockMini> stockMinis = new ArrayList<>();
                for(Model.Stock.Stock s : stockDAO.getStockByRawmaterial(m.getRawMaterial().getId())) {
                    stockMinis.add(new StockMini(s));
                }
                stockPossibilities.add(new StockPossibility(m.getRawMaterial().getId(),stockMinis));
            }
            q.setPossibilities(new StockPossibilities(stockPossibilities));
            List<Quality> qualities = qualityDAO.getQualitys().stream().map(Quality::new).filter(d->d.getId()!=1).collect(Collectors.toList());
            q.setQualities(new QualityPossibilites(qualities));
            List<DTO.MaterialMove.MaterialmoveItem> materialmoveItems = new ArrayList<>();
            for(MaterialmoveItem materialmoveItem1 : materialmoveDAO.getMaterialmoveById(id).getMaterialmoveItems()){
                try {
                    materialmoveItems.add(new DTO.MaterialMove.MaterialmoveItem(materialmoveItem1, rawMaterialDAO.getRawMaterialFullById(materialmoveItem1.getRawMaterial().getId())));
                } catch (HorribleException e) {
                    logger.error("no stockline for this material",materialmoveItem1.getRawMaterial().getId());
                    throw new RuntimeException("no stockline for this material: "+ materialmoveItem1.getRawMaterial().getId());
                }
            }
            q.setMaterialmoveItems(materialmoveItems);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(q, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

}
