package Controllers;

import DAL.*;
import DTO.Commission.CommissionEdit;
import DTO.Commission.CommissionNoId;
import DTO.Commission.StockmoveItemNoId;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.Commission;
import Model.Misc.Note;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by thijs on 28-5-2015.
 */
@RestController
@RequestMapping(value="/secure/reservations")
public class ReservationController {

//    private Logger logger = LoggerFactory.getLogger(ReservationController.class);
//
//    @Autowired
//    private Signer signer;
//
//    @Autowired
//    private UserDAO userDAO;
//
//    @Autowired
//    private NoteDAO noteDAO;
//
//    @Autowired
//    private StockmoveDAO stockmoveDAO;
//
//    @Autowired
//    private ProductDAO productDAO;
//
//    @Autowired
//    private StockDAO stockDAO;
//
//    @Autowired
//    private CommissionDAO commissionDAO;
//
//    @Autowired
//    private CustomerDAO customerDAO;
//
//    @Autowired
//    private InvoiceDAO invoiceDAO;
//
//    @Autowired
//    private Gson gson;
//
//    @Autowired
//    private PackingSlipDAO packingSlipDAO;
//
//    @Transactional
//    @RequestMapping(value="/create", method = RequestMethod.POST)
//    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            CommissionNoId commissionNoId = gson.fromJson(str, CommissionNoId.class);
//            List<StockmoveItem> stockmoveItems = new ArrayList<>();
//            for(StockmoveItemNoId stockmoveItemNoId : commissionNoId.getStockmoveItemNoIdList()){
//                stockmoveItems.add(stockmoveDAO.createStockmoveItem(productDAO.getProductById(stockmoveItemNoId.getProductId()),stockmoveItemNoId.getAmount(), stockDAO.getStockById(stockmoveItemNoId.getStockId())));
//            }
//            Stockmove stockmove = stockmoveDAO.createStockmove(true,false,true,stockmoveItems,userDAO.getUserById(userid),false,userDAO.getUserById(666));
//            Note note = noteDAO.createNote(commissionNoId.getNote());
//            Commission commission = commissionDAO.createReservation(userDAO.getUserById(userid), customerDAO.getCustomerById(commissionNoId.getCustomerId()), commissionNoId.getStartmillis(),commissionNoId.getReturnmillis(), stockmove,userDAO.getUserById(666), note);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/edit", method = RequestMethod.POST)
//    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            CommissionEdit commissionEdit = gson.fromJson(str, CommissionEdit.class);
//            List<StockmoveItem> stockmoveItems = new ArrayList<>();
//            List<StockmoveItem> oldStockmoveItems = commissionDAO.getById(commissionEdit.getId()).getOutStockmove().getStockmoveItems();
//            boolean used = false;
//            for(DTO.StockMove.StockmoveItem stockmoveItem : commissionEdit.getStockmoveItems()){
//                used = false;
//                for(StockmoveItem stockmoveItem1 : oldStockmoveItems){
//                    if(stockmoveItem1.getProduct().getId()==stockmoveItem.getProductId()&&stockmoveItem.getAmount()!=0){
//                        stockmoveItems.add(stockmoveDAO.editStockMoveItem(stockmoveItem1.getId(), stockmoveItem.getAmount()));
//                        used=true;
//                    }
//                }
//                if(!used&&stockmoveItem.getId()==0){
//                    stockmoveItems.add(stockmoveDAO.createStockmoveItem(productDAO.getProductById(stockmoveItem.getProductId()),stockmoveItem.getAmount(),stockDAO.getStockById(stockmoveItem.getStockId())));
//                    used = true;
//                }
//
//            }
//            Stockmove stockmove = stockmoveDAO.editStockmove(commissionDAO.getById(commissionEdit.getId()).getOutStockmove().getId(), stockmoveItems, userDAO.getUserById(userid));
//            Note note = noteDAO.editNote(commissionEdit.getNote().getId(), commissionEdit.getNote().getContent());
//            try {
//                Commission commission = commissionDAO.editCommission(commissionEdit.getId(),stockmove,commissionEdit.getStartmillis(),commissionEdit.getReturnmillis(),note,userDAO.getUserById(userid));
//            } catch (HorribleException e) {
//                logger.error("user: "+ userDAO.getUserById(userid).getName() + " tried to edit a commission that has already been sent");
//                throw new RuntimeException("user: "+ userDAO.getUserById(userid).getName() + " tried to edit a commission that has already been sent");
//            }
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
}
