package Controllers;

import DAL.NoteDAO;
import DAL.StockDAO;
import DAL.UserDAO;
import DTO.RawStock.RawStock;
import DTO.Stock.Stock;
import DTO.Stock.StockEdit;
import DTO.Stock.StockNoId;
import Model.Misc.Note;
import Security.Signer;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/stocks")
public class StockController {

    private Logger logger = LoggerFactory.getLogger(StockController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Gson gson;

    @Autowired
    private NoteDAO noteDAO;

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing Stock with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            StockEdit si = gson.fromJson(str, StockEdit.class);
            Note n = noteDAO.editNote(si.getNote().getId(), si.getNote().getContent());
            Model.Stock.Stock s = stockDAO.editStock(si.getId(),si.getName(),n,userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is creating a Stock with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            StockNoId si = gson.fromJson(str, StockNoId.class);
            Note n = noteDAO.createNote(si.getNote());
            Model.Stock.Stock s = stockDAO.createStock(si.isRaw(), si.getName(), si.getLocationid(),n,userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/raw/all", method = RequestMethod.GET)
    public ResponseEntity<List<RawStock>> getRaw(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<RawStock> stocks = stockDAO.getAllRawStocks().stream().map(RawStock::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(stocks,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/finished/all", method = RequestMethod.GET)
    public ResponseEntity<List<Stock>> getFinished(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Stock> stocks = stockDAO.getAllFinishedStocks().stream().map(Stock::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(stocks,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/finished/byid", method = RequestMethod.GET)
    public ResponseEntity<Stock> getStockById(@RequestParam(value="userid") int userid, @RequestParam(value="stockid") int stockid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Stock stock = new Stock(stockDAO.getStockById(stockid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(stock,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }


    @Transactional
    @RequestMapping(value="/raw/byid", method = RequestMethod.GET)
    public ResponseEntity<RawStock> getRawStockById(@RequestParam(value="userid") int userid, @RequestParam(value="stockid") int stockid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            RawStock stock = new RawStock(stockDAO.getStockById(stockid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(stock,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }
}
