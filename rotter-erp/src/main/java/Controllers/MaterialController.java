package Controllers;

import DAL.*;
import DTO.Material.*;
import DTO.Material.RawMaterial;
import DTO.RawStock.RawStock;
import Model.Misc.Note;
import Model.RawMaterial.*;
import Model.Stock.Stock;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 8-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/raw")
public class MaterialController {

    private Logger logger = LoggerFactory.getLogger(MaterialController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private SeriesDAO seriesDAO;

    @Autowired
    private SizeDAO sizeDAO;

    @Autowired
    private SupplierDAO supplierDAO;

    @Autowired
    private ProductTypeDAO productTypeDAO;

    @Autowired
    private ColorDAO colorDAO;

    @Autowired
    private QualityDAO qualityDAO;

    @Autowired
    private QueryBuilder queryBuilder;

    @Autowired
    private RawMaterialDAO rawMaterialDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private Gson gson;

    @Transactional
    @RequestMapping(value = "/searchterms", method = RequestMethod.GET)
    public ResponseEntity<SearchTerm> getSearches(@RequestParam(value = "userid") int userid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,6}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<SearchTerm>(new SearchTerm(seriesDAO.getSeries(), sizeDAO.getSizes(), supplierDAO.getSuppliers(), productTypeDAO.getProductTypes(), colorDAO.getColors(), qualityDAO.getSearchQualitys().stream().filter(m->m.getId()!=1).collect(Collectors.toList())), responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<SearchTerm>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/filtered", method = RequestMethod.POST)
    public ResponseEntity<List<RawMaterial>> getFiltered(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,6}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            List<RawMaterial> materials = new ArrayList<>();
            MaterialFilter m = gson.fromJson(str, MaterialFilter.class);
            for (Model.RawMaterial.RawMaterial r : queryBuilder.BuildQueryForRawMaterial(m)) {
                try {
                    materials.add(rawMaterialDAO.getRawMaterialFullById(r.getId()));
                } catch (HorribleException e) {
                    logger.error("no stockline for this material", r.getId());
                    throw new RuntimeException("no stockline for this material: " + r.getId());
                }
            }
            return new ResponseEntity<List<RawMaterial>>(materials, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<List<RawMaterial>>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/bydescription", method = RequestMethod.GET)
    public ResponseEntity<List<RawMaterial>> getByDescription(@RequestParam(value = "userid") int userid, @RequestParam(value = "searchstring") String search) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,6}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            List<RawMaterial> materials = new ArrayList<>();
            for (Model.RawMaterial.RawMaterial r : rawMaterialDAO.getByDescription(search)) {
                try {
                    materials.add(rawMaterialDAO.getRawMaterialFullById(r.getId()));
                } catch (HorribleException e) {
                    logger.error("no stockline for this material", r.getId());
                    throw new RuntimeException("no stockline for this material: " + r.getId());
                }

            }
            return new ResponseEntity<List<RawMaterial>>(materials, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<List<RawMaterial>>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("" + str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            MaterialNoId m = gson.fromJson(str, MaterialNoId.class);
            logger.warn(m.getDescription());
            Note note = noteDAO.createNote(m.getNote());
            try {
                for (Model.RawMaterial.Quality q : qualityDAO.getQualitys()) {
                    Model.RawMaterial.RawMaterial mm = rawMaterialDAO.createRawMaterial(m.getDescription(), colorDAO.getColorById(m.getColorId()), supplierDAO.getSupplierById(m.getSupplierId()), q, m.getPrice(), seriesDAO.getSeriesById(m.getSeriesId()), sizeDAO.getSizeById(m.getSizeId()), productTypeDAO.getProductTypeById(m.getProductTypeId()), note, m.getMOQ(), userDAO.getUserById(userid), m.getBaseAmount());
                    ProductSupplierLine p = rawMaterialDAO.createProductSupplierLine(productTypeDAO.getProductTypeById(m.getProductTypeId()), sizeDAO.getSizeById(m.getSizeId()), colorDAO.getColorById(m.getColorId()), m.getSupplierCode(), mm.getSupplier(), mm, mm.getCreatedBy());
                    mm = rawMaterialDAO.addSupplierLine(mm.getId(), p);
                    for (int stockid : m.getStockIds()) {
                        if(stockDAO.checkIfExists(stockid, mm)){
                            Stock s = stockDAO.createMaterialStockLine(stockid, mm);
                        }
                    }
                }
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to create a rawmaterial but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to create a rawmaterial but it already exists");
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            MaterialEdit m = gson.fromJson(str, MaterialEdit.class);
            Note note = noteDAO.editNote(m.getNoteId(), m.getNote());
            Model.RawMaterial.RawMaterial mm = rawMaterialDAO.editRawMaterial(m.getId(), m.getDescription(), m.getPrice(), m.getMOQ(), userDAO.getUserById(userid), m.getBaseAmount(), note);
            List<Model.RawMaterial.RawMaterial> rawMaterials = new ArrayList<>();
            rawMaterials.add(mm);
            updatePrices(rawMaterials);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/editforqualities", method = RequestMethod.POST)
    public ResponseEntity<String> editforqualities(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            MaterialEdit m = gson.fromJson(str, MaterialEdit.class);
            List<Model.RawMaterial.RawMaterial> rawMaterials = new ArrayList<>();
            for (Model.RawMaterial.RawMaterial r : rawMaterialDAO.getAllQualityVariants(m.getId())){
                Model.RawMaterial.RawMaterial mm = rawMaterialDAO.editRawMaterial(r.getId(), m.getPrice(), m.getMOQ(), userDAO.getUserById(userid), m.getBaseAmount());
                rawMaterials.add(mm);
            }
            updatePrices(rawMaterials);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/editforcolors", method = RequestMethod.POST)
    public ResponseEntity<String> editforcolors(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            MaterialEdit m = gson.fromJson(str, MaterialEdit.class);
            List<Model.RawMaterial.RawMaterial> rawMaterials = new ArrayList<>();
            for (Model.RawMaterial.RawMaterial r : rawMaterialDAO.getAllColorVariants(m.getId())){
                Model.RawMaterial.RawMaterial mm = rawMaterialDAO.editRawMaterial(r.getId(), m.getPrice(), m.getMOQ(), userDAO.getUserById(userid), m.getBaseAmount());
                rawMaterials.add(mm);
            }
            updatePrices(rawMaterials);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/editmoqforsupplier", method = RequestMethod.POST)
    public ResponseEntity<String> editmoqforsupplier(@RequestParam(value = "userid") int userid, @RequestParam(value="moq") int moq, @RequestParam(value="supplierid") int supplierid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            rawMaterialDAO.editMOQForSupplier(supplierid, moq);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/editforcolorsandqualities", method = RequestMethod.POST)
    public ResponseEntity<String> editforcolorsandqualites(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            MaterialEdit m = gson.fromJson(str, MaterialEdit.class);
            List<Model.RawMaterial.RawMaterial> rawMaterials = new ArrayList<>();
            List<Model.RawMaterial.RawMaterial> toedit = rawMaterialDAO.getAllQualityVariants(m.getId());
            toedit.addAll(rawMaterialDAO.getAllColorVariants(m.getId()));
            for (Model.RawMaterial.RawMaterial r : toedit){
                Model.RawMaterial.RawMaterial mm = rawMaterialDAO.editRawMaterial(r.getId(), m.getPrice(), m.getMOQ(), userDAO.getUserById(userid), m.getBaseAmount());
                rawMaterials.add(mm);
            }
            updatePrices(rawMaterials);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/addtostock", method = RequestMethod.POST)
    public ResponseEntity<String> addToStock(@RequestParam(value = "userid") int userid, @RequestParam(value = "stockid") int stockid, @RequestParam(value = "rawmaterialid") int rawmaterialid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Model.RawMaterial.RawMaterial> rawMaterials = rawMaterialDAO.getAllQualityVariants(rawmaterialid);
            for(Model.RawMaterial.RawMaterial raw : rawMaterials){
                if(stockDAO.checkIfExists(stockid, raw)) {
                    Stock s = stockDAO.createMaterialStockLine(stockid, raw);
                }else{
                    logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to create a MaterialStockline but the stock was already kept");
                    throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to create a MaterialStockline but the stock was already kept");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
                }
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/byid", method = RequestMethod.GET)
    public ResponseEntity<RawMaterial> getById(@RequestParam(value = "userid") int userid, @RequestParam(value = "rawmaterialid") int rawmaterialid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            List<StockDetail> ms = rawMaterialDAO.getStockLineById(rawmaterialid).stream().map(StockDetail::new).collect(Collectors.toList());
            int stockAmount = 0;
            try {
                stockAmount = rawMaterialDAO.getStockAmount(rawmaterialid);
            } catch (HorribleException e) {
                logger.error("no stockline for this material", rawmaterialid);
                throw new RuntimeException("no stockline for this material: " + rawmaterialid);
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
            }
            try {
                return new ResponseEntity<>(new RawMaterial(rawMaterialDAO.getRawMaterialById(rawmaterialid),stockAmount, rawMaterialDAO.getMinStockAmount(rawmaterialid), rawMaterialDAO.getLineColor(rawmaterialid),ms), responseHeaders, HttpStatus.OK);
            } catch (HorribleException e) {
                logger.error("no stockline for this material", rawmaterialid);
                throw new RuntimeException("no stockline for this material: " + rawmaterialid);
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }


    @Transactional
    @RequestMapping(value = "/filteredfororder", method = RequestMethod.POST)
    public ResponseEntity<List<RawMaterial>> getFilteredForOrder(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            List<RawMaterial> materials = new ArrayList<>();
            MaterialFilter m = gson.fromJson(str, MaterialFilter.class);
            for (Model.RawMaterial.RawMaterial r : queryBuilder.BuildQueryForOrderRawMaterial(m)) {
                try {
                    materials.add(rawMaterialDAO.getRawMaterialForOrderFullById(r.getId()));
                } catch (HorribleException e) {
                    logger.error("no stockline for this material", r.getId());
                    throw new RuntimeException("no stockline for this material: " + r.getId());
//                    return new ResponseEntity<List<RawMaterial>>(null, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            return new ResponseEntity<List<RawMaterial>>(materials, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<List<RawMaterial>>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/bydescriptionfororder", method = RequestMethod.GET)
    public ResponseEntity<List<RawMaterial>> getByDescriptionForOrder(@RequestParam(value = "userid") int userid, @RequestParam(value = "searchstring") String search) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            List<RawMaterial> materials = new ArrayList<>();
            for (Model.RawMaterial.RawMaterial r : rawMaterialDAO.getByDescriptionForOrder(search)) {
                try {
                    materials.add(rawMaterialDAO.getRawMaterialFullById(r.getId()));
                } catch (HorribleException e) {
                    logger.error("no stockline for this material", r.getId());
                    throw new RuntimeException("no stockline for this material: " + r.getId());
//                    return new ResponseEntity<List<RawMaterial>>(null, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            return new ResponseEntity<List<RawMaterial>>(materials, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<List<RawMaterial>>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    public void updatePrices(List<Model.RawMaterial.RawMaterial> rawMaterials){
        rawMaterialDAO.checkProductPrices(rawMaterials, unitDAO.getWorkprice().getNumericalValue());
    }

    @Transactional
    @RequestMapping(value="/getqualityvariants", method = RequestMethod.GET)
    public ResponseEntity<List<RawMaterial>> getQualityVariants(@RequestParam(value = "userid") int userid, @RequestParam(value = "materialid") int materialid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            List<RawMaterial> materials = new ArrayList<>();
            for (Model.RawMaterial.RawMaterial r : rawMaterialDAO.getAllQualityVariants(materialid)) {
                try {
                    materials.add(rawMaterialDAO.getRawMaterialFullById(r.getId()));
                } catch (HorribleException e) {
                    logger.error("no stockline for this material", r.getId());
                    throw new RuntimeException("no stockline for this material: " + r.getId());
//                    return new ResponseEntity<List<RawMaterial>>(null, responseHeaders, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            return new ResponseEntity<List<RawMaterial>>(materials, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<List<RawMaterial>>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/freestocks", method = RequestMethod.GET)
    public ResponseEntity<List<RawStock>> getFreeStocks(@RequestParam(value="userid") int userid, @RequestParam(value="materialid") int materialid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Stock> stocks = stockDAO.getFreeStockByMaterial(materialid);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(stocks.stream().map(RawStock::new).collect(Collectors.toList()),responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

}
