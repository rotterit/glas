package Controllers;

import DAL.CustomerDAO;
import DAL.UserDAO;
import DTO.Customer.CustomerNoId;
import DTO.Customer.Customer;
import Model.User.User;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/customers")
public class CustomerController {

    private Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private CustomerDAO CustomerDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Gson gson;

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{ 2,3,5,7}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing Customer with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Customer s = gson.fromJson(str, Customer.class);
            Model.Invoicing.Customer t = CustomerDAO.editCustomer(s.getId(),userDAO.getUserById(userid), s.getName(), s.getDescription(), s.getBaseVAT(), s.getBaseDiscountPercentage(), s.getBaseLineDiscountPercentage(), s.getBasePackingCostPercentage(), s.getCompanyName(), s.getAdress(), s.getZIP(), s.getCity(), s.getCountry(), s.getTEL(), s.getEmail(), s.getVATnumber());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5,7}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is creating a Customer with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            CustomerNoId s = gson.fromJson(str, CustomerNoId.class);
            Model.Invoicing.Customer q = CustomerDAO.createCustomer(userDAO.getUserById(userid), s.getName(), s.getDescription(), s.getBaseVAT(), s.getBaseDiscountPercentage(), s.getBaseLineDiscountPercentage(), s.getBasePackingCostPercentage(), s.getCompanyName(), s.getAdress(), s.getZIP(), s.getCity(), s.getCountry(), s.getTEL(), s.getEmail(), s.getVATnumber());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<List<Customer>> get(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5,7}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Customer> sizes = CustomerDAO.getCustomers().stream().map(Customer::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<List<Customer>>(sizes,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/byname", method = RequestMethod.GET)
    public ResponseEntity<List<Customer>> getByName(@RequestParam(value="userid") int userid, @RequestParam(value="searchterm") String searchterm){
            //permissions
            List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5,7}));
            //
            if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
                List<Customer> sizes = CustomerDAO.getCustomersByName(searchterm).stream().map(Customer::new).collect(Collectors.toList());
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.add("token", signer.sign(userid));
                return new ResponseEntity<>(sizes,responseHeaders,HttpStatus.OK);
            }else{
                logger.warn("bad permissions for user with id", userid);
                HttpHeaders responseHeaders = new HttpHeaders();
                return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
            }
    }

    @Transactional
    @RequestMapping(value="/byid", method = RequestMethod.GET)
    public ResponseEntity<Customer> getByName(@RequestParam(value="userid") int userid, @RequestParam(value="customerid") int customerid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5,7}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Model.Invoicing.Customer customer = CustomerDAO.getCustomerById(customerid);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(new Customer(customer),responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }
}
