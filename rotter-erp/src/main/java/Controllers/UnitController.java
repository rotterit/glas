package Controllers;

import DAL.NoteDAO;
import DAL.ProductDAO;
import DAL.UnitDAO;
import DAL.UserDAO;
import DTO.Misc.Unit;
import Model.Misc.Note;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value="/secure/units")
public class UnitController {

    private Logger logger = LoggerFactory.getLogger(ColorController.class);

    @Autowired
    private Security.Signer signer;

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private Gson gson;

    @Autowired
    private ProductDAO productDAO;

    @Transactional
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Unit>> getAll(@RequestParam(value="userid") int userid){
        logger.warn("test for all");
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Unit> units = unitDAO.getUnits().stream().map(Unit::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(0));
            return new ResponseEntity<>(units,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        logger.warn("unitcontroller");
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2}));
        //
        logger.warn("@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        logger.warn(str);
        logger.warn(""+userid);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Unit u = gson.fromJson(str, Unit.class);
            logger.warn("User: " + userDAO.getUserById(userid).getName() + " is editting unit: " + u.getName());
            Note n = noteDAO.editNote(u.getNote().getId(), u.getNote().getContent());
            Model.Misc.Unit ut = unitDAO.edit(u.getId(),u.getName(),u.getTextValue(), u.getNumericalValue(), n);
            if(ut.getId() == 1){
                productDAO.recalculatePrice(ut);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }
}
