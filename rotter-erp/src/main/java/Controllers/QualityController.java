package Controllers;

import DAL.*;
import DTO.Misc.StringJson;
import DTO.Quality.Quality;
import Model.Misc.Note;
import Model.RawMaterial.ProductSupplierLine;
import Model.RawMaterial.RawMaterial;
import Model.Stock.Stock;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/qualities")
public class QualityController {

    private Logger logger = LoggerFactory.getLogger(QualityController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private QualityDAO qualityDAO;

    @Autowired
    private RawMaterialDAO rawMaterialDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private Gson gson;

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing Quality with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Quality s = gson.fromJson(str, Quality.class);
            Model.RawMaterial.Quality t = qualityDAO.editQuality(s.getId(),s.getName(),userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is creating a Quality with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            StringJson s = gson.fromJson(str, StringJson.class);
            Model.RawMaterial.Quality q = qualityDAO.createQuality(s.getData(),userDAO.getUserById(userid));
            for(RawMaterial m : rawMaterialDAO.getRawMaterials().stream().filter(raw->raw.getQuality().getId() == 1).collect(Collectors.toList())){
                Note note = noteDAO.createNote(m.getNote().getContent());
                try {
                        Model.RawMaterial.RawMaterial mm = rawMaterialDAO.createRawMaterial(m.getDescription(), m.getColor(), m.getSupplier(), q, m.getPrice(), m.getSeries(), m.getMySize(), m.getProductType(), note, m.getMinimumOrderQuantity(), userDAO.getUserById(userid), m.getBaseminStockAmount());
                        ProductSupplierLine p = rawMaterialDAO.createProductSupplierLine(m.getProductType(), m.getMySize(), m.getColor(), m.getProductSupplierLine().getRawMaterialCode(), mm.getSupplier(), mm, mm.getCreatedBy());
                        mm = rawMaterialDAO.addSupplierLine(mm.getId(), p);
                        for (int stockid : stockDAO.getStockByRawmaterial(m.getId()).stream().mapToInt(st->st.getId()).boxed().collect(Collectors.toList())) {
                            if(stockDAO.checkIfExists(stockid, mm)){
                                Stock stock = stockDAO.createMaterialStockLine(stockid, mm);
                            }
                        }
                } catch (HorribleException e) {
                    logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to create a rawmaterial but it already exists");
                    throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to create a rawmaterial but it already exists");
//                    HttpHeaders responseHeaders = new HttpHeaders();
//                    responseHeaders.add("token", signer.sign(userid));
//                    return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
                }
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<List<Quality>> get(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Quality> sizes = qualityDAO.getQualitys().stream().map(Quality::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<List<Quality>>(sizes,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }
}
