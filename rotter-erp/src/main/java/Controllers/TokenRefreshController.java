package Controllers;

/**
 * Created by thijs on 10-5-2015.
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;


@RestController
@RequestMapping(value="/secure/suchcrypto")
public class TokenRefreshController {

    @Autowired
    private Security.Signer Signer;

    @Transactional
    @RequestMapping(value="/muchToken", method = RequestMethod.GET)
    public ResponseEntity<String> getToken(@RequestParam(value="userid") int userid){
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("token", Signer.sign(userid));
        return new ResponseEntity<>(null,responseHeaders, HttpStatus.OK);
    }

    @Transactional
    @RequestMapping(value="/whattimeisit", method = RequestMethod.GET)
    public ResponseEntity<LocalDateTime> getTime(@RequestParam(value="userid") int userid){
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add("token", Signer.sign(userid));
        LocalDateTime now = LocalDateTime.now();
        return new ResponseEntity<>(now,responseHeaders, HttpStatus.OK);
    }
}
