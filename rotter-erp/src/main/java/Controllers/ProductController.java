package Controllers;

import DAL.*;
import DTO.Material.RawMaterial;
import DTO.Product.*;
import DTO.Size.Size;
import DTO.Supplier.Supplier;
import Model.FinishedProduct.*;
import Model.FinishedProduct.Color;
import DTO.Design.Design;
import Model.FinishedProduct.Description;
import Model.FinishedProduct.Product;
import Model.FinishedProduct.ProductType;
import Model.FinishedStock.StockCorrection;
import Model.FinishedStock.StockLine;
import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.Invoice;
import Model.Stock.Stock;
import Security.Signer;

import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 6-5-2015.
 */
@RestController
@RequestMapping("/secure/products")
public class ProductController {

    private Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private SizeDAO sizeDAO;

    @Autowired
    private SupplierDAO supplierDAO;

    @Autowired
    private ProductTypeDAO productTypeDAO;

    @Autowired
    private ColorDAO colorDAO;

    @Autowired
    private QueryBuilder queryBuilder;

    @Autowired
    private RawMaterialDAO rawMaterialDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private DesignDAO designDAO;

    @Autowired
    private ManufacturingTypeDAO manufacturingTypeDAO;

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private InvoiceDAO invoiceDAO;

    @Autowired
    private StockCorrectionDAO stockCorrectionDAO;

    @Autowired
    private Gson gson;

    @Transactional
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid")int userid, @RequestBody String str ){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is creating a product with data: "+ str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            ProductNoId p = gson.fromJson(str, ProductNoId.class);
            Color c = colorDAO.getColorById(p.getColorId());
            ProductType pt = productTypeDAO.getProductTypeById(p.getProductTypeId());
            Model.FinishedProduct.Design d = designDAO.getDesignById(p.getDesignId());
            MySize s = sizeDAO.getSizeById(p.getSizeId());
            String productCode = productDAO.generateProductCode(pt, d, s ,c );
            Model.RawMaterial.Supplier supplier = supplierDAO.getSupplierById(p.getSupplierid());
            Model.RawMaterial.RawMaterial rawMaterial=null;
            try {
                rawMaterial = rawMaterialDAO.getRawMaterialByProps(c,pt,s,supplier);
            } catch (HorribleException e) {
                logger.error("there's no correct rawmaterial for the product");
                throw new RuntimeException("there is no raw material that can be used for this product");
            }
            try {
                List<Description> descriptions = productDAO.createDescriptions(p.getDescriptions(),userDAO.getUserById(userid));
                Product product  =  productDAO.createProduct(supplier,
                        p.getBaseTime(),
                        p.getMinTime(),
                        p.getMaxTime(),
                        p.getPrice(),
                        c,
                        descriptions,
                        s,
                        pt,
                        manufacturingTypeDAO.getManufacturingTypeById(p.getManufacturingtypeId()),
                        d,
                        p.getName(),
                        userDAO.getUserById(userid),
                        rawMaterial,
                        productCode,
                        unitDAO.getWorkprice(),
                        categoryDAO.getCategoryById(p.getCategoryId()));
                productDAO.bindProduct(descriptions, product.getId());
                List<Stock> stocks = p.getStockIds().stream().map(st->stockDAO.getStockById(st)).collect(Collectors.toList());
                List<StockLine> stockLines = stocks.stream().map(sl->stockDAO.createStockLine(sl, product)).collect(Collectors.toList());
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to create a finished but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to create a finished but it already exists");
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/addstock", method = RequestMethod.POST)
    public ResponseEntity<String> addStock(@RequestParam(value="userid")int userid, @RequestParam(value="stockid") int stockid, @RequestParam(value="productid") long productid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            StockLine st = stockDAO.createStockLine(stockDAO.getStockById(stockid), productDAO.getProductById(productid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid")int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing a product with data: "+ str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            ProductEdit p = gson.fromJson(str , ProductEdit.class);
            Product product = productDAO.editProduct(p.getId(), p.getName(), p.getBaseTime(), p.getMinTime(), p.getMaxTime(), p.getPrice(), userDAO.getUserById(userid), userDAO.getUserById(1), unitDAO.getWorkprice());
            List<StockLine> stocklines = stockDAO.getStocklineByProduct(p.getId());
            List<StockLine> stockLines = stocklines.stream().map(sl->stockDAO.createStockLine2(sl.getStock(),sl.getAmount(), product)).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/editforcolors", method = RequestMethod.POST)
    public ResponseEntity<String> editforcolors(@RequestParam(value="userid")int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing a product with data: "+ str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            ProductEdit p = gson.fromJson(str, ProductEdit.class);
            for (Product looped : productDAO.getColorVariants(p.getId())){
                Product product = productDAO.editProduct(looped.getId(), p.getName() ,p.getBaseTime(), p.getMinTime(), p.getMaxTime(), userDAO.getUserById(userid), userDAO.getUserById(1), unitDAO.getWorkprice());
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/priceforcategorysize", method = RequestMethod.POST)
    public ResponseEntity<String> editpriceforcategory(@RequestParam(value="userid") int userid, @RequestParam(value="categoryid") int categorysizeid, @RequestParam(value="price") double price){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing a product to set the price of category with id: " + categorysizeid + " to " +price);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            for (Product looped : productDAO.getByCategorySize(categorysizeid)){
                Product product = productDAO.setPrice(looped.getId(), price,userDAO.getUserById(userid));
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/bydescription", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Product.Product>> searchByDescription(@RequestParam(value="userid") int userid, @RequestParam(value="searchstring") String searchstring){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,7}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Product> products = productDAO.getByDescription(searchstring);
            List<DTO.Product.Product> products1 = new ArrayList<>();
            List<Invoice> openInvoices = invoiceDAO.getOpenInvoices();
            List<StockCorrection> openStockCorrections = stockCorrectionDAO.getOpenStockCorrections();
            List<ProductInvoiceDetail> productInvoiceDetails = new ArrayList<>();
            List<ProductStockCorrectionDetail> productStockCorrectionDetails = new ArrayList<>();
            for(Product p : products){
                for(Invoice i : openInvoices){
                    for(StockmoveItem si : i.getNeeded().getStockmoveItems()){
                        if(si.getProduct().getId() == p.getId()){
                            productInvoiceDetails.add(new ProductInvoiceDetail(i, si));
                        }
                    }
                }
                for(StockCorrection s: openStockCorrections){
                    for(StockmoveItem si : s.getStockmove().getStockmoveItems()){
                        if(si.getProduct().getId() == p.getId()){
                            productStockCorrectionDetails.add(new ProductStockCorrectionDetail(s, si));
                        }
                    }
                }
                products1.add(new DTO.Product.Product(p, productDAO.getStockAmount(p.getId()), productDAO.getReservedStock(p.getId()),productDAO.getOnComission(p.getId()), productDAO.getInProduction(p.getId()), productDAO.getInProductionQueue(p.getId()), productInvoiceDetails, productStockCorrectionDetails));
                productInvoiceDetails.clear();
                productStockCorrectionDetails.clear();
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(products1, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/editdescription", method = RequestMethod.POST)
    public ResponseEntity<String> editDescription(@RequestParam(value="userid") int userid, @RequestParam(value="descriptionid") int descriptionid, @RequestParam(value="description") String description){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Description d = productDAO.editDescription(descriptionid,description);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/filtered", method = RequestMethod.POST)
    public ResponseEntity<List<DTO.Product.Product>> getFiltered(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,7}));
        //
        logger.warn("filter for: "+ str);

        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<DTO.Product.Product> products = new ArrayList<>();
            ProductFilter m = gson.fromJson(str, ProductFilter.class);
            List<Invoice> openInvoices = invoiceDAO.getOpenInvoices();
            List<StockCorrection> openStockCorrections = stockCorrectionDAO.getOpenStockCorrections();

            List<ProductInvoiceDetail> productInvoiceDetails = new ArrayList<>();
            List<ProductStockCorrectionDetail> productStockCorrectionDetails = new ArrayList<>();
            for(Product p : queryBuilder.BuildQueryForProduct(m)){
//                in comment for performance reasons
//                for(Invoice i : openInvoices){
//                    for(StockmoveItem si : i.getNeeded().getStockmoveItems()){
//                        if(si.getProduct().getId() == p.getId()){
//                            productInvoiceDetails.add(new ProductInvoiceDetail(i, si));
//                        }
//                    }
//                }
//                for(StockCorrection s: openStockCorrections){
//                    for(StockmoveItem si : s.getStockmove().getStockmoveItems()){
//                        if(si.getProduct().getId() == p.getId()){
//                            productStockCorrectionDetails.add(new ProductStockCorrectionDetail(s, si));
//                        }
//                    }
//                }
//                products.add(new DTO.Product.Product(p, productDAO.getStockAmount(p.getId()), productDAO.getReservedStock(p.getId()),productDAO.getOnComission(p.getId()), productDAO.getInProduction(p.getId()), productDAO.getInProductionQueue(p.getId()), productInvoiceDetails, productStockCorrectionDetails));
                products.add(new DTO.Product.Product(p, productDAO.getStockAmount(p.getId()), productDAO.getReservedStock(p.getId()),0, 0,0, productInvoiceDetails, productStockCorrectionDetails));
                productInvoiceDetails.clear();
                productStockCorrectionDetails.clear();
            }
            logger.warn("# results: "+ products.size());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(products, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/searchterms", method = RequestMethod.GET)
    public ResponseEntity<SearchTerm> getSearches(@RequestParam(value = "userid") int userid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,7}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            List<Model.FinishedProduct.Design> designs = designDAO.getDesigns();
            return new ResponseEntity<SearchTerm>(new SearchTerm(productTypeDAO.getProductTypes().stream().map(DTO.ProductType.ProductType::new).collect(Collectors.toList()),
                    sizeDAO.getSizes().stream().map(Size::new).collect(Collectors.toList()),
                    designDAO.getDesigns().stream().map(Design::new).collect(Collectors.toList()),
                    manufacturingTypeDAO.getManufacturingTypes().stream().map(DTO.ManufacturingType.ManufacturingType::new).collect(Collectors.toList()),
                    categoryDAO.getCategories().stream().map(DTO.Category.Category::new).collect(Collectors.toList()),
                    supplierDAO.getSuppliers().stream().map(Supplier::new).collect(Collectors.toList()),
                    colorDAO.getColors().stream().map(DTO.Color.Color::new).collect(Collectors.toList())
                    ), responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<SearchTerm>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value= "/filteredperstock", method = RequestMethod.POST)
    public ResponseEntity<List<DTO.Product.ProductPerStock>> getFilteredPerStock(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,7}));
        //
        logger.warn("filter for: "+ str);

        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<DTO.Product.ProductPerStock> products = new ArrayList<>();
            ProductFilter m = gson.fromJson(str, ProductFilter.class);
            List<Invoice> openInvoices = invoiceDAO.getOpenInvoices();
            List<StockCorrection> openStockCorrections = stockCorrectionDAO.getOpenStockCorrections();
            List<ProductInvoiceDetail> productInvoiceDetails = new ArrayList<>();
            List<ProductStockCorrectionDetail> productStockCorrectionDetails = new ArrayList<>();
            for(Product p : queryBuilder.BuildQueryForProduct(m)){
                List<StockLine> stockLines = stockDAO.getStocklineByProduct(p.getId());
                for(StockLine stockLine : stockLines) {
                    for (Invoice i : openInvoices) {
                        for (StockmoveItem si : i.getNeeded().getStockmoveItems()) {
                            if (si.getProduct().getId() == p.getId() && si.getStock().getId() == stockLine.getStock().getId()) {
                                productInvoiceDetails.add(new ProductInvoiceDetail(i, si));
                            }
                        }
                    }
                    for (StockCorrection s : openStockCorrections) {
                        for (StockmoveItem si : s.getStockmove().getStockmoveItems()) {
                            if (si.getProduct().getId() == p.getId()&& si.getStock().getId() == stockLine.getStock().getId()) {
                                productStockCorrectionDetails.add(new ProductStockCorrectionDetail(s, si));
                            }
                        }
                    }
                    products.add(new DTO.Product.ProductPerStock(p, stockLine.getAmount(), productDAO.getReservedStock(p.getId(),stockLine.getStock()), productDAO.getOnComission(p.getId(), stockLine.getStock()), productDAO.getInProduction(p.getId(), stockLine.getStock()), productDAO.getInProductionQueue(p.getId(), stockLine.getStock()), productInvoiceDetails, productStockCorrectionDetails, stockLine.getStock()));
                    productInvoiceDetails.clear();
                    productStockCorrectionDetails.clear();
                }
            }
            logger.warn("# results: "+ products.size());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(products, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }


    @Transactional
    @RequestMapping(value="/byid", method = RequestMethod.GET)
    public ResponseEntity<DTO.Product.Product> getById(@RequestParam(value = "userid") int userid,@RequestParam(value = "productid") long productid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5,7}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Product product = productDAO.getProductById(productid);
            List<Invoice> openInvoices = invoiceDAO.getOpenInvoices();
            List<StockCorrection> openStockCorrections = stockCorrectionDAO.getOpenStockCorrections();
            List<ProductInvoiceDetail> productInvoiceDetails = new ArrayList<>();
            List<ProductStockCorrectionDetail> productStockCorrectionDetails = new ArrayList<>();
            for(Invoice i : openInvoices){
                    for(StockmoveItem si : i.getNeeded().getStockmoveItems()){
                        if(si.getProduct().getId() == product.getId()){
                            productInvoiceDetails.add(new ProductInvoiceDetail(i, si));
                        }
                    }
                }
                for(StockCorrection s: openStockCorrections){
                    for(StockmoveItem si : s.getStockmove().getStockmoveItems()){
                        if(si.getProduct().getId() == product.getId()){
                            productStockCorrectionDetails.add(new ProductStockCorrectionDetail(s, si));
                        }
                    }
                }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(new DTO.Product.Product(product, productDAO.getStockAmount(product.getId()), productDAO.getReservedStock(product.getId()),productDAO.getOnComission(product.getId()), productDAO.getInProduction(product.getId()), productDAO.getInProductionQueue(product.getId()), productInvoiceDetails, productStockCorrectionDetails), responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/freestocks", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Stock.Stock>> getFreeStocks(@RequestParam(value = "userid") int userid,@RequestParam(value = "productid") long productid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Stock> stocks = stockDAO.getFreeStockByProduct(productid);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(stocks.stream().map(DTO.Stock.Stock::new).collect(Collectors.toList()),responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }
}
