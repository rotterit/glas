package Controllers;

import DAL.*;
import DAL.Util.CommissionToInvoice;
import DTO.Commission.CommissionEdit;
import DTO.Commission.CommissionNoId;
import DTO.Commission.CommissionRecieve;
import DTO.Commission.StockmoveItemNoId;
import DTO.Invoice.*;
import DTO.Product.MiniProduct;
import Model.FinishedStock.StockLine;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.Commission;
import Model.Invoicing.Invoice;
import Model.Invoicing.PackingSlip;
import Model.Invoicing.PriceLine;
import Model.Misc.Note;
import Model.User.User;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by thijs on 27-5-2015.
 */
@RestController
@RequestMapping(value="/secure/commission")
public class CommissionController {

//    private Logger logger = LoggerFactory.getLogger(CommissionController.class);
//
//    @Autowired
//    private Signer signer;
//
//    @Autowired
//    private UserDAO userDAO;
//
//    @Autowired
//    private NoteDAO noteDAO;
//
//    @Autowired
//    private StockmoveDAO stockmoveDAO;
//
//    @Autowired
//    private ProductDAO productDAO;
//
//    @Autowired
//    private StockDAO stockDAO;
//
//    @Autowired
//    private CommissionDAO commissionDAO;
//
//    @Autowired
//    private CustomerDAO customerDAO;
//
//    @Autowired
//    private InvoiceDAO invoiceDAO;
//
//    @Autowired
//    private Gson gson;
//
//    @Autowired
//    private UnitDAO unitDAO;
//
//    @Autowired
//    private PackingSlipDAO packingSlipDAO;
//
//    @Transactional
//    @RequestMapping(value="/create", method = RequestMethod.POST)
//    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            CommissionNoId commissionNoId = gson.fromJson(str, CommissionNoId.class);
//            List<StockmoveItem> stockmoveItems = new ArrayList<>();
//            for(StockmoveItemNoId stockmoveItemNoId : commissionNoId.getStockmoveItemNoIdList()){
//                stockmoveItems.add(stockmoveDAO.createStockmoveItem(productDAO.getProductById(stockmoveItemNoId.getProductId()),stockmoveItemNoId.getAmount(), stockDAO.getStockById(stockmoveItemNoId.getStockId())));
//            }
//            Stockmove stockmove = stockmoveDAO.createStockmove(true,false,true,stockmoveItems,userDAO.getUserById(userid),false,userDAO.getUserById(666));
//            Note note = noteDAO.createNote(commissionNoId.getNote());
//            Commission commission = commissionDAO.createCommission(userDAO.getUserById(userid), customerDAO.getCustomerById(commissionNoId.getCustomerId()), commissionNoId.getStartmillis(),commissionNoId.getReturnmillis(), stockmove,userDAO.getUserById(666), note);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/edit", method = RequestMethod.POST)
//    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            CommissionEdit commissionEdit = gson.fromJson(str, CommissionEdit.class);
//            List<StockmoveItem> stockmoveItems = new ArrayList<>();
//            List<StockmoveItem> oldStockmoveItems = commissionDAO.getById(commissionEdit.getId()).getOutStockmove().getStockmoveItems();
//            boolean used = false;
//            for(DTO.StockMove.StockmoveItem stockmoveItem : commissionEdit.getStockmoveItems()){
//                used = false;
//                for(StockmoveItem stockmoveItem1 : oldStockmoveItems){
//                    if(stockmoveItem1.getProduct().getId()==stockmoveItem.getProductId()&&stockmoveItem.getAmount()!=0){
//                        stockmoveItems.add(stockmoveDAO.editStockMoveItem(stockmoveItem1.getId(), stockmoveItem.getAmount()));
//                        used=true;
//                    }
//                }
//                if(!used&&stockmoveItem.getId()==0){
//                    stockmoveItems.add(stockmoveDAO.createStockmoveItem(productDAO.getProductById(stockmoveItem.getProductId()),stockmoveItem.getAmount(),stockDAO.getStockById(stockmoveItem.getStockId())));
//                    used = true;
//                }
//
//            }
//            Stockmove stockmove = stockmoveDAO.editStockmove(commissionDAO.getById(commissionEdit.getId()).getOutStockmove().getId(), stockmoveItems, userDAO.getUserById(userid));
//            Note note = noteDAO.editNote(commissionEdit.getNote().getId(), commissionEdit.getNote().getContent());
//            try {
//                Commission commission = commissionDAO.editCommission(commissionEdit.getId(),stockmove,commissionEdit.getStartmillis(),commissionEdit.getReturnmillis(),note,userDAO.getUserById(userid));
//            } catch (HorribleException e) {
//                logger.error("user: "+ userDAO.getUserById(userid).getName() + " tried to edit a commission that has already been sent");
//                throw new RuntimeException("user: "+ userDAO.getUserById(userid).getName() + " tried to edit a commission that has already been sent");
//            }
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/complete", method = RequestMethod.POST)
//    public ResponseEntity<String> complete(@RequestParam(value="userid") int userid, @RequestParam(value="commissionid") long commissionid){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            try {
//                Commission commission = commissionDAO.completeCommission(commissionid, userDAO.getUserById(userid));
//            } catch (HorribleException e) {
//                logger.error("user: " + userDAO.getUserById(userid).getName() +" tried to complete a commission but it\'s in and out were different and there was no invoice");
//            }
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/recieve", method = RequestMethod.POST)
//    public ResponseEntity<String> recieve(@RequestParam(value="userid") int userid, @RequestBody String str){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            CommissionRecieve commissionRecieve = gson.fromJson(str, CommissionRecieve.class);
//            List<StockmoveItem> stockmoveItems = new ArrayList<>();
//            for(StockmoveItemNoId stockmoveItemNoId : commissionRecieve.getStockmoveItemNoIdList()){
//                stockmoveItems.add(stockmoveDAO.createStockmoveItem(productDAO.getProductById(stockmoveItemNoId.getProductId()),stockmoveItemNoId.getAmount(), stockDAO.getStockById(stockmoveItemNoId.getStockId())));
//            }
//            Stockmove stockmove = stockmoveDAO.createStockmove(false,false,true,stockmoveItems,userDAO.getUserById(userid),false,userDAO.getUserById(666));
//            Commission commission = commissionDAO.recieveCommission(commissionRecieve.getCommissionId(), userDAO.getUserById(userid), stockmove);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/send", method = RequestMethod.POST)
//    public ResponseEntity<String> send(@RequestParam(value="userid") int userid, @RequestParam(value="commissionid") long commissionid ){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            Commission commission = null;
//            try {
//                commission = commissionDAO.sendCommission(commissionid,userDAO.getUserById(userid));
//            } catch (HorribleException e) {
//                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to send a commission that's not completely packed yet");
//                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to send a commission that's not completely packed yet");
//            }
//            for(StockmoveItem stockmoveItem: commission.getOutStockmove().getStockmoveItems()){
//                for(StockLine stockLine : stockmoveItem.getStock().getStockLines()){
//                    if(stockLine.getProduct().getId()== stockmoveItem.getProduct().getId()){
//                        stockDAO.subtractAmount(stockLine.getId(), stockmoveItem.getAmount());
//                    }
//                }
//            }
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/getbill", method = RequestMethod.POST)
//    public ResponseEntity<InvoiceNoId> getBill(@RequestParam(value="userid") int userid, @RequestParam(value="commissionid") long commissionid){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            List<CommissionToInvoice> commissionToInvoices = commissionDAO.invoiceFromCommission(commissionid);
//            Commission commission = commissionDAO.getById(commissionid);
//            List<InvoiceCreateLine> invoiceCreateLines = new ArrayList<>();
//            for(CommissionToInvoice commissionToInvoice : commissionToInvoices){
//                invoiceCreateLines.add(new InvoiceCreateLine(commissionToInvoice.getProductId(), commissionToInvoice.getAmount(), commissionToInvoice.getStockid()));
//            }
//            InvoiceNoId invoiceNoId = new InvoiceNoId(commission.getCustomer().getId(), "This is the invoice for comission: "+ commission.getId(), invoiceCreateLines );
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(invoiceNoId, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/setinvoice", method = RequestMethod.POST)
//    public ResponseEntity<String> setInvoice(@RequestParam(value="userid") int userid, @RequestParam(value="commissionid") long commissionid, @RequestBody String str){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        //TODO create packingslips set invoice
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            InvoiceNoId n = gson.fromJson(str, InvoiceNoId.class);
//            Model.Invoicing.Customer customer = customerDAO.getCustomerById(n.getCustomerId());
//            Note note= noteDAO.createNote(n.getNote());
//            List<StockmoveItem> stockmoveItems = new ArrayList<>();
//            for(InvoiceCreateLine invoiceCreateLine : n.getInvoiceCreateLines()){
//                StockmoveItem temp = stockmoveDAO.createStockmoveItem(productDAO.getProductById(invoiceCreateLine.getProductId()), invoiceCreateLine.getAmount(), stockDAO.getStockById(invoiceCreateLine.getStockId()));
//                PriceLine priceLine = invoiceDAO.createPriceLine(invoiceCreateLine.getInlinePrice(), invoiceCreateLine.getInlineDiscount() , invoiceCreateLine.getInlineDiscountPercentage() , temp);
//            }
//            Stockmove stockmove = stockmoveDAO.createStockmove(true,false,true,stockmoveItems,userDAO.getUserById(userid),false, userDAO.getUserById(666));
//            Stockmove emptystockmove = stockmoveDAO.createStockmove(true,false,true,new ArrayList<StockmoveItem>(),userDAO.getUserById(userid),false, userDAO.getUserById(666));
//            Invoice i = invoiceDAO.createInvoice(customer,
//                    stockmove,
//                    stockmove,
//                    emptystockmove,
//                    n.getPaymentDueDate(),
//                    n.getPriceWithoutVAT(),
//                    n.getVATpercentage(),
//                    n.getVAT(),
//                    n.getPrice(),
//                    n.getUpfrontpercentage(),
//                    n.getPackingCost(),
//                    n.getPackingCostPercentage(),
//                    n.getDiscount(),
//                    n.getDiscountpercentage(),
//                    n.getLocalPrice(),
//                    n.getShippingCost(),
//                    n.getDueDate(),
//                    userDAO.getUserById(userid),
//                    userDAO.getUserById(666),
//                    note,
//                    n.getValuta(),
//                    n.getShippingMethod()
//            );
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/new", method = RequestMethod.GET)
//    public ResponseEntity<List<DTO.Commission.Commission>> getNew(@RequestParam(value="userid") int userid){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            List<Commission> commissions = commissionDAO.getNew();
//            List<DTO.Commission.Commission> commissionList = new ArrayList<>();
//            for(Commission commission : commissions){
//                commissionList.add(new DTO.Commission.Commission(commission, null, unitDAO.getinvoicestart().getNumericalValue()));
//            }
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(commissionList, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/sent", method = RequestMethod.GET)
//    public ResponseEntity<List<DTO.Commission.Commission>> getSent(@RequestParam(value="userid") int userid){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            List<Commission> commissions = commissionDAO.getSent();
//            List<DTO.Commission.Commission> commissionList = new ArrayList<>();
//            for(Commission commission : commissions){
//                commissionList.add(new DTO.Commission.Commission(commission, null, unitDAO.getinvoicestart().getNumericalValue()));
//            }
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(commissionList, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/recieved", method = RequestMethod.GET)
//    public ResponseEntity<List<DTO.Commission.Commission>> getRecieved(@RequestParam(value="userid") int userid){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            List<Commission> commissions = commissionDAO.getRecieved();
//            List<DTO.Commission.Commission> commissionList = new ArrayList<>();
//            for(Commission commission : commissions){
//                commissionList.add(new DTO.Commission.Commission(commission, null, unitDAO.getinvoicestart().getNumericalValue()));
//            }
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(commissionList, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/completed", method = RequestMethod.GET)
//    public ResponseEntity<List<DTO.Commission.Commission>> getCompleted(@RequestParam(value="userid") int userid){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            List<Commission> commissions = commissionDAO.getCompleted();
//            List<DTO.Commission.Commission> commissionList = new ArrayList<>();
//            for(Commission commission : commissions){
//                if(commission.getInvoice() != null) {
//                    Invoice i = commission.getInvoice();
//                    List<InvoiceDetailLine> invoiceDetailLines = new ArrayList<>();
//                    for (StockmoveItem stockmoveItem : i.getOrder().getStockmoveItems()) {
//                        InvoiceDetailLine invoiceDetailLine = new InvoiceDetailLine();
//                        invoiceDetailLine.setProduct(new MiniProduct(stockmoveItem.getProduct(), stockmoveItem.getStock()));
//                        invoiceDetailLine.setAmount(stockmoveItem.getAmount());
//                        invoiceDetailLine.setInlinePrice(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getPrice());
//                        invoiceDetailLine.setStockId(stockmoveItem.getStock().getId());
//                        invoiceDetailLine.setInlineDiscountPercentage(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscountPercentage());
//                        invoiceDetailLine.setInlineDiscount(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscount());
//                        invoiceDetailLines.add(invoiceDetailLine);
//                    }
//                    commissionList.add(new DTO.Commission.Commission(commission, invoiceDetailLines, unitDAO.getinvoicestart().getNumericalValue()));
//                }else {
//                    commissionList.add(new DTO.Commission.Commission(commission, null, unitDAO.getinvoicestart().getNumericalValue()));
//                }
//            }
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(commissionList, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/packingslips/add", method = RequestMethod.POST)
//    public ResponseEntity<List<Commission>> addPackingslip(@RequestParam(value="userid") int userid, @RequestParam(value="commissionid") long commissionid, @RequestBody String str){
//        //permissions
//        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
//            //TODO figure out warining return
//            PackingSlipNoId packingSlipNoId = gson.fromJson(str , PackingSlipNoId.class);
//            Commission commission = commissionDAO.getById(commissionid);
//            List<StockmoveItem> stockmoveItems = new ArrayList<>();
//            for(PackingSlipLine packingslipline : packingSlipNoId.getPackingSlipLineList()){
//                StockmoveItem stockmoveItem = stockmoveDAO.createStockmoveItem(productDAO.getProductById(packingslipline.getProductid()), packingslipline.getAmount(), stockDAO.getStockById(1));
//                stockmoveItems.add(stockmoveItem);
//            }
//            Stockmove stockMove = stockmoveDAO.createStockmove(true, false,false,stockmoveItems,userDAO.getUserById(userid),true, userDAO.getUserById(666));
//            Model.Invoicing.PackingSlip packingSlip = packingSlipDAO.createPackingSlip(commission.getCustomer(),userDAO.getUserById(userid),userDAO.getUserById(666),stockMove);
//            commission = commissionDAO.addPackingslip(commission.getId(), packingSlip, userDAO.getUserById(userid));
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        }else{
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/packingslips/delete", method=RequestMethod.POST)
//    public ResponseEntity<String> removePackingslip(@RequestParam(value="userid") int userid, @RequestParam(value="commisionid") long commissionid, @RequestParam(value="packingslipid") long packingslipid ){
//        //permissions
//        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
//            Commission commission = commissionDAO.removePackingslip(commissionid,packingSlipDAO.getPackingSlipById(packingslipid),userDAO.getUserById(userid));
//            packingSlipDAO.deletePackingslip(packingslipid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        } else {
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/packingslips/ready", method = RequestMethod.POST)
//    public ResponseEntity<String> readyPackingslip(@RequestParam(value="userid") int userid, @RequestParam(value="packingslipid") long packingslipid){
//        //permissions
//        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
//            PackingSlip packingSlip = packingSlipDAO.setPacked(packingslipid, userDAO.getUserById(userid));
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        } else {
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
//        }
//    }
//
//    @Transactional
//    @RequestMapping(value="/packingslips/ship", method = RequestMethod.POST)
//    public ResponseEntity<String> shipPackingslip(@RequestParam(value="userid") int userid, @RequestParam(value="packingslipid") long packingslipid){
//        //permissions
//        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{1, 2}));
//        //
//        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
//            try {
//                PackingSlip packingSlip = packingSlipDAO.setshipped(packingslipid, userDAO.getUserById(userid));
//            } catch (HorribleException e) {
//                logger.error("user: " + userDAO.getUserById(userid).getName() +" tried to ship a package that has not been set to packed");
//                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() +" tried to ship a package that has not been set to packed");
//            }
//            HttpHeaders responseHeaders = new HttpHeaders();
//            responseHeaders.add("token", signer.sign(userid));
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
//        } else {
//            logger.warn("bad permissions for user with id", userid);
//            HttpHeaders responseHeaders = new HttpHeaders();
//            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
//        }
//    }
}
