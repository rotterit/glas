package Controllers;

import DAL.*;
import DAL.Util.InvoiceAndStockCorrections;
import DAL.Util.StockMoveItemInvoiceConnector;
import DTO.Worksheet.ProductionQueueLine;
import DTO.Worksheet.QualityLine;
import DTO.Worksheet.WorksheetLine;
import DTO.Worksheet.WorksheetNoID;
import Model.FinishedStock.StockCorrection;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.Invoice;
import Model.Invoicing.Worksheet;
import Model.Misc.Note;
import Model.RawStock.MaterialStockLine;
import Model.RawStock.Materialmove;
import Model.RawStock.MaterialmoveItem;
import Model.Stock.Stock;
import Model.User.User;
import Security.Signer;
import com.google.gson.Gson;
import com.sun.javafx.tk.DummyToolkit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 26-5-2015.
 */
@RestController
@RequestMapping(value="/secure/worksheets")
public class WorksheetController {


    private Logger logger = LoggerFactory.getLogger(InvoiceController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Gson gson;

    @Autowired
    private WorksheetDAO worksheetDAO;

    @Autowired
    private StockmoveDAO stockmoveDAO;

    @Autowired
    private MaterialmoveDAO materialmoveDAO;

    @Autowired
    private RawMaterialDAO rawMaterialDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private InvoiceDAO invoiceDAO;

    @Autowired
    private StockCorrectionDAO stockCorrectionDAO;

    @Autowired
    private ProductDAO productDAO;

    @Transactional
    @RequestMapping(value="/getproductionqueue", method = RequestMethod.GET)
    public ResponseEntity<List<ProductionQueueLine>> getProductionQueue(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<ProductionQueueLine> productionQueueLines = worksheetDAO.getProductionQueue();
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            logger.warn(""+productionQueueLines.size());
            return new ResponseEntity<>(productionQueueLines, responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            WorksheetNoID data = gson.fromJson(str, WorksheetNoID.class);
            List<MaterialmoveItem> materialmoveItems = new ArrayList<>();
            List<StockmoveItem> stockmoveItems = new ArrayList<>();
            for(WorksheetLine wl : data.getWorksheetLineList()){
                StockmoveItem s = stockmoveDAO.getStockMoveItemById(wl.getStockmoveitemid());
                stockmoveItems.add(s);
                for(QualityLine ql : wl.getQualityLines()){
                    materialmoveItems.add(materialmoveDAO.createMaterialMoveItem(rawMaterialDAO.getForDifferentQuality(s.getProduct().getMaterial().getId(), ql.getQualityId()), ql.getAmount(), stockDAO.getStockById(ql.getStockId())));
                }
            }
            Stockmove stockmove = stockmoveDAO.createStockmove(true,false,false,stockmoveItems,userDAO.getUserById(userid), false,userDAO.getUserById(3));
            Note note = noteDAO.createNote("this is a materialmove for a worksheet");
            Materialmove materialmove = materialmoveDAO.createMaterialMove(userDAO.getUserById(3), null, userDAO.getUserById(userid),note);
            Note note2 = noteDAO.createNote(data.getNote());
            materialmove = materialmoveDAO.bindMoveItemsToOrder(materialmove.getId(), materialmoveItems);
            Worksheet worksheet = worksheetDAO.createWorksheet(materialmove, stockmove, userDAO.getUserById(userid), userDAO.getUserById(data.getExecutorid()), note2);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/delete", method = RequestMethod.POST)
    public ResponseEntity<String> delete(@RequestParam(value="userid") int userid,@RequestParam(value="worksheetid") long worksheetid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            worksheetDAO.deleteWorksheet(worksheetid);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/start", method = RequestMethod.POST)
    public ResponseEntity<String> start(@RequestParam(value="userid") int userid, @RequestParam(value="worksheetid") long worksheetid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{4}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Worksheet worksheet = worksheetDAO.startWorksheet(worksheetid, userDAO.getUserById(userid));
            for(MaterialmoveItem m : worksheet.getMaterialmove().getMaterialmoveItems()){
                MaterialStockLine materialStockLine = stockDAO.getMaterialStockLine(m.getRawMaterial().getId(), m.getStock().getId());
                MaterialStockLine materialStockLine1 = stockDAO.subtractAmount(materialStockLine.getId(), m.getAmount());
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/getcutters", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getCutters(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<User> cutters = userDAO.getCutters();
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(cutters, responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/complete", method = RequestMethod.POST)
    public ResponseEntity<String> completeWorksheet(@RequestParam(value="userid") int userid, @RequestParam(value="worksheetid") long worksheetid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{4}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Worksheet worksheet = worksheetDAO.finishWorksheet(worksheetid, userDAO.getUserById(userid));
            List<Invoice> invoices = invoiceDAO.getProductionInvoices();
            List<StockCorrection> stockCorrections = stockCorrectionDAO.getOpenStockCorrections();
            List<Long> stockmoveitemids = worksheet.getStockmove().getStockmoveItems().stream().mapToLong(m->m.getId()).boxed().collect(Collectors.toList());
            List<StockMoveItemInvoiceConnector> connectors = new ArrayList<>();
            for(Invoice invoice : invoices){
                for(StockmoveItem stockmoveItem : invoice.getNeeded().getStockmoveItems()) {
                    if(stockmoveitemids.contains(stockmoveItem.getId())) {
                        connectors.add(new StockMoveItemInvoiceConnector(stockmoveItem.getId(), invoice.getId()));
                    }
                }
            }
            List<Long> StockCorrectionids = new ArrayList<>();
            for(StockCorrection stockCorrection : stockCorrections){
                if(stockmoveitemids.contains(stockCorrection.getStockmove().getStockmoveItems().get(0).getId())){
                        StockCorrectionids.add(stockCorrection.getId());
                }
            }
            stockCorrectionDAO.completeStockcorrections(StockCorrectionids);
            invoiceDAO.completeStockmoveitems(connectors);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/new", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Worksheet.Worksheet>> getNew(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Worksheet> worksheets = worksheetDAO.getNewWorksheets();
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(worksheets.stream().map(DTO.Worksheet.Worksheet::new).collect(Collectors.toList()), responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/started", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Worksheet.Worksheet>> getStarted(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Worksheet> worksheets = worksheetDAO.getStartedWorksheets();
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(worksheets.stream().map(DTO.Worksheet.Worksheet::new).collect(Collectors.toList()), responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/completed", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Worksheet.Worksheet>> getCompleted(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Worksheet> worksheets = worksheetDAO.getCompletedWorksheets();
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(worksheets.stream().map(DTO.Worksheet.Worksheet::new).collect(Collectors.toList()), responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/mine", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Worksheet.Worksheet>> getMine(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{4}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Worksheet> worksheets = worksheetDAO.getWorksheetsForCutters(userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(worksheets.stream().map(DTO.Worksheet.Worksheet::new).collect(Collectors.toList()), responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/stockformaterial", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.RawStock.RawStock>> getForMaterial(@RequestParam(value="userid") int userid, @RequestParam(value="materialid") int materialid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Stock> stocks = stockDAO.getStockByRawmaterial(materialid);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(stocks.stream().map(DTO.RawStock.RawStock::new).collect(Collectors.toList()), responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/rawstockbyproduct", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.RawStock.RawStock>> getByProduct(@RequestParam(value="userid") int userid, @RequestParam(value="productid") long productid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Stock> stocks = stockDAO.getRawStockByProduct(productDAO.getProductById(productid).getMaterial().getId());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(stocks.stream().map(DTO.RawStock.RawStock::new).collect(Collectors.toList()), responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders , HttpStatus.UNAUTHORIZED);
        }
    }
}
