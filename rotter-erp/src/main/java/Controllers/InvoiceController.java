package Controllers;

import DAL.*;
import DTO.Invoice.*;
import DTO.Product.MiniProduct;
import Model.FinishedStock.Stockmove;
import Model.FinishedStock.StockmoveItem;
import Model.Invoicing.Invoice;
import Model.Invoicing.PackingSlip;
import Model.Invoicing.Payment;
import Model.Invoicing.PriceLine;
import Model.Misc.Note;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 22-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/invoices")
public class InvoiceController {
    //TODO productinfo

    private Logger logger = LoggerFactory.getLogger(InvoiceController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private NoteDAO noteDAO;

    @Autowired
    private InvoiceDAO invoiceDAO;

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private StockmoveDAO stockmoveDAO;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private StockDAO stockDAO;

    @Autowired
    private PaymentDAO paymentDAO;

    @Autowired
    private PackingSlipDAO packingSlipDAO;

    @Autowired
    private UnitDAO unitDAO;

    @Autowired
    private Gson gson;

    @Transactional
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn(str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            InvoiceNoId n = gson.fromJson(str, InvoiceNoId.class);
            Model.Invoicing.Customer customer = customerDAO.getCustomerById(n.getCustomerId());
            Note note = noteDAO.createNote(n.getNote());
            List<StockmoveItem> stockmoveItems = new ArrayList<>();
            for (InvoiceCreateLine invoiceCreateLine : n.getInvoiceCreateLines()) {
                StockmoveItem temp = stockmoveDAO.createStockmoveItem(productDAO.getProductById(invoiceCreateLine.getProductId()), invoiceCreateLine.getAmount(), stockDAO.getStockById(invoiceCreateLine.getStockId()));
                PriceLine priceLine = invoiceDAO.createPriceLine(invoiceCreateLine.getInlinePrice(), invoiceCreateLine.getInlineDiscount(), invoiceCreateLine.getInlineDiscountPercentage(), temp);
                stockmoveItems.add(temp);
            }
            Stockmove stockmove = stockmoveDAO.createStockmove(true, false, true, stockmoveItems, userDAO.getUserById(userid), false, userDAO.getUserById(3));
            Stockmove empty1 = stockmoveDAO.createStockmove(true, false, true, new ArrayList<StockmoveItem>(), userDAO.getUserById(userid), false, userDAO.getUserById(3));
            Stockmove empty2 = stockmoveDAO.createStockmove(true, false, true, new ArrayList<StockmoveItem>(), userDAO.getUserById(userid), false, userDAO.getUserById(3));
            //List<Stockmove> stockmoves = stockmoveDAO.splitByStock(stockmove, userDAO.getUserById(userid), userDAO.getUserById(666));
            Invoice i = invoiceDAO.createInvoice(customer, userDAO.getUserById(userid),stockmove, empty1, empty2, n.getDueDate() ,n.getPaymentDueDate(), n.getPriceWithoutVAT(), n.getVATpercentage(), n.getVAT(), n.getPrice(), n.getUpfrontpercentage(), n.getPackingCost(), n.getPackingCostPercentage(), n.getDiscount(), n.getDiscountpercentage(), n.getLocalPrice(), n.getValuta(), note, n.getShippingCost(), n.getShippingMethod(), userDAO.getUserById(3));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/byid", method = RequestMethod.GET)
    public ResponseEntity<DTO.Invoice.Invoice> byId(@RequestParam(value = "userid") int userid, @RequestParam(value="invoiceid") long invoiceid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Invoice invoice = invoiceDAO.getInvoiceById(invoiceid);
            List<InvoiceDetailLine> invoiceDetailLines = new ArrayList<>();
            for (StockmoveItem stockmoveItem : invoice.getOrder().getStockmoveItems()) {
                InvoiceDetailLine invoiceDetailLine = new InvoiceDetailLine();
                invoiceDetailLine.setProduct(new MiniProduct(stockmoveItem.getProduct(), stockmoveItem.getStock()));
                invoiceDetailLine.setAmount(stockmoveItem.getAmount());
                invoiceDetailLine.setInlinePrice(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getPrice());
                invoiceDetailLine.setStockId(stockmoveItem.getStock().getId());
                invoiceDetailLine.setInlineDiscountPercentage(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscountPercentage());
                invoiceDetailLine.setInlineDiscount(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscount());
                invoiceDetailLines.add(invoiceDetailLine);
            }
            DTO.Invoice.Invoice invoice2 = new DTO.Invoice.Invoice(invoice, invoiceDetailLines, invoiceDAO.isReady(invoiceid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(invoice2, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn(str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            InvoiceEdit invoiceEdit = gson.fromJson(str, InvoiceEdit.class);
            InvoiceNoId n = invoiceEdit.getInvoice();
            Note note = noteDAO.createNote(n.getNote());
            List<StockmoveItem> stockmoveItems = new ArrayList<>();
            logger.warn(""+n.getInvoiceCreateLines().size());
            for (InvoiceCreateLine invoiceCreateLine : n.getInvoiceCreateLines()) {
                StockmoveItem temp = stockmoveDAO.createStockmoveItem(productDAO.getProductById(invoiceCreateLine.getProductId()), invoiceCreateLine.getAmount(), stockDAO.getStockById(invoiceCreateLine.getStockId()));
                PriceLine priceLine = invoiceDAO.createPriceLine(invoiceCreateLine.getInlinePrice(), invoiceCreateLine.getInlineDiscount(), invoiceCreateLine.getInlineDiscountPercentage(), temp);
                stockmoveItems.add(temp);
            }
            Stockmove stockmove = stockmoveDAO.createStockmove(true, false, true, stockmoveItems, userDAO.getUserById(userid), false, userDAO.getUserById(3));
            Stockmove empty1 = stockmoveDAO.createStockmove(true, false, true, new ArrayList<StockmoveItem>(), userDAO.getUserById(userid), false, userDAO.getUserById(3));
            Stockmove empty2 = stockmoveDAO.createStockmove(true, false, true, new ArrayList<StockmoveItem>(), userDAO.getUserById(userid), false, userDAO.getUserById(3));
            try {
                Invoice i = invoiceDAO.editInvoice(invoiceEdit.getInvoiceId(), userDAO.getUserById(userid),stockmove, empty1, empty2, n.getDueDate() ,n.getPaymentDueDate(), n.getPriceWithoutVAT(), n.getVATpercentage(), n.getVAT(), n.getPrice(), n.getUpfrontpercentage(), n.getPackingCost(), n.getPackingCostPercentage(), n.getDiscount(), n.getDiscountpercentage(), n.getLocalPrice(), n.getValuta(), note, n.getShippingCost(), n.getShippingMethod());
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to edit a sent invoice");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to edit a sent invoice");
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/cancel", method = RequestMethod.POST)
    public ResponseEntity<String> cancel(@RequestParam(value="userid") int userid, @RequestParam(value="invoiceid") long invoiceid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            invoiceDAO.cancelInvoice(invoiceid, userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/offers", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Invoice.Invoice>> getNotSent(@RequestParam(value = "userid") int userid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Invoice> invoices = invoiceDAO.getOpenInvoices();
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            List<DTO.Invoice.Invoice> invoiceList = new ArrayList<>();
            for (Invoice i : invoices) {
                List<InvoiceDetailLine> invoiceDetailLines = new ArrayList<>();
                for (StockmoveItem stockmoveItem : i.getOrder().getStockmoveItems()) {
                    InvoiceDetailLine invoiceDetailLine = new InvoiceDetailLine();
                    invoiceDetailLine.setProduct(new MiniProduct(stockmoveItem.getProduct(), stockmoveItem.getStock()));
                    invoiceDetailLine.setAmount(stockmoveItem.getAmount());
                    invoiceDetailLine.setInlinePrice(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getPrice());
                    invoiceDetailLine.setStockId(stockmoveItem.getStock().getId());
                    invoiceDetailLine.setInlineDiscountPercentage(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscountPercentage());
                    invoiceDetailLine.setInlineDiscount(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscount());
                    invoiceDetailLines.add(invoiceDetailLine);
                }
                DTO.Invoice.Invoice invoice = new DTO.Invoice.Invoice(i, invoiceDetailLines,invoiceDAO.isReady(i.getId()));
                invoiceList.add(invoice);
            }
            return new ResponseEntity<>(invoiceList, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/confirmed", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Invoice.Invoice>> getConfirmed(@RequestParam(value = "userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Invoice> invoices = invoiceDAO.getConfirmedInvoices();
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            List<DTO.Invoice.Invoice> invoiceList = new ArrayList<>();
            for (Invoice i : invoices) {
                List<InvoiceDetailLine> invoiceDetailLines = new ArrayList<>();
                for (StockmoveItem stockmoveItem : i.getOrder().getStockmoveItems()) {
                    InvoiceDetailLine invoiceDetailLine = new InvoiceDetailLine();
                    invoiceDetailLine.setProduct(new MiniProduct(stockmoveItem.getProduct(), stockmoveItem.getStock()));
                    invoiceDetailLine.setAmount(stockmoveItem.getAmount());
                    invoiceDetailLine.setInlinePrice(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getPrice());
                    invoiceDetailLine.setStockId(stockmoveItem.getStock().getId());
                    invoiceDetailLine.setInlineDiscountPercentage(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscountPercentage());
                    invoiceDetailLine.setInlineDiscount(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscount());
                    invoiceDetailLines.add(invoiceDetailLine);
                }
                DTO.Invoice.Invoice invoice = new DTO.Invoice.Invoice(i, invoiceDetailLines, invoiceDAO.isReady(i.getId()));
                invoiceList.add(invoice);
            }
            return new ResponseEntity<>(invoiceList, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/confirm", method = RequestMethod.POST)
    public ResponseEntity<String> confirm(@RequestParam(value = "userid") int userid, @RequestParam(value="invoiceid") long invoiceid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Invoice i = invoiceDAO.confirmInvoice(invoiceid, userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/produce", method = RequestMethod.POST)
    public ResponseEntity<String> produce(@RequestParam(value = "userid") int userid, @RequestParam(value="invoiceid") long invoiceid){
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Invoice invoice = invoiceDAO.getInvoiceById(invoiceid);
            List<Stockmove> stockmoves = stockmoveDAO.splitByStock(invoice.getOrder(), userDAO.getUserById(userid), userDAO.getUserById(3));
            try {
                Invoice i = invoiceDAO.produce(invoiceid, userDAO.getUserById(userid),stockmoves.get(0),stockmoves.get(1));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() +" tried to produce an invoice that has not been confirmed");
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/sent", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Invoice.Invoice>> getSent(@RequestParam(value = "userid") int userid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Invoice> invoices = invoiceDAO.getSentInvoices();
            List<DTO.Invoice.Invoice> invoiceList = new ArrayList<>();
            for (Invoice i : invoices) {
                List<InvoiceDetailLine> invoiceDetailLines = new ArrayList<>();
                for (StockmoveItem stockmoveItem : i.getOrder().getStockmoveItems()) {
                    InvoiceDetailLine invoiceDetailLine = new InvoiceDetailLine();
                    invoiceDetailLine.setProduct(new MiniProduct(stockmoveItem.getProduct(), stockmoveItem.getStock()));
                    invoiceDetailLine.setAmount(stockmoveItem.getAmount());
                    invoiceDetailLine.setInlinePrice(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getPrice());
                    invoiceDetailLine.setStockId(stockmoveItem.getStock().getId());
                    invoiceDetailLine.setInlineDiscountPercentage(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscountPercentage());
                    invoiceDetailLine.setInlineDiscount(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscount());
                    invoiceDetailLines.add(invoiceDetailLine);
                }
                DTO.Invoice.Invoice invoice = new DTO.Invoice.Invoice(i, invoiceDetailLines,invoiceDAO.isReady(i.getId()));
                invoiceList.add(invoice);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(invoiceList, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/inprodcution", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Invoice.Invoice>> getInProduction(@RequestParam(value = "userid") int userid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Invoice> invoices = invoiceDAO.getProductionInvoices();
            List<DTO.Invoice.Invoice> invoiceList = new ArrayList<>();
            for (Invoice i : invoices) {
                List<InvoiceDetailLine> invoiceDetailLines = new ArrayList<>();
                for (StockmoveItem stockmoveItem : i.getOrder().getStockmoveItems()) {
                    InvoiceDetailLine invoiceDetailLine = new InvoiceDetailLine();
                    invoiceDetailLine.setProduct(new MiniProduct(stockmoveItem.getProduct(), stockmoveItem.getStock()));
                    invoiceDetailLine.setAmount(stockmoveItem.getAmount());
                    invoiceDetailLine.setInlinePrice(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getPrice());
                    invoiceDetailLine.setStockId(stockmoveItem.getStock().getId());
                    invoiceDetailLine.setInlineDiscountPercentage(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscountPercentage());
                    invoiceDetailLine.setInlineDiscount(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscount());
                    invoiceDetailLines.add(invoiceDetailLine);
                }
                DTO.Invoice.Invoice invoice = new DTO.Invoice.Invoice(i, invoiceDetailLines,invoiceDAO.isReady(i.getId()));
                invoiceList.add(invoice);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(invoiceList, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/completed", method = RequestMethod.GET)
    public ResponseEntity<List<DTO.Invoice.Invoice>> getCompleted(@RequestParam(value = "userid") int userid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<Invoice> invoices = invoiceDAO.getCompletedInvoices();
            List<DTO.Invoice.Invoice> invoiceList = new ArrayList<>();
            for (Invoice i : invoices) {
                List<InvoiceDetailLine> invoiceDetailLines = new ArrayList<>();
                for (StockmoveItem stockmoveItem : i.getOrder().getStockmoveItems()) {
                    InvoiceDetailLine invoiceDetailLine = new InvoiceDetailLine();
                    invoiceDetailLine.setProduct(new MiniProduct(stockmoveItem.getProduct(), stockmoveItem.getStock()));
                    invoiceDetailLine.setAmount(stockmoveItem.getAmount());
                    invoiceDetailLine.setInlinePrice(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getPrice());
                    invoiceDetailLine.setStockId(stockmoveItem.getStock().getId());
                    invoiceDetailLine.setInlineDiscountPercentage(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscountPercentage());
                    invoiceDetailLine.setInlineDiscount(invoiceDAO.getPriceLinesByStockMoveId(stockmoveItem.getId()).getInLineDiscount());
                    invoiceDetailLines.add(invoiceDetailLine);
                }
                DTO.Invoice.Invoice invoice = new DTO.Invoice.Invoice(i, invoiceDetailLines,invoiceDAO.isReady(i.getId()));
                invoiceList.add(invoice);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(invoiceList, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public ResponseEntity<String> send(@RequestParam(value = "userid") int userid, @RequestParam(value = "invoiceid") long invoiceid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            try {
                Invoice i = invoiceDAO.sendInvoice(invoiceid, userDAO.getUserById(userid), unitDAO.getinvoicestart().getNumericalValue());
            } catch (HorribleException e) {
                logger.error("User: " + userDAO.getUserById(userid).getName() + " tried to send an invoice that has not been completely packed");
                throw new RuntimeException("User: " + userDAO.getUserById(userid).getName() + " tried to send an invoice that has not been completely packed");
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/complete", method = RequestMethod.POST)
    public ResponseEntity<String> complete(@RequestParam(value = "userid") int userid, @RequestParam(value = "inoviceid") long invoiceid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            try {
                Invoice i = invoiceDAO.completeInvoice(invoiceid, userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("User: " + userDAO.getUserById(userid).getName() + " tried to send an invoice that has not been completely payed");
                throw new RuntimeException("User: " + userDAO.getUserById(userid).getName() + " tried to send an invoice that has not been completely payed");
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/payments/add", method = RequestMethod.POST)
    public ResponseEntity<String> addPayment(@RequestParam(value = "userid") int userid, @RequestParam(value = "inoviceid") long invoiceid, @RequestParam(value = "amount") double amount) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Invoice invoice = invoiceDAO.getInvoiceById(invoiceid);
            Payment payment = paymentDAO.createPayment(invoice, amount, userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Deprecated
    @Transactional
    @RequestMapping(value = "/payments/edit", method = RequestMethod.POST)
    public ResponseEntity<String> editPayment(@RequestParam(value = "userid") int userid, @RequestParam(value = "paymentid") int paymentid, @RequestParam(value = "amount") double amount) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Payment payment = paymentDAO.editPayment(paymentid, amount, userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/payments/delete", method = RequestMethod.POST)
    public ResponseEntity<String> removePayment(@RequestParam(value = "userid") int userid, @RequestParam(value = "inoviceid") long invoiceid, @RequestParam(value = "paymentid") int paymentid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Invoice invoice = invoiceDAO.getInvoiceById(invoiceid);
            paymentDAO.deletePayment(paymentid, invoice);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/packingslips/add", method = RequestMethod.POST)
    public ResponseEntity<String> addPackingslip(@RequestParam(value = "userid") int userid, @RequestParam(value = "inoviceid") long invoiceid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            PackingSlipNoId packingSlipNoId = gson.fromJson(str, PackingSlipNoId.class);
            Model.Invoicing.Invoice invoice = invoiceDAO.getInvoiceById(invoiceid);
            List<StockmoveItem> stockmoveItems = new ArrayList<>();
            for (PackingSlipLine packingslipline : packingSlipNoId.getPackingSlipLineList()) {
                StockmoveItem stockmoveItem = stockmoveDAO.createStockmoveItem(productDAO.getProductById(packingslipline.getProductid()), packingslipline.getAmount(), stockDAO.getStockById(1));
                stockmoveItems.add(stockmoveItem);
            }
            Stockmove stockMove = stockmoveDAO.createStockmove(true, false, false, stockmoveItems, userDAO.getUserById(userid), true, userDAO.getUserById(3));
            Model.Invoicing.PackingSlip packingSlip = packingSlipDAO.createPackingSlip(invoice.getCustomer(), userDAO.getUserById(userid), userDAO.getUserById(3), stockMove);
            Invoice invoice2 = invoiceDAO.addPackingslip(invoiceid, packingSlip, userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/packingslips/delete", method = RequestMethod.POST)
    public ResponseEntity<String> removePackingslip(@RequestParam(value = "userid") int userid, @RequestParam(value = "invoceid") long invoiceid, @RequestParam(value = "packingslipid") long packingslipid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Invoice invoice = invoiceDAO.removePackingslip(invoiceid, packingSlipDAO.getPackingSlipById(packingslipid), userDAO.getUserById(userid));
            packingSlipDAO.deletePackingslip(packingslipid);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/packingslips/ready", method = RequestMethod.POST)
    public ResponseEntity<String> readyPackingslip(@RequestParam(value = "userid") int userid, @RequestParam(value = "packingslipid") long packingslipid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            try {
                Invoice invoice = invoiceDAO.getInvoiceByPackingSlipId(packingslipid);
            } catch (HorribleException e) {
                logger.error("User: "+ userDAO.getUserById(userid).getName() + " tried to pack a packingslip for an invoice which is not ready for packing yet");
                throw new RuntimeException("User: "+ userDAO.getUserById(userid).getName() + " tried to pack a packingslip for an invoice which is not ready for packing yet");
            }
            PackingSlip packingSlip = packingSlipDAO.setPacked(packingslipid, userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/packingslips/ship", method = RequestMethod.POST)
    public ResponseEntity<String> shipPackingslip(@RequestParam(value = "userid") int userid, @RequestParam(value = "packingslipid") long packingslipid) {
        //permissions
        List<Integer> permissions = new ArrayList<Integer>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            try {
                PackingSlip packingSlip = packingSlipDAO.setshipped(packingslipid, userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to ship a package that has not been set to packed");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to ship a package that has not been set to packed");
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }
}
