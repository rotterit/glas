package Controllers;

import DAL.CategoryDAO;
import DAL.NoteDAO;
import DAL.UserDAO;
import DTO.Category.CategoryNoId;
import DTO.Category.CategorySizeNoId;
import DTO.Category.Category;
import DTO.Category.CategorySize;
import Model.Misc.Note;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/categories")
public class CategoryController {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Signer signer;

    @Autowired
    private Gson gson;

    private Logger logger = LoggerFactory.getLogger(CategoryController.class);

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private NoteDAO noteDAO;

    @Transactional
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2, 3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is creating Category with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            CategoryNoId c = gson.fromJson(str, CategoryNoId.class);
            Note n = noteDAO.createNote(c.getNote());
            Model.FinishedProduct.Category cm = categoryDAO.createCategory(c.getBaseTime(),c.getName(),n, userDAO.getUserById(userid));
            List<Model.FinishedProduct.CategorySize> categorySizes = new ArrayList<>();
            for(CategorySizeNoId cc : c.getCategorySizes()){
                Model.FinishedProduct.CategorySize ccm = null;
                try {
                    ccm = categoryDAO.createCategorySize(cc.getBasePrice(),cc.getMaxTime(), cc.getMinTime(), cc.getSizeId(), cm, userDAO.getUserById(userid));
                } catch (HorribleException e) {
                    logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to edit a categorySize but the times overlap");
                    throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to edit a categorySize but the times overlap");
//                    HttpHeaders responseHeaders = new HttpHeaders();
//                    responseHeaders.add("token", signer.sign(userid));
//                    return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
                }
                categorySizes.add(ccm);
            }
            cm = categoryDAO.BindCategorySizes(cm.getId(),categorySizes,userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing category with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Category c = gson.fromJson(str, Category.class);
            Note n = noteDAO.editNote(c.getNote().getId(), c.getNote().getContent());
            Model.FinishedProduct.Category cm = categoryDAO.editCategory(c.getId(), c.getBaseTime(),c.getName(),n,userDAO.getUserById(userid));
            List<Model.FinishedProduct.CategorySize> categorySizes = new ArrayList<>();
            for(CategorySize cc : c.getCslist()){
                Model.FinishedProduct.CategorySize ccm = null;
                if(cc.getId()==0){
                    logger.warn("create new categorySize for sizeId: " + cc.getSizeId());
                    try {
                        ccm = categoryDAO.createCategorySize(cc.getBasePrice(),cc.getMaxTime(), cc.getMinTime(), cc.getSizeId(), cm, userDAO.getUserById(userid));
                    } catch (HorribleException e) {
                        logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to edit a categorySize but the times overlap");
                        throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to edit a categorySize but the times overlap");
//                        HttpHeaders responseHeaders = new HttpHeaders();
//                        responseHeaders.add("token", signer.sign(userid));
//                        return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
                    }
                }else {
                    ccm = categoryDAO.editCategorySize(cc.getId(), cc.getMaxTime(), cc.getMinTime(), userDAO.getUserById(userid));
                }
                categorySizes.add(ccm);
            }
            cm = categoryDAO.BindCategorySizes(cm.getId(),categorySizes,userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/all", method= RequestMethod.GET)
    public ResponseEntity<List<Category>> get(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Category> categories = categoryDAO.getCategories().stream().map(Category::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<List<Category>>(categories, responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

}
