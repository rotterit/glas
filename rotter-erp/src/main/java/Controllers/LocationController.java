package Controllers;

import DAL.LocationDAO;
import DTO.Misc.StringJson;
import DAL.UserDAO;
import DTO.Location.Location;
import Security.Signer;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value="/secure/locations")
public class LocationController {

    private Logger logger = LoggerFactory.getLogger(LocationController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private LocationDAO locationDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Gson gson;

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing Location with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Location s = gson.fromJson(str, Location.class);
            Model.Stock.Location t = locationDAO.editLocation(s.getId(),s.getDescription(),userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is creating a Location with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            StringJson s = gson.fromJson(str, StringJson.class);
            Model.Stock.Location q = locationDAO.createLocation(s.getData(),userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<List<Location>> get(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Location> sizes = locationDAO.getLocations().stream().map(Location::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<List<Location>>(sizes,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

}
