package Controllers;

import DAL.ProductDAO;
import DAL.ProductTypeDAO;
import DAL.RawMaterialDAO;
import DAL.UserDAO;
import DTO.ProductType.ProductTypeNoId;
import DTO.ProductType.ProductType;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/producttypes")
public class ProductTypeController {

    private Logger logger = LoggerFactory.getLogger(ProductTypeController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private ProductTypeDAO ProductTypeDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Gson gson;

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private RawMaterialDAO rawMaterialDAO;

    @Transactional
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() + " is editing ProductType with data: " + str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            ProductType s = gson.fromJson(str, ProductType.class);
            try {
                Model.FinishedProduct.ProductType t = ProductTypeDAO.editProductType(s.getId(), s.getCode(), s.getName(), s.getDescription(),userDAO.getUserById(userid));
                productDAO.refreshProductCode();
                rawMaterialDAO.refreshMaterialCode();
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to edit a productType with code to " + s.getCode() + " but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to edit a productType with code to " + s.getCode() + " but it already exists");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value = "userid") int userid, @RequestBody String str) {
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() + " is creating a ProductType with data: " + str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            ProductTypeNoId s = gson.fromJson(str, ProductTypeNoId.class);
            try {
                Model.FinishedProduct.ProductType q = ProductTypeDAO.createProductType(s.getCode(), s.getName(), s.getDescription(),userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to create a productType with code to " + s.getCode() + " but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to create a productType with code to " + s.getCode() + " but it already exists");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<ProductType>> get(@RequestParam(value = "userid") int userid) {
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<ProductType> sizes = ProductTypeDAO.getProductTypes().stream().map(ProductType::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<List<ProductType>>(sizes, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }
}
