package Controllers;

import DAL.MaterialNotificationDAO;
import DAL.RawMaterialDAO;
import DAL.UserDAO;
import DTO.MaterialNotification.MaterialNotification;
import DTO.MaterialNotification.MaterialNotificationEdit;
import DTO.MaterialNotification.MaterialNotificationNoId;
import DTO.ProductType.ProductTypeNoId;
import Security.Signer;
import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 18-5-2015.
 */
@RestController
@RequestMapping(value="/secure/materialnotifications")
public class MaterialNotificationController {

    private Logger logger = LoggerFactory.getLogger(MaterialNotificationController.class);

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Gson gson;

    @Autowired
    private Signer signer;

    @Autowired
    private RawMaterialDAO rawMaterialDAO;

    @Autowired
    private MaterialNotificationDAO materialNotificationDAO;

    @Transactional
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() + " is creating a materialnotification with data: " + str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            MaterialNotificationNoId m = gson.fromJson(str, MaterialNotificationNoId.class);
            try {
                Model.RawStock.MaterialNotification mn = materialNotificationDAO.create(m.getPredate(),m.getStartdate(),m.getEnddate(),rawMaterialDAO.getRawMaterialById(m.getRawMaterialId()), m.getAmount(), userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + "tried to create a materialnotification but there's one for these times already");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + "tried to create a materialnotification but there's one for these times already");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.CONFLICT);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        } else {
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() + " is editing a materialnotification with data: " + str);
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            MaterialNotificationEdit m = gson.fromJson(str, MaterialNotificationEdit.class);
            try {
                Model.RawStock.MaterialNotification mn = materialNotificationDAO.edit(m.getPredate(), m.getStartdate(), m.getEnddate(), m.getAmount(), userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + "tried to edit a materialnotification but there's one for these times already");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + "tried to create a materialnotification but there's one for these times already");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders, HttpStatus.CONFLICT);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/delete", method = RequestMethod.POST)
    public ResponseEntity<String> delete(@RequestParam(value="userid") int userid, @RequestParam(value="id") int id){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() + " is deleting a materialnotification");
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            Model.RawStock.MaterialNotification mn = materialNotificationDAO.remove(id);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<List<MaterialNotification>> get(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if (permissions.contains(userDAO.getUserById(userid).getRole().getId())) {
            List<MaterialNotification> materialNotifications = materialNotificationDAO.getAll().stream().map(MaterialNotification::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(materialNotifications, responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders, HttpStatus.UNAUTHORIZED);
        }
    }
}
