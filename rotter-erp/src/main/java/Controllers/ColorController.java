package Controllers;

import DAL.ColorDAO;
import DAL.ProductDAO;
import DAL.RawMaterialDAO;
import DAL.UserDAO;
import DTO.Color.Color;
import DTO.Color.ColorNoId;
import Security.Signer;

import Util.HorribleException;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by thijs on 6-5-2015.
 */
@RestController
@RequestMapping(value="/secure/colors")
public class ColorController {

    @Autowired
    private ColorDAO colorDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Signer Signer;

    @Autowired
    private Gson gson;

    @Autowired
    private RawMaterialDAO rawMaterialDAO;

    @Autowired
    private ProductDAO productDAO;

    private Logger logger = LoggerFactory.getLogger(ColorController.class);

    @Transactional
    @RequestMapping(value = "/byid", method = RequestMethod.GET)
    public ResponseEntity<Color> getColor(@RequestParam(value="userid") int userid, @RequestParam(value="colorid") int colorid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(userid));
            return new ResponseEntity<Color>(new Color(colorDAO.getColorById(colorid)),responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<Color>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Color>> getAll(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Color> colors = new ArrayList<>();
            List<Model.FinishedProduct.Color> modelroles= colorDAO.getColors();
            for(Model.FinishedProduct.Color c : modelroles){
                colors.add(new Color(c));
            }HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(0));
            return new ResponseEntity<List<Color>>(colors,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<List<Color>>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is creating Color with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            ColorNoId m = gson.fromJson(str,ColorNoId.class);
            try {
                Model.FinishedProduct.Color c = colorDAO.createColor(m.getCode(),m.getName(),m.getValue(),userDAO.getUserById(userid));
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to create a color with code to " + m.getCode() + " but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to create a color with code to " + m.getCode() + " but it already exists");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", Signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders ,HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing Color with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Color m = gson.fromJson(str,Color.class);
            logger.warn("" + m.getValue());
            try {
                Model.FinishedProduct.Color c = colorDAO.editColor(m.getId(),m.getCode(),m.getName(),m.getValue(), userDAO.getUserById(userid));
                productDAO.refreshProductCode();
                rawMaterialDAO.refreshMaterialCode();
            } catch (HorribleException e) {
                logger.error("user: " + userDAO.getUserById(userid).getName() + " tried to edit a color with code to " + m.getCode() + " but it already exists");
                throw new RuntimeException("user: " + userDAO.getUserById(userid).getName() + " tried to edit a color with code to " + m.getCode() + " but it already exists");
//                HttpHeaders responseHeaders = new HttpHeaders();
//                responseHeaders.add("token", Signer.sign(userid));
//                return new ResponseEntity<>(null, responseHeaders ,HttpStatus.NOT_ACCEPTABLE);
            }
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", Signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders, HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }
}
