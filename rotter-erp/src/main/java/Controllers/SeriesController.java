package Controllers;

import DAL.SeriesDAO;
import DAL.UserDAO;
import DTO.Series.SeriesNoId;
import DTO.Series.Series;
import Security.Signer;
import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thijs on 14-5-2015.
 */
@RestController
@RequestMapping(value = "/secure/series")
public class SeriesController {

    private Logger logger = LoggerFactory.getLogger(SeriesController.class);

    @Autowired
    private Signer signer;

    @Autowired
    private SeriesDAO SeriesDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private Gson gson;

    @Transactional
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public ResponseEntity<String> edit(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is editing Series with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            Series s = gson.fromJson(str, Series.class);
            Model.RawMaterial.Series t = SeriesDAO.editSeries(s.getId(),s.getName(), s.getDescription(), s.getSupplierid(),userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/create", method = RequestMethod.POST)
    public ResponseEntity<String> create(@RequestParam(value="userid") int userid, @RequestBody String str){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        logger.warn("user: " + userDAO.getUserById(userid).getName() +" is creating a Series with data: "+ str);
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            SeriesNoId s = gson.fromJson(str, SeriesNoId.class);
            Model.RawMaterial.Series q = SeriesDAO.createSeries(s.getName(), s.getDescription(), s.getSupplierid(),userDAO.getUserById(userid));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<>(null,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }

    @Transactional
    @RequestMapping(value="/all", method = RequestMethod.GET)
    public ResponseEntity<List<Series>> get(@RequestParam(value="userid") int userid){
        //permissions
        List<Integer> permissions = new ArrayList<>(Arrays.<Integer>asList(new Integer[]{2,3,5}));
        //
        if(permissions.contains(userDAO.getUserById(userid).getRole().getId())){
            List<Series> sizes = SeriesDAO.getSeries().stream().map(Series::new).collect(Collectors.toList());
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add("token", signer.sign(userid));
            return new ResponseEntity<List<Series>>(sizes,responseHeaders,HttpStatus.OK);
        }else{
            logger.warn("bad permissions for user with id", userid);
            HttpHeaders responseHeaders = new HttpHeaders();
            return new ResponseEntity<>(null, responseHeaders ,HttpStatus.UNAUTHORIZED);
        }
    }
}
