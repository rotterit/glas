package Util;

/**
 * Created by thijs on 6-5-2015.
 */
public class AuthorizationError extends Throwable {

    private String errorMessage;

    public AuthorizationError(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
