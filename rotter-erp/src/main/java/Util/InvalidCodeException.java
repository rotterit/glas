package Util;

/**
 * Created by thijs on 22-4-2015.
 */
public class InvalidCodeException extends Exception {

    public InvalidCodeException(String message) {
        super(message);
    }
}
