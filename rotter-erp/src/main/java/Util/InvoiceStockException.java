package Util;

import Model.Invoicing.Invoice;
import Model.FinishedProduct.Product;

import java.util.Map;

/**
 * Created by thijs on 4-5-2015.
 */
public class InvoiceStockException extends Exception{


    private final String message;
    private final Map<Product, Integer> needs;
    private final Invoice invoice;

    public InvoiceStockException(String s, Invoice i, Map<Product, Integer> needed) {
        this.message = s;
        this.invoice = i;
        this.needs = needed;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Map<Product, Integer> getNeeds() {
        return needs;
    }

    public Invoice getInvoice() {
        return invoice;
    }


}
